<?php
include_once("Veritrans.php");

if(empty($_POST['token-id'])){
        echo "Empty token-id";
        exit;
}

// production key = VT-server-OLcM_hbTCgyX3b4dhCbcC-9s
// development key = VT-server-RqSFpuzPbBxei4U16EEGp-UP
Veritrans_Config::$serverKey = "VT-server-OLcM_hbTCgyX3b4dhCbcC-9s";
Veritrans_Config::$isProduction = true;

$token_id = $_POST['token-id'];
//echo $token_id;

$items = array(
    array(
        'id'       => $_POST['item_id'],
        'price'    => $_POST['item_price'],
        'quantity' => $_POST['item_quantity'],
        'name'     => $_POST['item_name']
    ));

$customer_details = array(
    'first_name'       => $_POST['customer_firstname'],
    'last_name'        => $_POST['customer_lastname'],
    'email'            => $_POST['customer_email'],
    'phone'            => $_POST['customer_mobilephone']
);

$transaction_details = array(
    'order_id'    => $_POST['order_id'],
    'gross_amount'  => $_POST['price']
);

$transaction_data = array(
    'payment_type'      => 'credit_card',
    'credit_card'       => array(
        'token_id'  => $token_id,
        // 'bank'    => 'bni'
    ),
    'transaction_details'   => $transaction_details,
    'item_details'        => $items,
    'customer_details'    => $customer_details
);

$return = array(
    'status' => json_encode($transaction_data)
);
// echo json_encode($return);
// die();
try{
        $result = Veritrans_VtDirect::charge($transaction_data);
        $json = json_encode($result);

        $response = array(
            'status' => 'success',
            'body' => $json
        );

        echo json_encode($response);
        // echo "{\"status\":\"success\", \"body\" : $json}";
}catch(Exception $e){
        echo "{\"status\":\"error\",\"body\": \".$e.\"}";
}

?>