<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <meta name="description" content="We are a microfinance institution that provides financial services to the low-income people living in Indonesia's rural areas who have limited access to affordable and quality financial services or even to the knowledge about such services.">
    <meta name="author" content="Kotakwarna Studio">
	<meta name="keywords" content="amartha, microfinance, microfinance indonesia, grameen bank">
	
	<!--<meta property="og:url" content="http://amartha.co.id/"/>
	<meta property="og:title" content="<?php echo $sitetitle; ?> | Amartha Microfinance"/>
	<meta property="og:site_name" content="<?php echo $sitetitle; ?> | Amartha Microfinance"/>
	<meta property="og:description" content="We are a microfinance institution that provides financial services to the low-income people living in Indonesia's rural areas who have limited access to affordable and quality financial services or even to the knowledge about such services."/>
	<meta property="og:image" content="http://amartha.co.id/themes/default/img/v3.jpg"/>
	-->
	
	
	
    <title><?php echo $sitetitle; ?> | Amartha Microfinance</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo $this->template->get_theme_path(); ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo $this->template->get_theme_path(); ?>/css/custom.css" rel="stylesheet">
	<!--
    <link href="<?php echo $this->template->get_theme_path(); ?>/css/nanogallery.css" rel="stylesheet">
    <link href="<?php echo $this->template->get_theme_path(); ?>/css/themes/clean/nanogallery_clean.css" rel="stylesheet">
    <link href="<?php echo $this->template->get_theme_path(); ?>/css/themes/light/nanogallery_light.css" rel="stylesheet">-->
    <link href="<?php echo $this->template->get_theme_path(); ?>/css/animate.css" rel="stylesheet">
	<link REL="SHORTCUT ICON" HREF="<?php echo $this->template->get_theme_path(); ?>/img/favicon.ico">
	

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="<?php echo $this->template->get_theme_path(); ?>./../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
	 <!-- header -->
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_amartha.png" alt="Amartha Microfinance" height="30px" /></a>
        </div>
		<div class="navbar-collapse collapse ">
          <ul class="nav navbar-nav">
           <!-- <li class="<?php echo $menu_home; ?>"><a href="<?php echo site_url(); ?>" title="home">home</a></li>-->
			<li class="dropdown <?php echo $menu_about; ?>">
              <a href="<?php echo site_url(); ?>/about" class="dropdown-toggle " data-toggle="dropdown" >about <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class=""><a href="<?php echo site_url(); ?>/about">who we are</a></li>
                <li class=""><a href="<?php echo site_url(); ?>/about/purpose">purpose</a></li>
                <li class=""><a href="<?php echo site_url(); ?>/about/history">history</a></li>
                <li class=""><a href="<?php echo site_url(); ?>/leadership">leadership</a></li>
                <li class=""><a href="<?php echo site_url(); ?>/about/our_approach">our approach</a></li>
                <li class=""><a href="<?php echo site_url(); ?>/about/values_principles">values and principles</a></li>
                <li class=""><a href="<?php echo site_url(); ?>/awards">awards</a></li>
              </ul>
            </li>
			<li class="dropdown <?php echo $menu_about; ?>">
              <a href="<?php echo site_url(); ?>/product" class="dropdown-toggle " data-toggle="dropdown" >products <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class=""><a href="<?php echo site_url(); ?>/product/financing">financing</a></li>
                <li><a href="<?php echo site_url(); ?>/product/savings">savings</a></li>
                <!--<li class=""><a href="<?php echo site_url(); ?>/product/mobile_savings">Mobile Savings</a></li>-->
                <li><a href="<?php echo site_url(); ?>/product/microinvestments">microinvestments</a></li>
              </ul>
            </li>
			<li class="dropdown <?php echo $menu_about; ?>">
              <a href="<?php echo site_url(); ?>/impact" class="dropdown-toggle " data-toggle="dropdown" >impacts <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class=""><a href="<?php echo site_url(); ?>/impact/progress">progress</a></li>
                <li><a href="<?php echo site_url(); ?>/story">story from our entrepreneurs</a></li>
              </ul>
            </li>
            <li class="dropdown <?php echo $menu_about; ?>">
              <a href="<?php echo site_url(); ?>/about" class="dropdown-toggle " data-toggle="dropdown" >careers <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo site_url(); ?>/career/culture">company Culture</a></li>
                <li><a href="<?php echo site_url(); ?>/career/life">life at amartha</a></li>
                <li><a href="<?php echo site_url(); ?>/career/opportunities">jobs opportunities</a></li>
              </ul>
            </li>
            <li class="dropdown <?php echo $menu_about; ?>">
              <a href="<?php echo site_url(); ?>/about" class="dropdown-toggle " data-toggle="dropdown" >partners <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class=""><a href="<?php echo site_url(); ?>/partners/annual_report">annual report</a></li>
                <li class=""><a href="<?php echo site_url(); ?>/partners/quarter_report">quarterly report</a></li>
                <li><a href="<?php echo site_url(); ?>/partners">partners</a></li>
              </ul>
            </li>
			
            <li class="dropdown <?php echo $menu_news; ?>">
              <a href="<?php echo site_url(); ?>/news" class="dropdown-toggle " data-toggle="dropdown" >ideas <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li class=""><a href="<?php echo site_url(); ?>/news/">news</a></li>
                <li class=""><a href="<?php echo site_url(); ?>/blog/">blog</a></li>
              </ul>
            </li>
            <!--<li class="<?php echo $menu_contact; ?>"><a href="<?php echo site_url(); ?>/contact" title="contact">contact</a></li>-->
            
          </ul>
		  
        </div><!--/.nav-collapse -->
        
      </div>
    </div>

	<?php echo $template['body']; ?>
	
    <!-- Footer -->
	<footer>
		<div class="container">
		  <div class="row">
			<div class="col-md-6 col-xs-12">
				<br/><br/>
				<p><a class="" href="<?php echo site_url();?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo-black.png" alt="airlearn" /></a></p>
				<div class="text-sm">
				What we offer is the financial services that serve as a platform for people living in rural areas to explore their potentials and ultimately to reach life for greater purposes. <br/>
				<br/>
				<small class="hidden-xs">&copy; <?php echo date("Y");?>. Amartha Microfinance. Web by <a href="http://www.kotakwarna.co.id" title="Kotakwarna Studio" target="_blank">Kotakwarna Studio</a></small>
				</div>
				
			</div>
			<div class="col-md-6 col-xs-12">
				<div class="row">
					
					<div class="col-md-7 col-xs-12">
						<br/><br/>
						<ul><b class="purple">Microfinance</b><br/><br/>
							<li><a href="#">Understanding Microfinance</a></li>
							<li><a href="#">Indonesian Perspective</a></li>
							<li><a href="#">Islamic Microfinance</a></li>
							<li><a href="#">Financial Inclusion</a></li>
							<li><a href="<?php echo site_url(); ?>/faqs">FAQs</a></li>
						</ul>
					</div>
					<div class="col-md-5 col-xs-12">
						<br/><br/>
						<b class="purple">Get in touch</b><br/><br/>
							<a href="<?php echo site_url(); ?>/contact">Contact us</a><br/><a href="mailto:info@amartha.co.id" title="Email to info@amartha.co.id">info@amartha.co.id</a><br/><br/>
						<a href="https://www.facebook.com/AmarthaMicrofinance" title="Amartha Facebook" target="_blank"><img src="<?php echo $this->template->get_theme_path(); ?>/img/icon_fb.png" alt="Facebook" width="24px"/></a> 
						<a href="https://twitter.com/amarthamf" title="Tweet @amarthamf" target="_blank"><img src="<?php echo $this->template->get_theme_path(); ?>/img/icon_tw.png" alt="Twitter" width="24px"/></a> 
						<a href="http://linkedin.com/company/amartha-microfinance" title="Linked In" target="_blank"><img src="<?php echo $this->template->get_theme_path(); ?>/img/icon_li.png" alt="LinkedIn" width="24px"/></a> 
						&nbsp;&nbsp;<a href="http://www.mixmarket.org/mfi/amartha-microfinance" title="MixMarket" target="_blank"><img src="<?php echo $this->template->get_theme_path(); ?>/img/icon_mixmarket.png" alt="Mixmarke" height="16px"/></a> 
						<br/><br/>
						<small class="visible-xs">&copy; <?php echo date("Y");?>. Amartha Microfinance. Web by <a href="http://www.kotakwarna.co.id" title="Kotakwarna Studio" target="_blank">Kotakwarna Studio</a></small>
					</div>  
				</div>
			</div>
		  </div>
		</div> 
		<br/><br/>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>/js/bootstrap.min.js"></script> 
	<script src="<?php echo $this->template->get_theme_path(); ?>/js/tweetie.js"></script>

	
	<script class="" type="text/javascript">
        $('.example2 .tweet').twittie({
            username: 'amarthamf',
            list: 'c-oo-l-e-s-t-nerds-i-know',
            dateFormat: '%b. %d, %Y',
            template: '<strong class="date">{{date}}</strong> - {{screen_name}} {{tweet}}',
            count: 10
        }, function () {
            setInterval(function() {
                var item = $('.example2 .tweet ul').find('li:first');

                item.animate( {marginTop: '-100px', 'opacity': '0'}, 500, function() {
                    $(this).detach().appendTo('.example2 .tweet ul').removeAttr('style');
                });
            }, 5000);
        });
    </script>
  </body>
</html>
