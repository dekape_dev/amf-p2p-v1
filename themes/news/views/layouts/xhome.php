<!-- mainslide -->
<div class="mainslide mainslide1">
  <div class="container">
    <div class="row">
        <div id="mainslide_text" class="col-lg-7  col-xs-12">
            <p>Online Learning</p>
            <h1>Learn on the web and mobile</h1>
            <hr/>
            <h3>With a web site crafted to work on every major device we're there to help you learn.</h3>
            <p><a class="btn btn-primary btn-lg" role="button">Learn more &raquo;</a></p>
        </div>
    </div>
  </div>
</div>

<div class="home_promo">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-12">
            <p><a class="" href="index.html"><img src="<?php echo $this->template->get_theme_path(); ?>/img/img1.png" alt="airlearn" class="img-responsive" /></a></p>				
        </div>
        <div class="col-lg-5 col-md-6  col-xs-12">
            <br/><br/><h2>Learn at your pace</h2>	
            <p>Learn from over 1000 videos created by our expert teachers on formal education, business, and much more. Our library is continually refreshed with the latest on web technology so you'll never fall behind.</p>
            <p><a class="btn btn-success btn-md" role="button">Check out our courses</a></p>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-12">
            <br/><br/><br/><br/><h2>Practice make perfect</h2>	
            <p>Practice what you've learned through quizzes and communities. This style of practicing will allow you to retain information you've learned so you can apply it to your own future projects.</p>
            <p><a class="btn btn-info btn-md" role="button">Compare pricing plans</a></p>
        </div>
        <div class="col-lg-6 col-md-6 col-xs-12">
            <p><a class="" href="index.html"><img src="<?php echo $this->template->get_theme_path(); ?>/img/img2.png" alt="airlearn" class="img-responsive"/></a></p>				
        </div>
      </div>
    </div> 
<div>

<!-- mainslide -->
<div class="mainslide mainslide2">
  <div class="container">
    <div class="row">
        <div id="mainslide_text" class="col-lg-7">
            <h1>Get ready, the online learning revolution has begun! </h1>
            <hr/>
            <h3>Learn from over 1000 videos created by our expert teachers on web</h3>
            <p><a class="btn btn-primary btn-lg" role="button">Learn more &raquo;</a></p>
        </div>
    </div>
  </div>
</div>