 <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="We are a microfinance institution that provides financial services to the low-income people living in Indonesia's rural areas who have limited access to affordable and quality financial services or even to the knowledge about such services.">
    <meta name="author" content="Kotakwarna Studio">
	<meta name="keywords" content="amartha, microfinance, microfinance indonesia, grameen bank">

	<!--<meta property="og:url" content="http://amartha.co.id/"/>
	<meta property="og:title" content="<?php echo $sitetitle; ?> | Arkana"/>
	<meta property="og:site_name" content="<?php echo $sitetitle; ?> | Arkana"/>
	<meta property="og:description" content="We are a microfinance institution that provides financial services to the low-income people living in Indonesia's rural areas who have limited access to affordable and quality financial services or even to the knowledge about such services."/>
	<meta property="og:image" content="http://amartha.co.id/themes/default/img/v3.jpg"/>
	-->



    <title><?php echo $sitetitle; ?> | Amartha</title>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo $this->template->get_theme_path(); ?>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo $this->template->get_theme_path(); ?>/css/custom.css" rel="stylesheet">
	<link href="<?php echo $this->template->get_theme_path(); ?>/css/archive.css" rel="stylesheet">


	<!--
    <link href="<?php echo $this->template->get_theme_path(); ?>/css/nanogallery.css" rel="stylesheet">
    <link href="<?php echo $this->template->get_theme_path(); ?>/css/themes/clean/nanogallery_clean.css" rel="stylesheet">
    <link href="<?php echo $this->template->get_theme_path(); ?>/css/themes/light/nanogallery_light.css" rel="stylesheet">
    <link href="<?php echo $this->template->get_theme_path(); ?>/css/animate.css" rel="stylesheet">-->
	<link REL="SHORTCUT ICON" HREF="<?php echo $this->template->get_theme_path(); ?>/img/favicon.ico">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="<?php echo $this->template->get_theme_path(); ?>./../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!--// Share link
	<script type="text/javascript">var switchTo5x=true;</script>
	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
	-->



  </head>


	<?php echo $template['body']; ?>


  </body>
</html>
