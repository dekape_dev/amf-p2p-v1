<!-- mainslide -->
<div class="mainslide mainslide1">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>
        
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active"> 
      <img src="<?php echo $this->template->get_theme_path(); ?>/img/bg_home.jpg" alt="">
     <div class="carousel-caption"> 
        <div id="mainslide_text" class="col-lg-7  col-xs-12 text-left">
            <p>AMARTHA MICROFINANCE</p>
			<h1>Expanding Outreach, Delivering Happiness</h1>
            <hr/>
            <h3>To empower low-income families in rural areas with affordable financial services, enabling them to pursue life for greater purposes.</h3>
            
        </div>
      </div>
    </div>
    <div class="item">
      
      <img src="<?php echo $this->template->get_theme_path(); ?>/img/bg_home2.jpg" alt="" />
	  <div class="carousel-caption"> 
        <div id="mainslide_text" class="col-lg-7  col-xs-12 text-left">
            <p>AMARTHA MICROFINANCE</p>
			<h1>Expanding Outreach, Delivering Happiness</h1>
            <hr/>
            <h3>To empower low-income families in rural areas with affordable financial services, enabling them to pursue life for greater purposes.</h3>
            
        </div>
      </div>
    </div>
	
	 <div class="item">
      
      <img src="<?php echo $this->template->get_theme_path(); ?>/img/v1.jpg" alt="" />
	  <div class="carousel-caption"> 
        <div id="mainslide_text" class="col-lg-7  col-xs-12 text-left">
            <p>AMARTHA MICROFINANCE</p>
			<h1>Expanding Outreach, Delivering Happiness</h1>
            <hr/>
            <h3>To empower low-income families in rural areas with affordable financial services, enabling them to pursue life for greater purposes.</h3>
            
        </div>
      </div>
    </div>
   
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

<div class="home_promo_bg">
    <div class="container">
      <div class="row">
        
        <div class="col-lg-8 col-lg-offset-2 col-md-12  col-xs-12 text-center animated fadeInUp">
            <br/><br/>
			<h1 class="animated fadeInUp">SERVING UNDERSERVED AREAS,<br/>GROWING RURAL ECONOMIES</h1>	
            <br/><br/>
			<!--<p>" Our mission is to become a nationwide MFI that provides affordable financial services and delivers high quality services to low-income people in a large outreach. "</p>-->
            <a href="<?php echo site_url();?>/about" title="" class="white"><b>WHO WE ARE</b></a>
			<br/><br/><br/>
        </div>
      </div>
    </div> 
</div>

<!-- footslide -->
<div class="mainslide mainslide2">
  <div class="container">
    <div class="row">
        <div id="mainslide_text" class="col-lg-7">
            <p>WHY WOMEN?</p>
            <h1>100% of Our Members are Women</h1>
            <hr/>
            <p>Women sacrifice the most of every hardship in poverty. Thus we believe that every empowerment effort dedicated to women will directly contribute to the welfare enhancement of the entire family. The life improvement is reflected through better children education, healthcare, and sanitary infrastructure. Therefore, we are committed to support those women at the bottom of the pyramid through financial literacy and community organising in order to arrive at the community improvement as a whole.</p>
           
        </div>
    </div>
  </div>
</div>

<div class="mainslide mainslide_home_job">
	<div class="container">
    <div class="row">
        <div class="col-lg-4 col-xs-4 text-center animated fadeInUp">
			<a href="<?php echo site_url(); ?>/career/culture" title="Company Culture" class="text-white">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon_job_2.png" width="50%" /><br/><br/>
				Company Culture
			</a>
		</div>
        <div class="col-lg-4 col-xs-4 text-center animated fadeInUp">
			<a href="<?php echo site_url(); ?>/career/life" title="Life at Amartha" class="text-white">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon_job_1.png" width="50%" /><br/><br/>
				Life at Amartha
			</a>
		</div>
        <div class="col-lg-4 col-xs-4 text-center animated fadeInUp">
			<a href="<?php echo site_url(); ?>/career/opportunities" title="Jobs Opportunities" class="text-white">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon_job_3.png" width="50%" /><br/><br/>
				Jobs Opportunities
			</a>
		</div>
	</div>
	</div>
</div>
 
<div class="home_social ">
	<div class="container">
      <div class="row">
        
        <div  class="col-lg-4  col-md-12  col-xs-12 text-white">
			<div id="home_news">
				<div class="blog_title ">LATEST NEWS</div>
				<?php foreach($news as $p):  ?>
					<div class="home_blog">
						<a class="purple" href="<?php echo site_url();?>/news/detail/<?php echo $p->news_id; ?>" ><?php echo "<b>".$p->news_date."</b>, ".$p->news_title; ?></a>
						
					</div>  
					<?php endforeach; ?> 
					<div class="text-center white"><br/><a class="white" href="<?php echo site_url();?>/news">view all</a></div>
			 </div>
        </div>
		
		<div  class="col-lg-4  col-md-12  col-xs-12  text-white"> 
			<div id="home_blog">
				<div class="blog_title ">LATEST BLOG</div>
				<?php foreach($blog as $p):  ?>
					<div class="home_blog">
						<a class="purple" href="<?php echo site_url();?>/blog/detail/<?php echo $p->blog_id; ?>" ><?php echo $p->blog_title; ?></a>
						
					</div>  
					<?php endforeach; ?> 
					<div class="text-center white"><br/><a class="white" href="<?php echo site_url();?>/blog">view all</a></div>
			 </div>
        </div>
		
		<div  class="col-lg-4  col-md-12  col-xs-12  text-white text-center"> 
			<div id="home_social_twitter">
					<br/><br/>
						<a href="https://twitter.com/amarthamf" title="follow @amarthamf" target="_blank"><img src="<?php echo $this->template->get_theme_path(); ?>/img/icon_twitter.png" /></a><br/>
						<div class="example2">
							<div class="tweet">
						
						</div>
						</div>

			 </div>
        </div>
    </div> 
    </div>
</div>


<!-- Current Outreach -->
<div class="mainslide_outreach">
  <div class="container">
    <div class="row">
        <div id="" class="col-lg-8 home_outreach">
            <p>CURRENT OUTREACH</p>
			<hr/>
            <div class="row">
				<div class="col-lg-3"><b>MEMBERS:</b><br/><b class="home_outreach_ammount">6.763</b></div>
				<div class="col-lg-3"><b>VILLAGES:</b><br/><b class="home_outreach_ammount">87</b></div>
				<div class="col-lg-6"><b>FINANCING DISBURSED:</b><br/><b class="home_outreach_ammount">$ 792.744</b></div>
			</div>
			<hr/>
			<small>* Data based on December 31, 2014</small>
        </div>
    </div>
  </div>
</div>