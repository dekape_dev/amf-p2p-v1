<!-- mainslide -->
<div class="mainslide mainslide_home">


	<div class="container">
        <div id="mainslide_text" class="col-lg-7  col-xs-12 text-left">
            <p>AMARTHA MICROFINANCE</p> 
			<h1>Expanding Outreach, Delivering Happiness</h1>
            <hr/>
            <h3>To empower low-income families in rural areas with affordable financial services, enabling them to pursue life for greater purposes.</h3>
            
        </div>
	
	</div>

</div>

<div class="home_promo_bg">
    <div class="container">
      <div class="row">
        
        <div class="col-lg-12 col-md-12  col-xs-12 text-center animated fadeInUp">
            <h1 class="animated fadeInUp">SERVING UNDERSERVED AREAS,<br/>GROWING RURAL ECONOMIES</h1>	
            <br/>
			<!--<p>" Our mission is to become a nationwide MFI that provides affordable financial services and delivers high quality services to low-income people in a large outreach. "</p>-->
            <a href="<?php echo site_url();?>/about" title="" class="white"><b>WHO WE ARE</b></a>
			<br/><br/><br/>
        </div>
      </div>
    </div> 
</div>

<!-- footslide -->
<div class="mainslide mainslide2">
  <div class="container">
    <div class="row">
        <div id="mainslide_text" class="col-lg-7">
            <p>WHY WOMEN?</p>
            <h1>100% of Our Members are Women</h1>
            <hr/>
            <p>Women sacrifice the most of every hardship in poverty. Thus we believe that every empowerment effort dedicated to women will directly contribute to the welfare enhancement of the entire family. The life improvement is reflected through better children education, healthcare, and sanitary infrastructure. Therefore, we are committed to support those women at the bottom of the pyramid through financial literacy and community organising in order to arrive at the community improvement as a whole.</p>
           
        </div>
    </div>
  </div>
</div>

<div class="mainslide mainslide_home_job">
	<div class="container">
    <div class="row">
        <div class="col-lg-4 col-xs-4 text-center animated fadeInUp">
			<a href="<?php echo site_url(); ?>/career/culture" title="Company Culture" class="text-white">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/job_com.png" width="50%" /><br/><br/>
				Company Culture
			</a>
		</div>
        <div class="col-lg-4 col-xs-4 text-center animated fadeInUp">
			<a href="<?php echo site_url(); ?>/career/life" title="Life at Amartha" class="text-white">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/job_life.png" width="50%" /><br/><br/>
				Life at Amartha
			</a>
		</div>
        <div class="col-lg-4 col-xs-4 text-center animated fadeInUp">
			<a href="<?php echo site_url(); ?>/career/opportunities" title="Jobs Opportunities" class="text-white">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/job_op.png" width="50%" /><br/><br/>
				Jobs Opportunities
			</a>
		</div>
	</div>
	</div>
</div>

<div class="home_youtube" style="">
  <iframe frameborder="0" height="100%" width="100%" 
    src="http://youtube.com/embed/FbwQwmDoeps?autoplay=0&controls=0&showinfo=0&autohide=1">
  </iframe>
</div>
<!--
<div class="mainslide_annual">
  <div class="container">
        <div class="col-lg-7">
            <p>As we continue to evaluate our approach, it is proven that women’s access to financial services create great impact to the improvement of gender equality and community cohesiveness. 
We consistently expand our outreach to enable more families to earn higher income and improve their participation in the economy.</p>
           
		   
		
			
			<div style="margin-left:1040px; margin-top:-90px;">
			<a href="<?php echo site_url(); ?>/partners/annual_report" title="Annual Report" class="text-white">
			<button type="submit" class="btn btn-default"> Annual Report</button></a>
			</div>
			 <p style="font-size:35pt;">Progress</p>
			
		</div>
	</div>
</div>
-->
<!-- Current Outreach -->
<div class="mainslide_outreach hide">
  <div class="container">
    <div class="row">
        <div id="" class="col-lg-12 home_outreach">
            <p>CURRENT OUTREACH</p>
			<hr/>
            <div class="row">
				<div class="col-lg-3 newline"><b>MEMBERS</b><br/><b class="home_outreach_ammount">6.763</b></div>
				<div class="col-lg-3 newline"><b>GATHERING GROUPS</b><br/><b class="home_outreach_ammount">474</b></div>
				<div class="col-lg-3 newline"><b>VILLAGES</b><br/><b class="home_outreach_ammount">87</b></div>
				<div class="col-lg-3 newline"><b>FINANCING DISBURSED</b><br/><b class="home_outreach_ammount">$ 792.744</b></div>
			</div>
			<hr/>
			<small>* Data based on December 31, 2014</small>
        </div>
    </div>
  </div>
</div>

<!-- Social Media -->
<div class="home_social">
	<div class="container_home_social">
      <div class="row">
	  <div  class="col-lg-6  col-md-6  col-xs-12 timeline_left">
			 <div class="row pad_top_100">
				 <div  class="col-lg-10  col-md-10  col-xs-12">
					<div class="home_box_title"><b>N E W S</b></div>
					<?php foreach($news as $p):  ?>
					<div class="home_news_box">
						<div class="home_news_box_photo"><img src="<?php echo base_url();?>/files/<?php echo $p->news_picture; ?>" /></div>
						<div class="home_news_box_caption">
							<div class="home_news_box_caption_title"><?php echo $p->news_title; ?></div>
							<div class="home_news_box_caption_date"><?php echo date("Y F, d",strtotime($p->news_date)); ?></div>
							<div class="home_news_box_caption_content">
								<?php 
									function limit_text($text, $limit) {
									  if (str_word_count($text, 0) > $limit) {
										  $words = str_word_count($text, 2);
										  $pos = array_keys($words);
										  $text = substr($text, 0, $pos[$limit]) . '...';
									  }
									  return $text;
									}
									echo limit_text(strip_tags($p->news_content), 20); ?>
								
							</div>
							<div class="home_news_box_caption_more">
								<a href="<?php echo site_url();?>/news/detail/<?php echo $p->news_id; ?>" title="READ MORE" class="readmore">READ MORE</a>
							</div>
						</div>
					</div>			 
					<?php endforeach; ?> 
				</div>
				<div class="col-lg-2  col-md-2  col-xs-12 hidden-xs text-right timeline_garis"><img src="<?php echo $this->template->get_theme_path(); ?>/img/garis_kanan.png" class="img-responsive"/></div>
			</div>	
        </div>
		<div  class="col-lg-6  col-md-6  col-xs-12">
			 <div class="row">
				 <div  class="col-lg-10  col-md-10  col-xs-12 pull-right">
					<div class="home_box_title"><b>B L O G</b></div>
					<?php foreach($blog as $p):  ?>
					<div class="home_news_box">
						<div class="home_news_box_photo"><img src="<?php echo base_url();?>/files/<?php echo $p->blog_picture; ?>" /></div>
						<div class="home_news_box_caption">
							<div class="home_news_box_caption_title"><?php echo $p->blog_title; ?></div>
							<div class="home_news_box_caption_date"><?php echo date("Y F, d",strtotime($p->blog_date)); ?></div>
							<div class="home_news_box_caption_content">
								<?php 
									
									echo limit_text(strip_tags($p->blog_content), 20); ?>
								
							</div>
							<div class="home_news_box_caption_more">
								<a href="<?php echo site_url();?>/blog/detail/<?php echo $p->blog_id; ?>" title="READ MORE" class="readmore">READ MORE</a>
							</div>
						</div>
					</div>			 
					<?php endforeach; ?> 
				</div>
				<div class="col-lg-2  col-md-2  col-xs-12 hidden-xs timeline_garis"><img src="<?php echo $this->template->get_theme_path(); ?>/img/garis_kiri.png" class="img-responsive"/></div>
			</div>	
        </div>
		
        <!--
        <div  class="col-lg-4  col-md-12  col-xs-12 text-white">
			<div id="home_news">
				<div class="blog_title ">LATEST NEWS</div>
				<?php foreach($news as $p):  ?>
					<div class="home_blog">
						<a class="purple" href="<?php echo site_url();?>/news/detail/<?php echo $p->news_id; ?>" ><?php echo "<b>".$p->news_date."</b>, ".$p->news_title; ?></a>
						
					</div>  
					<?php endforeach; ?> 
					<div class="text-center white"><br/><a class="white" href="<?php echo site_url();?>/news">view all</a></div>
			 </div>
        </div>
		
		<div  class="col-lg-4  col-md-12  col-xs-12  text-white"> 
			<div id="home_blog">
				<div class="blog_title ">LATEST BLOG</div>
				<?php foreach($blog as $p):  ?>
					<div class="home_blog">
						<a class="purple" href="<?php echo site_url();?>/blog/detail/<?php echo $p->blog_id; ?>" ><?php echo $p->blog_title; ?></a>
						
					</div>  
					<?php endforeach; ?> 
					<div class="text-center white"><br/><a class="white" href="<?php echo site_url();?>/blog">view all</a></div>
			 </div> 
        </div>
		
		<div  class="col-lg-4  col-md-12  col-xs-12  text-white text-center"> 
			<div id="home_social_twitter">
					
						<a href="https://twitter.com/amarthamf" title="follow @amarthamf" target="_blank"><img src="<?php echo $this->template->get_theme_path(); ?>/img/icon_twitter.png" /></a><br/>
						<div class="example2">
							<div class="tweet">
						
						</div>
						</div>

			 </div>
        </div>-->
    </div> 
    </div>
</div>
<!--
<div class="home_progress hidden-xs">
	 <div class="container"> 
		<div class="row">
			<div class="col-lg-9 col-xs-12 text-white">
				<p style="font-size:12pt;">
					As we continue to evaluate our approach, it is proven that women’s access to financial services create great impact to the improvement of gender equality and community cohesiveness. 
					We consistently expand our outreach to enable more families to earn higher income and improve their participation in the economy.
				</p>
			</div>
			<div class="col-lg-3 col-xs-12 text-right">
				<a href="<?php echo site_url(); ?>/partners/annual_report" title="Annual Report" class="text-white">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> &nbsp; Annual Report</button></a>
			</div>
		</div> 
		<div class="row">
			<div class="col-lg-12 col-xs-12 text-white"><br/><br/>
				<p style="font-size:30pt; font-weight: normal">Progress</p>
			<hr/><br/>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 ">
				<div class="row">
					<div class="progress_box">
						<div class="col-lg-5 col-xs-12 text-white nopadding" >						
							<p style="font-size:12pt; line-height: 30px">total members as of december 2014</p>
						</div>					
						<div class="col-lg-6 col-xs-12 text-white text-right nopadding">
							<p style="font-size:40pt;" class="">6673</p>
						</div>
					</div>
				</div>				
			</div>
			
			<div class="col-lg-4 ">
				<div class="row">
					<div class="progress_box">
						<div class="col-lg-6 col-xs-12 text-white nopadding" >						
							<p style="font-size:12pt;">total villages</p>
						</div>					
						<div class="col-lg-6 col-xs-12 text-white text-right nopadding">
							<p style="font-size:40pt;" class="">91</p>
						</div>
					</div>
				</div>				
			</div>
			
			<div class="col-lg-4 ">
				<div class="row">
					<div class="progress_box">
						<div class="col-lg-6 col-xs-12 text-white nopadding" >						
							<p style="font-size:12pt; line-height: 30px">total amount disburshed since inception: </p>
						</div>					
						<div class="col-lg-6 col-xs-12 text-white text-right nopadding">
							<span style="font-size:20pt">Rp</span><span style="font-size:40pt;" class="">20</span>
							
						</div>
						<div class="col-lg-6 col-xs-12 text-white text-right nopadding">
						
							<p style="font-size:35pt; padding-top: -10px;" class="">billion</p>
						</div>
					</div>
				</div>				
			</div>
				
			
		</div>
    </div>
</div>
</div>-->
