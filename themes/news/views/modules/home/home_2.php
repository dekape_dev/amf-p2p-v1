<!-- mainslide -->
<div class="mainslide mainslide1">
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
	<li data-target="#carousel-example-generic" data-slide-to="3"></li>
	<li data-target="#carousel-example-generic" data-slide-to="4"></li>
	
	
  </ol>
        
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="<?php echo $this->template->get_theme_path(); ?>/img/bg_home.jpg" alt="">
     <div class="carousel-caption"> 
        <div id="mainslide_text" class="col-lg-7  col-xs-12 text-left">
            <p>AMARTHA MICROFINANCE</p> 
			<h1>Expanding Outreach, Delivering Happiness</h1>
            <hr/>
            <h3>To empower low-income families in rural areas with affordable financial services, enabling them to pursue life for greater purposes.</h3>
            
        </div>
      </div>
    </div>
    <div class="item">
      
      <img src="<?php echo $this->template->get_theme_path(); ?>/img/bg_home2.jpg" alt="" />
	  <div class="carousel-caption"> 
        <div id="mainslide_text" class="col-lg-7  col-xs-12 text-left">
            <p>AMARTHA MICROFINANCE</p>
			<h1>Expanding Outreach, Delivering Happiness</h1>
            <hr/>
            <h3>To empower low-income families in rural areas with affordable financial services, enabling them to pursue life for greater purposes.</h3>
            
        </div>
      </div>
    </div>
	<div class="item">
      
      <img src="<?php echo $this->template->get_theme_path(); ?>/img/bg_home3.jpg" alt="" />
	  <div class="carousel-caption"> 
        <div id="mainslide_text" class="col-lg-7  col-xs-12 text-left">
            <p>AMARTHA MICROFINANCE</p>
			<h1>Expanding Outreach, Delivering Happiness</h1>
            <hr/>
            <h3>To empower low-income families in rural areas with affordable financial services, enabling them to pursue life for greater purposes.</h3>
            
        </div>
      </div>
    </div>
	<div class="item">
      
      <img src="<?php echo $this->template->get_theme_path(); ?>/img/bg_home4.jpg" alt="" />
	  <div class="carousel-caption"> 
        <div id="mainslide_text" class="col-lg-7  col-xs-12 text-left">
            <p>AMARTHA MICROFINANCE</p>
			<h1>Expanding Outreach, Delivering Happiness</h1>
            <hr/>
            <h3>To empower low-income families in rural areas with affordable financial services, enabling them to pursue life for greater purposes.</h3>
            
        </div>
      </div>
    </div>
	<div class="item">
      
      <img src="<?php echo $this->template->get_theme_path(); ?>/img/bg_home5.jpg" alt="" />
	  <div class="carousel-caption"> 
        <div id="mainslide_text" class="col-lg-7  col-xs-12 text-left">
            <p>AMARTHA MICROFINANCE</p>
			<h1>Expanding Outreach, Delivering Happiness</h1>
            <hr/>
            <h3>To empower low-income families in rural areas with affordable financial services, enabling them to pursue life for greater purposes.</h3>
            
        </div>
      </div>
    </div>
   
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</div>

<div class="home_promo_bg">
    <div class="container">
      <div class="row">
        
        <div class="col-lg-12 col-md-12  col-xs-12 text-center animated fadeInUp">
            <h1 class="animated fadeInUp">SERVING UNDERSERVED AREAS,<br/>GROWING RURAL ECONOMIES</h1>	
            <br/>
			<!--<p>" Our mission is to become a nationwide MFI that provides affordable financial services and delivers high quality services to low-income people in a large outreach. "</p>-->
            <a href="<?php echo site_url();?>/about" title="" class="white"><b>WHO WE ARE</b></a>
			<br/><br/><br/>
        </div>
      </div>
    </div> 
</div>

<!-- footslide -->
<div class="mainslide mainslide2">
  <div class="container">
    <div class="row">
        <div id="mainslide_text" class="col-lg-7">
            <p>WHY WOMEN?</p>
            <h1>100% of Our Members are Women</h1>
            <hr/>
            <p>Women sacrifice the most of every hardship in poverty. Thus we believe that every empowerment effort dedicated to women will directly contribute to the welfare enhancement of the entire family. The life improvement is reflected through better children education, healthcare, and sanitary infrastructure. Therefore, we are committed to support those women at the bottom of the pyramid through financial literacy and community organising in order to arrive at the community improvement as a whole.</p>
           
        </div>
    </div>
  </div>
</div>

<div class="mainslide mainslide_home_job">
	<div class="container">
    <div class="row">
        <div class="col-lg-4 col-xs-4 text-center animated fadeInUp">
			<a href="<?php echo site_url(); ?>/career/culture" title="Company Culture" class="text-white">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/job_com.png" width="50%" /><br/><br/>
				Company Culture
			</a>
		</div>
        <div class="col-lg-4 col-xs-4 text-center animated fadeInUp">
			<a href="<?php echo site_url(); ?>/career/life" title="Life at Amartha" class="text-white">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/job_life.png" width="50%" /><br/><br/>
				Life at Amartha
			</a>
		</div>
        <div class="col-lg-4 col-xs-4 text-center animated fadeInUp">
			<a href="<?php echo site_url(); ?>/career/opportunities" title="Jobs Opportunities" class="text-white">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/job_op.png" width="50%" /><br/><br/>
				Jobs Opportunities
			</a>
		</div>
	</div>
	</div>
</div>


<div style="position: relative; z-index: 2000; width: 100%; height: 600px">
  <iframe frameborder="0" height="100%" width="100%" 
    src="http://youtube.com/embed/ixssYuKDyV0?autoplay=0&controls=0&showinfo=0&autohide=1">
  </iframe>
</div>


	

   <div class="home_progress">d	12
	 <div class="container"> 
		<div class="row">
			<div class="col-lg-9 col-xs-12 text-white">
				<p style="font-size:12pt;">
					As we continue to evaluate our approach, it is proven that women’s access to financial services create great impact to the improvement of gender equality and community cohesiveness. 
					We consistently expand our outreach to enable more families to earn higher income and improve their participation in the economy.
				</p>
			</div>
			<div class="col-lg-3 col-xs-12 text-right">
				<a href="<?php echo site_url(); ?>/partners/annual_report" title="Annual Report" class="text-white">
				<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> &nbsp; Annual Report</button></a>
			</div>
		</div> 
		<div class="row">
			<div class="col-lg-12 col-xs-12 text-white"><br/><br/>
				<p style="font-size:30pt; font-weight: normal">Progress</p>
			<hr/><br/>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-xs-12">
				<div class="row">
					<div class="progress_box">
						<div class="col-lg-6 col-xs-12 text-white nopadding" >						
							<p style="font-size:12pt; line-height: 30px">total members as of december 2014</p>
						</div>					
						<div class="col-lg-6 col-xs-12 text-white text-right nopadding">
							<p style="font-size:58pt; margin-top:20px;" class="">6415</p>
						</div>
					</div>
				</div>				
			</div>
			
			<div class="col-lg-4 col-xs-12">
				<div class="row">
					<div class="progress_box">
						<div class="col-lg-6 col-xs-12 text-white nopadding" >						
							<p style="font-size:12pt; line-height: 30px">total villages</p>
						</div>					
						<div class="col-lg-6 col-xs-12 text-white text-right nopadding">
							<p style="font-size:58pt; margin-top:20px;" class="">91</p>
						</div>
					</div>
				</div>				
			</div>
			
			<div class="col-lg-4 col-xs-12">
				<div class="row">
					<div class="progress_box progress_boxlast">
						<div class="col-lg-7 col-xs-12 text-white nopadding" >						
							<p style="font-size:12pt; line-height: 30px">total amount disburshed since inception: </p>
						</div>					
						<div style=" margin-top:20px;" class="col-lg-5 col-xs-12 text-white text-right nopadding">
							<span style="font-size:20pt">Rp</span><span style="font-size:58pt;" class="">20</span>
							
						</div>
						<div style="margin-left:190px;" class="col-lg-5 col-xs-12 text-white text-right nopadding">
						
							<p style="font-size:35pt; padding-top: -10px;" class="">billion</p>
						</div>
					</div>
				</div>				
			</div>
				
			
		</div>
    </div>
</div>   
 

<!-- Social Media -->
<div class="home_social ">
	<div class="container">
      <div class="row">
        <div  class="col-lg-6  col-md-6  col-xs-12 timeline_left">
			 <div class="row pad_top_100">
				 <div  class="col-lg-10  col-md-10  col-xs-12">
					<div class="home_box_title"><b>N E W S</b></div>
					<?php foreach($news as $p):  ?>
					<div class="home_news_box">
						<div class="home_news_box_photo"><img src="<?php echo base_url();?>/files/<?php echo $p->news_picture; ?>" /></div>
						<div class="home_news_box_caption">
							<div class="home_news_box_caption_title"><?php echo $p->news_title; ?></div>
							<div class="home_news_box_caption_date"><?php echo date("Y F, d",strtotime($p->news_date)); ?></div>
							<div class="home_news_box_caption_content">
								<?php 
									function limit_text($text, $limit) {
									  if (str_word_count($text, 0) > $limit) {
										  $words = str_word_count($text, 2);
										  $pos = array_keys($words);
										  $text = substr($text, 0, $pos[$limit]) . '...';
									  }
									  return $text;
									}
									echo limit_text(strip_tags($p->news_content), 20); ?>
								
							</div>
							<div class="home_news_box_caption_more">
								<a href="<?php echo site_url();?>/news/detail/<?php echo $p->news_id; ?>" title="READ MORE" class="readmore">READ MORE</a>
							</div>
						</div>
					</div>			 
					<?php endforeach; ?> 
				</div>
				<div class="col-lg-2  col-md-2  col-xs-12 hidden-xs text-right timeline_garis"><img src="<?php echo $this->template->get_theme_path(); ?>/img/garis_kanan.png" class="img-responsive"/></div>
			</div>	
        </div>
		<div  class="col-lg-6  col-md-6  col-xs-12">
			 <div class="row">
				 <div  class="col-lg-10  col-md-10  col-xs-12 pull-right">
					<div class="home_box_title"><b>B L O G</b></div>
					<?php foreach($blog as $p):  ?>
					<div class="home_news_box">
						<div class="home_news_box_photo"><img src="<?php echo base_url();?>/files/<?php echo $p->blog_picture; ?>" /></div>
						<div class="home_news_box_caption">
							<div class="home_news_box_caption_title"><?php echo $p->blog_title; ?></div>
							<div class="home_news_box_caption_date"><?php echo date("Y F, d",strtotime($p->blog_date)); ?></div>
							<div class="home_news_box_caption_content">
								<?php 
									
									echo limit_text(strip_tags($p->blog_content), 20); ?>
								
							</div>
							<div class="home_news_box_caption_more">
								<a href="<?php echo site_url();?>/blog/detail/<?php echo $p->blog_id; ?>" title="READ MORE" class="readmore">READ MORE</a>
							</div>
						</div>
					</div>			 
					<?php endforeach; ?> 
				</div>
				<div class="col-lg-2  col-md-2  col-xs-12 hidden-xs timeline_garis"><img src="<?php echo $this->template->get_theme_path(); ?>/img/garis_kiri.png" class="img-responsive"/></div>
			</div>	
        </div>
		
		<!--
		<div  class="col-lg-4  col-md-12  col-xs-12  text-white text-center"> 
			<div id="home_social_twitter">
					
						<a href="https://twitter.com/amarthamf" title="follow @amarthamf" target="_blank"><img src="<?php echo $this->template->get_theme_path(); ?>/img/icon_twitter.png" /></a><br/>
						<div class="example2">
							<div class="tweet">
						
						</div>
						</div>

			 </div>
        </div>-->
    </div> 
    </div>
</div>

