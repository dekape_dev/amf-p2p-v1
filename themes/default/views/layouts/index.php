<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $sitetitle; ?> | Amartha</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Connect micro and small businesses seeking for affordable working capital with investors wanting to make impact investing">
    <meta name="keywords" content="amartha, microfinance, microfinance indonesia, grameen bank, impact investing, micro and small businesses, ukm indonesia, peer to peer, lending, p2p, investasi">
    <meta name="author" content="PT Amartha Fintech">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,300,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css'>

    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://lipis.github.io/flag-icon-css/css/flag-icon.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>css/bootstrap-theme.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>css/jasny-bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>css/datatables.min.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>css/style.css">
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>js/jquery-latest.js"></script>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>js/jasny-bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>js/main.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="shortcut icon" href="<?php echo $this->template->get_theme_path(); ?>img/favicon.ico">
  </head>
  <body>
    
    <?php include_once("analyticstracking.php") ?>
    <?php echo $template['body']; ?>
	<footer>
    	<div class="container">
    		<div class="row footerNav">
    			<div class="col-md-6 opensans years">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<a href="<?php echo site_url("career");?>" title="">CAREERS</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="<?php echo site_url("partners");?>" title="">PARTNERS</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<!-- <a href=" <?php echo site_url("faq");?>" title="">FAQ</a>&nbsp;&nbsp;&nbsp;&nbsp; -->
					<a href="<?php echo site_url("contact");?>" title="">CONTACT</a>

					<?php } else {?>
					<a href="<?php echo site_url("career");?>" title="">KARIR</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="<?php echo site_url("partners");?>" title="">MITRA</a>&nbsp;&nbsp;&nbsp;&nbsp;
					<!-- <a href=" <?php echo site_url("faq");?>" title="">FAQ</a>&nbsp;&nbsp;&nbsp;&nbsp; -->
					<a href="<?php echo site_url("contact");?>" title="">KONTAK</a>

					<?php }?>

				</div>


    			<!-- <div class="col-md-2">

    			</div>
    			<div class="col-md-4 flag">
    					<a href=""><img src="http://lipis.github.io/flag-icon-css/flags/4x3/id.svg"></a>
						<a href=""><img src="http://lipis.github.io/flag-icon-css/flags/4x3/gb.svg"></a>

    			</div> -->

                <div class="col-md-6 sosmed text-right">
                    <a target="_blank" href="https://www.facebook.com/amarthaid"><img src="<?php echo $this->template->get_theme_path(); ?>/img/sosmed/icon_fb_circle.png"></a>
                    <a target="_blank"  href="https://twitter.com/amarthaid"><img src="<?php echo $this->template->get_theme_path(); ?>/img/sosmed/icon_tw_circle.png"></a>
                    <a target="_blank"  href="https://www.instagram.com/amarthaid"><img src="<?php echo $this->template->get_theme_path(); ?>/img/sosmed/icon_ig_circle.png"></a>
                    <a target="_blank"  href="http://linkedin.com/company/amartha-microfinance"><img src="<?php echo $this->template->get_theme_path(); ?>/img/sosmed/icon_in_circle.png"></a>
                    <a target="_blank"  href="https://www.youtube.com/channel/UC17rm8IoI7blxKxd1Wwmwcg"><img src="<?php echo $this->template->get_theme_path(); ?>/img/sosmed/icon_yt_circle.png"></a>
                    </div>
    		</div>
    		<div class="copyright">
    			<div class="row">


    				<div class="col-md-6 left">
    					<small>&copy; <?php echo date("Y");?> - PT Amartha Mikro Fintek &nbsp;&nbsp;|&nbsp;&nbsp;
							<?php if ($this->session->userdata('site_lang') == 'english') { ?>
							<a href="">Privacy</a>&nbsp;&nbsp;|&nbsp;&nbsp;
							<a href="<?php echo site_url('termofservices');?>">Terms</a>

							<?php } else {?>
							<a href="">Privasi</a>&nbsp;&nbsp;|&nbsp;&nbsp;
							<a href="<?php echo site_url('termofservices');?>">Syarat &amp; Ketentuan</a>

							<?php }?>

						</small>
    				</div>



                    <div class="col-md-2">

                </div>
                <div class="col-md-4 flag">
                        <!--
                        <a href=""><img src="http://lipis.github.io/flag-icon-css/flags/4x3/id.svg"></a>
                        <a href=""><img src="http://lipis.github.io/flag-icon-css/flags/4x3/gb.svg"></a>
                        -->
                        <a href="<?php echo site_url('language/english'); ?>">English <span class="flag-icon flag-icon-gb"></span></a>
                        <a href=""> | </a>
                        <a href="<?php echo site_url('language/indonesia'); ?>"><span class="flag-icon flag-icon-id"></span> Bahasa</a>

                </div>
    			</div>


    		</div>
    	</div>
    </footer>
  </body>
</html>
