 <!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $sitetitle; ?> | Amartha</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="We are a microfinance institution that provides financial services to the low-income people living in Indonesia's rural areas who have limited access to affordable and quality financial services or even to the knowledge about such services.">
    <meta name="keywords" content="amartha, microfinance, microfinance indonesia, grameen bank">
    <meta name="author" content="PT. Amartha Fintech">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/bootstrap-theme.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/jasny-bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/jquery.circliful.css"  media="screen">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/datatables.min.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/style.css">

    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/jquery-latest.js"></script>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/jasny-bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/main.js"></script>
	 <!-- fancybox -->
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/popup.css" type="text/css" media="screen" />
	
    <link rel="shortcut icon" href="<?php echo $this->template->get_theme_path(); ?>/img/favicon.ico">
  </head>
  <body>
	
	<?php echo $template['body']; ?>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/jquery.popup.min.js"></script>
	<script>
		$(document).ready(function(){
			$('a.popup').popup();
		});
	</script>
  </body>
</html>
