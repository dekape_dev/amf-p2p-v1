 <!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $sitetitle; ?> | Amartha</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="We are a microfinance institution that provides financial services to the low-income people living in Indonesia's rural areas who have limited access to affordable and quality financial services or even to the knowledge about such services.">
    <meta name="keywords" content="amartha, microfinance, microfinance indonesia, grameen bank">
    <meta name="author" content="PT. Amartha Fintech">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300italic,300,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/bootstrap-theme.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/jasny-bootstrap.min.css" media="screen">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/jquery.circliful.css"  media="screen">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/datatables.min.css">
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/style.css">

    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/jquery-latest.js"></script>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/jasny-bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/main.js"></script>
	 <!-- fancybox -->
    <link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>/css/popup.css" type="text/css" media="screen" />
	
    <link rel="shortcut icon" href="<?php echo $this->template->get_theme_path(); ?>/img/favicon.ico">
  </head>
  <body>
	<div class="headerSimple">
		<a href="<?php echo site_url("dashboard");?>" title="Amartha Dashboard"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png"></a>
	</div>

	<div class="headerUser">
		<div class="userNav">
			<div class="container">
				<div class="row hidden-sm hidden-xs">
					<div class="col-md-4 text-center"><a href="<?php echo site_url('investor/wallet'); ?>" title="My Wallet">Hi, <?php echo $this->session->userdata('ufullname'); ?> <strong>(Rp <?php echo number_format($this->session->userdata('wallet')); ?>)</strong></a></div>
					<div class="col-md-4 text-center"><a href="<?php echo site_url('profile'); ?>" title="Account and Setting">Edit Profile</a></div>
					<div class="col-md-4 text-center"><a href="<?php echo site_url('logout'); ?>" title="Sign Out">Sign Out</a></div>
				</div>
				<div class="row visible-sm visible-xs">
					<div class="col-sm-6 col-xs-6 text-left"><a href="<?php echo site_url('investor/wallet'); ?>" title="My Wallet">Hi, <?php echo $this->session->userdata('ufullname'); ?> <strong>(Rp <?php echo number_format($this->session->userdata('wallet')); ?>)</strong></a></div>
					<div class="col-sm-6 col-xs-6 text-right"><a href="<?php echo site_url('profile'); ?>">Edit Profile</a> | <a href="<?php echo site_url('logout'); ?>">Sign Out</a></div>
				</div>
			</div>
		</div>
	</div>

	<div style="border-bottom: 1px solid #919191;">
		<div class="container">
			<div class="row userNav2">
				<div class="<?php echo "col-md-3 right dropdown ".$menu_wallet; ?>">
					<a href="<?php echo site_url('investor/wallet'); ?>" title="My Wallet">My Wallet</a>

				</div>
				<div class="<?php echo "col-md-3 right ".$menu_portfolio; ?>"><a href="<?php echo site_url('investor/portfolio'); ?>" title="Portfolio">Portfolio</a></div>
				<div class="<?php echo "col-md-3 right ".$menu_history; ?>"><a href="<?php echo site_url('investor/history'); ?>" title="History">History</a></div>
				<div class="<?php echo "col-md-3 left dropdown ".$menu_marketperformance; ?>">
					<a href="<?php echo site_url('marketperformance/investor'); ?>" title="Market Performance">Market Perfomance</a>
				</div>
			</div>

			<div class="stepNav">
				<ul>
					<li class="<?php echo $menu_overview; ?>">
						<a href="<?php echo site_url('dashboard'); ?>" title="Overview">Overview</a>
					</li>
					<li>></li>
					<li class="<?php echo $menu_browse_loan; ?>">Browse Loan</li>
					<li>></li>
					<li class="<?php echo $menu_confirm; ?>">Confirmation</li>
					<li>></li>
					<li class="<?php echo $menu_transfer; ?>">Transfer Fund</li>
				</ul>
			</div>

		</div>
	</div>
  <div>
    <div class="hidden container">
      <?php //echo print_message_admin($this->session->flashdata('message')); ?>
    </div>
  </div>


	<?php echo $template['body']; ?>

	<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/datatables.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/jquery.popup.min.js"></script>
	<script>
		$(document).ready(function(){
			$('.datatable').DataTable();
			$('a.popup').popup({
				width : 750,
				height : 500,
			});
			$('a.popupsmall').popup({
				width : 400,
				height : 80,
			});
		});
	</script>
  </body>
</html>
