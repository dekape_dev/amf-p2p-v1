    <div id="headerImageStory" style="background: url(<?php echo $this->template->get_theme_path(); ?>/img/bg_homepage.jpg) no-repeat center bottom fixed;background-size:cover;">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN/a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent clearfix">

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2>Amartha help business grow faster. We know because they tell us everyday</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">SIGN UP FOR AMARTHA</a>
				
				<?php } else {?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Amartha membantu bisnis tumbuh lebih cepat <br/>Kami tahu karena mereka menceritakannya setiap hari</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">Mulai Investasi Sekarang</a>
				
				<?php } ?>

		    	<h3>&nbsp;</h3>
		    	<!--<button class="btn btn-default" type="submit">SIGN UP FOR AMARTHA</button>-->
	    	</div>
		</div>
	</div>
	<div class="container text-center">
				<h2 class="text-center purple"><b>Apsiah: Kerja Keras Sampai Puas</b></h2>
				<p class="opensans" style="padding-bottom:20px;">“Alhamdulillah lah, sekarang ini usaha udah mulai enak. Sebulannya bisa lebih lah dari Rp 5.000.000 pemasukannya,” cerita Apsiah mengenai usaha budidaya dan jual beli ikan cupangnya yang terus berkembang. Sepanjang bercerita mengenai usahanya, Apsiah tidak lepas dari senyum.</p>

				<p class="opensans" style="padding-bottom:20px;">Apsiah adalah salah satu anggota Amartha yang terbilang sukses mengembangkan usahanya. Jika pada pembiayaan ke-1 banyak anggota yang masih fokus pada pembiayaan konsumtif (seperti renovasi rumah, biaya anak sekolah, dll), Apsiah justru mulai meniti langkah kecil untuk usahanya. Dengan pembiayaan Rp 500.000 untuk pertama kali, Apsiah membeli 500 ekor cupang.</p>

				<p class="opensans" style="padding-bottom:20px;">Kini usaha yang dijalaninya bersama suami semakin berkembang. Sebelumnya ia baru memiliki lapak di Jatinegara, sekarang sudah tambah satu kios kecil di Blok M.</p>


				<h3 class="text-center purple"><b>Berangkat dari Keahlian</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Sebelum menikah Apsiah sudah menjalani usaha cupang. Ia ikut bersama kakak laki-lakinya yang memulai usaha lebih dahulu. Bersama sang kakak ia memperhatikan betul-betul mulai dari memilih bibit, mengatur cara merawat cupang untuk mendulang untung-menurunkan risiko kerugian. Apsiah dengan keahliannya mengerti betul, musim adalah faktor yang sangat mempengaruhi untung-rugi. </p>

				<p class="opensans" stule="padding-bottom:20px;">Keahliannya di masa muda terus dibawa hingga akhirnya ia menikah dan memutuskan usaha cupang bersama suami, “ usaha cupang tuh bisa dibilang sederhana. Untungnya lumayan, selain itu saya bisa sambil ngurus rumah, anak, dan suami.”</p>

				<h3 class="text-center purple"><b>Tim Solid</b></h3>


				<p class="opensans" stule="padding-bottom:20px;">“Saya sama suami bagi-bagi kerjaan. Di rumah nih, nanti saya yang kasih empan, saya gantungin satu ekor-satu ekor gitu karena kalau gak digantungin satu-satu, cupang mah bisa berantem, abis pada rusak ekornya. Selain itu juga saya juga ganti airnya. Kalau si bapak, dia yang berangkat ke Pasar Jatinegara jam 2 pagi, nungguin lapak di sana. Dia juga tuh yang nantinya ke ruko di Blok M. Orangnya raji banget Bapak. Kalau belom sakit, dia mah masih ayo aja tiap pagi berangkat,” Apsiah bercerita bagaimana perjuangan ia dan suami membesarkan usaha bersama.</p>

				<p class="opensans" stule="padding-bottom:20px;">Tim solid menurut Apsiah adalah salah satu kunci mengapa usahanya bersama suami terus berkembang. Ia dan suami memiliki tujuan yang sama, membesarkan usaha dan menyekolahkan anak satu-satunya hingga ke perguruan tinggi. </p>

				<p class="opensans" stule="padding-bottom:20px;">Meski suami-istri, Apsiah bercerita bisnis adalah bisnis. Dalam usaha ini Apsiah sebagai petani cupang. Ia membeli bibit cupang untuk dibesarkan selama kurang lebih tiga bulan. Setelah cupang siap panen, ia menjualnya ke suami Rp 2.000/ekor. “Bisnis ya bisnis, heheh... dengan begitu saya juga kan di rumah akhirnya menghasilkan pendapatan pribadi,” jelas Apsiah sambil terkekeh. </p>



				<h3 class="text-center purple"><b>Titik Cerah</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Apsiah, ia dan suami mengaku bahwa mereka bukan dari keluarga mampu yang bisa meminjamkan dana untuk usaha. Oleh karena itu saat memulai usaha ia perlu mengumpulkan sepereka-dua perak dengan ikut arisan.</p>

				<p class="opensans" style="padding-bottom:20px;">Dari menang arisan itulah usahanya di mulai. “Memang jadi masalah sendiri karena arisan dapatnya gak banyak dan putarannya lama,” cerita Apsiah mengingat awal usahanya. </p>

				<p class="opensans" style="padding-bottom:20px;">Pinjaman dari tetangga pun bukan pilihan solusi, menurut Apsiah pinjam tetangga belum tentu dipercaya dan khawatir justru menjadi bahan cibiran. Sampai akhirnya Apsiah mengenal Amartha dari Ibu RT, ia berpikir modal pembiayaan tersebut adalah titik cerah untuk mengembangkan usahanya. </p>

				<p class="opensans" style="padding-bottom:20px;">Di awal ia mendapat pembiayaan sebesar Rp 500.000 yang ia habiskan untuk membeli bibit ikan cupang. Kini pembiayaan yang sedang berjalan, Rp 5.000.000, ia manfaatkan untuk membuat kolam, membeli bibit, dan merenovasi teras rumahnya. Sejak awal Apsiah memang menggunakan pembiayaan Amartha untuk modal usaha karena uang untuk usaha tidak akan cepat habis, justru terus bertambah seiring berkembangnya usaha. </p>

				<p class="opensans" style="padding-bottom:20px;">Hingga kini ia berhasil memutar uangnya, dari usaha yang dijalaninya bersama suami ia telah membeli tanah dan membangun rumah sendiri, membeli dua buah motor yang juga digunakan untuk usaha, membeli beberapa bidang tanah untuk kolam cupang, dan memiliki lapak di Jatinegara serta sewa ruko di Blok M.</p>

				<p class="opensans" style="padding-bottom:20px;">Apsiah merasa bersyukur dengan apa yang ia miliki sekarang ini, terutama motor, “dulu suami berangkat ke Pasar Jatinegara naik angkot, udah mah mahal, sampe-nya juga lama.” “Sekarang saya dan suami udah pisah juga dari rumah orang tua, Alhamdulillah rasanya lebih mandiri lagi,”tandas Apsiah sambil tersenyum. </p>

				<h3 class="text-center purple"><b>Mimpi ke Depan</b></h3>
				<p class="opensans" style="padding-bottom:20px;">Apsiah mendobrak mitos bahwa di desa dan keadaan ekonomi yang pas-pasan tidak perlu bermimpi banyak. Ia masih memngasuh impiannya. Apsiah ingin memiliki ruko di pinggir jalan, dengan begitu dirinya bisa mengembangkan usaha lain lagi, selain cupang sebagai tambahan pemasukan. </p>

				<p class="opensans" style="padding-bottom:20px;">Ia ingin juga menyekolahkan anaknya hingga kuliah, “ya walaupun emak-bapaknya, gak punya ijazah, saya pengen betul, anak saya kuliah sampai tinggi!” Rencana tersebut tentu bukan dibangun satu-dua malam, ia dan suami rajin menabung bahkan meminta Amartha untuk bisa menyediakan tabungan pendidikan yang baru akan ia ambil sepuluh tahun kemudian. </p>

				<p class="opensans" style="padding-bottom:20px;">“Biaya pendidikan kan pasti engga sedikit. Mumpung saya dan suami masih kuat usaha, pengen nabung banyak-banyak. Jadi nanti pas anak masuk SMP-SMA-sampai kuliah, dananya ada, tinggal ambil,” tutup Apsiah ketika ditanya apa yang mau ia lakukan untuk impiannya. </p>

			</div>
		</div>
	</div>
