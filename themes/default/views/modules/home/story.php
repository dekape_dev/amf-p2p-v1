    <div id="headerImageStory" style="background: url(<?php echo $this->template->get_theme_path(); ?>/img/bg_homepage.jpg) no-repeat center bottom fixed; background-size:cover;">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent clearfix">

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2>Amartha help business grow faster. We know because they tell us everyday</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">SIGN UP FOR AMARTHA</a>
				
				<?php } else {?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Amartha membantu bisnis tumbuh lebih cepat <br/>Kami tahu karena mereka menceritakannya setiap hari</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">Mulai Investasi Sekarang</a>
				
				<?php } ?>

		    	<h3>&nbsp;</h3>
		    	<!--<button class="btn btn-default" type="submit">SIGN UP FOR AMARTHA</button>-->
	    	</div>
		</div>
	</div>
	<div class="container text-center">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h2 class="opensans text-center purple"><b>Enah: Micro-entrepreneur with a Big Vision</b></h2>
				<p class="opensans" style="padding-bottom:20px;">“I just can’t stand it when I don’t do any business”, said one of Amartha’s clients, Enah (35 years old). Actively doing a business since she was a young girl, she has no longer seen that as a mere means of making a living. Doing a business has already deeply ingrained in her.</p>

				<p class="opensans" style="padding-bottom:20px;">Enah sells meatballs, various soups, chicken noodles, and fried finger foods. Founding her small business from home, she now has opened branches at two biggest traditional markets, Nyuncung and Ciseeng, in Ciseeng District, Bogor Regency. IDR 5 million is the average total revenue obtained over three days per week in both locations.</p>

				<h3 class="opensans text-center purple"><b>Determination to Go Forward</b></h3>

				<p class="opensans" style="padding-bottom:20px;">When asked about what motivates her to tirelessly do business, she replied, “I want my children to continue going to school. My husband and I want our children to succeed. I am just a primary school graduate. Hopefully, all their needs can get paid for until they reach their dreams.” She also revealed that her bitter past when they did not have a grain of rice to eat and money, even a penny, to buy her daughter a snack; has made her determined to continuously expand her business. She added that the challenges found in business can be overcome by keeping good relationships with buyers and fellow traders. Enah believes that by being sincere in doing business, she can enjoy God’s manifold provision.</p>

				<p class="opensans" stule="padding-bottom:20px;">Although only a primary school graduate, Enah does not feel inferior, “Even I’m only a primary school graduate, I don’t need no calculator! My workers also have no opportunity to fraud because I always remember how many meatballs and soup bowls are sold. I also records cash coming in and going out in detail. I record everything.”</p>

				<h3 class="opensans text-center purple"><b>Family Teamwork</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Enah is not alone in running the business, her husband cooperates. Even their eight children also work together to raise their family business. She said that her eldest daughter who has graduated the vocational high school and had worked, finally decided to grow the business with her. “Every day at 3:30 a.m. my children rise to help me getting ready for the day. They wash rice, grate coconuts and chop celeries. My eldest daughter also helps selling the foods. She is now capable of selling the foods on her own in Nyuncung market. The eldest sells the soups and her younger siblings help selling the drinks,” told Enah passionately.</p>

				<p class="opensans" style="padding-bottom:20px;">Having eight children is not a problem for Enah. Not only is she able to grow her business, but she is also capable to develop a sense of teamwork in her family. Enah said that her older children always take the initiative to look after their younger siblings when she and her husband are busy working in the market.</p>

				<h3 class="opensans text-center purple"><b>Business and Social Interaction</b></h3>
				<p class="opensans" style="padding-bottom:20px;">“I used the first financing of IDR 500,000 from Amartha to install ceramic tile in my house that previously had earthen floor. I then used the second financing amounting to IDR 1,500,000 to open a branch in Ciseeng Market”, explained Enah, who joined Amartha in 2010.</p>

				<p class="opensans" style="padding-bottom:20px;">In addition to the benefit she got from the financing, Enah also felt the benefit from having a saving account in Amartha. “Before joining Amartha, I had never had a saving account. But it turns out to be useful to have savings when there is a sudden need for business-related or other matters.” With regards to the group lending model that Amartha puts forward, she said, “I am too busy with my business that I rarely interact with my neighbours. By participating in the weekly group meeting, I can restore my relationship with my neighbours. "</p>

				<h3 class="opensans text-center purple"><b>Moving Forward Together</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Through several conversations with Enah, it is clear that she has a vision of her business. She aims to rent another shop for her eldest child and utilise her land for room renting business. By starting such businesses, she plans to continue embracing her relatives, who are still unemployed, to work with her.</p>

				<p class="opensans" style="padding-bottom:20px;">The fact that there is a tenacious female entrepreneur with integrity and vision like Enah, Amartha believes that she has set a noble example for other Amartha’s clients. Amartha also believes that the financing provided to her so far has contributed to and will continue to contribute to something useful.</p>
				</div>

				<?php } else {?>

				<h2 class="opensans text-center purple"><b>Enah: Pengusaha Kecil Bervisi Besar</b></h2>
				<p class="opensans" style="padding-bottom:20px;">"Saya mah kalau tidak dagang tidak betah”, demikian kalimat yang terucap dari mulut salah satu anggota Amartha, Enah (35 tahun). Terbiasa berdagang sejak gadis, Enah tidak lagi melihat berdagang sebagai sesuatu yang harus dilakukan semata untuk menyambung hidup. Kegiatan berdagang sudah mendarahdaging dalam dirinya. </p>

				<p class="opensans" style="padding-bottom:20px;">Enah adalah pedagang bakso, aneka soto, mie ayam, dan gorengan. Berawal dari rumah, ia kini telah membuka cabang di Pasar Nyuncung dan Pasar Ciseeng di Kecamatan Ciseeng, Kabupaten Bogor. Lima juta  rupiah adalah rata-rata total omset dagang mingguan yang ia peroleh selama tiga hari berjualan dalam seminggu di kedua lokasi tersebut.</p>

				<h3 class="opensans text-center purple"><b>Tekad Untuk Terus Maju</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Ketika ditanya akan motivasi yang mendorongnya terus semangat berdagang, ia menjawab, “saya ingin anak-anak terus sekolah. Suami dan Saya ingin anak-anak sukses, tidak seperti saya yang hanya lulusan SD. Semoga terbiayai sampai tercapai cita-cita mereka.” Ia juga mengungkapan bahwa perihnya masa lalu dimana mereka tidak memiliki beras dan tidak memiliki uang, meskipun seribu rupiah, untuk anak membeli makanan membuatnya bertekad untuk terus mengembangkan usahanya. Beliau menambahkan bahwa tantangan berdagang dapat diatasi dengan terus menjalin hubungan baik dengan pembeli dan sesama pedagang. Enah percaya bahwa dengan ikhlas dalam berdagang, rejeki yang didapat akan berlipat.</p>

				<p class="opensans" stule="padding-bottom:20px;">Meski hanya lulusan SD, Enah tidak merasa rendah diri, “saya mah lulusan SD juga ngitung nggak perlu pakai kalkulator! Terus pekerja Saya juga nggak bisa korupsi, karena saya selalu ingat berapa mangkok bakso dan soto yang terjual. Saya juga rinciin uang yang masuk dan keluar berapa. Saya catat semuanya.”</p>

				<h3 class="opensans text-center purple"><b>Kerjasama Keluarga</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Enah tidak sendiri dalam menjalankan usahanya, suaminya ikut serta. Bahkan delapan anaknya bahu membahu membesarkan usaha keluarga. Ia bercerita anakya yang sudah lulus SMK dan bekerja memutuskan untuk memilih mengembangkan usaha bersamanya.  “Pagi jam 3:30 anak-anak semua bangun. Mereka bantu cuci beras, marut kelapa, peras santan, dan iris seledri. Anak sulung saya juga bantu jualan. Bahkan sekarang ini si teteh mah udah bisa jualan sendiri di Pasar Nyucung. Teteh yang paling tua jualin soto, ade-ade-nya ikut jualin minuman botol,” Enah menceritakan dengan semangat. </p>

				<p class="opensans" style="padding-bottom:20px;">Delapan anak sama sekali bukan kendala bagi Enah. Ia bukan hanya mampu membesarkan usahanya, tetapi juga membangun kerjasama antar anggota keluarga. Enah mengaku anak-anaknya yang lebih besar selalu berinisiatif menjaga adik-adiknya ketika ia dan suami sibuk di pasar. </p>

				<h3 class="opensans text-center purple"><b>Usaha dan Bermasyarakat</b></h3>
				<p class="opensans" style="padding-bottom:20px;">“Pembiayaan pertama sebesar Rp 500.000 dari Amartha saya gunakan untuk pasang keramik rumah yang sebelumnya masih tanah. Pembiayaan kedua sebesar Rp 1.500.000 saya pakai untuk buka cabang di Pasar Ciseeng”, demikian ungkap Enah, anggota Amartha yang bergabung pada 2010.</p>

				<p class="opensans" style="padding-bottom:20px;">Selain pembiayaan modal usaha, Enah juga merasakan manfaat Tabungan Amartha. “Saya sebelum di Amartha tidak pernah nabung. Tapi ternyata enak punya tabungan, pas ada kebutuhan mendadak untuk urusan dagang atau yang lain bisa dipake. ”Mengenai sistem berkelompok yang Amartha kedepankan, ia berujar, “saya sibuk dagang sampai tidak ngobrol dengan tetangga. Dengan ikut kumpulan majelis mingguan, saya bisa silaturahmi lagi dengan tetangga.”</p>

				<h3 class="opensans text-center purple"><b>Maju Bersama</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Melalui percakapan dengan Enah, terlihat jelas bahwa ia memiliki visi tentang kewirausahaannya. Ia bercita-cita untuk menyewa satu ruko lagi untuk anak tertuanya serta memanfaatkan aset tanahnya untuk dikontrakan. </p>

				<p class="opensans" style="padding-bottom:20px;">Melalui usaha-usaha ini, ia juga terus merangkul saudara-saudaranya yang masih menganggur. Bahwa terdapat pengusaha wanita yang ulet, berintegritas, dan memiliki visi seperti Enah, Amartha percaya ia merupakan contoh bagi anggota Amartha lainnya. Amartha juga percaya pembiayaan yang diberikan telah dan akan terus berkontribusi kepada sesuatu yang bermanfaat.</p>
				</div>

				<?php }?>
				
