
<!-- HEADER -->
	<div id="headerImageStoryGlasses">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="100px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

			<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<!-- Hero Text -->
			<div class="startContent singleHead clearfix">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2>Free Glasses to Increase Women Productivity</h3>


				<?php } else {?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Kacamata Gratis: Untuk Produktivitas Perempuan</h2>


				<?php }?>
		 	 </div>
	    </div>
	</div>
	<!-- END OF HEADER -->

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="about_WhoWeAre  text-justify ">

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<p class="opensans fs-14" style="padding-top: 20px; ">Amartha Microfinance (Amartha) successfully executed the Free Glasses Program for a number of residents in Ciseeng and Tenjo Districts, Bogor, West Java, on April 25 and August 27, 2015. In collaboration with the Berani Bhakti Bangsa Foundation, the programme was expected to improve vision problem of housewives working as craftsmen and students starting from the primary level up to the secondary level.</p>

				<p class="opensans fs-14" style="padding-top: 20px; ">This programme had distributed a number of glasses to 200 housewives in Ciseeng District and 222 housewives in Tenjo District, as well as 200 students in Ciseeng and Tenjo Districts. This was an attempt by Amartha to continuously be part of the solution to the problems that occur at the grassroots level over the last five years operating.</p>

				<?php } else {?>

				<p class="opensans fs-14" style="padding-top: 20px; ">Amartha Microfinance (Amartha) berhasil melaksanakan Program Pemberian Kacamata Gratis untuk sejumlah warga Kecamatan Ciseeng dan Tenjo, Kabupaten Bogor, Jawa Barat pada 25 April dan 27 Agustus 2015. Bekerja sama dengan Yayasan Berani Bhakti Bangsa, program ini diharapkan dapat meningkatkan kualitas kesehatan mata para ibu rumah tangga yang bekerja sebagai perajin dan pelajar di tingkat SD hingga SMA.</p>

				<p class="opensans fs-14" style="padding-top: 20px; ">Program ini telah mendistribusikan sejumlah kacamata kepada 200 ibu rumah tangga di Kecamatan Ciseeng dan 222 orang ibu di Kecamatan Tenjo, serta 200 pelajar di Kecamatan Ciseeng dan Tenjo. Hal ini merupakan upaya Amartha dalam menjadi bagian dari solusi atas permasalahan yang terjadi di tingkat akar rumput selama lima tahun terakhir.</p>

				<?php }?>

				
			</div>


			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="about_WhoWeAre  text-justify ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>

					<h1 class="opensans text-left purple">Why the Free Glasses Programme?</h1>
					<p  class="opensans fs-14" style="padding-top: 20px; ">Living in remote villages in Bogor District makes it difficult for people to have regular access to proper healthcare. The local health centre has not been able to correct the eyesight problem. Meanwhile, to be treated at the nearest hospital would cause them financial burden as they mainly earn daily income that is just sufficient to meet their basic needs.</p>

					<p  class="opensans fs-14" style="padding-top: 20px; ">Most mothers in their middle age who work as craftsmen and tailors has been facing productivity problem as their eyesight began to deteriorate. A pair of glasses are hoped to help mothers to overcome this problem so as to maintain productivity.</p>

					<h1 class="opensans text-left purple">Support Productivity = Promote Women's Empowerment</h1>

					<p class="opensans fs-14"style="padding-top: 20px; ">The Free Glasses Programme was also a form of support from Amartha in commemoration of the International Women's Day on 8 March. Through this programme, Amartha hoped to support women's empowerment discourse in which women are entitled to have the opportunity to work.</p>

					<p class="opensans fs-14" style="padding-top: 20px; ">In addition, the programme also serves as an affirmative action (affirmative action) that departs from an analogy that: if a small child with a short body wanted to take a cup on the table is high, our task is not to fetch the cup. Our job is to fetch a chair so that he could take his own cup.</p>

					<?php } else {?>

					<h1 class="opensans text-left purple">Mengapa Program Kacamata Gratis? </h1>
					<p  class="opensans fs-14" style="padding-top: 20px; ">Tinggal di pelosok desa Kabupaten Bogor membuat masyarakat jauh dari akses kesehatan yang layak. Puskesmas setempat belum mampu menangangi masalah kesehatan mata. Sementara, untuk berobat di rumah sakit terdekat akan memberikan beban biaya bagi masyarakat yang mayoritas berpenghasilan harian dan hanya cukup untuk memenuhi kebutuhan pokok.</p>

					<p  class="opensans fs-14" style="padding-top: 20px; ">Sebagian besar ibu yang bekerja sebagai perajin dan penjahit mengalami masalah produktifitas di tengah usia yang tak lagi muda dan kualitas penglihatan mulai menurun. Kacamata diharapkan mampu membantu para ibu untuk mengatasi persoalan penglihatan sehingga mampu menjaga produktifitasnya.</p>

					<h1 class="opensans text-left purple">Mendukung Produktivitas = Mendorong Pemberdayaan Perempuan</h1>

					<p class="opensans fs-14"style="padding-top: 20px; ">Program Kacamata Gratis juga merupakan bentuk dukungan Amartha dalam memperingati Hari Perempuan Internasional pada 8 Maret. Melalui program ini, Amartha mendukung wacana pemberdayaan perempuan dimana perempuan berhak memiliki kesempatan untuk berkarya.</p>

					<p class="opensans fs-14" style="padding-top: 20px; ">Di samping itu, program ini juga sebuah aksi afirmatif (affirmative action) yang berangkat dari sebuah analogi: jika seorang anak kecil dengan tubuh yang pendek ingin mengambil sebuah cangkir di atas meja yang tinggi, tugas kita bukanlah mengambilkan cangkir tersebut. Tugas kita adalah mengambilkan kursi agar ia bisa mengambil sendiri cangkirnya.</p>

					<?php }?>
				
				</div>
				<div class="col-md-6 social4 no-mobile">
					<img src="<?php echo $this->template->get_theme_path(); ?>img/stories/glasses_450x500.jpg">
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 social" style="margin-bottom:20px;">
					<img src="<?php echo $this->template->get_theme_path(); ?>img/stories/glasses_430x500.jpg">
				</div>
				<div class="col-md-6 social3" style="margin-bottom:20px;">
					<img src="<?php echo $this->template->get_theme_path(); ?>img/stories/glasses_430x250.jpg">
				</div>
				<div class="col-md-6 social5" style="margin-bottom:20px;">
					<img src="<?php echo $this->template->get_theme_path(); ?>img/stories/glasses_260x270.jpg">&nbsp;&nbsp;&nbsp;&nbsp;
					<img src="<?php echo $this->template->get_theme_path(); ?>img/stories/glasses_260x270.jpg">
				</div>
				<div class="col-md-6 social2" style="margin-bottom:20px;">
					<img src="<?php echo $this->template->get_theme_path(); ?>img/stories/glasses_430x250-3.jpg">
				</div>
			</div>
		</div>
	</div>


	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-12">
				<div class="about_WhoWeAre  text-justify ">

					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h1 class="opensans text-center purple">Social Collaboration</h1>

					<p class="opensans fs-14" style="padding-top: 20px; ">Amartha believes that collaboration is a good indicator of public awareness on social issues. Therefore, we work with other organisations and initiatives deemed to share a common vision.</p>

					<p class="opensans fs-14" style="padding-top: 20px; ">Berani Bhakti Bangsa Foundation is a movement that has concern for education of Indonesian children, particularly in the field of improvement of reading literacy. They believe reading literacy is the basis for developing a more independent and life. With that, Indonesia’s young generation will be the engine of growth to improve welfare in the future.</p>

					<p class="opensans fs-14" style="padding-top: 20px; ">For this programme, we also collaborated with Kitabisa, an online crowdfunding platform that connects the social project owners and donors. With the campaign duration of approximately thirty days, Amartha managed to garner support from 141 people with a total donation of more than IDR 60,000,000.</p>

					<p class="opensans fs-14" style="padding-top: 20px; ">During the process, we also established collaboration with Yoe Optic located in Semarang. With the same mission to help people with eyesight problem, Yoe Optic helped Amartha to facilitate the procurement of another 142 pairs of glasses for women in Tenjo District, West Java..</p>

					<p class="opensans fs-14" style="padding-top: 20px; ">Free Glasses Programme is a form of encouragement to women's empowerment. Collaborations to support the productivity of Amartha’s clients will continue to be done in the future. Therefore, Amartha is open to any collaboration opportunities with various parties towards women's empowerment.</p>

					<?php } else {?>

					<h1 class="opensans text-center purple">Kolaborasi Sosial</h1>

					<p class="opensans fs-14" style="padding-top: 20px; ">Amartha percaya bahwa kolaborasi adalah salah satu indikator yang baik untuk menggambarkan kepedulian masyarakat Indonesia atas permasalahan sosial. Oleh karena itu, kami bekerjasama dengan beberapa gerakan yang dirasa memiliki kesatuan visi. </p>

					<p class="opensans fs-14" style="padding-top: 20px; ">Yayasan Berani Bhakti Bangsa adalah sebuah gerakan yang memiliki kepedulian terhadap pendidikan anak-anak Indonesia, khususnya dalam bidang peningkatan kemampuan membaca (reading literacy). Reading Literacy merupakan dasar untuk mengembangkan kehidupan yang lebih mandiri dan bermutu. Melaluinya anak-anak Indonesia akan lahir sebagai tunas kemajuan dan kesejahteraan bangsa Indonesia di masa depan.</p>

					<p class="opensans fs-14" style="padding-top: 20px; ">Kolaborasi juga kami lakukan dengan kitabisa, sebuah platform crowdfunding (patungan online) yang menghubungkan pemilik ide sosial dan para donatur. Dengan kampanye selama lebih kurang tiga puluh hari, Amartha berhasil mengumpulkan dukungan dari 141 orang dengan total donasi lebih dari Rp 60.000.000,-.</p>

					<p class="opensans fs-14" style="padding-top: 20px; ">Dalam perjalanannya, kami juga menjalin kolaborasi dengan YOE Optic yang berlokasi di Semarang. Melalui visi yang sama, membantu kebutuhan penglihatan, YOE Optic memudahkan Amartha dalam pengadaan tambahan kacamata sejumlah 142 buah untuk para ibu di Kecamatan Tenjo, Jawa Barat.</p>

					<p class="opensans fs-14" style="padding-top: 20px; ">Program Kacamata Gratis merupakan salah satu bentuk dorongan terhadap pemberdayaan perempuan. Kolaborasi untuk mendorong produktivitas anggota Amartha akan terus dilakukan di masa yang akan datang. Untuk itu, Amartha membuka peluang kolaborasi selebar-lebarnya kepada berbagai pihak untuk mendorong pemberdayaan perempuan.</p>

					<?php }?>

					


			</div>


			</div>
		</div>
	</div>

