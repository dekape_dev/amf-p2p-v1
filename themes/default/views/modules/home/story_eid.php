    <div id="headerImageStoryIed" >
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent singleHead  clearfix">

				<div class="container">

					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    		<h2 class="text-center">Eid Al-Adha Sacrificial Meat Distribution</h2>

					<?php } else {?>
		    		<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Idul Adha Kurban</h2>

					<?php }?>

	    	</div>
	    </div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="about_WhoWeAre  text-justify ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>

					<p style="padding-top: 20px; ">In 2011 and 2012 Amartha Microfinance (Amartha) took part in celebrating Eid Al-Adha by sacrificing animals in the name of God and distributed the sacrificial meat to underprivileged people. In 2011, the programme was conducted in three villages, Cigelap, Cikupa, and Curug, in Ciseeng District; while in 2012, the programme was conducted in Jasinga District. Both districts are located in Bogor Regency, West Java. All beneficiaries were Amartha’s clients. </p>

					<p style="padding-top: 20px; ">Sacrificial meat was distributed in a fair and equitable manner as a form of concern for the surrounding community. For the middle- or upper-income groups, it is easier to meet the needs of animal protein that is beneficial for human body. Their ability to meet the animal protein intake of lamb, beef, or fish is bigger than that of the low-income group whose level of daily expenditure is no more than IDR 35,000 - IDR 50,000.</p>

					<p style="padding-top: 20px; ">Amartha also emphasises social cohesion within each of its community-based social programmes. In the case of sacrificial meat distribution, for instance, the rural communities participated in organising the distribution to ensure its fairness and equal distribution.</p>

					<?php } else {?>

					<p style="padding-top: 20px; ">Pada 2011 dan 2012 Amartha Microfinance (Amartha) turut merayakan Idul Adha dengan melaksanakan (ibadah) kurban. Program dilaksanakan di desa Cigelap, Cikupa, dan Curug di Kecamatan Ciseeng, Kabupaten Bogor, Jawa Barat. Masyarakat yang menjadi target ialah mereka anggota Amartha yang berpenghasilan rendah.</p>

					<p style="padding-top: 20px; ">Daging kurban didistribuskan secara adil dan merata sebagai bentuk kepedulian terhadap lingkungan sekitar. Bagi masyarakat menengah atau atas, mudah untuk memenuhi kebutuhan protein hewani yang besar manfaatnya bagi tubuh. Kemampuan untuk memenuhi asupan protein hewani dari daging kambing, sapi, atau ikan lebih besar. Akan tetapi berbeda halnya dengan masyarakat berpenghasilan rendah yang belanja sehari-harinya tidak lebih dari Rp 35.000 – Rp 50.000.</p>

					<p style="padding-top: 20px; ">Amartha juga menekankan kohesivitas sosial dalam setiap program sosial berbasis komunitas. Dalam program kurban misalnya masyarakat turun tangan dalam mengatur distribusi secara adil dan merata.</p>

					<?php }?>

				
			</div>


			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
<!--
	<div id="gallery">
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/talkshow_1.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/talkshow_2.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/talkshow_3.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/talkshow_4.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/talkshow_5.jpg" />
	</div>
-->

	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-9">
				<div class="about_WhoWeAre  text-justify ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>

					<h2 class="opensans text-left purple">Empati dan Altruis  </h2>
					<p style="padding-top: 20px; ">Interacting with low-income people on a daily basis can produce two diametrically opposite attitudes, which are, empathy and apathy.</p>

					<p style="padding-top: 20px; ">Apathy occurs because someone is consistently exposed to the same thing through every day interaction. This is likely to happen to social workers who interact directly with the rural communities on a daily basis. They are characterised by losing their ability to feel the emotions or difficulties of other people.</p>


					<p style="padding-top: 20px; ">For Amartha team, the sacrificial meat distribution programme is a refreshment program that could replenish the empathy deficit. This programme can also instil altruism, cultivating an attitude of promoting the common interest.  </p>

					<p style="padding-top: 20px; ">No less smart, Aan, a vegetables vendor in Tenjo, looks for strategies when many other vegetable vendors start to come in. She lowers the price by IDR 500 to IDR 1,000 to attract buyers. She deliberately allows gado-gado (Indonesian vegetable salad with peanut dressing) sellers and cooked food sellers to buy vegetables from her on credit. From her agility in running her business, she has managed to repay the instalment of a car for angkot (city carrier), so that her husband can operate that car as an angkot driver, as well as a vehicle to purchase vegetables.</p>

					<p style="padding-top: 20px; ">Through Inspirational Talk Show, we expect other clients to be inspired by the experience of the speakers so that they continue to be motivated to grow business and strive to be more independent. Furthermore, Inspirational Talk Show pushes the rural communities to network with each other, particularly to encourage each other in terms of business and economy.</p>

					<p style="padding-top: 20px; ">Inspirational Talk Show will not stop in 2015. It will continue to take place so that its positive impact continues to snowball and benefits the rural communities.</p>

					<?php } else {?>

					<h2 class="opensans text-left purple">Empati dan Altruis</h2>
					<p style="padding-top: 20px; ">Berinteraksi secara langsung dengan masyarakat berpenghasilan rendah setiap harinya, bisa menghasilkan dua kutub yang saling bertolak belakang, empati atau justru ‘tumpul’ (compassion fatigue). </p>

					<p style="padding-top: 20px; ">‘Tumpul’ terjadi karena secara konsisten terpapar hal yang sama melalui interaksi setiap harinya. Hal tersebut cenderung terjadi kepada pekerja sosial yang berinteraksi langsung dengan masyarakat setiap harinya. Ia mudah dicirikan dengan hilangnya kemampuan dalam merasakan emosi atau kesulitan orang lain.</p>

					<p style="padding-top: 20px; ">Bagi tim Amartha, program kurban ialah refreshment yang bisa mengisi kembali empati yang defisit. Kurban juga dapat menanamkan altruis, menanamkan sikap mengedepankan kepentingan bersama.</p>

					<?php }?>
			</div>


			</div>
		</div>
	</div>







		<script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.touchSwipe.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.imagesloaded.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.scrollTo-1.4.3.1-min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/spin.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/portfolio.js" type="text/javascript"></script>

    <script type="text/javascript">
    $(document).ready(function() {
            var p = $("#gallery").portfolio();
            var q = $("#gallery2").portfolio();
            p.init();

    });
    </script>
