    <div id="headerImageStoryTooth" >
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent singleHead  clearfix">

				<div class="container">

					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    		<h2 class="text-center">Tooth Fairies Paint a Million Smiles on World Oral Health Day</h2>


					<?php } else {?>
		    		<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Peri Gigi Warnai Sejuta Senyum di Hari Kesehatan Mulut Sedunia</h2>


					<?php }?>

	    	</div>
	    </div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="about_WhoWeAre  text-justify ">

					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<p style="padding-top: 20px; ">Every year on March 20, efforts to promote oral health awareness is taken to an international stage as nations across the globe commemorate World Oral Health Day. The FDI World Dental Federation reports that more than 70 nations were involved in this year’s celebration.</p>

					<p style="padding-top: 20px; ">In Indonesia last Saturday, Peri Gigi Indonesia, or Indonesia Tooth Fairies, joined forces with Amartha Microfinance in gathering more than 1,000 people from the Jasinga district in the Bogor regency, West Java, to participate in the festivities.</p>

					<p style="padding-top: 20px; ">Under the theme “One Man, One Toothbrush, One Million Smiles,” Peri Gigi Indonesia, a local initiative that promotes dental and oral health to the public through a series of community empowerment activities, visited the Setu village which is one of Amartha Microfinance’s remote operational areas.</p>


					<?php } else {?>

					<p style="padding-top: 20px; ">Setiap 20 Maret, upaya untuk mempromosikan kesadaran kesehatan mulut selalu dilakukan bertepatan dengan Hari Kesehatan Mulut Dunia. FDI World Dental Federation mencatat lebih dari 70 negara ikut serta pada perayaan Hari Kesehatan Mulut Sedunia. </p>

					<p style="padding-top: 20px; ">Bekerjasama dengan Amartha Microfinance, Peri Gigi Indonesia berhasil melibatkan 1.000 orang di Jasinga, Jawa Barat turut berpartisipasi.</p>

					<p style="padding-top: 20px; ">Dengan tema, “Satu Orang, Satu Sikat Gigi, Sejuta Senyum” Peri Gigi Indonesia mempromosikan kesehatan mulut melalui serangkaian aktivitas dengan basis penguatan komunitas. Peri Gigi Indonesia adalah sebuh gerakan inisiatif yang gencar mempromosikan kesehatan gigi dan mulut bagi masyarakat. Sementara, Amartha Microfinance ialah institusi bisnis sosial yang bekerja menamping masyarakat prasejahtera di pedalaman pedesaan melalui jasa keuangan mikro dan program berbasis komunitas. </p>

					<?php }?>

				
			</div>


			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

	<div id="gallery">
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/tooth_1.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/tooth_2.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/tooth_3.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/tooth_4.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/tooth_5.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/tooth_6.jpg" />

	</div>


	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-9">
				<div class="about_WhoWeAre  text-justify ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<!-- <p style="padding-top: 20px; ">Amartha Microfinance itself is a social business institution that works towards empowering low-income families in rural areas through affordable financial services and community-organizing programs.</p> -->

					<p style="padding-top: 20px; ">“Based on our survey on the villagers and interview with the people from the local Public Health Centres [Puskesmas] and schools conducted prior to this event, we have come to a conclusion that the people of Jasinga District are the right people to benefit from this program, as access to information on dental and oral health, as well as health services [in the area] are still limited,” said Peri Gigi Indonesia public relations coordinator Anindyajati Nuralifiana.</p>

					<p style="padding-top: 20px; ">The program comprised two main activities, namely the interactive dental and oral education, which saw both parents and children divided into different classes, as well as a tooth-brushing activity for children, in which they were accompanied by their parents and assisted by “tooth fairies.”</p>

					<p style="padding-top: 20px; ">“We are glad to be a part of this program. Health education is definitely one of the keys to improve the lives of people with low income,” said Achmad Mulyadi, the appointed project leader for the event.</p>

					<p style="padding-top: 20px; ">Dina Anggraini, a parent who participated in the event, said she and her 12-year-old son were glad to participate in the event.  “We learned many things, such as the correct way to brush our teeth and the right time to brush our teeth,” Dina said.</p>

					<p style="padding-top: 20px; ">Kokom, another participant, said she often shared her toothbrush with her 10-year-old daughter Tina. “Today we learned from the doctors that we are not supposed to share a toothbrush,” Kokom said with a laugh.</p>

					<p style="padding-top: 20px; ">“Tina now has her own toothbrush and she is very happy about it,” she added.</p>

					<?php } else {?>

					<p style="padding-top: 20px; ">“Dari survey dan interview dengan masyarakat lokal dan Puskesmas setempat, kami menyimpulkan, masyarakat di Jasinga adalah sasaran program yang tepat untuk program kami karena akses informasi kesehatan gigi dan mulut masih terbatas, begitu pun akses kesehatannya,”jelas Anindyajati Nurlifiana, Koordinator Humas Peri Gigi Indonesia.</p>

					<p style="padding-top: 20px; ">Rangkaian program terdiri dari dua kegiatan utama, yaitu pendidikan interaktif kesehatan gigi dan mulut, yang membagi anak dan orang tua dalam kelas berbeda. Kedua, kegiatan menyikat gigi untuk anak-anak, di mana mereka didampingi oleh orang tua dan dibantu oleh "peri gigi”.</p>

					<p style="padding-top: 20px; ">“Kami senang sekali berpartisipasi dalam program ini karena pendidikan kesehatan adalah satu yang penting untuk meningkatkan kualitas hidup masyarakat prasejahtera,” tutur Achmad Mulyadi, Project Leader dari Amartha Microfinance.</p>

					<p style="padding-top: 20px; ">Dina Anggaraini, seorang peserta orang tua yang berpatisipasi, mengatakan bahwa anak perempuannya yang berusia 12 tahun senang berparsipasi dalam event ini. “Kami belajar banyak hal, seperti bagimana cara menyikat gigi yang benar dan waktu yang tepat untuk menyikat gigi,”cerita Dina saat ditanya kesannya mengenai acara.</p>

					<p style="padding-top: 20px; ">Kokom, partisipan lain mengaku ia sering secara bergantian menggunakan sikat gigi yang sama dengan anak perempuannya berusia 10 tahun. “Namun hari ini kami belajar dari para dokter gigi, seharusnya berganti siakat gigi tidak dilakukan,” cerita Kokom sambil tersenyum.</p>

					<p style="padding-top: 20px; ">“Melalui program Peri Gigi Tina sekarang memiliki sikat giginya sendiri dan ia merasa senang,”Kokom menambahkan.</p>

					<p style="padding-top: 20px; ">Kesehatan gigi dan mulut selama ini tidak dipandang begitu penting, padahal seringkali kefokusan anak belajar seringkali terganggu karena masalah gigi dan mulut. Melalui program ini Amartha dan Peri Gigi berhasil menanamkan pentingnya kesehatan gigi dan mulut dengan cara yang mudah dicerna anak.</p>

					<?php }?>

					
			</div>


			</div>
		</div>
	</div>








		<script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.touchSwipe.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.imagesloaded.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.scrollTo-1.4.3.1-min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/spin.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/portfolio.js" type="text/javascript"></script>

    <script type="text/javascript">
    $(document).ready(function() {
            var p = $("#gallery").portfolio();
            p.init();
    });
    </script>
