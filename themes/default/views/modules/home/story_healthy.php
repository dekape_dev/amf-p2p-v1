    <div id="headerImageStoryHealthy" >
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent singleHead clearfix">

				<div class="container">


		    	<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2 class="text-center">Healthy Children, Happy Families</h2>


				<?php } else {?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Anak Sehat, Keluarga Bahagia</h2>


				<?php }?>
	    	</div>
	    </div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="about_WhoWeAre  text-justify ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<p style="padding-top: 20px; ">Sunday, May 10, 2015, was not the ordinary one. The yard of Islamic Primary School, Nurul Huda, was filled with children dressed in white-blue-bright red school uniform. No less than a total of 150 mothers and children filled up the seats that were prepared. Promptly at 9 am, the event titled “Healthy Children, Happy Family” was opened with a speech from Mr. Mas'ud as the Chairman of Nurul Huda Foundation. The event continued with remarks from Amartha Microfinance (Amartha) and an Indonesian NGO called Sahabat Sehat Kita.</p>

					<p style="padding-top: 20px; ">The children looked impatient as they were waiting to take part in the event. Hence, soon after the opening session was finished, they energetically waved their parents good-bye as they were being directed to attend five classes prepared beforehand. The children were divided into five small groups, each accompanied and guided by a staff from Amartha. Each class talked on different topic facilitated by two volunteers from Sahabat Sehat Kita. Most of the volunteers came from the health sector. Each group had the opportunity to attend classes in turns.</p>

					<?php } else {?>
					<p style="padding-top: 20px; ">Minggu, 10 Mei 2015 tidak seperti Minggu biasanya. Halaman sekolah MI Nurul Huda dipenuhi oleh anak-anak muda berseragam putih-biru dan merah cerah. Tidak kurang 150 ibu dan anak memadati kursi yang telah disiapkan. Tepat pukul 9 pagi, acara bertajuk Anak Sehat Keluarga Bahagia dibuka dengan sambutan oleh Bapak Mas’ud selaku Ketua Yayasan MI Nurul Huda. Acara dilanjutkan dengan perkenalan oleh Amartha Microfinance (Amartha) dan Sahabat Sehat Kita.</p>

					<p style="padding-top: 20px; ">Anak-anak kecil berkalung pita warna-warni terlihat tidak sabar untuk mengikuti kegiatan. Segera setelah pembukaan selesai, mereka dipisahkan dengan para orang tua untuk mengikuti lima kelas yang disiapkan. Anak-anak dibagi menjadi lima kelompok kecil, masing-masing didampingi oleh seorang kakak pembina dari Amartha. </p>

					<?php }?>

				
			</div>


			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

	<div id="gallery">
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/healthy_1.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/healthy_2.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/healthy_3.jpg" />

	</div>


	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-9">
				<div class="about_WhoWeAre  text-justify ">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<p style="padding-top: 20px; ">The first class was the Anthropometry Class. Here, the children’s height and weight were measured. Then, their nutritional status were assessed and classified using the World Health Organisation (WHO) weight-for-age table. From the total of 64 children aged 6-10 years, it was identified that 50%, 36%, 13%, and 1% of them had severe malnutrition, malnutrition, good nutrition, and over-nutrition status respectively. The second class was on Nutritional Substances. This class talked about the importance of healthy food and the functions of nutrients, such as carbohydrates, proteins, fats, vitamins, minerals, and water. Then, the children were taught on combining healthy foods into dinner plates in the Healthy Plate Class. In this third class, they learned to put together a complete nutritional substance with different sizes of composition into a dinner plate. The fourth class was on Healthy Foods VS Junk Foods. Here, the children were advised on how to determine whether a food is healthy or not. It was interesting to see that most of them had been aware of several kinds of unhealthy food and yet they had continued to consume those foods. The last but not least was the Oral and Hand Hygiene Class where the children were educated on the correct way to brush teeth and to wash hands to avoid germs that cause disease.</p>

				<p style="padding-top: 20px; ">Meanwhile, in the school yard, the parents were also occupied with activities. They listened to a health seminar by Dr Yusuf Guest Speaker from Sahabat Sehat Kita about NEWSTART. NEWSTART stands for Nutrition (healthy and nutritious food), Exercise (sports), Water (clean water), Sunshine (sunlight), Temperance (temperance or self-control), Air (fresh air), Rest (adequate rest), Trust in God (surrender to God Almighty). Those eight points, according to Dr Yusuf, are keys to achieving optimal health.</p>

				<p style="padding-top: 20px; ">The activities for parents continued with the talk on nutritional information and the cooking demonstration of healthy-but-affordable meals by nutritionists. Nutritional information emphasizes the balanced nutrition for children of school age. Children as the young generation and pride of their families should be provided with adequate nutrients through a healthy diet from an early age as to support their growth and physical and mental development.</p>

				<p style="padding-top: 20px; ">The cooking demonstration was the last session for the parents. The dishes cooked during the demo included Steamed Eggs with Spinach, Basil and Tomato Sauce, and Corn Pudding with Brown Sugar Sauce. All dishes are easy to cook and healthy for the family. The ingredients used for the dishes are easy to obtain and even can be grown in the backyard. At the same time, all dishes contain complete nutritional substance that is healthy for children and the whole family.</p>

				<p style="padding-top: 20px; ">While the mothers were enthusiastically tasting the dishes, the children came out of the classes as the event was approaching its end. The event was closed with the distribution of goody bag containing merchandise from the sponsors, such as Pertamina, Pepsodent, and MNC TV.</p>

				<p style="padding-top: 20px; ">The children’s merry laughter and the mothers’ smile gave the organiser satisfaction. May the knowledge shared could be useful towards building healthy and happy families.</p>

				<?php } else {?>

				<p style="padding-top: 20px; ">Setiap kelas memiliki satu pembahasan informasi yang berbeda, dipandu dua orang kakak pengajar dari Sahabat Sehat Kita. Mayoritas mereka berlatar belakang kesehatan. Setiap kelompok akan mendapat kesempatan untuk mengikuti kelas-kelas secara bergiliran.Kelas pertama adalah Kelas Antropometri. Di sini, anak-anak diukur tinggi dan berat badannya. Setelah itu, anak-anak tersebut dilihat status gizinya berdasarkan berat badan menurut umur dari tabel World Heatlh Organisation (WHO). Dari 64 anak berusia 6-10 tahun yang diukur, terlihat bahwa 50% berstatus gizi buruk, 36% gizi kurang, 13% gizi baik, dan 3% bergizi lebih.Berikutnya adalah Kelas Mengenal Zat Gizi. Kelas ini mengajarkan mengenai pentingnya makanan sehat dan fungsi beragam zat gizi bagi tubuh, seperti karbohidrat, protein, lemak, vitamin, mineral, dan air. Selain diajarkan mengenai pentingnya makanan sehat bagi tubuh, anak-anak juga diajarkan menyusun komposisi makanan sehat ke dalam piring makan dalam Kelas Piring Sehat. Mereka belajar menyusun zat gizi lengkap dengan besar komposisi yang berbeda dalam satu piring. Kemudian diajarkan pula Makanan Sehat vs Jajanan Di kelas ini anak-anak diarahkan untuk dapat menentukan makanan sehat atau tidak. Hal yang menarik adalah sebagian besar dari mereka sudah tahu beberapa jenis makanan yang tidak sehat tetapi masih sering dikonsumsi. Kelas terakhir yang tidak kalah penting dalah Kelas Kebersihan Mulut dan Tangan. Selain memahami makanan sehat sebagai sumber gizi bagi tubuh, anak-anak juga diajarkan untuk senantiasa menjaga kebersihan mulut dan tangan. Kelas ini mengajarkan bagaimana menggosok gigi yang baik serta mencuci tangan yang benar agar terhindar dari kuman penyebab penyakit.</p>

				<p style="padding-top: 20px; ">Sementara itu, di halaman sekolah para orangtua juga sibuk berkegiatan. Para orang tua menyimak seminar kesehatan yang dibawakan oleh dr. Yusuf mengenai NEWSTART. NEWSTART adalah singkatan dari Nutrition (makanan sehat dan bergizi), Exercise (olahraga), Water (air bersih), Sunshine (sinar matahari), Temperance (pertarakan atau pengendalian diri), Air (udara segar), Rest (istirahat yang cukup), Trust in God (berserah kepada Tuhan YME). Delapan poin tersebut adalah kiat dalam mencapai kesehatan optimal.</p>

				<p style="padding-top: 20px; ">Kegiatan dilanjutkan dengan informasi gizi dan demo masak makanan sehat, namun murah oleh ahli gizi. Informasi gizi menekankan gizi seimbang bagi anak usia sekolah. Anak-anak sebagai tunas bangsa dan kebanggaan keluarga sebaiknya didukung dengan pemberian zat gizi yang adekuat melalui makanan sehat sejak dini sehingga mendukung pertumbuhan dan perkembangan fisik dan mental.</p>

				<p style="padding-top: 20px; ">Demo masak menjadi acara penutup sesi orangtua. Menu yang dimasak pada sesi tersebut adalah Telur Kukus Bayam, Kemangi Saus Tomat, dan Puding Jagung Saus Gula Merah. Ketiga menu ini adalah menu praktis sekaligus sehat bagi keluarga. Praktis karena bahan yang digunakan mudah diperoleh, bahkan dapat tumbuh di pekarangan rumah. Meski demikian, menu tersebut mengandung zat gizi lengkap yang dapat menjadi makanan tambahan bagi anak atau seluruh keluarga.</p>

				<p style="padding-top: 20px; ">Sementara para ibu antusias mencicipi masakan, anak-anak mulai berdatangan dari kelas-kelas pertanda  sudah mendekati penghujung acara. Acara ditutup dengan pembagian goody bag berisi merchandise dari sponsor, seperti Pertamina, Pepsodent dan MNC TV. </p>

				<p style="padding-top: 20px; ">Tawa riang anak-anak dan senyuman para ibu menjadi kebahagiaan tersendiri bagi panitia. Semoga ilmu pengetahuan yang dibagikan dapat menjadi bekal membangun keluarga yang sehat dan bahagia. </p>

				<?php }?>

				
			</div>


			</div>
		</div>
	</div>








		<script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.touchSwipe.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.imagesloaded.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.scrollTo-1.4.3.1-min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/spin.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/portfolio.js" type="text/javascript"></script>

    <script type="text/javascript">
    $(document).ready(function() {
            var p = $("#gallery").portfolio();
            p.init();
    });
    </script>
