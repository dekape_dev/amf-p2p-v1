

	<link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>landing/css/magnific-popup.css">
	<link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>landing/css/datepicker3.css">
	<!-- <link rel="stylesheet" href="css/font-awesome.min.css"> -->
	<link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>landing/css/style.css">


</head>

<body>
	<div class="fullCover">
		<div class="container Landing">
			<div class="row">
				<div class="col-md-6 TitleSection">
					<div class="logoLanding">

						<a href="<?php echo site_url('borrow/under20mil'); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>landing/img/logo.png"></div></a>
					<h1>AYO DAFTARKAN DIRI ANDA</h1>

					<h3>Microfinance Terbesar di Indonesia</h3>

					<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.  </p>
				</div>
				<div class="col-md-1"></div>
				<div class="col-md-5 formSection">

					<h3>Daftarkan diri anda sekarang!</h3>
					<form>
						<!-- FIRSNAME-LASTNAME -->
					  <div class="row form-group">
					  	<div class="col-md-6">
					  		<input type="text" class="form-control" id="exampleInputEmail1" placeholder="First Name">
					  	</div>
					  	<div class="col-md-6">
					  		<input type="text" class="form-control" id="exampleInputEmail1" placeholder="Last Name">
					  	</div>
					  </div>

					  <!-- EMAIL -->
					  <div class="form-group">
					    <input type="email" class="form-control" id="" placeholder="Email">
					  </div>

					  <!-- NO KTP -->
					  <div class="form-group">
					    <input type="text" class="form-control" id="" placeholder="*No KTP">
					  </div>


					  <div class="form-group">
					    <input class="form-control" id="datetimepicker4" name="tanggal lahir" placeholder="mm/dd/yyyy">
					  </div>

					  <!-- Plafond -->
					  <div class="form-group">
					    <input type="text" class="form-control" id="" placeholder="Plafond">
					  </div>

					  <!-- Sector -->
					  <div class="form-group">
					    <input type="text" class="form-control" id="" placeholder="Sector">
					  </div>

					  <!-- Desa -->
					  <div class="form-group">
					    <input type="text" class="form-control" id="" placeholder="Desa">
					  </div>


					  <!-- STUDIO -->

					  <div class="form-group">
						  <select class="form-control" id="sel1" >
						    <option style="color:red;">Select your location</option>
						    <option>2</option>
						    <option>3</option>
						    <option>4</option>
						  </select>
						</div>

						<p>*Ketentuan Berlaku</p>

						<!-- TERM AND CONDITION -->

					  <div class="checkbox">
					    <label>
					      <input type="checkbox"> <a href="#test-popup" class="open-popup-link">Saya setuju dengan syarat & ketentuan yang berlaku</a>
					    </label>
					  </div>

					  <!-- BUTTON -->

					  <button type="submit" class="btn btn-default btn-submit">DAFTAR SEKARANG</button>
					</form>

				</div>
			</div>
			<div id="test-popup" class="white-popup mfp-hide"> <h3>Syarat &amp; Ketentuan</h3>
				<ul>
					<li>Promo ini hanya berlaku untuk member yang belum pernah melakukan latihan EMS di studio 20FIT.</li>
					<li>Promo ini tidak dapat digabungkan dengan promo yang lain.</li>
					<li>Apabila sudah pernah latihan di 20FIT sebelumnya dan ingin latihan kembali tetapi tidak mengambil paket maka akan dikenakan biaya per-visit.</li>
					<li>Latihan EMS (Electro Muscle Stimulation) tidak dianjurkan untuk individu berusia dibawah 15 tahun.</li>
					<li>Member yang sudah terdaftar tidak bisa digantikan oleh orang lain.</li>
					<li>Member yang sudah mendaftarkan diri akan mendapatkan kode voucher dengan masa berlaku selama 2 minggu.</li>
					<li>Pembelian paket tidak dapat dibatalkan atau diuangkan kembali.</li>
					<li>Member diharuskan membawa kartu identitas yang valid untuk proses verifikasi.</li>
					<li>20FIT tidak akan menyebarluaskan email member ke publik.</li>
					<li>Member hanya bisa menggunakan sesi selama waktu yang sudah ditentukan dimulai dari pertama kali latihan, apabila masa berlaku habis maka sisa sesi akan hangus dan tidak dapat diuangkan kembali.</li>
					<li>Paket yang dibeli hanya berlaku di cabang yang ditentukan oleh member (tidak dapat ditransfer ke cabang lain).</li>
					<li>Durasi latihan hanya 20 menit ditambah dengan program relaksasi metabolisme selama 5 menit. Tidak boleh melebihi waktu yang telah ditentukan, karena bisa terjadi over training dan kerusakan pada sistem saraf.</li>
				</ul>
			</div>
		</div>
	</div>












<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>landing/js/moment.js"></script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>landing/js/transition.js"></script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>landing/js/collapse.js"></script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>landing/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>landing/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>landing/js/jquery.magnific-popup.min.js"></script>

 <script type="text/javascript">

 var dateObject = $("#datetimepicker4").datepicker("getDate");
var dateString = $.datepicker.formatDate("dd-mm-yy", dateObject);



        </script>
	<script type="text/javascript" src="js/main.js"></script>





</body>

</html>
