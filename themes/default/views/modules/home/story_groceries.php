    <div id="headerImageStoryGroceries" >
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent singleHead clearfix">

				<div class="container">

					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="text-center">Ramadhan Cheap Groceries</h2>

					<?php } else {?>
					<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Sembako Murah Ramadhan</h2>

					<?php }?>

			    	
	    	</div>
	    </div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="about_WhoWeAre  text-justify ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<p style="padding-top: 20px; ">Every year ahead of Eid Al-Fitr, Indonesians always face the same issue that is, the increase in prices of basic commodities. Although the government often conducts market operations, however, food scarcity due to looming food distribution and rising prices of basic commodities have always been difficult to control.</p>

					<p style="padding-top: 20px; ">In 2014 for example, the price increase reached 25%. Tribunnews reported the highest increase on flour and eggs, which reached IDR 5,000/Kg. Any price increases ahead of Eid would burden the people, especially for the low-income group.</p>

					<?php } else {?>

					<p style="padding-top: 20px; ">Setiap tahun jelang Idul Fitri kita selalu merasakan siklus yang sama, kenaikan harga bahan pokok. Meski pemerintah kerap kali melakukan operasi pasar, kelangkaan karena arus distribusi dan kenaikan harga bahan pokok selalu sulit dikendalikan.</p>

					<p style="padding-top: 20px; ">Pada 2014 misalnya, kenaikan harga mencapai 25%. Dilansir dari tribunnews, kenaikan tertinggi yakni pada terigu dan telur yang mencapai Rp 5.000/Kg. Kenaikan harga setiap jelang Idul Fitri tentu memberatkan masyarakat, terutama bagi masyarakat prasejahtera.</p>

					<?php }?>

					
			</div>


			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

	<div id="gallery">
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/groceries_1.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/groceries_2.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/groceries_3.jpg" />
	</div>


	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-9">
				<div class="about_WhoWeAre  text-justify ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>

					<h1 class="opensans text-left purple">Distribute 7,000 #CheapGroceries in the Month of Ramadhan 2015</h1>

					<p style="padding-top: 20px; ">Since 2011, Amartha has organised the cheap groceries programme as its annual programme. In 2014, the total number of cheap groceries packages distributed was more than double from the previous year, i.e. 6,000 packages.</p>

					<p style="padding-top: 20px; ">In 2015 our target is to distribute 7,000 packages of cheap groceries. The packages will be distributed to the low-income earners living across more than 92 villages in Bogor and South Bandung regencies. In Bogor Regency, we plan to distribute to those living in Ciseeng, Bojong Gede, Kemang, Jasinga, and Tenjo districts while in South Bandung Regency we plan to distribute packages in Cangkuang District.</p>


					<h1 class="opensans text-left purple">Implementation with Character Building Approach</h1>

					<p style="padding-top: 20px; ">What distinguishes #CheapGroceries programme by Amartha from other similar programmes is the fact that we work together with our clients throughout the planning, grouping, and distribution phases. Our clients participate actively so that the packages are successfully distributed to Amartha’s clients located in over 92 villages in Bogor and South Bandung districts.</p>

					<h1 class="opensans text-left purple">Why #CheapGroceries?</h1>


					<p style="padding-top: 20px; "><strong>Amartha’s spirit to serve lies on the objective of supporting those at the bottom of the economic ladder to become more resilient.</strong> #CheapGroceries is a programme designed to help the low-income earners to independently meet their basic needs during Ramadhan with more affordable basic commodities, subsidised through donations made by the general public.</p>

					<?php } else {?>

					<h1 class="opensans text-left purple">Tebar 7000 #SembakoMurah Ramadhan di 2015</h1>

					<p style="padding-top: 20px; ">Sejak 2011, Amartha rutin mengadakan program sembako murah. Pada 2014, total sembako murah yang didistribusikan meningkat dua kali lipat dari tahun sebelumnya, yakni 6.000 paket.</p>

					<p style="padding-top: 20px; ">Di 2015 ini kami menargetkan 7.000 sembako murah. Paket sembako akan didistribusikan bagi masyarakat prasejahtera yang tersebar di lebih dari 92 desa Kabupaten Bogor dan Kabupaten Bandung Selatan. Di Kabupaten Bogor kami menyasar, Kecamatan Ciseeng, Bojong Gede, Kemang, Jasinga, dan Tenjo. Di Bandung Selatan kami menyasar Kecamatan Cangkuang.</p>


					<h1 class="opensans text-left purple">Pelaksanaan dengan Pendekatan Character Building</h1>

					<p style="padding-top: 20px; ">Hal yang membedakan program #sembakomurah oleh Amartha ialah pada cara kami melibatkan melibatkan anggota untuk turut serta menyukseskan pelaksanaan kegiatan dari perencanaan, pengiriman, hingga pembagian. Anggota berpartisipasi aktif sehingga paket sembako akan didistribusikan bagi masyarakat prasejahtera yang tersebar di 92 desa Kabupaten Bogor dan Kabupaten Bandung Selatan.</p>

					<h1 class="opensans text-left purple">Mengapa #SembakoMurah?</h1>


					<p style="padding-top: 20px; "><strong>Semangat Amartha melayani adalah menjadikan masyarakat di tangga ekonomi terbawah menjadi lebih mandiri.</strong> #SembakoMurah merupakan program untuk mendukung kemandirian masyarakat di tangga ekonomi terbawah agar mereka tetap bisa memenuhi kebutuhan pokoknya di bulan Ramadhan. Dengan subsidi berbentuk donasi, kita juga bisa </p>

					<?php }?>
					


			</div>


			</div>
		</div>
	</div>
	<div id="gallery2">
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/groceries_4.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/groceries_5.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/groceries_6.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/groceries_7.jpg" />
	</div>




		<script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.touchSwipe.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.imagesloaded.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.scrollTo-1.4.3.1-min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/spin.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/portfolio.js" type="text/javascript"></script>

    <script type="text/javascript">
    $(document).ready(function() {
            var p = $("#gallery").portfolio();
            var q = $("#gallery2").portfolio();
            p.init();
            q.init();
    });
    </script>
