
<!-- HEADER -->
	<div id="headerImageBorrow">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo.png" height="110px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

			<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<!-- Hero Text -->
			<div class="container">

			<div class="startContent  clearfix">
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #000; ">We currently serve several regions. We are expanding our business and aim to serve more regions across Indonesia. Please tell us your location and leave your contact here.</h2>


	    </div></div>
	    </div>
	</div>
	<!-- END OF HEADER -->


	<div class="container">
		<div class="about_WhoWeAre  text-justify ">
			<h1 class="opensans text-center purple">With the right approach, business loans can help Indonesia’s micro and small businesses grow sustainably</h1>

			<p style="padding-top: 20px; ">Whether you just need a small amount of loan to get your business through a though time or a more substantial amount loan to help it expand, Amartha can help. We have already helped many businesses, like yours, to fulfil their aspirations.</p>

			<p style="padding-top: 20px; ">We offer two types of business loan. Business loans under IDR 20,000,000 are treated as group loans which means that while you are personally responsible for paying back the loan, the members of your group have agreed to the purpose of your loan and have pledged to be collectively responsible for any unpaid instalment. Meanwhile, we also offer larger amount of business loans ranging from IDR 20,000,000 up to IDR 200,000,000 which are provided directly to individual businesses and not treated as group loans. Our procedure for giving out loans is simple, transparent, and fair so that you can get the support you need. Find out how to apply below.</p>

		</div>

		<div class="about_WhoWeAre  text-justify ">
			<h1 class="opensans text-center purple">Why borrowers choose us</h1>

			<ul style="padding-top:20px;">
				<li>Negotiable rate – based on the agreement between lenders and borrowers</li>
				<li>Various combinations of amount and tenor – borrow between IDR 1,500,000 - IDR 200,000,000 and repay within 3 months to 2 years</li>
				<li>Collateral free, easy process, and real client support</li>
				<li>Credit history building – enable borrowers to start building their credit history and hence improve their credit worthiness</li>
			</ul>
		</div>



		<div class="row tabBorrow">
					<a href="<?php echo site_url('borrow/under20mil'); ?>"><div class="col-md-6 pasive"><h3>For loans below IDR 20,000,000</h3></div></a>
					<a href="<?php echo site_url('borrow/above20mil'); ?>"><div class="col-md-6 pasive active"><h3>For loans between IDR 20,000,000 and IDR 200,000,000</h3></div></a>

		</div>

		<div class="about_WhoWeAre  text-justify ">
			<h1 class="opensans text-left purple">Requirements</h1>
			<ul style="padding-top:20px;list-style-type: circle;">
				<li>Holding Indonesian citizenship</li>
				<li>Female</li>
				<li>At least 21 years old and max. 60 years old</li>
				<li>Holding an Indonesian bank account</li>
				<li>Domiciled in Jawa island</li>
			</ul>
		</div>
	</div>

	<div id="ImageBorrowProcess">
		<div class="container">
			<div class="row borrowProcess" style="padding-top:60px;">
				<div class="col-md-3">
					<span>1</span>
					<img src="<?php echo $this->template->get_theme_path(); ?>/img/proses1.png">
					<p>Get personalise quote </p>

				</div>
				<div class="col-md-3">
					<span>2</span>
					<img src="<?php echo $this->template->get_theme_path(); ?>/img/proses1.png">
					<p>Complete full application</p>

				</div>
				<div class="col-md-3">
					<span>3</span>
					<img src="<?php echo $this->template->get_theme_path(); ?>/img/proses1.png">
					<p>Our staff will contact you</p>

				</div>

				<div class="col-md-3">
					<span>4</span>
					<img src="<?php echo $this->template->get_theme_path(); ?>/img/proses1.png">
					<p>Fund will be in your account</p>
				</div>

			</div>
			<h2 class="text-center" style="margin-top:20px; color:#FFF;">Once you complete the application form, our staff will contact you.</h2>

		</div>
	</div>

	<!-- <div class="container FaqBorrow">
		<h1>BASIC FAQ</h1>
		<div class="col-md-3"><h2>Borrower</h2></div>
		<div class="col-md-9">
			<ul>
				<li><h3>Informasi atau dokumen apa sajakah yang dibutuhkan untuk pengajuan pinjaman?</h3></li>
				<li><h3>Loren ipsum</h3></li>
				<li><h3>Loren ipsum dolor si amet</h3></li>
				<li><h3>Loren Ipsum dolor</h3></li>
			</ul>
		</div>
	</div> -->

<section class="">
	<div class="container startContent" style="margin-bottom: 50px;">
		<h2>Join millions of other business who signed on Amartha</h2>
		<button onclick="location.href='<?php echo site_url('formborrow/above20mil'); ?>';" class="btn btn-default" type="submit">SIGN UP NOW</button>
	</div>
</section>




