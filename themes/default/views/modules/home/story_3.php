    <div id="headerImageStory" style="background: url(<?php echo $this->template->get_theme_path(); ?>/img/bg_homepage.jpg) no-repeat center bottom fixed;background-size:cover;">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTOR</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent clearfix">

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2>Amartha help business grow faster. We know because they tell us everyday</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">SIGN UP FOR AMARTHA</a>
				
				<?php } else {?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Amartha membantu bisnis tumbuh lebih cepat <br/>Kami tahu karena mereka menceritakannya setiap hari</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">Mulai Investasi Sekarang</a>
				
				<?php } ?>

		    	<h3>&nbsp;</h3>
		    	<!--<button class="btn btn-default" type="submit">SIGN UP FOR AMARTHA</button>-->
	    	</div>
		</div>
	</div>
	<div class="container text-center">
		<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		<h2 class="text-center purple"><b>Onih Triana: Striving for a Dream</b></h2>
		<p class="opensans" style="padding-bottom:20px;">"Three years of trying and failing may make people tired and lazy to try again. It is not the case for Onih Triana (39 years old). She is a mother of two who was experiencing a failure in her catfish cultivation business and has chosen to re-start other businesses with limited capital. “At that time, my capital was drained out. I was thinking hard trying to look for ideas, as in what can be done with little capital. Finally I decided to start weaving doormat”, told Onih story when asked on how she started her business.</p>

		<p class="opensans" style="padding-bottom:20px;">Onih is one of Amartha’s clients who have joined since 2012. Initially, Onih was only a labourer who received a wage of IDR 1,500 per woven doormat. With the help of financing from Amartha, Onih now has succeeded in developing her own woven doormat business and has even employed seven workers.</p>

		<h3 class="text-center purple"><b>The Bittersweet of Running a Business</b></h3>

		<p class="opensans" style="padding-bottom:20px;">The first time Onih went into entrepreneurship was through selling fried finger foods and steamed coconut rice. She started her business with a capital of IDR 500,000 obtained from Amartha. Every morning, she sells her foods to nearby school cafeterias. However, day after day, she saw a decline in the sales of her foods. According to Onih, that was because there were already many sellers of fried finger foods and steamed coconut rice.</p>

		<p class="opensans" stule="padding-bottom:20px;">Onih decided to completely choose a different business path. Alongside with her husband, who worked as a security guard, she started cultivation of catfish. To develop her new business, Onih invested the second, third, and fourth working capital she got from Amartha amounting to IDR 1,500,000, IDR 3,000,000, and IDR 3,000,000 respectively.</p>


		<h3 class="text-center purple"><b>Capital and Trust </b></h3>

		<p class="opensans" style="padding-bottom:20px;">During all this time Onih felt help with the financing from Amartha. All three businesses that she has tried to build were supported with the working capital obtained from Amartha. “The source of my working capital is only from Amartha. Really, my purpose to borrow [money] from the beginning is solely for business. So that I can generate cash flow that I can revolve and that way I do not need to ask for my husband’s help to pay the instalments", explained Onih when asked whether she has any other sources of capital.</p>

		<p class="opensans" style="padding-bottom:20px;">Onih lives in the neighbourhood where the people are all of low-income earners who are incapable of providing financial assistance for businesses. The alternative source of financing only comes from the loan sharks who collect daily instalment and charge high interest rates. “I really feel that Amartha has helped me on providing the working capital and continues to have confidence in me to keep on trying”, added Onih.</p>


		<h3 class="text-center purple"><b>Nurturing Dreams</b></h3>
		<p class="opensans" style="padding-bottom:20px;">Onih is really keen on expanding her business. She wants to be able to shop rags and burlap sacks directly from Tanah Abang (the biggest textile market in Southeast Asia based in Jakarta). Her objective is to lower the cost of raw materials. “I want to buy the rags in bulk and have a barn built next to my house to store the rags. So I do not need to go back and forth to shop. That way, I can have more rags for anyone who wishes to buy some and weave on their own. So far, I sometimes have to refuse some orders because I am running out of raw materials. At the same time, my cash flow has not yet enough to pay wages”, said Onih.</p>

		<p class="opensans" style="padding-bottom:20px;">Onih is very concerned with the education of her children. Her first child is 23 years old, previously working as an office boy in a private school. Her son has now been appointed as an additional teaching staff member for arts because of his skills. Meanwhile, her second child is still attending vocational school.</p>

		<p class="opensans" style="padding-bottom:20px;">To the first child, Onih is very supportive of his desire to go to college. This is shown through diligently saving her money in Amartha. For her child who is still in vocational school education, Onih does not hesitate to pay for his favourite extracurricular activity, that is, Paskibra (flag raisers). She says that she was really proud when she learned that her child's school Paskibra won the district-level flag raising competition.</p>

		<h3 class="text-center purple"><b>Creating Opportunities</b></h3>
		<p class="opensans" style="padding-bottom:20px;">Onih is someone who cares about other people in her surroundings. She always thinks of expanding her business and employing more people. Onih has embraced her relatives to weave doormats. With her growing business, she wants to see more women who are unemployed to join her and start weaving doormats with her. According to Onih, by creating opportunities, she can invite other people to improve their lives, just like what she is doing to herself.</p>

		<?php } else {?>

		<h2 class="text-center purple"><b>Onih Triana: Bekerja Keras untuk Impian </b></h2>
		<p class="opensans" style="padding-bottom:20px;">Tiga tahun berusaha dan gagal boleh jadi membuat orang lelah dan malas untuk berusaha kembali. Lain halnya dengan Onih (39 tahun), ibu dua anak yang menuai kegagalan dalam usaha budi daya lele memilih untuk memulai kembali usaha lain dengan modal terbatas. “Waktu itu modal saya abis, saya muter-muter cari ide, apa ya yang bisa dibikin dengan modal kecil. Akhirnya saya mutusin untuk mulai nganyam keset,” cerita Onih ketika ditanya bagaimana ia memulai bisnisnya. </p>

		<p class="opensans" style="padding-bottom:20px;">Onih adalah salah seorang anggota Amartha yang bergabung sejak 2012. Awalnya, Onih hanya seorang buruh anyam yang menerima upah Rp 1.500/keset. Dengan bantuan modal pembiayaan dari Amartha, Onih kini telah berhasil mengembangkan usahanya, bahkan mempekerjakan tujuh orang buruh anyam.</p>

		<h3 class="text-center purple"><b>Pahit Manis Usaha</b></h3>

		<p class="opensans" style="padding-bottom:20px;">Pertama kali terjun ke dunia usaha Onih memutuskan berjualan gorengan dan nasi uduk. Ia memulai bisnisnya dengan modal Rp 500.000 dari Amartha. Setiap pagi ia mengirimkan dagangannya ke sekolah-sekolah terdekat. Hari demi hari, ia melihat penjualannya justru menurun. Menurut Onih, hal tersebut karena banyaknya penjual gorengan dan nasi uduk.</p>

		<p class="opensans" stule="padding-bottom:20px;">Onih memutuskan banting stir, ia memulai budi daya ikan lele bersama suaminya yang bekerja sebagai security. Untuk membesarkan usahanya Onih menginvestasikan modal dari Amartha di tahun kedua, ketiga, dan kempat, masing-masing Rp 1.500.000, Rp 3.000.000, dan Rp 3.000.000 untuk budi daya lele.</p>

		<p class="opensans" stule="padding-bottom:20px;">Awal usaha selalu menuntut masa belajar, tiga tahun menjalankan usaha lele, ikannya habis! Lele milik Onih gagal panen dan banyak yang mati karena kemarau panjang. Dengan sedikit sisa modal, ia memutar otak. Bagaimana caranya terus mandiri dan membantu keuangan suami. Gaji suami Onih tidak bisa dibilang sedikit (Rp 3.000.000), namun ia ingin pendapatan yang lebih. Onih ingin anak-anaknya kuliah. Ia mulai lagi menganyam keset. Kali ini bukan menjadi buruh anyam melainkan membangun usaha sendiri. </p>

		<h3 class="text-center purple"><b>Setiap Usaha Pasti Ada Proses</b></h3>

		<p class="opensans" stule="padding-bottom:20px;">“Seperti keset, orang awalnya pasti selalu melihat sulit untuk dikerjain, tetapi kalau udah biasa, akhirnya ya sederhana. Di usaha juga gitu, proses pasti ada. Lama-lama juga... ya lumayan dah,” cerita Onih mengalir sementara jari-jarinya lincah menganyam satu demi satu perca di atas karung goni.</p>

		<p class="opensans" stule="padding-bottom:20px;">Onih mengawali prosesnya dengan modal kecil. Ia mengambil kain perca dari Cilangkap dan membeli karung goni sebagai media anyam. Di awal usaha ia baru mampu memenuhi satu kodi setiap minggunya. Sekarang ini, dengan tujuh orang buruh anyam, ia sudah bisa memenuhi permintaan sampai sembilan puluh keset perminggunya. </p>

		<p class="opensans" stule="padding-bottom:20px;">Dari usaha yang Onih bangun, ia berhasil memperoleh pendapatan sebesar Rp 450.000 per minggu. Pendapatan tersebut ia putar lagi untuk membeli bahan baku, membayar buruh, sisanya ia tabung untuk sekolah anak.</p>



		<h3 class="text-center purple"><b>Modal dan Kepercayaan</b></h3>

		<p class="opensans" style="padding-bottom:20px;">Selama ini Onih merasa sangat terbantu dengan pembiayaan di Amartha, dari tiga usaha yang dijalaninya selalu berasal dari modal pembiayaan di Amartha. “Modal saya ya cuman dari Amartha, memang sedari awal pinjem tujuan saya ya usaha, supaya bisa saya putar lagi, supaya gak perlu minta suami untuk bayar angsuran,” tutur Onih ketika ditaya apakah ada sumber modal lainnya. </p>

		<p class="opensans" style="padding-bottom:20px;">Di lingkungan Onih tinggal, masyarakat sekelilingnya adalah sama-sama masyarakat kecil yang tidak bisa untuk memberikan bantuan untuk usaha. Sementara alternatif pembiayaan lain hanya bank keliling (lintah darat) yang menagih setiap hari dan memberikan bunga besar. “Saya merasakan betul, Amartha telah membantu modal dan memberikan kepercayaan untuk saya terus berusaha,” tutup Onih.</p>


		<h3 class="text-center purple"><b>Memupuk Impian</b></h3>
		<p class="opensans" style="padding-bottom:20px;">Onih ingin sekali mengembangkan usahanya. Ia ingin bisa berbelanja kain perca dan karung goni langsung dari Tanah Abang. Tujuannya tidak lain untuk menurunkan biaya bahan baku. “Saya ingin menyetok kain perca dan punya gudang di samping rumah. Jadi saya gak perlu mondar-mandir belanja. Dengan begitu juga, siapapun orang yang mau ikutan nganyam bisa gabung. Selama ini saya kadang terpaksa gak nerima karena emang keabisan bahan. Selain itu juga, saya terpaksa ngerem karena kalau tetep dikerjain saya belum punya cukup modal untuk bayar upahnya,” tutur Onih. </p>

		<p class="opensans" style="padding-bottom:20px;">Onih sangat peduli dengan pendidikan anak-anaknya. Anak pertamanya berusia 23 tahun, sebelumnya bekerja sebagai OB di sekolah swasta. Sekarang ini anaknya diangkat sebagai tenaga pendidik tambahan di bidang seni karena keahliannya. Sementara anak keduanya masih duduk di bangku SMK.</p>

		<p class="opensans" style="padding-bottom:20px;">Terhadap anak pertama, ia sangat mendukung keinginannya untuk kuliah, dibuktikan dengan rajinnya Onih menabung di Amartha. Untuk anaknya yang masih SMK, ia tidak segan membayarkan kebutuhan ekstrakulikuler yang digemari anaknya, Paskibra. Ia bangga sekali ketika mengetahui tim Paskibra sekolah anaknya berhasil menjuarai perlombaan setingkat kabupaten.</p>

		<h3 class="text-center purple"><b>Menciptakan Peluang</b></h3>
		<p class="opensans" style="padding-bottom:20px;">Onih adalah sosok yang peduli dengan lingkungan sekitar. Ia selalu  berpikir untuk bisa mengembangkan usahanya dan mempekerjakan lebih banyak orang. Selama ini Onih telah mengajakserta saudaranya untuk menganyam keset. Dengan usahanya yang terus berkembang, ia ingin melihat lebih banyak lagi ibu yang tidak menganggur dan ikut menganyam bersama dirinya. Menurut Onih dengan menciptakan peluang, ia bisa mengajak orang lain lebih maju, seperti dirinya sekarang ini.</p>

		<?php }?>
		
	</div>
