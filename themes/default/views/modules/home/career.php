
<!-- HEADER -->
	<div id="headerImageCareer">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="110px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

			<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<!-- Hero Text -->
			<div class="startContent clearfix">
				<br/><br/>
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">We aim to raise the bar of<br> the financial services industry</h2>
				
				<?php } else {?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Kami bertekad untuk meningkatkan <br/>standar industri jasa keuangan</h2>
				
				<?php }?>

		    	<!-- <h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; "><a href="mailto:career@amartha.com" title="Drop us email" class="hoverglow">career@amartha.com</a></h2> -->

	    </div>
	    </div>
	</div>
	<!-- END OF HEADER -->

	<div class="container">
		<div class="about_WhoWeAre careerContent  text-center ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h1 class="opensans text-center purple">Working at Amartha</h1>
			<p class="opensans fs-14 text-center" style="padding-top: 20px; ">At Amartha, we work to create hope for future generations and aspire to create a meaningful life. As we deal with poverty, we are aware of the complexity of our task and acknowledge that some of the problems we find at the grassroots level are not easily solvable. Nevertheless, we are up for that long journey and strive to work for a more prosperous Indonesia.</p>
			<p class="opensans fs-14 text-center" style="padding-top: 20px;" >Our energy is excellence, sincerity, and passion. And we are looking for the very people who can give us that energy. Discover the impact you could make by joining us in providing access to financial services for micro and small businesses through a career in the following area: </p>
			
			<?php } else {?>
			<h1 class="opensans text-center purple">Bekerja di Amartha</h1>
			<p class="opensans fs-14 text-center" style="padding-top: 20px; ">Melalui Amartha, kami bekerja untuk mewujudkan harapan generasi mendatang, menciptakan hidup yang bermakna. Kami menyadari, berkarya untuk mengentaskan kemiskinan bukanlah hal mudah. Dan permasalahan pada akar rumput tidaklah gampang diselesaikan. Namun demikian, kami berkomitmen pada perjalanan panjang tersebut, melakukan yang terbaik demi Indonesia sejahtera.</p>
			<p class="opensans fs-14 text-center" style="padding-top: 20px;" >Energi kami adalah keunggulan, keikhlasan dan passion. Kami mengundang para individu terpilih untuk bergabung bersama kami dengan membawa energi tersebut. Ciptakanlah perubahan bersama tim kami dengan menyediakan akses keuangan pada usaha mikro dan UKM melalui karir pada bidang berikut: </p>
			
			<?php }?>

			<!-- <p class="opensans fs-14 text-center" style="padding-top: 20px;"> Want to join our team? Please drop your resume and letter of intent to career@amartha.com</p> -->
		</div>
	</div>
			
	<div class="container">
		<div class="row careerIcon">
			<div class="col-md-2 col-sm-4 col-xs-6">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/career/engineering.png">
				<h2>Engineering</h2>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/career/prodmen.png">
				<h2>Product Management</h2>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/career/ui-ux.png">
				<h2>Design &amp; UX</h2>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/career/analytic.png">
				<h2>Data and Analytics</h2>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/career/security.png">
				<h2>Security</h2>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/career/infrastructure.png">
				<h2>Infrastructure</h2>
			</div>
		</div>

		<div class="row careerIcon">

			<div class="col-md-2 col-sm-4 col-xs-6">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/career/marketing.png">
				<h2>Marketing</h2>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/career/legal.png">
				<h2>Legal &amp; public policy</h2>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/career/FO.png">
				<h2>Field Operations</h2>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/career/recruit.png">
				<h2>People and Recruiting</h2>
			</div>

			<div class="col-md-2 col-sm-4 col-xs-6">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/career/finance.png">
				<h2>Finance</h2>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/career/social.png">
				<h2>Social Impacts</h2>
			</div>
		</div>
	</div>
	
	<?php if ($this->session->userdata('site_lang') == 'english') { ?>
	<p class="opensans fs-18 text-center" style="font-weight:400;padding-top: 20px;padding-bottom:50px;"> Want to join our team? Please drop your resume and letter of intent to <b class="purple">career@amartha.com</b></p>
	
	<?php } else {?>
	<p class="opensans fs-18 text-center" style="font-weight:400;padding-top: 20px;padding-bottom:50px;"> Ingin bergabung dengan team kami? Silakan kirim CV dan surat lamaran ke <b class="purple">career@amartha.com</b></p>

	<?php }?>
