
<!-- HEADER -->
	<div id="headerImageBorrow">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="100px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

			<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein_to_borrow">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Borrower? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<!-- Hero Text -->
			<div class="container">

			<div class="startContent  clearfix">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">With the right approach, business loans can help <br> Indonesia’s micro and small businesses grow sustainably</h2>
		    	<?php
						if($this->session->userdata('logged_in_borrower')) {
							$navigation                = site_url('borrower');
							$navigation_text           = "Go to Dashboard";
							$borrower_logged_in_class  = "hidden";

						}else{
							$navigation                = site_url('register_to_borrow');
							$navigation_text           = "Sign Up as Borrower";
							$borrower_logged_in        = site_url('login_to_borrow');
							$borrower_logged_in_text   = "Login as Borrower";
						}
				?>
				<!--<a href="<?php echo $borrower_logged_in; ?>" class="button_main btn btn-default <?php echo $borrower_logged_in_class; ?>" type="submit"><?php echo $borrower_logged_in_text; ?></a>-->	
				<a href="<?php echo $navigation; ?>" class="button_main btn btn-default" type="submit"><?php echo $navigation_text; ?></a>

				<?php } else {?>
				<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Dengan pendekatan yang tepat, pembiayaan modal usaha dapat membantu usaha mikro dan kecil di Indonesia untuk terus tumbuh dan berkelanjutan</h2>
		    	<?php
						if($this->session->userdata('logged_in_borrower')) {
							$navigation                = site_url('borrower');
							$navigation_text           = "Pergi ke Dashboard";
							$borrower_logged_in_class  = "hidden";

						}else{
							$navigation                = site_url('register_to_borrow');
							$navigation_text           = "Daftar sebagai Peminjam";
							$borrower_logged_in        = site_url('login_to_borrow');
							$borrower_logged_in_text   = "Masuk sebagai Peminjam";
						}
				?>
				<!--<a href="<?php echo $borrower_logged_in; ?>" class="button_main btn btn-default <?php echo $borrower_logged_in_class; ?>" type="submit"><?php echo $borrower_logged_in_text; ?></a>-->
				<a href="<?php echo $navigation; ?>" class="button_main btn btn-default" type="submit"><?php echo $navigation_text; ?></a>
				<?php }?>
	    </div></div>
	    </div>
	</div>
	<!-- END OF HEADER -->



	<div class="container">
		<div class="about_WhoWeAre  text-center ">
			<!--<h1 class="opensans text-center purple">We currently serve West Java & Banten. We are expanding our business and aim to serve more regions across Indonesia. Please tell us your location and register here.</h1>-->
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<p class="opensans fs-14" style="padding-top: 20px; ">Whether you just need a small amount of loan to get your business through a though time or a more substantial amount loan to help it expand, Amartha can help. We have already helped many businesses, like yours, to fulfil their aspirations.</p>
			<?php } else {?>
			<p class="opensans fs-14" style="padding-top: 20px; ">Amartha dapat membantu Anda untuk pembiayaan dalam jumlah kecil guna memastikan usaha di masa sulit atau pembiayaan dalam jumlah lebih besar untuk memperluas usaha. Karena kami telah membantu berbagai jenis usaha, untuk memastikan kebutuhan Anda.</p>
			<?php }?>

		</div>


		<div class="about_WhoWeAre  text-center ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h1 class="opensans text-center purple">Polishing Micro Entrepreneurs into Credit Worthy Borrowers</h1>

			<p class="opensans fs-14" style="padding-top: 20px; ">We are passionate about working with micro-entrepreneurs who are discipline, honest, and own a vision. Therefore, we wish to always be at the frontline to help them in building their credit history.</p>

			<p class="opensans fs-14" style="padding-top: 20px; ">Our recruitment process, training process, recognition test, loan proposal process, and weekly group meeting are designed such that once our clients receive the loans, even in the smallest amount we offer, they continue to value that loan highly, use it responsibly, and respect their own commitment to repay it. As their credit history is being built, so is their character. This probably explains Amartha’s ability to maintain the repayment rate at more than 90% despite requiring no collaterals.</p>

			<?php } else {?>
			<h1 class="opensans text-center purple">Meningkatkan pengusaha mikro menjadi peminjam yang layak</h1>

			<p class="opensans fs-14" style="padding-top: 20px; ">Kami antusias bekerja dengan pengusaha mikro yang disiplin, jujur​​, dan memiliki visi. Oleh karena itu, kami mengambil langkah terdepan untuk membantu mereka dalam pengembangan riwayat pembiayaan.</p>

			<p class="opensans fs-14" style="padding-top: 20px; ">Proses pendaftaran peminjam, melalui: proses pelatihan, uji pengesahan, proses pengajuan pembiayaan, dan pertemuan kelompok per minggu, dirancang secara cermat dan terstruktur sehingga ketika anggota menerima pembiayaan, bahkan dalam nilai terkecil yang kami berikan, mereka dapat menghargai dan menggunakannya secara bertanggung jawab dan memegang komitmen untuk mebayarkannya. Dari proses tersebut tidak hanya riwayat pembiayaan yang dapat dibangun, tetapi juga karakter masyarakat. Hal tersebut menjadi alasan tingkat pembayaran dapat lebih dari 90% meski tanpa jaminan.</p>
			<?php }?>
		</div>

		<!--<div class="row tabBorrow">

			<a href="<?php echo site_url('borrow/under20mil'); ?>">
				<div class="col-md-6 pasive active"><h3>For loans below IDR 20,000,000</h3></div></a>
			<a href="<?php echo site_url('borrow/above20mil'); ?>"><div class="col-md-6 pasive"><h3>For loans between IDR 20,000,000 and IDR 200,000,000</h3></div></a>
		</div>-->

		<div class="about_WhoWeAre  text-justify ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h1 class="opensans text-center purple">Why Borrowers Choose Us</h1>

			<ul class="opensans fs-14" style="padding-top:20px;">
				<li class="opensans fs-14">Negotiable rate – based on the agreement between lenders and borrowers</li>
				<li class="opensans fs-14">Various combinations of amount and tenor – borrow between IDR 1,500,000 - IDR 20,000,000 and repay within 3 months to 2 years</li>
				<li calss="opensans fs-14">No penalty for early payoff </li>
				<li class="opensans fs-14">Collateral free, easy process, real client support, and clear rate </li>
				<li class="opensans fs-14">Credit history building – enable borrowers to start building their credit history and hence improve their credit worthiness</li>
			</ul>
			<?php } else {?>
			<h1 class="opensans text-center purple">Mengapa Peminjam Memilih Kami</h1>

			<ul class="opensans fs-14" style="padding-top:20px;">
				<li class="opensans fs-14">Bagi Hasil dapat dinegoisasikan – sesuai kesepakatan antara investor dan peminjam</li>
				<li class="opensans fs-14">Beragam pilihan angsuran dan waktu – peminjam dengan nilai pembiayaan Rp 1.500.000 – Rp 20.000.000 dapat membayar dalam kurun waktu 3 – 24 bulan</li>
				<li calss="opensans fs-14">Tidak menerapkan pinalti untuk pelunasan lebih cepat</li>
				<li class="opensans fs-14">Tanpa jaminan, kemudahan proses, dukungan mitra</li>
				<li class="opensans fs-14">Sejarah pengembangan pembiayaan – memungkinkan peminjam membangun laporan pembiayaan serta turut meningkatkan kelayakan pembiayaan.</li>
			</ul>
			<?php }?>
		</div>
	</div>
	<?php if ($this->session->userdata('site_lang') == 'english') { ?>

	<?php } else {?>
	<div id="ImageBorrowProcess">
		<div class="container">
			<div class="row borrowProcess">
				<h1 class="text-center" style="margin-bottom:30px;">4 cara sederhana untuk dapatkan pembiayaan</h1>
				<div class="col-md-3">
					<span>1</span>
					<img src="<?php echo $this->template->get_theme_path(); ?>/img/proses2.png">
					<p>Dapatkan kutipan personal Anda</p>

				</div>
				<div class="col-md-3">
					<span>2</span>
					<img src="<?php echo $this->template->get_theme_path(); ?>/img/proses1.png">
					<p>Lengkapi berkas aplikasi</p>

				</div>
				<div class="col-md-3">
					<span>3</span>
					<img src="<?php echo $this->template->get_theme_path(); ?>/img/proses3.png">
					<p>Staff kami akan menghubungi Anda</p>

				</div>
				<div class="col-md-3">
					<span>4</span>
					<img src="<?php echo $this->template->get_theme_path(); ?>/img/proses4.png">
					<p>Dana segera masuk ke rekening Anda </p>
				</div>
				<h2 class="text-center" style="margin-top:200px;">Setelah Anda melengkapi berkas aplikasi, staff kami akan segera menghubungi Anda</h2>
			</div>
		</div>
	</div><?php }?>
	
	<div class="container">
		<div class="about_WhoWeAre  text-center ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>

			<h1 class="opensans text-center purple">Compulsory Group Training</h1>

			<p p class="opensans fs-14" style="padding-top: 20px; ">We require the potential and continuing clients/borrowers to create a group comprising of 15-20 people (known as majelis) in which further smaller groups consisting of 5 people are formed. Before the potential clients are inaugurated and are allowed to make their loan proposals, they will receive a 3-day compulsory group training. The training communicates to them on Amartha's mission, procedures, schemes, terms & conditions. Besides emphasizing on the joint-responsibility principle, the training also aims to build the culture of trust, discipline, and participatory. At the end of the training, a recognition test is held to check their understanding on the information taught during the training.</p>
			<?php } else {?>
			<h1 class="opensans text-center purple">Pelatihan wajib kelompok</h1>

			<p p class="opensans fs-14" style="padding-top: 20px; ">Kami mewajibkan calon anggota yang berjumlah 15 – 20 orang untuk membentuk satu majelis, dengan masing-masing kelompok terdiri dari lima orang. Sebelum calon anggota disahkan dan mengajukan pembiayaannya, mereka menerima pelatihan selama tiga hari. Pelatihan tersebut mengenai berbagai hal, seperti misi Amartha, prosedur, skema, ketentuan, dan persyaratan. Di samping itu, kami menekankan prinsip tanggung jawab bersama antar anggota majelis. Pelatihan juga bertujuan untuk membangun budaya saling percaya, disiplin, dan partisipasi. Di akhir masa pelatihan, kami melaksanakan uji kelayakan untuk memastikan calon anggota memahami informasi yang telah disampaikan pada saat pelatihan.</p>
			<?php }?>
		</div>

		<div class="about_WhoWeAre  text-center ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>

			<h1 class="opensans text-center purple">Online Psychometric Survey</h1>

			<p class="opensans fs-14" style="padding-top: 20px; ">We visit every potential and continuing client at their homes for an in-depth interview to analyse their social and economic conditions and assess if their conditions correspond with our criteria. Loan disbursement is done within 7 (seven) working days after the loan proposal is approved.</p>

			<p class="opensans fs-14" style="padding-top: 20px; ">We have moved from paper survey to online survey through mobile apps because online survey allows a more time-efficient and cost-effective data collection and analysis. </p>
			<?php } else {?>
			<h1 class="opensans text-center purple">Survei Psikometri <i>Online</i></h1>

			<p class="opensans fs-14" style="padding-top: 20px; ">Kami mengunjungi semua calon anggota secara langsung, ke rumah masing-masing, untuk wawancara mendalam, sebagai bahan analisis kondisi sosial dan ekonomi. Dan memastikan bahwa kondisi mereka sesuai dengan persyaratan kami.</p>

			<p class="opensans fs-14" style="padding-top: 20px; ">Kami telah pindah dari survei kertas kepada survei online melalui aplikasi mobile dikarenakan survei online memungkinkan penghematan biaya dan waktu dalam pengumpulan data dan analisis.</p>
			<?php }?>
		</div>

		<div class="about_WhoWeAre  text-center ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>

			<h1 class="opensans text-center purple">Weekly Group Meeting </h1>

			<p class="opensans fs-14" style="padding-top: 20px; ">Weekly Group Meetings are where the financial transactions, such as loan disbursement, instalment, saving, and cash withdrawal, take place. All these transactions are recorded by every field officer using mobile apps. Integrating technology into our core activities can minimise fraud and allows real-time monitoring on individual loan performance, individual attendance, and exercise of joint-responsibility principle among members in a group.</p>

			<p class="opensans fs-14" style="padding-top: 20px; ">During weekly group meetings, clients also have the opportunity to discuss new loan applications; discuss their business and/or seek for business advice from our trained field officers; or discuss on any community issues. All of those activities are conducted with discipline and efficiency. Weekly Group Meetings are held in the morning so as to not disturb clients’ daily activities.</p>
			<?php } else {?>
			<h1 class="opensans text-center purple">Pertemuan Kelompok Mingguan</h1>

			<p class="opensans fs-14" style="padding-top: 20px; ">Pertemuan kelompok mingguan ialah jadwal rutin transaksi, seperti penyaluran pembiayaan, angsuran, tabungan, dan penarikan secara tunai. Selain itu anggota juga mendapat kesempatan untuk mendiskusikan pengajuan pembiayaan lanjutan, usaha, meminta saran dari field officer (petugas lapang) kami, atau bahkan mendiskusikan permasalah lain yang bergulir di komunitas. Semua kegiatan dilakukan dengan displin dan efisien. Pertemuan kelompok mingguan berlangsung hanya sekitar 20 – 30 menit sehingga tidak mengganggu aktivitas harian anggota. </p>

			<?php }?>
		</div>

		<div class="about_WhoWeAre  text-center ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<p class="opensans fs-14" style="padding-top: 20px; ">We offer two types of business loan. Business loans under IDR 20,000,000 are treated as group loans which means that while you are personally responsible for paying back the loan, the members of your group have agreed to the purpose of your loan and have pledged to be collectively responsible for any unpaid instalment. Meanwhile, we also offer larger amount of business loans ranging from IDR 20,000,000 up to IDR 200,000,000 which are provided directly to individual businesses and not treated as group loans. Our procedure for giving out loans is simple, transparent, and fair so that you can get the support you need. Find out how to apply below.</p>
			<?php } else {?>
			<p class="opensans fs-14" style="padding-top: 20px; ">Kami menawarkan dua jenis pembiayaan usaha, a) pembiayaan usaha ≤ Rp 20.000.000 sebagai pembiayaan kelompok, dengan demikian Anda secara pribadi wajib membayar kembali pembiayaan, serta sebagai bagian dari kelompok, Anda turut bertanggung jawab untuk membayar angsuran gagal bayar secara kolektif. Selain itu kami menawarkan pembiayaan Rp 20.000.000 – Rp 200.000.000 untuk usaha individual, artinya Anda tidak dikenakan aturan pembiayaan berbasis kelompok.  Prosedur pembiayaan kami sederhana, transparan, dan berimbang sehingga Anda dengan mudah memperoleh apa yang dibutuhkan. Cari tahu bagaimana mengajukan pembiayaan di bawah ini. </p>
			<?php }?>

		</div>

		<div class="about_WhoWeAre  text-center ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<p class="opensans fs-14" style="padding-top: 20px; ">We currently serve West Java &amp; Banten. We are expanding our business and aim to serve more regions across Indonesia. Please tell us your location and register here.</p>
			<?php } else {?>
			<p class="opensans fs-14" style="padding-top: 20px; ">Saat ini kami melayani banyak desa di pelosok Kabupaten Bogor dan Bandung. Kami memperluas bisnis  dengan tujuan untuk menjangkau  lebih banyak lagi masyarakat desa di seluruh pelosok Indonesia.</p>
			<?php }?>

		</div>

		<!--
		<div class="container startContent" style="margin-bottom: 50px; padding-top:10px">
			<button onclick="location.href='<?php echo site_url('formborrow/under20mil'); ?>';" class="btn btn-default" type="submit">For loans below IDR 20,000,000</button>
		</div>-->





		<!--<div class="row tabBorrow">

			<a href="<?php echo site_url('borrow/under20mil'); ?>">
				<div class="col-md-6 pasive active"><h3>For loans below IDR 20,000,000</h3></div></a>
			<a href="<?php echo site_url('borrow/above20mil'); ?>"><div class="col-md-6 pasive"><h3>For loans between IDR 20,000,000 and IDR 200,000,000</h3></div></a>
		</div>-->

		<!--
		<div class="about_WhoWeAre  text-justify ">
			<h1 class="opensans text-left purple">Requirements</h1>
			<ul class="opensans fs-14"style="padding-top:20px;list-style-type: circle;">
				<li class="opensans fs-14">Female</li>
				<li class="opensans fs-14">Forming a group of 15-20 women</li>
				<li class="opensans fs-14">Holding Indonesian citizenship</li>
				<li class="opensans fs-14">Max. 60 years old</li>
				<li class="opensans fs-14">No collateral required but group members must provide peer support</li>
			</ul>
		</div>-->

		<!--

		<div class="container startContent" style="margin-bottom: 50px; padding-top:10px;">
			<button onclick="location.href='<?php echo site_url('formborrow/above20mil'); ?>';" class="btn btn-default" type="submit">For loans between IDR 20,000,000 and IDR 200,000,000</button>
		</div-->
		<!--
		<div class="about_WhoWeAre  text-justify ">
			<h1 class="opensans text-left purple">Requirements</h1>
			<ul class="fs-14" style="padding-top:20px;list-style-type: circle;">
				<li class="opensans fs-14">At least 21 years old </li>
				<li class="opensans fs-14">Holding Indonesian citizenship</li>
				<li class="opensans fs-14">Holding an Indonesian bank account</li>
				<li class="opensans fs-14">Get personalised quote</li>
				<li class="opensans fs-14">Complete full application form</li><br>
				<h2 class="fs-14" style="margin-top:20px;">Once you complete the application form, our staff will contact you.</h2>
			</ul>
		</div>-->
	</div>
</div>




	

	<!-- <div class="container FaqBorrow">
		<h1>BASIC FAQ</h1>
		<div class="col-md-3"><h2>Borrower</h2></div>
		<div class="col-md-9">
			<ul>
				<li><h3>Informasi atau dokumen apa sajakah yang dibutuhkan untuk pengajuan pinjaman?</h3></li>
				<li><h3>Loren ipsum</h3></li>
				<li><h3>Loren ipsum dolor si amet</h3></li>
				<li><h3>Loren Ipsum dolor</h3></li>
			</ul>
		</div>
	</div> -->

<section class="">
	<div class="container startContent" style="margin-bottom: 50px;">
		<!-- <h2>Join millions of other business who signed on Amartha</h2> -->
		<a href="<?php echo $navigation; ?>"><button class="btn btn-default"><?php echo $navigation_text; ?></button></a>
	</div>
</section>

	<!-- <a href="<?php echo $investor_navigation; ?>" class="button_main btn btn-default" type="submit">Sign Up for Borrower </a> -->



