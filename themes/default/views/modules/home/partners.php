
<!-- HEADER -->
	<div id="headerImagePartners">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="110px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

			<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<!-- Hero Text -->
			<div class="startContent clearfix">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2>Expanding Outreach, Delivering Happiness</h2>
		    	<h3>To empower low-income families in rural areas with affordable financial services, enabling them to pursue life for greater purposes.</h3>

				<?php } else {?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Menjangkau lebih luas, mengantarkan lebih banyak kebahagiaan</h2>
		    	<h3 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Memberdayakan keluarga prasejahtera di pelosok desa dengan layanan keuangan yang terjangkau, <br/>memungkinkan mereka untuk meraih kehidupan dengan tujuan yang lebih besar</h3>

				<?php }?>

	    </div>
	    </div>
	</div>
	<!-- END OF HEADER -->


	<div class="container ">
		<div class="fs-14 about_WhoWeAre  text-center ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h1 class="opensans text-center purple">PARTNERS</h1>
			<p>Amartha partners with cooperatives, BPR, Kelompok Usaha Tani, or any organisations that interact with micro and small businesses and care about their development.</p>
			<p>If your organisation is interested in partnering with Amartha, please send email to <a href="mailto:partner@amartha.com.?Subject=Partner%20request" target="_top"><b>partner@amartha.com</b></a>. We will contact you soon to schedule a time to speak with our partnership team.</p>
			
			<?php } else {?>
			<h1 class="opensans text-center purple">REKAN KERJA</h1>
			<p>Amartha bermitra dengan koperasi, BPR, Kelompok Usaha Tani dan organisasi lainnya yang memiliki kepedulian terhadap pengembangan usaha mikro dan UKM.</p>
			<p>Jika organisasi Anda tertarik untuk bermitra dengan Amartha, silakan mengirim email ke  <a href="mailto:partner@amartha.com.?Subject=Partner%20request" target="_top"><b>partner@amartha.com</b></a>. Team kemitraan kami akan dengan senang hati menghubungi Anda segera.</p>
			<?php }?>

		</div>
	</div>

	<div class="container partnerContent hidden">
		<div class="col-md-4 fs-14">
			<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_partner.png">
			<h2>Partner A</h2>
			<p>	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus eros vitae tellus bibendum, sed finibus mauris fringilla. Mauris id nibh non velit bibendum ornare.</p>
		</div>
		<div class="col-md-4 fs-14">
			<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_partner.png">
			<h2>Partner B</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus eros vitae tellus bibendum, sed finibus mauris fringilla. Mauris id nibh non velit bibendum ornare.</p>
		</div>
		<div class="col-md-4 fs-14">
			<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_partner.png">
			<h2>Partner C</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus eros vitae tellus bibendum, sed finibus mauris fringilla. Mauris id nibh non velit bibendum ornare.</p>
		</div>

	</div>

	<div class="container partnerContent hidden">
		<div class="col-md-4 fs-14">
			<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_partner.png">
			<h2>Partner D</h2>
			<p>	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus eros vitae tellus bibendum, sed finibus mauris fringilla. Mauris id nibh non velit bibendum ornare.</p>
		</div>
		<div class="col-md-4 fs-14">
			<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_partner.png">
			<h2>Partner E</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus eros vitae tellus bibendum, sed finibus mauris fringilla. Mauris id nibh non velit bibendum ornare.</p>
		</div>
		<div class="col-md-4 fs-14">
			<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_partner.png">
			<h2>Partner F</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec finibus eros vitae tellus bibendum, sed finibus mauris fringilla. Mauris id nibh non velit bibendum ornare.</p>
		</div>

	</div>


