    <div id="headerImageStoryVote" >
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent singleHead  clearfix">

				<div class="container">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    		<h1 class="text-center">Let’s Vote Smart!</h1>


					<?php } else {?>
		    		<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Ayo Memilih Cerdas!</h2>


					<?php }?>

	    	</div>
	    </div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="about_WhoWeAre  text-justify ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<p style="padding-top: 20px; ">Since the fall of the New Order regime which ruled Indonesia for more than 30 years, it seems that Indonesians have had some hope on the democratic process. But, of course, in embracing the democratic spirit, the people, the educated ones particularly, have a big homework, so that we would not get trapped, again, in the politics of money. The 2014 General Election thus served as a good beginning for our nation to learn about democracy.</p>

					<p style="padding-top: 20px; ">In 2014, democracy in Indonesia has been developing very rapidly in line with the vast growth of both print and electronic media that had been heavily monitored by the previous authorities. Politics has become an everyday conversation; people have become active politically; and political efficacy of the urban communities has also increased. There is hope that politics can be a way to improve the life of the society.</p>

					<p style="padding-top: 20px; ">We are aware of political perspective and political activity being influenced by demographic factors such as education level, income level, and many more. For the urban communities it may be easier to absorb, filter, and process all the information that determine political activity. But what about the people at the grassroots? Since, as mentioned earlier, it is a big homework we need to collectively answer, therefore through "Let’s Vote Smart!" we attempted to bring the democratic process closer to the eyes of the underprivileged, thus increasing their understanding and awareness.</p>

					<?php } else {?>

					<p style="padding-top: 20px; ">Sejak runtuhnya masa orde baru yang memerintah lebih dari 30 tahun, rasanya Indonesia memiliki harapan dari proses demokrasi. Namun, tentu saja untuk menyambut semangat tersebut, kelompok terdidik (terutama) memiliki PR besar, agar lagi-lagi kita tidak terjebak pada politik uang. Pemilu 2014 ialah awal baik bagi banga kita untuk belajar demokrasi.</p>

					<p style="padding-top: 20px; ">Di 2014, demokrasi Indonesia berkembang amat pesat sejalan dengan dibukanya kanal-kanal seperti media elektronik atau cetak, sebelumnya berbicara soal politik kita hanya mendapatkannya dari TV Nasional yang muatannya disetir habis oleh penguasa. Politik telah menjadi perbincangan sehari-hari, masyarakat mulai aktif secara politik, efikasi politik kelompok urban pun meningkat. Ada harapan bahwa politik adalah cara untuk menyejahterakan masyarakat.</p>

					<p style="padding-top: 20px; ">Kita tahu, cara pandang politik hingga aktivitas politik sangat dipengaruhi oleh demografi, tingkat pendidikan, tingkat ekonomi, dan tentu masih banyak lagi. Bagi kaum urban tentu lebih mudah menyerap, menyaring, dan memproses segala informasi yang menentukan aktivitas politik. Namun bagaimana dengan masyarakat di akar rumput? Seperti yang telah disinggung di awal, PR besar tersebut perlu kita jawab bersama melalui “Ayo, Memilih Cerdas!” sebuah upaya untuk membahasakan ramainya proses demokrasi ke masyarakat prasejahtera.</p>

					<?php }?>

				
			</div>


			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

	<div id="gallery">
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/vote_1.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/vote_2.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/vote_3.jpg" />

	</div>


	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-9">
				<div class="about_WhoWeAre  text-justify ">

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<p style="padding-top: 20px; ">This programme was targeted at people at the bottom of the economic ladder who lived in the remote rural areas in West Java. The aim was to increase public awareness and understanding about the general legislative election held on April 9, 2014. This programme also aimed at providing the necessary information so that people could exercise their rights wisely and responsibly.</p>

				<p style="padding-top: 20px; ">“Let’s Vote Smart!” was conducted over the period of three weeks during the legislative campaign. About 4,255 families or more than 9,000 voters in 50 remote villages in Bogor District, Lebak, Banten became the target of this programme. This programme was done by Amartha Microfinance in collaboration with the Global Shapers Community.</p>


				<p style="padding-top: 20px; ">This programme delivered general information about the democratic system in Indonesia; the importance of the general elections; the characteristics of leaders as representatives of the people; and the role of the House of Representatives. All information was presented in a manner that was simple and easy to understand by the attendees who were Amartha’s clients.</p>

				<p style="padding-top: 20px; ">At the end of the programme, it was emphasised that all of us were smart voters who must make wise choices and that the general elections would determine the fate of the nation for the next five years.</p>

				<p style="padding-top: 20px; ">Democracy is certainly not an easy and short process. Learning from other nations, democracy is a challenging process and its consolidation takes a long time. Through "Let’s Vote Smart!" we hope that the spirit and enthusiasm of welcoming democracy can be transmitted to more people, especially those who have limited access to information. </p>

				<p style="padding-top: 20px; ">We also hope that "Let’s Vote Smart!" would positively impact the general presidential election that is going to take place on July 9, so that those who vote really are being responsible for their choices and are not easily trapped by money politics.</p>

				<?php } else {?>

				<p style="padding-top: 20px; "> Program ini ditargetkan untuk masyarakat di tangga ekonomi terbawah yang berada di pelosok pedesaan Jawa Barat. Tujuannya adalah untuk meningkatkan kesadaran dan pengetahuan masyarakat tentang pemilihan umum yang jatuh pada 9 April 2014. Selain itu untuk memberikan informasi yang dibutuhkan supaya masyarakat dapat menggunakan haknya secara bijaksana dan bertanggung jawab. </p>

				<p style="padding-top: 20px; ">Ayo, Memilih Cerdas! Dilaksanakan selama tiga minggu selama masa kampanye legislatif. 4255 keluarga atau lebih dari 9.000 calon pemilih di sekitar 50 desa terpencil Kabupaten Bogor, Lebak, dan Banten menjadi target program. Program dilakukan Amartha bekerja sama dengan Global Shappers Community.</p>


				<p style="padding-top: 20px; ">Program menyampaikan informasi umum mengenai sistem demokrasi yang berjalan di Indonesia, pentingnya pemilu, hingga karakteristik pemimpin sebagai wakil rakyat serta peran yang dijalankan olehnya, DPR. Semua informasi disampaikan dengan cara sederhana agar mudah dipahami oleh para ibu yang merupakan anggota Amartha.</p>

				<p style="padding-top: 20px; ">Di akhir program, ditekankan bahwa kita semua sebagai pemilih cerdas yang sudah seharusnya bijak dalam menentukan pilihan. Para ibu diajak berdiskusi bahwa pemilu hanya sebuah awal dari nasib bangsa selama lima tahun ke depan.</p>

				<p style="padding-top: 20px; ">Demokrasi tentu bukan proses yang mudah dan singkat, belajar dari bangsa lain, demokrasi adalah proses yang sukar dan memakan waktu yang panjang. Akan tetapi melalui “Ayo, Memilih Cerdas!” kami berharap semangat dan antuasisme menyabut demokrasi dapat dialirkan kepada lebih banyak orang, terutama pada mereka yang untuk mendapatkan akses informasi saja sulit.</p>

				<p style="padding-top: 20px; ">Kami berharap, “Ayo, Memilih Cerdas!” membawa dampak bagi pemilihan umum yang berlangsung di 9 Juli nanti bahwa mereka yang memilih adalah bertanggung jawab dengan pilihannya dan tidak mudah terjebak dengan politik uang.</p>

				<?php }?>


				
			</div>
			</div>
		</div>
	</div>







		<script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.touchSwipe.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.imagesloaded.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.scrollTo-1.4.3.1-min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/spin.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/portfolio.js" type="text/javascript"></script>

    <script type="text/javascript">
    $(document).ready(function() {
            var p = $("#gallery").portfolio();
            var q = $("#gallery2").portfolio();
            p.init();

    });
    </script>
