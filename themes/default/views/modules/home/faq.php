
<!-- HEADER -->
	<div id="headerImageAbout">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="110px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

			<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<!-- Hero Text -->
			<div class="startContent clearfix">
		    	<h2>Expanding Outreach, Delivering Happiness</h2>
		    	<h3>To empower low-income families in rural areas with affordable financial services, enabling them to pursue life for greater purposes.</h3>

	    </div>
	    </div>
	</div>
	<!-- END OF HEADER -->


	<div class="container faqContent">
		<div class="row">
			<div class="col-md-3 navFaq">
				<h1>CATEGORY</h1>
				<a href="javascript:void(0)" onClick="document.getElementById('faqborrower').scrollIntoView();"><h3>Borrower</h3></a>
				<a href="javascript:void(0)" onClick="document.getElementById('faqinvestor').scrollIntoView();"><h3>Investor</h3></a>

			</div>
			<div class="col-md-8 faqDetail">
				<h1 id="faqborrower">FAQ BORROWER</h1>
				<h3>APLIKASI</h3>
				<ul>
					<li>Fusce gravida ipsum risus, eget malesuada metus malesuada quis. Proin auctor bibendum nisi, at elementum quam.</li>
					<li>Nam vitae quam consectetur, imperdiet neque at, consequat orci. Nullam congue hendrerit porttitor.</li>
					<li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam vel semper metus, quis tempus velit.</li>
					<li>Nulla eu purus erat. Vivamus in varius est. Mauris accumsan molestie condimentum.</li>
					<li>Nulla vitae dapibus felis, laoreet pharetra metus. Praesent tortor nulla, sollicitudin a hendrerit in, luctus mollis sem.</li>

				</ul>


				<h3>Payment</h3>
				<ul>
					<li>Fusce gravida ipsum risus, eget malesuada metus malesuada quis. Proin auctor bibendum nisi, at elementum quam.</li>
					<li>Nam vitae quam consectetur, imperdiet neque at, consequat orci. Nullam congue hendrerit porttitor.</li>
					<li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam vel semper metus, quis tempus velit.</li>
					<li>Nulla eu purus erat. Vivamus in varius est. Mauris accumsan molestie condimentum.</li>
					<li>Nulla vitae dapibus felis, laoreet pharetra metus. Praesent tortor nulla, sollicitudin a hendrerit in, luctus mollis sem.</li>
				</ul>


				<h1 id="faqinvestor">FAQ INVESTOR</h1>
				<h3>APLIKASI</h3>
				<ul>
					<li>Fusce gravida ipsum risus, eget malesuada metus malesuada quis. Proin auctor bibendum nisi, at elementum quam.</li>
					<li>Nam vitae quam consectetur, imperdiet neque at, consequat orci. Nullam congue hendrerit porttitor.</li>
					<li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam vel semper metus, quis tempus velit.</li>
					<li>Nulla eu purus erat. Vivamus in varius est. Mauris accumsan molestie condimentum.</li>
					<li>Nulla vitae dapibus felis, laoreet pharetra metus. Praesent tortor nulla, sollicitudin a hendrerit in, luctus mollis sem.</li>

				</ul>


				<h3>Payment</h3>
				<ul>
					<li>Fusce gravida ipsum risus, eget malesuada metus malesuada quis. Proin auctor bibendum nisi, at elementum quam.</li>
					<li>Nam vitae quam consectetur, imperdiet neque at, consequat orci. Nullam congue hendrerit porttitor.</li>
					<li>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam vel semper metus, quis tempus velit.</li>
					<li>Nulla eu purus erat. Vivamus in varius est. Mauris accumsan molestie condimentum.</li>
					<li>Nulla vitae dapibus felis, laoreet pharetra metus. Praesent tortor nulla, sollicitudin a hendrerit in, luctus mollis sem.</li>
				</ul>

			</div>
		</div>
	</div>
