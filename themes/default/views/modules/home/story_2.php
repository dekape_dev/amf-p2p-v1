    <div id="headerImageStory" style="background: url(<?php echo $this->template->get_theme_path(); ?>/img/bg_homepage.jpg) no-repeat center bottom fixed; background-size:cover;">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent clearfix">

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2>Amartha help business grow faster. We know because they tell us everyday</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">SIGN UP FOR AMARTHA</a>
				
				<?php } else {?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Amartha membantu bisnis tumbuh lebih cepat <br/>Kami tahu karena mereka menceritakannya setiap hari</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">Mulai Investasi Sekarang</a>
				
				<?php } ?>

		    	<h3>&nbsp;</h3>
		    	<!--<button class="btn btn-default" type="submit">SIGN UP FOR AMARTHA</button>-->
	    	</div>
		</div>
	</div>
	<div class="container text-center">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h2 class="text-center purple"><b>Yanih: Samak Maker, Pejuang Keluarga</b></h2>
				<p class="opensans" style="padding-bottom:20px;">"I have been weaving samak (pandan woven mat) since 1991. Initially, my parents asked me to help purchasing pandan. Then, I decided to practice myself, learnt how to weave, how to make straight lines ,” told Yanih on how she started her samak business.</p>

				<p class="opensans" style="padding-bottom:20px;">Yanih has already joined Amartha for more than five years. Throughout her membership she feels that she has enjoyed an easy access to working capital for her business. Through weaving samak, she can supplement her husband’s income and add to family income.</p>

				<h3 class="text-center purple"><b>Together with Amartha</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Yanih quickly registered herself when she first heard about Amartha. She hoped to get soft financing to expedite her business. "An Amartha’s field officer came to survey my house and business. Oh, he asked all kinds of questions, so many, until I felt bored to answer! Then I showed him the unfinished samak that I was still weaving. The next week, he came again and now with the working capital I need for making samak. At the beginning, I did feel that Amartha’s first procedure was complicated, but after that everything runs smoothly. The field officers are friendly too", expressed Yanih when asked on how she thinks about Amartha.</p>

				<p class="opensans" stule="padding-bottom:20px;">Before being introduced to Amartha, she said that she borrowed working capital from her neighbour. At that time, in 2012, the price of raw material (pandan) was still cheap, which was IDR 2,500 per bundle. She needs eight to ten bundles of pandan to make one samak. Now, as the price has increased to IDR 6,000 per bundle, she automatically needs a greater working capital and her neighbour is no longer able to lend her that amount of money. Therefore, the financing from Amartha is a great help to her in purchasing pandan whose price has gone up.</p>

				<p class="opensans" stule="padding-bottom:20px;">The first financing obtained by Yanih for working capital was amounting to IDR 500,000. The second one, amounting to IDR 2,000,000, and further financings were still being utilised for working capital purposes. She is now at the fifth financing amounting to IDR 4,000,000 which is partly used to support the business and partly used to pay for her children’s education expenses. "Now, Praise to Allah, I can add on my husband's income, to cover our living cost, our children’s education, electricity, and house renovation. Amartha has given me the way to further grow my business. Amartha has helped my business running smoothly," added Yanih.</p>

				<h3 class="text-center purple"><b>Knows No Tired</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Yanih is a person who knows no tired. She spends approximately 13 hours a day to weave samak. Usually, she starts to weave from 10am, after sending all her children to school, until 5pm. She then resumes weaving starting at 7pm to 10pm or 11pm while chatting with her husband. She does this routine every day. To make one samak, it takes her four to five days.</p>

				<p class="opensans" style="padding-bottom:20px;">The revenue generated for every ten days ranges at around IDR 300,000 from selling two samaks. By taking into account the cost of capital of eight to ten bundles of pandan, the profit earned by Yanih is IDR 180,000 or IDR 18,000 on a daily average. Her income is indeed not a great amount of money, but she can cover her children’s transportation cost to go to school every day. Yanih also states that she has no longer borrowed money from her neighbours. After getting financing from Amartha, she does not use it entirely, but saves it partly so that her cash flow can better support the working capital need.</p>

				<p class="opensans" style="padding-bottom:20px;">Yanih had faced hard times. She fell from a running motorcycle until she could barely walk. Consequently, Yanih could not purchase pandan from the market which is located far away in Sasakan area. However, that condition did not then make her sit all day with her hands on her lap. She chose to spend her time beading scarves. But, to bead is not an income-earning activity she prefers as she earns so little from it, that is, only IDR 2,000.</p>

				<h3 class="text-center purple"><b>Fighting for Family</b></h3>
				<p class="opensans" style="padding-bottom:20px;">With a husband who works as a construction labourer, Yanih feels the need to help her family financial condition. She is not being calculating as to who earns the income, how much the income is, etc. According to her, as a married couple, they must share the good as well as hard times, including financial aspect. There were times when her husband was unemployed because of the nature of his work as a construction labourer did not always guarantee him a project. Yet, from the income earned through weaving samak Yanih says, “Praise to Allah, the income I earn from making and selling samak can help our household income at times when my husband has no project. Also, we can continue to meet our children’s daily transportation need to get them to school.”</p>

				<p class="opensans" style="padding-bottom:20px;">Two of her four children are still in school. Her third child is still pursuing her high school education level 1, while the youngest is still in primary school education. Her two older daughters are married and have lived with their husbands in Jakarta. However, although already married, their financial conditions still need support. Shown during time when her eldest daughter was giving birth to her first grandchildren, Yanih still had to help covering the expenses.</p>

				<p class="opensans" style="padding-bottom:20px;">Yanih says that her dream is simple. She hopes that by joining Amartha she can increase her family’s economic level and that she will no longer face any difficulties in obtaining working capital. Also, she hopes that financing from Amartha can help her ensuring her children to graduate from high school and to become independent.</p>

				<?php } else {?>

				<h2 class="text-center purple"><b>Yanih: Pembuat Samak, Pejuang Keluarga</b></h2>
				<p class="opensans" style="padding-bottom:20px;">“Saya mah mulai nganyam samak (tikar pandan) dari 1991. Awalnya disuruh orang tua belanja pandan. Lanjutnya saya belajar sendiri, gimana nganyam, gimana biar bikin garisnya lurus kalau pakai sipuhan pandan,” cerita Yanih bagaimana dirinya memulai usaha samak.</p>

				<p class="opensans" style="padding-bottom:20px;">Yanih sudah bergabung dengan Amartha lebih dari lima tahun. Selama bergabung ia merasakan kemudahan modal untuk usahanya. Melalui menganyam samak ia dapat menambah penghasilan suami dan menambah pemasukan keluarga.</p>

				<h3 class="text-center purple"><b>Bersama Amartha </b></h3>

				<p class="opensans" style="padding-bottom:20px;">Yanih langsung mendaftarkan dirinya ketika pertama kali mengenal Amartha. Ia berpikir dengan mendapat kemudahan modal usahanya bisa lancar. “Petugas Amartha datang untuk men-survey rumah saya dan usaha saya. Ditanya macem-macem, banyak, sampe bosen jawabnya. Saya tunjukin aja samak yang masih saya anyam. Minggu depannya petugas datang lagi ngasih saya bantuan modal usaha untuk bikin samak. Awalnya doang sih Amartha ribet, tapi selanjutnya mah enak-enak aja. Petugas juga ramah-ramah,” tutur Yanih ketika ditanya bagaimana menurutnya mengenai Amartha. </p>

				<p class="opensans" stule="padding-bottom:20px;">Sebelum mengenalkan Amartha ia mengaku meminjam modal usaha dari tetangga. Saat itu, 2012, harga pandan (bahan baku samak) masih murah, Rp 2.500/ikat. Ia butuh delapan hingga sepuluh untuk membuat satu samak. Kini dengan harga pandan seikatnya Rp 6.000, otomastis modalnya lebih besar, tetangganya tidak lagi bisa meminjamkan dirinya untuk modal. Pembiayaan dari Amartha sangat membantu untuk membeli harga pandan yang sudah naik. </p>

				<p class="opensans" stule="padding-bottom:20px;">Pembiayaan pertama diperoleh Yanih sebesar Rp 500.000 untuk modal pandan. Pembiayaan kedua Rp 2.000.000 masih untuk menambah modal pandan. Pembiayaan selanjutnya masih ia gunakan untuk menambah modal pandan. Kini pembiayaanya masuk putaran kelima sebesar Rp 4.000.000 yang ia gunakan untuk pandan dan menambah biaya pendidikan anak. “Sekarang, Alhamdulillah saya bisa nambah-nambah penghasilan suami, buat nambah-nambah biaya hidup, bayar sekolah anak, bayar listrik, renovasi rumah. Amartha ngasih saya jalan untuk usaha lebih. Amartha sudah bikin usaha saya lancar,” imbuh Yanih.  </p>

				<h3 class="text-center purple"><b>Tak Kenal Lelah </b></h3>

				<p class="opensans" style="padding-bottom:20px;">Yanih adalah sosok yang tak mengenal kata lelah. Ia terbiasa menghabiskan waktu lebih kurang tiga belas jam seharinya untuk menganyam samak. Biasanya ia mulai menganyam dari pukul 10:00, selepas menunggu anak di sekolah, hingga pukul 17:00. Dilanjutkan di pukul 19:00 hingga 22:00 atau 23:00 sambil berbincang-bincang dengan suami. Di pukul 03:00 dini hari ia bangun dan mulai lagi menganyam hingga pagi, tanpa tidur lagi. Hal tersebut ia lakukan setiap harinya. Satu samak bisa menghabiskan empat hingga lima hari.</p>

				<p class="opensans" style="padding-bottom:20px;">Omset menganyam samak setiap sepuluh harinya berkisar Rp 300.000 (dari hasil menjual dua samak). Dikurangi modal delapan hingga sepuluh ikat pandan, keuntungan yang diperoleh Yanih adalah Rp 180.000 atau rata-rata harian Rp 18.000. Penghasilannya memang tidak besar, tetapi ia bisa memberikan ongkos sekolah kedua anaknya setiap hari. Yanih juga mengaku dirinya tidak pernah lagi berhutang dari tetangga. Dari pembiayaan Amartha, ia tidak langsung menghabiskan melainkan ia simpan sehingga ketika butuh modal bisa digunakan. </p>

				<p class="opensans" style="padding-bottom:20px;">Yanih pernah mengaIami masa sulit, ia terjatuh dari motor hingga kepayahan berjalan. Hal tersebut menyebabkan Yanih tidak bisa belanja pandan yang letaknya jauh di daerah Sasakan.  Di fase itu, ia tidak lantas berpangku tangan. Ia memilih mengerjakan manih-manik (<i>mute</i>) milik orang lain. Namun usaha <i>mute</i> bukanlah yang ia pilih karena pemasukannya sangat sedikit, sehari ia hanya bisa mendapatkan Rp 2.000 dari <i>mute</i>. </p>

				<h3 class="text-center purple"><b>Berjuang untuk Keluarga </b></h3>
				<p class="opensans" style="padding-bottom:20px;">Dengan suami yang bekerja sebagai buruh bangun, tentu Yanih merasa perlu untuk membantu keuangan keluarga. Ia tidak menghitung-hitung pengeluarannya berasal dari suami atau dirinya, menurut Yanih ya namanya suami-isteri harus sependapat, susah senang ditangggung bersama, termasuk keuangan. Ada fase dimana suaminya menanggur karena buruh bangunan memang tidak selamanya mendapatkan proyek. Dari hasil samak Yani menceritakan, “Alhamdulillah dah dari hasil samak bisa bantu-bantu bapak, kalu lagi gak ada proyekan. Jadi anak juga sehari-harinya bisa dah ketemu ongkos sekolah.”  </p>

				<p class="opensans" style="padding-bottom:20px;">Dua dari empat anaknya masih sekolah. Anak ketiganya masih mengenyam pendidikan di tingkat 1 SMA, sementara si bungsu masih di sekolah dasar. Dua anak perempuannya sudah menikah dan ikut suami ke Jakarta. Meski sudah menikah, kehidupan perekonomiannya masih juga perlu dibantu. Terbukti saat anak pertamanya melahirkan, ia masih perlu turun tangan menyambung dana persalinan.</p>

				<p class="opensans" style="padding-bottom:20px;">Impiannya sederhana, ia berharap dengan bergabung bersama ekonomi keluarganya bisa naik, tidak mengalami kesulitan modal. Ia juga berharap pembiayaan Amartha dapat membantunya mengantar anak ketiganya hingga lulus SMA dan menjadi mandiri.</p>

				<?php }?>
	</div>
