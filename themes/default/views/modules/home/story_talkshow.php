    <div id="headerImageStoryTalkshow" >
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent singleHead clearfix">

				<div class="container">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="text-center">Inspirational Talk Show</h2>

					<?php } else {?>
					<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Talkshow Inspiratif</h2>

					<?php }?>

			    	
	    	</div>
	    </div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="about_WhoWeAre  text-justify ">

					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<p style="padding-top: 20px; ">Communication is like a basketball game. To convey a message, we must be able to measure several things: the distance between where we stand and the basket ring; the material used to make up the ring which determines how the ball bounces; or even noise that distracts a message. Inspirational Talk Show is our new way of communicating with our clients.</p>

					<p style="padding-top: 20px; ">Starting in 2015, Inspirational Talk Show has routinely been carried out by Amartha Microfinance (Amartha). Inspirational Talk Show is a programme designed to encourage the entrepreneurial spirit and independence of our clients. The interesting aspect from Inspirational Talk Show is that its speakers are derived from the clients themselves. Why? Because that way, the messages are being delivered more intimately and are easier to digest.</p>

					<p style="padding-top: 20px; ">The purpose of the talk show is to act as a platform for experience and knowledge sharing among Amartha’s clients. The talk show is designed in a way such that interactive discussions with the speakers (owing and running a business for two years or more) are created.</p>

					<?php } else {?>
					<p style="padding-top: 20px; ">Komunikasi ibarat permainan basket, untuk menyampaikan sebuah pesan, kita harus mampu mengukur beberapa hal: kedekatan jarak lempar ke ring, material ring yang mengakibatkan gaya pantul, atau bahkan derau (noise) yang mendistraksi sebuah pesan. Talkshow Inspiratif ialah cara baru kami dalam berkomunikasi dengan anggota.</p>

					<p style="padding-top: 20px; ">Bermula di 2015, Talkshow Inspiratif secara rutin dilaksanakan oleh Amartha Microfinance (Amartha). Talkshow Inspiratif merupakan program untuk mendorong semangat kewirausahaan dan kemandirian anggota kami. Hal yang menarik dari Talkshow Inspiratif adalah para pembircara berasal dari anggota itu sendiri. Mengapa? karena penyampaian pesan menjadi lebih dekat serta mudah untuk dicerna.</p>

					<p style="padding-top: 20px; ">Tujuan dari Talkshow Inspiratif ialah sebagai wadah berbagi pengalaman dan pengetahuan antara anggota Amartha. Program berbentuk diskusi interaktif dengan pembicara langsung dari anggota Amartha yang sudah memiliki usaha dan berjalan selama dua tahun atau lebih.</p>

					<?php }?>

					
			</div>


			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

	<div id="gallery">
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/talkshow_1.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/talkshow_2.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/talkshow_3.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/talkshow_4.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/talkshow_5.jpg" />
	</div>


	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-9">
				<div class="about_WhoWeAre  text-justify ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<p style="padding-top: 20px; ">The topics raised during the talk show are tailored to the business opportunities and the types of work majorly found in the surrounding areas. In 2015, Amartha has been running Inspirational Talk Show in three different districts, Bojong Gede, Jasinga, and Tenjo in Bogor, West Java. </p>

					<p style="padding-top: 20px; ">In Bojong Gede District, the topic raised was about cilok business with a cilok business owner, Munjiah, as the speaker. Munjiah shared her experience of running the business with her husband, recruiting workers to help their business, and growing the business until they are able to acquire assets such as paddy rice field and vehicles. Not only did she share her experiences through stories, Munijah also taught the audience directly on how to make cilok (chewy snack made from tapioca starch, water, garlic, and scallion).</p>


					<p style="padding-top: 20px; ">Through Inspirational Talk Show Inspiring, we get to know our clients more and we find that the agility of communities at the grassroots level is unquestionable. They are resilient in the face of challenges and business risks. For instance, Nurhayati told her story of how she works two times harder to irrigate her paddy fields during the dry season, walking back and forth between home and field to fetch water. She saves up gold when the harvest is abundant, so that when it comes to crop failure she still has capital reserves.</p>

					<p style="padding-top: 20px; ">No less smart, Aan, a vegetables vendor in Tenjo, looks for strategies when many other vegetable vendors start to come in. She lowers the price by IDR 500 to IDR 1,000 to attract buyers. She deliberately allows gado-gado (Indonesian vegetable salad with peanut dressing) sellers and cooked food sellers to buy vegetables from her on credit. From her agility in running her business, she has managed to repay the instalment of a car for angkot (city carrier), so that her husband can operate that car as an angkot driver, as well as a vehicle to purchase vegetables.</p>

					<p style="padding-top: 20px; ">Through Inspirational Talk Show, we expect other clients to be inspired by the experience of the speakers so that they continue to be motivated to grow business and strive to be more independent. Furthermore, Inspirational Talk Show pushes the rural communities to network with each other, particularly to encourage each other in terms of business and economy.</p>

					<p style="padding-top: 20px; ">Inspirational Talk Show will not stop in 2015. It will continue to take place so that its positive impact continues to snowball and benefits the rural communities.</p>

					<?php } else {?>

					<p style="padding-top: 20px; ">Topik yang diangkat pada Talkshow Inspiratif merupakan disesuaikan dengan peluang bisnis di daerah pelaksanaan dan pekerjaan mayoritas masyarakat sekitar. Di 2015, Amartha telah melaksankan program di tiga kecamatan berbeda, Kecamatan Bojong Gede, Jasinga, dan Tenjo di Kabupaten Bogor, Jawa Barat. </p>

					<p style="padding-top: 20px; ">Di Kecamatan Bojong Gede, topik yang kami angkat adalah mengenai perdagangang dengan pembicara Munjiah (pemiliki usaha cilok). Ia berbagi pengalaman menjalani usaha cilok bersama suaminya, hingga memiliki anak buah, dan berlanjut bagaimana dirinya berhasil mengembangkan aset seperti memiliki sawah dan kendaraan. Tidak hanya berbagi pengalaman melalui cerita, Munijah juga mengajarkan secara langsung praktik membuat cilok.</p>


					<p style="padding-top: 20px; ">Di Kecamatan Jasingan dan Tenjo yang mayoritas masyarakatnya petani dan pedagang kami mengangkat topik mengenai pertanian dan perdagangan (lebih spesifiknya pedagang sayur). Di Tenjo, Nurhayati (petani) berbagi pengalaman bagaimana dirinya harus pintar-pintar dalam menabung ketika panen besar demi bersiap jika menghadapi gagal panen. Melalui Talkshow Inspiratif kami lebih mengenal anggota kami, bahwa agility (kelincahan) masyarakat di akar rumput tidak perlu diragukan. Mereka tangguh dalam menghadapi tantangan dan risiko usaha. Contohnya Nurhayati yang bercerita bagaimana ia bekerja dua kali lebih giat ketika musim kemarau datang, untuk mengairi sawahnya, ia mondar mandiri rumah-sawah-rumah-sawah untuk mengambil air. Ia menabung emas ketika panen melimpah, sehingga ketika datang gagal panen ia punya modal cadangan. </p>

					<p style="padding-top: 20px; ">Tidak kalah cerdas, Aan pengusaha sayur di Tenjo, mencari strategi ketika sudah mulai banyak pedagang sayur lainnya. Ia menurukan harga Rp 500 hingga Rp 1.000 sehingga pembeli lebih memilih dagangannya. Ia sengaja memberikan hutang sebagai kemudahan pada para penjual gado-gado atau lauk matang. Dari kelincahannya usaha sayur, ia berhasil mencicil angkot, sehingga suaminya mendapatkan peluang usaha, supir angkot, sekaligus sebagai kendaraan untuk berbelanja sayur.</p>

					<p style="padding-top: 20px; ">Dengan kelincahan tersebut, kami berharap anggota lain dapat terinspirasi dari pengalaman para pembicara. Sehingga ketika masuk tahun ketiga dipendampingan kami, setiap anggota terdorong untuk berwirausaha dan menjadi lebih mandiri. Lebih jauh dari itu, Talkshow Inspiratif merupakan dorongan untuk masyarakat desa untuk berjejaring, terutama untuk saling mendorong dalam usaha dan perekonomian.</p>

					<p style="padding-top: 20px; ">Talkshow Inspiratif tidak berhenti di 2015, ia akan terus berlanjut sehingga bola salju kebaikan di pedesan terus dan semakin membesar.</p>

					<?php }?>


					





			</div>


			</div>
		</div>
	</div>







		<script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.touchSwipe.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.imagesloaded.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.scrollTo-1.4.3.1-min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/spin.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/portfolio.js" type="text/javascript"></script>

    <script type="text/javascript">
    $(document).ready(function() {
            var p = $("#gallery").portfolio();
            var q = $("#gallery2").portfolio();
            p.init();

    });
    </script>
