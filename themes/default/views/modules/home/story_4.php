    <div id="headerImageStory" style="background: url(<?php echo $this->template->get_theme_path(); ?>/img/bg_homepage.jpg) no-repeat center bottom fixed;background-size:cover;">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent clearfix">

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2>Amartha help business grow faster. We know because they tell us everyday</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">SIGN UP FOR AMARTHA</a>
				
				<?php } else {?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Amartha membantu bisnis tumbuh lebih cepat <br/>Kami tahu karena mereka menceritakannya setiap hari</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">Mulai Investasi Sekarang</a>
				
				<?php } ?>

		    	<h3>&nbsp;</h3>
		    	<!--<button class="btn btn-default" type="submit">SIGN UP FOR AMARTHA</button>-->
	    	</div>
		</div>
	</div>
	<div class="container text-center">
		<?php if ($this->session->userdata('site_lang') == 'english') { ?>

		<h2 class="text-center purple"><b>Fitri Afriany: Building Partnerships, Growing Business</b></h2>
		<p class="opensans" style="padding-bottom:20px;">Starting a convection business in 2009, Fitri Afriany (33 years old) now has employed seven workers and had more than 200 students. Fitri has also successfully partnered with a distributor who delivers products to Malaysia and Somalia.</p>

		<p class="opensans" style="padding-bottom:20px;">Fitri is one of Amartha Microfinance’s (Amartha) clients who have joined since 2009. At the beginning of her membership, Fitri received working capital financing amounting to IDR 500,000. She immediately used it to add to her existing cash allocated for the purchase of a sewing machine priced at IDR 1,200,000. “At that time, my boss from the place I used to work asked me if I still want to sew clothes and offered me that sewing machine”, told Fitri when asked on how she got a sewing machine at a low price. Before starting up her own convection business, Fitri worked at a French-originated garment. She was her boss’s right hand who was trusted to examine the work of other employees, checking the stitches.</p>

		<h3 class="text-center purple"><b>Curiosity</b></h3>

		<p class="opensans" style="padding-bottom:20px;">Fitri did not acquire her sewing ability through formal education or courses, but self-taught. It is curiosity and perseverance that make Fitri as skilled in sewing as she is today. She recalled that she once worked in a convection business owned by her relative. However, at that time, she worked as an administrator for the bookkeeping and accounting. So, during lunch break, she would ask for permission to her relative to learn to sew.</p>

		<p class="opensans" stule="padding-bottom:20px;">“At that time, I was learning to sew for fun. Since the business was owned by my relative, during lunch break I was allowed to use the sewing machine. There, I would sew some torn clothes”, told Fitri when asked on whether she got her sewing ability from taking some courses.</p>


		<h3 class="text-center purple"><b>Consistency and Focus</b></h3>

		<p class="opensans" style="padding-bottom:20px;">Joining Amartha since 2011  has helped Fitri in developing her business because previously her access to capital was only through arisan (regular informal gathering held within a neighbourhood). “Before receiving financing from Amartha, I have got two sewing machines and one overlock machine. Once I got the working capital disbursed from Amartha in 2011, I bought another machine then I had a total of 4 machines,” explained Fitri when asked on how she has built her business assets.</p>

		<p class="opensans" style="padding-bottom:20px;">Fitri is a consistent and focused individual. Since receiving the first to the third financing, she has always utilised it to purchase a sewing machine. When she got the fourth financing of IDR 4,000,000, she bought another overdeck machine so that she can work on sportswear. At present, she has 10 sewing machines that are capable of meeting demands from more than three online shops.</p>


		<h3 class="text-center purple"><b>Quick in Seizing Opportunities</b></h3>
		<p class="opensans" style="padding-bottom:20px;">In addition to producing clothes, Fitri also offers courses for those who want to learn sewing. Since opening the course in 2012, now the number of her students has reached more than 200 people.</p>

		<p class="opensans" style="padding-bottom:20px;">Fitri admits that the objective to offer sewing classes in the first place is not only to share her knowledge but also to increase her income. Convection business, similar to other businesses, may face slow times where the demand is low and many sewing machines left idle. To overcome that, Fitri, who also sees that as an opportunity, immediately decided to offer the course. The teachers are none other than her husband and herself. Her husband is also adept at tailoring having previously worked in a garment company. For every meeting conducted for approximately one to two hours, Fitri charges IDR 600,000 per student.</p>

		<p class="opensans" style="padding-bottom:20px;">Running this sewing course is also proven useful when her business is flooded with orders. Fitri can conveniently recruit workers directly from the students or graduates of the course itself. It is a symbiotic mutualism between the students/graduates who need jobs and Fitri who needs skilled workers.</p>

		<p class="opensans" style="padding-bottom:20px;">Not limiting herself to her convection business, Fitri opens a kiosk from which people can top-up their phone balance. She works on this additional business is not because of a mere trial and error but seeing the market for it. Opening this kiosk is actually responding to one of her students who asked, "The nearest kiosk is located so far away, why not open a kiosk, Teh (sister)?"</p>

		<p class="opensans" style="padding-bottom:20px;">Fitri who has opened a top-up counter, has started to diversify the types of products her kiosk sells in order to accommodate the needs of her students. In 2014, the kiosk was still made non-permanently with one small display window. In 2015, she has managed to build a permanent stall selling a wider range of products.</p>

		<h3 class="text-center purple"><b>Expanding Business</b></h3>

		<p class="opensans" style="padding-bottom:20px;">According to Fitri, in growing a business the necessary requirements are working capital and skill. But to her, the skill is the main thing, "For example, I lack of capital, but with my skill and courage, I am still able to run a business. Eventually that capital will come on its own.” She learns from her brother who had a convection business with bigger capital. But with limited skills and relying solely on the ability of others, his business was finally discontinued.</p>

		<p class="opensans" style="padding-bottom:20px;">With her hard work and ability in building partnerships, her business has been growing even bigger. She has begun to enlarge her sewing class after the discussion she had with a Principal of an Islamic Junior High School on the idea to cooperate. “In the last discussion, we chatted about the system. For example, if there are 40 students in one class, students have to come in turn to my class each week,” explained Fitri on how the said partnership will run.</p>

		<p class="opensans" style="padding-bottom:20px;">Fitri is a tough businesswoman and is sharp in catching opportunities. Working capital assistance from Amartha has given positive contribution. Her ability to build partnerships has not only opened up opportunities in developing her business but also created new job opportunities for others.</p>

		<?php } else {?>

		<h2 class="text-center purple"><b>Fitri Afriany: Menebar Relasi - Membesarkan Bisnis</b></h2>
		<p class="opensans" style="padding-bottom:20px;">Memulai usaha konveksi sejak 2009, Fitri Afriany (33 tahun) kini sudah memiliki tujuh karyawan dan lebih dari 200 murid kurus. Fitri juga berhasil bermitra dengan perusahaan distributor yang mengantar produknya ke Malaysia dan Somalia.</p>

		<p class="opensans" style="padding-bottom:20px;">Fitri adalah salah satu anggota Amartha Microfinance (Amartha) yang bergabung sejak 2009. Pada awal bergabung, Fitri mendapat pembiayaan modal usaha sebesar Rp 500.000. Ia langsung memanfaatkannya untuk menambah pembelian mesin jahit seharga Rp 1.200.000. “Saat itu saya ditawarin sama bos tempat dulu saya bekerja, Fitri, kamu masih mau ngejait gak? Ini bayarin  mesin. Kalau kamu mau, saya gak kasih siapa-siapa,” cerita Fitri bagaimana ia mendapatkan mesin jahit dengan harga murah.  Sebelum memiliki usaha konveksi, Fitri bekerja di sebuah perusahaan konveksi, suplier brand asal Perancis. Ia adalah tangan kanan atasan yang dipercaya untuk memeriksa hasil jahitan karyawan lain. </p>

		<h3 class="text-center purple"><b>Kuriositas</b></h3>

		<p class="opensans" style="padding-bottom:20px;">Kemampuan Fitri menjahit tidak diperoleh dari pendidikan formal atau kursus, melainkan otodidak. Rasa ingin tahu dan ketekunan yang membuat Fitri semahir sekarang ini dalam bidang jahit menjahit.  Ia mengaku, dahulu sekali bekerja di usaha konveksi milik saudara, namun sebagai administrator untuk pembukuan dan pencatatan. Karena usaha milik saudara, di waktu istirahat dirinya meminta ijin untuk belajar menjahit. </p>

		<p class="opensans" stule="padding-bottom:20px;">“Waktu itu saya iseng-iseng aja waktu siang orang namanya punya saudara sendiri, jam istirahat saya belajar, ada yang robek-robek sedikit saya jahit,” cerita Fitri ketika kami bertanya, apakah kemampuan menjahitnya karena sebelumnya mengambil kursus.</p>


		<h3 class="text-center purple"><b>Konsistensi dan Fokus</b></h3>

		<p class="opensans" style="padding-bottom:20px;">Konsistensi dan Fokus Bergabung di Amartha sejak 2011 hingga kini sangat membantu Fitri dalam mengembangkan usahanya karena selama ini akses keuangan Fitri hanya melalui arisan. “Sebelum cair dari koperasi Amartha, saya sudah punya dua mesin jahit dan satu mesin obras. Waktu cair koperasi Amartha di tahun 2011 waktu itu punya total 4 mesin,” cerita Fitri ketika ditanya bagaimana cara ia menambah aset usaha.</p>

		<p class="opensans" style="padding-bottom:20px;">Fitri adalah seorang yang konsisten dan fokus, sejak menerima pembiayaan modal pertama hingga ketiga, selalu ia gunakan untuk membeli mesin jahit. Hingga pembiayaan keempat sebesar Rp 4.000.000, ia membeli lagi mesin overdeck sehingga ia bisa mengerjakan pesanan pakaian olahraga. Saat ini ia memiliki 10 mesin jahit yang mampu memproduksi lebih dari pesanan dari lebih dari tiga online shop. </p>


		<h3 class="text-center purple"><b>Jeli Melihat Peluang </b></h3>
		<p class="opensans" style="padding-bottom:20px;">Selain memproduksi pakaian, Fitri juga melayani kursus bagi umum yang ingin belajar menjahit. Sejak membuka kursus di 2012, kini muridnya telah mencapai lebih dari 200 orang.</p>

		<p class="opensans" style="padding-bottom:20px;">Fitri mengaku tujuannya membuka kursus selain berbagi ilmu ialah menambah penghasilan. Usaha konveksi seperti halnya usaha lain, kadang kala sepi dan mesin jahit terpaksa menganggur. Mengakali hal tersebut Fitri yang jeli melihat peluang segera memutuskan untuk membuka kursus. Gurunya tidak lain ia dan suami karena suaminya pun mahir dalam menjahit setelah sebelumnya bekerja di perusahaan konveksi. Untuk setiap hari pertemuan selama kurang lebih satu hingga dua jam, ia memberikan harga Rp 600.000/murid.</p>

		<p class="opensans" style="padding-bottom:20px;">Hal yang juga cerdas adalah ketika kebanjiran pesanan pakaian, Fitri dapat merekrut karyawan langsung dari murid/lulusan kursusnya sendiri. Simbiosis mutualisme, murid kursusnya butuh pekerjaan dan Fitri mendapatkan karyawan terampil.</p>

		<p class="opensans" style="padding-bottom:20px;">Tidak berhenti pada usaha konveksi Fitri mengembambangkan usaha warung dan jual pulsa. Usaha tersebut bukanlah coba-coba, melainakan karena ada pasarnya.Berawalx dari keluhan salah seorang murid kursusnya yang bertanya, “teh kok warung jauh ya, kenapa teteh nggak buka warung aja?” </p>

		<p class="opensans" style="padding-bottom:20px;">Fitri yang sebelumnya sudah membuka counter pulsa, membuka warung kecil-kecilan untuk memudahkan murid kursusnya. Di 2014 warungnya masih non permanen dengan satu etalase kecil. Di 2015, ia telah berhasil membangun warung permanen dengan produk jualan yang lebih beragam.</p>

		<h3 class="text-center purple"><b>Membesarkan Bisnis</b></h3>

		<p class="opensans" style="padding-bottom:20px;">Menurut Fitri, dalam membesarkan usaha yang diperlukan adalah modal dan kemampuan. Akan tetapi kemampuan adalah hal utama, “contohnya saya yang kekurangan modal, tetapi dengan kemampuan dan keberanian tetap saya jalankan. Dan akhirnya modal itu datang dengan sendirinya.” Ia berkaca pada saudaranya yang memiliki usaha konveksi dengan modal lebih banyak, namun dengan kemampuan terbatas dan hanya mengandalkan kemampuan orang lain, akhirnya usahanya tutup. </p>

		<p class="opensans" style="padding-bottom:20px;">Dengan kerja keras dan kepiawaian Fitri membangun relasi, usahanya kini semakin berkembang. Ia mulai membesarkan tempat kursus jahitnya karena sudah ada pembicaraan dari Kepala Sekolah di salah satu SMP Islam untuk mengadakan kerjasama. “Kemarin sempat ngobrol sistemnya, misal satu kelas ada 40 siswa jadi setiap minggu giliran setiap siswa untuk kursus di tempat saya,” cerita Fitri mengenai model kerjasama yang dijalankan. </p>

		<p class="opensans" style="padding-bottom:20px;">Fitri adalah sosok pengusaha yang tangguh dan jeli dalam melihat peluang. Pendampingan modal pembiayaan dari Amartha menunjukkan hasil positif. Ditambah dengan kepiawaiannya membangun relasi telah membuka peluang dalam mengembangkan bisnis, bahkan membuka lapangan pekerjaan baru bagi orang lain.</p>

		<?php }?>
		
	</div>
