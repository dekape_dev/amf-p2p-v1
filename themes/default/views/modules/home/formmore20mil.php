<!-- HEADER -->
	<div id="headerImageBorrow" style="height:300px;">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo.png" height="110px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

			<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACTS</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACTS</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PINJAM</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<!-- Hero Text -->
			<div class="container">

			<div class="startContent clearfix" >
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Fill The Form and Lets get started</h2>
		    	<h3></h3>
	    	</div>
	    </div>
	    </div>
	</div>
	<!-- END OF HEADER -->

	<!-- FORM RESGITRASTION -->

	<div class="container formBorrow">
		<div class="row">
			<div class="col-md-7">
				<h2>Please Complete Detail Below</h2>
				<form>
				  <!-- <div class="form-group">
				    <input type="text" class="form-control" id="" placeholder="Name">
				  </div> -->
				  <div class="form-group">
				  	<div class="input-group">
				      <div class="input-group-addon">IDR</div>
				      	<input type="text" class="form-control" id="exampleInputAmount" placeholder="Amount">
				      <div class="input-group-addon">.000</div>
				    </div>
				  </div>

					  <div class="form-group">

						<select class="form-control">
						  <option>18 Month</option>
						  <option>24 Month</option>
						  <option>3</option>
						  <option>4</option>
						  <option>5</option>
						</select>

					</div>
					<div class="form-group">
						<select class="form-control">
						  <option>Select purpose</option>
						  <option>2</option>
						  <option>3</option>
						  <option>4</option>
						  <option>5</option>
						</select>
					</div>

					<div class="form-group">
					    <input type="email" class="form-control" id="" placeholder="Email">
					</div>
				</form>

				<div class="row">
					<div class="col-md-2">
						<form>
							<div class="form-group">
								<select class="form-control">
								  <option>Title</option>
								  <option>Mr</option>
								  <option>Mrs</option>
								 </select>
							</div>
						</form>
					</div>
					<div class="col-md-5">
						<div class="form-group">
				    		<input type="text" class="form-control" id="" placeholder="First Name">
				  		</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
				    		<input type="text" class="form-control" id="" placeholder="Last Name">
				  		</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h3>Date of Birth</h3>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<form>
							<div class="form-group">
								<select class="form-control">
								  <option>Date</option>
								  <option></option>
								  <option>Mrs</option>
								 </select>
							</div>
						</form>
					</div>
					<div class="col-md-4">
						<form>
							<div class="form-group">
								<select class="form-control">
								  <option>Month</option>
								  <option>Mr</option>
								  <option>Mrs</option>
								 </select>
							</div>
						</form>
					</div>
					<div class="col-md-4">
						<form>
							<div class="form-group">
								<select class="form-control">
								  <option>Year</option>
								  <option>Mr</option>
								  <option>Mrs</option>
								 </select>
							</div>
						</form>
					</div>
				</div>
				<form>
					<div class="form-group">
				    <input type="number" class="form-control" id="" placeholder="Phone Number">
				  </div>
				  <label>Address</label>
				  	<div class="form-group">
				    	<textarea class="form-control" rows="3"></textarea>
				  	</div>

				  	<div class="form-group">
				    	<div class="form-group">
								<select class="form-control">
								  <option>City</option>
								  <option>1</option>
								  <option>2</option>
								 </select>
							</div>
				  	</div>

				  	<div class="form-group">
				    	<div class="form-group">
								<select class="form-control">
								  <option>Province/State</option>
								  <option>1</option>
								  <option>2</option>
								 </select>
						</div>
				  	</div>
				  	<div class="row">
				  		<div class="col-md-8">
				  		<div class="form-group">
					    	<div class="form-group">
									<select class="form-control">
									  <option>Country</option>
									  <option>1</option>
									  <option>2</option>
									 </select>
							</div>
				  		</div>

				  		</div>
				  		<div class="col-md-4">
				  			<div class="form-group">
					    		<input type="text" class="form-control" id="" placeholder="Postal Code">
					  		</div>
				  		</div>
				  	</div>
				  	<div class="form-group">
				    	<div class="form-group">
								<select class="form-control">
								  <option>How long you have lived there?</option>
								  <option>1</option>
								  <option>2</option>
								 </select>
						</div>
				  	</div>

				  	<div class="checkbox term">
					    <label>
					      <input type="checkbox"> Please click here to confirm you agree to Our <a href="#" style="color: purple;">Website & Exchange Term of Use</a> and <a href="#" style="color: purple;">Our Privacy and Policy.</a>
					    </label>
					</div>

					<div class="checkbox term">
					    <label>
					      <input type="checkbox"> I don’t want spam from 3rd parties but I am happy to receive RateSetter news and special offers. You can unsubscribe at any time.</a>
					    </label>
					</div>

					<div class="checkbox term">
					    <label>
					      <input type="checkbox">If we can’t help with a loan, one of our carefully selected partners may be able to. If you are happy to be contacted by one of them, please tick this box. RateSetter, acting as a Credit Broker, will receive a commission for loans arranged by partners.</a>
					    </label>
					</div>

					<div class="container startContent" style="padding-top:0px;">
		<button class="btn btn-default btn-save" type="submit">SUBMIT NOW</button>
	</div>

				</form>



			</div>
		</div>
	</div>









