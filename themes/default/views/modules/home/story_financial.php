    <div id="headerImageStoryFinancial" >
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent singleHead  clearfix">

				<div class="container">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    		<h1 class="text-center">Financial Literacy in Rural Areas?</h1>


					<?php } else {?>
		    		<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Literasi Keuangan di Pedesaan</h2>


					<?php }?>

	    	</div>
	    </div>
		</div>
	</div>

	<div class="container">
		<div class="row investCont2">
			<div class="col-md-6 text-justify opensans">

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<p style="padding-top: 20px; ">Rural life is often seen as identical to simple life with simple needs, unsophisticated lifestyle. That means no excessive debts here and there to keep up with the glamorous lifestyle. However, the reality found in the rural areas in Bogor Regency has its own face.</p>

					<p style="padding-top: 20px; ">Housewives within the underprivileged communities whose additional income comes from selling goods on credit, owning grocery shops, and working as household assistants can frequently be found in the rural areas in Bogor Regency. With those types of work, we can estimate an insignificant amount of income which is probably just enough to meet their daily food needs.</p>

					<p style="padding-top: 20px; ">However, there is one surprising finding. There is a contrast that is interesting enough to investigate. A mother whose house’s ceiling is cracked here and there, corner is covered in black soot, walls look dull, and the plaster starts to crake off, has a South Korean branded 23 inch flat-screen TV! From further conversation, she mentioned that the instalment for the TV has not been fully settled.</p>

					<p style="padding-top: 20px; ">Another surprising story comes from a mother  who applied to Amartha for a financing to redeem her son’s vocational school certificate. Every day, she manages a small rice shop with her sister from which she can take a net profit of IDR 150K home. From that profit, she takes out IDR 100K to give her 5 children pocket money. Then, how could she repay her loan if she spends at least IDR 100K per day for her children’s pocket money? </p>

				<?php } else {?>

					<p style="padding-top: 20px; ">Pedesaan seringkali diidentikan dengan kehidupan sederhana dengan tuntutan hidup yang wajar. Artinya tidak ada kemewahan dipaksakan dari hasil hutang sana-sini. Namun realitas yang bisa ditemukan di pedesaan Kabupaten Bogor memiliki wajahnya sendiri.</p>

					<p style="padding-top: 20px; ">Masyarakat prasejahtera yang mayoritas ibu rumah tangga dengan penghasilan tambahan dari kredit barang, warung sembako, dan asisten rumah tangga masih banyak bisa ditemui di pedesaan Kabupaten Bogor. Dengan pekerjaan seperti itu, kita dapat menaksir penghasilannya tak seberapa. Bahkan mungkin hanya cukup untuk memenuhi kebutuhan makan sehari-hari.</p>

					<p style="padding-top: 20px; ">Namun, ada satu hal yang mengejutkan. Terdapat kontras yang cukup menarik untuk ditelisik. Salah seorang ibu yang kondisi eternit rumahnya terbuka di sana-sini, tulang rumah penuh jelaga, dinding kusam dan plasternya mulai mengelupas, memiliki TV layar datar bermerek sebuah merek dari Korea Selatan dengan ukuran 23 inch! Setelah ditelisik, si ibu berseloroh bahwa kreditnya belum lunas.</p>

					<p style="padding-top: 20px; ">Cerita mengejutkan lagi datang dari seorang ibu, sebut saja Eti namanya. Ia mengajukan pembiayaan ke Amartha untuk menebus ijasah anaknya yang lulus SMK. Kesehariannya, Ibu Eti mengelola warung nasi bersama kakaknya. Setiap hari, Rp 150 ribu ia kantongi sebagai keuntungan bersih. Dari keuntungan tersebut, ia mengeluarkan setidaknya seratus ribu untuk jajan harian lima anaknya. Lantas, bagaimana bisa meminjam dana untuk menebus ijasah tetapi setiap hari setidaknya mengeluarkan Rp 100 ribu untuk jajan anaknya?</p>

				<?php }?>

				
			</div><br>
			<div class="col-md-6">
				<div class="">
					<img src="<?php echo $this->template->get_theme_path(); ?>/img/stories/financial_430x500.jpg" width="100%">
				</div>
			</div>

		</div>
	</div>

	<!-- <div id="gallery">
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/vote_1.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/vote_2.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/vote_3.jpg" />

	</div> -->


	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>

				<div class="about_WhoWeAre  text-justify ">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>

				<h1 class="opensans text-left purple">Rural Life, Simple Life?</h1>

				<p style="padding-top: 20px; ">Stereotype is often misleading. But humans unconsciously swim in the ocean of culture that slowly swallows them and pushes them to adapt.</p>

				<p style="padding-top: 20px; ">Indeed, not everyone in rural Bogor loves to borrow money or buys goods on credit. Many of them are also keen in saving money and growing business that allows them to have assets in the form of rice field, livestock, or car. On the other hand, those who buy goods on credit, ranging from light snacks to high quality knives from Italy, are also widespread. Everything is bought on credit. They always think that, without buying goods on credit they will never have any.</p>

				<?php } else {?>

				<h1 class="opensans text-left purple">Desa, Hidup Wajar?</h1>

				<p style="padding-top: 20px; ">Stereotype seringkali menyesatkan, tetapi tanpa sadar manusia berenang di lautan budaya yang pelan-pelan menyerap dan mendorongnya beradaptasi. </p>

				<p style="padding-top: 20px; ">Tidak semua memang masyarakat pedesaan Bogor senang berhutang atau mengkredit barang. Banyak dari mereka yang juga pandai berhemat, mengembangkan usaha hingga akhirnya memiliki asset berupa sawah, hewan ternak atau mobil baik. Di sisi lain, mereka yang kredit barang pun bermacam-macam mulai dari snack ringan hingga pisau kualitas mahal asal Italia. Semua dikredit. Mereka selalu berpikir, tanpa kredit mereka tidak akan memiliki apapun. </p>

				<?php }?>

				
			</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row investCont2">
			<div class="col-md-6 text-justify opensans">

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h1 class="opensans text-left purple">Financial Literacy</h1>

				<p style="padding-top: 20px; ">Bogor Regency is one of the friendly markets for the growth and development of financing (whether formal or informal), ranging from middlemen, mobile banks, credit goods, to microfinance institutions (MFIs).</p>

				<p style="padding-top: 20px; ">With the ease of accessing such financing, every family surely needs to have its own alarm. At least that alarm should ring when their debts have gone beyond their capacity to repay or when debts are merely used to support a consumptive lifestyle.</p>

				<p style="padding-top: 20px; ">Financial literacy is the best alarm. Lack of understanding of financial management can hinder a family’s ability to prioritise more crucial issues, such as children's education, healthy housing, and so forth. Kempson et al. (2005) suggests that financial literacy includes several aspects: 1) financial management; 2) future planning; 3) financial decision making; and 4) finding financial support.</p>

				<?php } else {?>

				<h1 class="opensans text-left purple">Literasi Keuangan</h1>

				<p style="padding-top: 20px; ">Kabupaten Bogor adalah salah satu tanah yang ramah bagi tumbuh kembang pembiayaan (baik formal atau informal), dari ijon (tengkulak), bank keliling, kkredit barang yang dikelola perseorangan, hingga Lembaga Keuangan Masyarakat (LKM).</p>

				<p style="padding-top: 20px; ">Dengan kemudahan akses pembiayaan tersebut, tentu sebuah keluarga harus memiliki alarm-nya tersendiri. Setidaknya alarm tersebut harus berbunyi ketika sebuah pembiayaan sudah melampaui daya bayar atau mayoritas digunakan hanya untuk ranah konsumtif.</p>

				<p style="padding-top: 20px; ">Literasi Keuangan adalah alarm terbaik. Kurangnya pemahaman mengenai pengelolaan keuangan dapat membuat sebuah keluarga gagal meraih hal yang lebih krusial, seperti pendidikan anak, rumah yang layak, dan lain sebagainya. Kempson et al. (2005) berpendapat literasi keuangan meliputi beberapa poin 1) pengelolaan keuangan 2) perencanaan masa depan 3) pembuatan keputusan keuangan 4) pencarian bantuan keuangan. </p>

				<?php }?>
				


			</div><br><br><br><br><br><br>
			<div class="col-md-6">
				<div class="social5">
					<img src="<?php echo $this->template->get_theme_path(); ?>/img/stories/financial_260x270.jpg">
					<img src="<?php echo $this->template->get_theme_path(); ?>/img/stories/financial2_260x270.jpg">
				</div>
			</div>
		</div>
	</div>


	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
				<div class="about_WhoWeAre  text-justify ">

					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<p style="padding-bottom: 20px; ">Family financial management is generally done by women. According to a research by the Indonesian’s Financial Services Authority, 51% of family financial management is determined by women. However, the level of women financial literacy is recorded at 18.84%, while the level of men financial literacy level has reached 24.87% (FSA, 2014).</p>

					<p style="padding-bottom: 20px; ">Improving the financial literacy of rural communities has been part and parcel of Amartha’s core activities.  Together with Allianz Indonesia, on August 14, 2015, Amartha conducted a Financial Management Seminar for families in Ciseeng and Bojong Gede Districts, Bogor, West Java. The content of the programme covered how to effectively manage income and expenditure, as well as the role of insurance in the spirit of mutual cooperation in the community.</p>

					<p style="padding-bottom: 20px; ">This programme is expected to prevent the consumptive behaviour from thriving, as well as serves as a great way of recognising the various types of financial institution (formal or informal) to avoid being abused by predatory money lending activities.</p>

					<?php } else {?>
					<p style="padding-bottom: 20px; ">Pada sebuah keluarga pengelolaan keuangan umumnya dilakukan oleh perempuan (menurut penelitian yang dilakukan OJK, 51% pengelolaan keuangan keluarga ditentukan oleh perempuan. Meski demikian tingkat financial literacy perempuan baru mencapai 18,84%, sementara tingkat financial literacy pria sudah mencapai 24,87% (OJK, 2014).</p>

					<p style="padding-bottom: 20px; ">Amartha Microfinance sebagai salah satu lembaga pembiayaan di Kabupaten Bogor senantiasa mendorong meningkatnya literasi keuangan masyarakat. Bersama Allianz, 14 Agustus 2015 lalu Amartha Microfinance menyelenggarakan Program Pengelolaan Keuangan untuk Keluarga di Kecamatan Ciseeng dan Bojong Gede, Kabupaten Bogor, Jawa Barat. Muatan program meliputi bagaimana mengelola pemasukan dan pengeluaran secara aplikatif, serta bagaimana peran asuransi yang tak ubahnya hidup dari semangat gotong royong di masyarakat.</p>

					<p style="padding-bottom: 20px; ">Program diharapkan dapat menjadi benteng dari dorongan prilaku konsumtif. Sekaligus menjadi cara terbaik untuk mengenali lembaga keuangan (formal atau informal) agar mampu menghindari kerentanan finansial.</p>

					<?php }?>

					
			</div>
			</div>
		</div>
	</div>






		<script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.touchSwipe.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.imagesloaded.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.scrollTo-1.4.3.1-min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/spin.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/portfolio.js" type="text/javascript"></script>

    <script type="text/javascript">
    $(document).ready(function() {
            var p = $("#gallery").portfolio();
            var q = $("#gallery2").portfolio();
            p.init();

    });
    </script>
