
<!-- HEADER -->
	<div id="headerImageImpact">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="100px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

			<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<!-- Hero Text -->
			<div class="startContent clearfix ">
		    	<!--<h2 >Amartha help business grow faster<br/>We know because they tell us everyday</h2>-->
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">We are honoured to listen to and to be part of their stories</h2>
				
				<?php } else {?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Kami bahagia mendengar dan menjadi bagian dari cerita mereka</h2>
				
				<?php }?>
				
				<!--
				<a href="<?php echo site_url();?>register" class="button_main btn btn-default" type="submit">Sign Up for Amartha</a>
				-->
			</div>
	    </div>
	</div>
	<!-- END OF HEADER -->

	<!-- PEOPLE STORY -->
	<section class="">

		<div class="container">
		<div class="about_WhoWeAre careerContent  text-center ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h1 class="opensans text-center purple">People's Stories</h1>
			<p class="opensans fs-14 text-center" style="padding-top: 20px; ">We are honoured with the privilege of serving hardworking and inspiring micro entrepreneurs. We provide these entrepreneurs with access to loans so that they continue to thrive and that they can get closer to their dreams.</p>
			<p class="opensans fs-14 text-center" style="padding-top: 20px; ">We work to create multiple positive effect. Beyond growing business, we hope to see: an increase in family income; children get to eat healthy food and remain at school; houses are improved; and women gain better status. With loans as small as IDR 1 million, we hope our clients to experience those transformations every day.</p>
			
			<?php } else {?>
			<h1 class="opensans text-center purple">Cerita Kami</h1>
			<p class="opensans fs-14 text-center" style="padding-top: 20px; ">Kami sangat bangga dan terhormat dapat melayani para pengusaha mikro dan UKM yang bekerja keras dan penuh inspirasi. Kami membantu para wirausahawan ini dengan akses yang mudah kepada permodalan sehingga mereka dapat terus berkarya dan semakin dekat dengan mimpi-mimpi mereka.</p>
			<p class="opensans fs-14 text-center" style="padding-top: 20px; ">kami bekerja untuk menciptakan hasil positif yang berlipat ganda. Disamping mengembangkan usaha, kami juga mendambakan : peningkatan pendapatan keluarga, anak-anak dengan makanan sehat dan tetap bersekolah, rumah yang lebih layak, dan perempuan yang lebih setara derajatnya. Dengan pembiayaan mulai dari IDR 1 juta, kami yakin nasabah-nasabah kami dapat memiliki pengalaman dan transformasi tersebut setiap harinya.</p>
			
			<?php }?>

	</div>
	</section>

	<div class="container storiesSquare">
		<!-- GANTI IMAGESNYA DI CSS YAA.. JANGAN DIPAKSA DISNI -->
		<div class="row">
			<div class="col-md-4" style="text-align:center;" >
				<div class=" picStory1 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white justMe ">Enah</h2>
					<h2 class="hd white justMe">Micro-entrepreneur with a Big Vision</h2>
					<a href="<?php echo site_url('story/story'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white justMe ">Enah</h2>
					<h2 class="hd white justMe">Pengusaha kecil bervisi besar</h2>
					<a href="<?php echo site_url('story/story'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>

				</div>
			</div>
			<div class="col-md-4" >
				<div class=" picStory2 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white marck ">Yanih</h2>
					<h2 class="hd white marck">Samak Maker,Pejuang Keluarga</h2>
					<a href="<?php echo site_url('story/story2'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white marck ">Yanih</h2>
					<h2 class="hd white marck" style="font-size:22px;">Pembuat Samak,Pejuang Keluarga</h2>
					<a href="<?php echo site_url('story/story2'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>
					
				</div>
			</div>
			<div class="col-md-4" >
				<div class=" picStory3 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white merienda " style="font-size:24px; padding-top:12px;">Onih Triana</h2>
					<h2 class="hd white merienda">Striving for a Dream</h2>
					<a href="<?php echo site_url('story/story3'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white merienda " style="font-size:24px; padding-top:12px;">Onih Triana</h2>
					<h2 class="hd white merienda " style="font-size:22px;">Bekerja keras untuk Mimpi</h2>
					<a href="<?php echo site_url('story/story3'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>
					
				</div>
			</div>
		</div>

		<div class="row" style="margin-top:px;">
			<div class="col-md-4" style="text-align:center;" >
				<div class=" picStory4 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white qwigley ">Fitri Afriany</h2>
					<h2 class="hd white qwigley">Building Partnerships, Growing Business</h2>
					<a href="<?php echo site_url('story/story4'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white qwigley ">Fitri Afriany</h2>
					<h2 class="hd white qwigley">Menebar Relasi - Membesarkan Bisnis</h2>
					<a href="<?php echo site_url('story/story4'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>
					
				</div>
			</div>
			<div class="col-md-4" >
				<div class=" picStory5 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white sacramento ">Nyai</h2>
					<h2 class="hd white sacramento">Tofu Business</h2>
					<a href="<?php echo site_url('story/story5'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white sacramento ">Nyai</h2>
					<h2 class="hd white sacramento">Terus Mandiri dengan Usaha Tahu</h2>
					<a href="<?php echo site_url('story/story5'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>
					
				</div>
			</div>
			<div class="col-md-4" >
				<div class=" picStory6 overlay black ">

					<?php if ($this->session->userdata('site_lang') == 'english') { ?>

					<h2 class="hs white intoLight ">Apsiah</h2>
					<h2 class="hd white intoLight">Working hard until satisfied</h2>
					<a href="<?php echo site_url('story/story6'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>

					<h2 class="hs white intoLight ">Apsiah</h2>
					<h2 class="hd white intoLight">Kerja Keras Sampai Puas</h2>
					<a href="<?php echo site_url('story/story6'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>
					
				</div>
			</div>
		</div>
	</div>


	<!-- <div class="container stories">
		<div class="row  ">
			<div class="col-md-4  storiesImage3">
						<img src="<?php echo $this->template->get_theme_path(); ?>img/story_2.jpg">
						<h3 class="justMe fs-22">Enah</h3>
					<div class="stories_title">
						<h2 class="opensans"><a href="<?php echo site_url('story/story'); ?>">Enah: Micro-entrepreneur <br>with a Big Vision</a></h2>
						<small class="opensans"><a href="<?php echo site_url('story/story'); ?>">Read this story</a></small>
					</div>
			</div>
			<div class="col-md-4  storiesImage3">
						<img src="<?php echo $this->template->get_theme_path(); ?>img/story_3.jpg">
						<h3 class="marck fs-20">Yanih</h3>
					<div class="stories_title">
						<h2 class="opensans"><a href="<?php echo site_url('story/story2'); ?>">Yanih: Samak Maker,<br> Pejuang Keluarga</a></h2>
						<small class="opensans"><a href="<?php echo site_url('story/story2'); ?>">Read this story</a></small>
					</div>
			</div>
			<div class="col-md-4  storiesImage3">
						<img src="<?php echo $this->template->get_theme_path(); ?>img/story_4.jpg">
						<h3>Onih Triana</h3>
					<div class="stories_title">
						<h2 class="opensans"><a href="<?php echo site_url('story/story3'); ?>">Onih Triana: Striving for a Dream</a></h2>
						<small class="opensans"><a href="<?php echo site_url('story/story3'); ?>">Read this story</a></small>
					</div>
			</div>
		</div><br>


		<div class="row ">
			<div class="col-md-4  storiesImage3">
						<img src="<?php echo $this->template->get_theme_path(); ?>img/story_1.jpg">
						<h3>Fitri Afriany</h3>

					<div class="stories_title">
						<h2 class="opensans"><a href="<?php echo site_url('story/story4'); ?>">Fitri Afriany: Building <br>Partnerships, Growing Business</a></h2>
						<small class="opensans"><a href="<?php echo site_url('story/story4'); ?>">Read this story</a></small>
					</div>
				</div>
				<div class="col-md-4  storiesImage3">
						<img src="<?php echo $this->template->get_theme_path(); ?>img/story_2.jpg">
						<h3>Siti</h3>

					<div class="stories_title">
						<h2 class="opensans"><a href="<?php echo site_url('story/story5'); ?>">Siti: striving for a dream</a></h2>
						<small class="opensans"><a href="<?php echo site_url('story/story5'); ?>">Read this story</a></small>
					</div>
				</div>
				<div class="col-md-4 storiesImage3">
						<img src="<?php echo $this->template->get_theme_path(); ?>img/story_3.jpg">
						<h3>Rahayu</h3>
					<div class="stories_title">
						<h2 class="opensans"><a href="<?php echo site_url('story/story5'); ?>">Rahayu: Sharp in capturing<br>business opportunity</a></h2>
						<small class="opensans"><a href="<?php echo site_url('story/story5'); ?>">Read this story</a></small>
					</div>
				</div>
		</div> -->

		<!--<div class="row">
			<div class="col-md-4 storiesbox storiesImage3">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_default2.jpg">
				<div class="stories_title">
					<h2 class="opensans"><a href="<?php echo site_url('story/default'); ?>">Story of Andy With Their Business</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/default'); ?>">Read this story</a></small>
				</div>

			</div>
			<div class="col-md-4 storiesbox storiesImage4">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_default3.jpg">
				<div class="stories_title">
					<h2 class="opensans"><a href="<?php echo site_url('story/default'); ?>">Story of Andy With Their Business</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/default'); ?>">Read this story</a></small>
				</div>

			</div>
			<div class="col-md-4 storiesbox storiesImage4">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_default3.jpg">
				<div class="stories_title">
					<h2 class="opensans"><a href="<?php echo site_url('story/default'); ?>">Story of Andy With Their Business</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/default'); ?>">Read this story</a></small>
				</div>

			</div>

		</div>-->


	</div>
	<!-- PEOPLE STORY END -->

	<!-- SOCIAL STORY -->

	<section class="">

		<div class="container">
		<div class="about_WhoWeAre careerContent  text-center ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h1 class="opensans text-center purple">Social Projects' Stories</h1>
			<p class="opensans fs-14 text-center" style="padding-top: 20px; ">Amartha serves as an avenue to integrate local communities, private companies, donors, international organisations and hopefully governments in improving the businesses and lives of borrowers. We set up projects and are committed to ensure our projects running with quality and impact.</p>
			
			<?php } else {?>
			<h1 class="opensans text-center purple">Kisah tentang Proyek Sosial Kami</h1>
			<p class="opensans fs-14 text-center" style="padding-top: 20px; ">Amartha berfungsi sebagai jembatan untuk megnintegrasikan berbagai komunitas, perusahaan swasta, donor, lembaga internasional dan mungkin juga pemerintah dalam meningkatkan usaha dan kehidupan para peminjam. Kami telah melakukan berbagai proyek sosial dan akan terus melanjutkannya dengan komitmen untuk senantiasa memelihara impact dan kualitas dari pekerjaan kami.</p>
			
			<?php }?>

	</div>
	</section>

	<div class="container storiesSquare">
		<!-- GANTI IMAGESNYA DI CSS YAA.. JANGAN DIPAKSA DISNI -->
		<div class="row">
			<div class="col-md-4" style="text-align:center;" >
				<div class=" picStory7 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white justMe ">Health</h2>
					<h2 class="hd white justMe">Healthy Children, Happy Families</h2>
					<a href="<?php echo site_url('story/healthy'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white justMe ">Kesehatan</h2>
					<h2 class="hd white justMe">Anak Sehat, Keluarga Bahagia</h2>
					<a href="<?php echo site_url('story/healthy'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>
					<?php }?>
				
				</div>
			</div>
			<div class="col-md-4" >
				<div class=" picStory8 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white marck ">Glasses</h2>
					<h2 class="hd white marck" style="font-size:22px;">Free Glasses to Increase Women Productivity</h2>
					<a href="<?php echo site_url('story/glasses'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white marck ">Kacamata</h2>
					<h2 class="hd white marck" style="font-size:22px;">Kacamata Gratis Untuk Produktivitas Perempuan</h2>
					<a href="<?php echo site_url('story/glasses'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>
				
				</div>
			</div>
			<div class="col-md-4" >
				<div class=" picStory9 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white merienda " style="font-size:24px; padding-top:12px;">TalkShow</h2>
					<h2 class="hd white merienda">Inspirational Talk Show</h2>
					<a href="<?php echo site_url('story/talkshow'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white merienda " style="font-size:24px; padding-top:12px;">TalkShow</h2>
					<h2 class="hd white merienda">Talkshow Inspiratif</h2>
					<a href="<?php echo site_url('story/talkshow'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>
				</div>
			</div>
		</div>

		<div class="row" style="margin-top:px;">
			<div class="col-md-4" style="text-align:center;" >
				<div class=" picStory10 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white qwigley ">Duck Breeding</h2>
					<h2 class="hd white qwigley">Duck Breeding: </br>Local Entrepreneurship Training</h2>
					<a href="<?php echo site_url('story/duck'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white qwigley ">Ternak Bebek</h2>
					<h2 class="hd white qwigley">Ternak Bebek: Pelatihan Kewirausahaan Lokal</h2>
					<a href="<?php echo site_url('story/duck'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>
					<?php }?>

				</div>
			</div>
			<div class="col-md-4" >
				<div class=" picStory11 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white sacramento ">Smart Vote</h2>
					<h2 class="hd white sacramento">Let’s Vote Smart!</h2>
					<a href="<?php echo site_url('story/vote'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white sacramento ">Memilih Cerdas</h2>
					<h2 class="hd white sacramento">Ayo Memilih Cerdas!</h2>
					<a href="<?php echo site_url('story/vote'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>

				</div>
			</div>
			<div class="col-md-4" >
				<div class=" picStory12 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white intoLight ">Eid Al-Adha</h2>
					<h2 class="hd white intoLight">Eid Al-Adha Sacrificial Meat Distribution </h2>
					<a href="<?php echo site_url('story/eid'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white intoLight ">Idul Adha</h2>
					<h2 class="hd white intoLight">Kurban Idul Adha</h2>
					<a href="<?php echo site_url('story/eid'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>
					
				</div>
			</div>
		</div>

		<div class="row" style="margin-top:px;">
			<div class="col-md-4" style="text-align:center;" >
				<div class=" picStory13 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white qwigley ">Tooth Fairies</h2>
					<h2 class="hd white qwigley" style="font-size:22px;">Tooth Fairies Paint a Million Smiles on World Oral Health Day</h2>
					<a href="<?php echo site_url('story/tooth'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white qwigley ">Peri Gigi</h2>
					<h2 class="hd white qwigley" style="font-size:22px;">Peri Gigi Warnai Sejuta Senyum di Hari Kesehatan Mulut Sedunia</h2>
					<a href="<?php echo site_url('story/tooth'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>

				</div>
			</div>
			<div class="col-md-4" >
				<div class=" picStory14 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white justMe ">Groceries</h2>
					<h2 class="hd white justMe">Ramadhan Cheap Groceries</h2>
					<a href="<?php echo site_url('story/groceries'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white justMe ">Sembako</h2>
					<h2 class="hd white justMe">Sembako Murah Ramadhan</h2>
					<a href="<?php echo site_url('story/groceries'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>
					
				</div>
			</div>
			<div class="col-md-4" >
				<div class=" picStory15 overlay black ">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white merienda "style="font-size:28px; padding-top:10px;;">Financial</h2>
					<h2 class="hd white merienda">Financial Literacy in Rural Areas</h2>
					<a href="<?php echo site_url('story/financial'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white merienda "style="font-size:28px; padding-top:10px;;">Keuangan</h2>
					<h2 class="hd white merienda">Pengelolaan Keuangan Keluarga</h2>
					<a href="<?php echo site_url('story/financial'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>
					
				</div>
			</div>
		</div>
	</div>

	<!-- <div class="container stories">
		<div class="row">
			<div class="col-md-4 storiesImage3">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_healthy_header.jpg">
				<h4></h4>
				<div class="stories_title">
					<h2 class="opensans"><a href="<?php echo site_url('story/healthy'); ?>">Healthy Children, Happy Families</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/healthy'); ?>">Read this story</a></small>
				</div>
			</div>
			<div class="col-md-4 storiesImage3">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_kacamata1.jpg">
				<h4></h4>
				<div class="stories_title twoRow">
					<h2 class="opensans"><a href="<?php echo site_url('story/glasses'); ?>">Free Glasses to </br> Increase Women Productivity</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/glasses'); ?>">Read this story</a></small>
				</div>
			</div>
			<div class="col-md-4 storiesImage3">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_talkshow.jpg">
				<h4></h4>
				<div class="stories_title">
					<h2 class="opensans"><a href="<?php echo site_url('story/talkshow'); ?>">Inspirational Talk Show</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/talkshow'); ?>">Read this story</a></small>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 storiesImage3">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_3.jpg">
				<h4></h4>
				<div class="stories_title twoRow">
					<h2 class="opensans"><a href="<?php echo site_url('story/duck'); ?>">Duck Breeding: </br>Local Entrepreneurship Training</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/duck'); ?>">Read this story</a></small>
				</div>

			</div>
			<div class="col-md-4 storiesImage3">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_pemilu.jpg">
				<h4></h4>
				<div class="stories_title">
					<h2 class="opensans"><a href="<?php echo site_url('story/vote'); ?>">Let’s Vote Smart!</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/vote'); ?>">Read this story</a></small>
				</div>
			</div>
			<div class="col-md-4 storiesImage3">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_3.jpg">
				<h4></h4>
				<div class="stories_title">
					<h2 class="opensans"><a href="<?php echo site_url('story/eid'); ?>">Eid Al-Adha Sacrificial Meat Distribution </a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/eid'); ?>">Read this story</a></small>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col-md-4 storiesImage3">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_gigi1.jpg">
				<h4></h4>
				<div class="stories_title twoRow">
					<h2 class="opensans"><a href="<?php echo site_url('story/tooth'); ?>">Tooth Fairies Paint a Million Smiles </br> on World Oral Health Day</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/tooth'); ?>">Read this story</a></small>
				</div>

			</div>
			<div class="col-md-4 storiesImage3">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_sembako1.jpg">
				<h4></h4>
				<div class="stories_title">
					<h2 class="opensans"><a href="<?php echo site_url('story/groceries'); ?>">Ramadhan Cheap Groceries</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/groceries'); ?>">Read this story</a></small>
				</div>
			</div>
			<div class="col-md-4 storiesImage3">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_literasi.jpg">
				<h4></h4>
				<div class="stories_title">
					<h2 class="opensans"><a href="<?php echo site_url('story/financial'); ?>">Financial Literacy </br>in Rural Areas</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/financial'); ?>">Read this story</a></small>
				</div>
			</div>
		</div>
	</div> -->

		<!--<div class="row">
			<div class="col-md-6 storiesbox storiesImage3">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_talkshow.jpg">
				<div class="stories_title">
					<h2 class="opensans"><a href="<?php echo site_url('story/talkshow'); ?>">Inspirational Talk Show</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/talkshow'); ?>">Read this story</a></small>
				</div>

			</div>
			<div class="col-md-6 storiesbox storiesImage4">
				<img src="<?php echo $this->template->get_theme_path(); ?>img/story_default.jpg">
				<div class="stories_title twoRow">
					<h2 class="opensans"><a href="<?php echo site_url('story/financial'); ?>">Financial Literacy </br>in Rural Areas</a></h2>
					<small class="opensans"><a href="<?php echo site_url('story/financial'); ?>">Read this story</a></small>
				</div>

			</div>

		</div>

	</div>-->
	<!-- SOCIAL STORY END -->
	<div class="container">
		<div class="row chartData">
			<div class="col-md-6">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h2 class="opensans purple">Financing Portfolio</h2>

			<?php } else {?>
			<h2 class="opensans purple">Portofolio Finansial</h2>

			<?php }?>

				<div class="info3">
					<div class="graph">
					<canvas id="investChart3"></canvas>
					</div>

				<script type="text/javascript">
					var investData3 = {
					    labels: ["1st", "2nd ", "3rd ", "4th", "5th"],
					    datasets: [
					        {
					            label: "amount of investment last month",
					            fillColor: "#cd8cec",
					            strokeColor: "#cd8cec",
					            pointColor: "#cd8cec",
					            pointStrokeColor: "#fff",
					            pointHighlightFill: "#fff",
					            pointHighlightStroke: "rgba(151,187,205,1)",
					            data: [4320, 1996, 933, 652, 270]
					        }
					    ]
					};
				</script>

				</div>
			</div>

			<div class="col-md-6">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h2 class="opensans purple" style="padding-bottom:20px;">Financing Sector</h2>

			<?php } else {?>
			<h2 class="opensans purple" style="padding-bottom:20px;">Sektor Finansial</h2>

			<?php }?>
			<!-- DATA CHART 1 -->
			<div class="col-md-6" style="padding-bottom:20px;">

				<div id="canvas-holder">
					<canvas id="chart-area" width="500" height="500"/>
				</div>

				<script>
					var doughnutData = [
							{
								value: 62.69,
								color:"#F7464A",
								highlight: "#FF5A5E",
								label: "Commerce"
							},
							{
								value: 5.81,
								color: "#46BFBD",
								highlight: "#5AD3D1",
								label: "Agriculture"
							},
							{
								value: 0.60,
								color: "#FDB45C",
								highlight: "#FFC870",
								label: "Home Industry"
							},
							{
								value: 1.42,
								color: "#949FB1",
								highlight: "#A8B3C5",
								label: "Service"
							},
							{
								value: 12.82,
								color: "#32ff4a",
								highlight: "#66ff78",
								label: "Housing"
							},
							{
								value: 0.10,
								color: "#a64ca6",
								highlight: "##bf7fbf",
								label: "WatSan"
							},
							{
								value: 12.49,
								color: "#ffc0cb",
								highlight: "#ffccd5",
								label: "Education"
							},
							{
								value: 0.48,
								color: "#abcc00",
								highlight: "#d6ff00",
								label: "Healthcare"
							},
							{
								value: 3.59,
								color: "#4D5360",
								highlight: "#616774",
								label: "Other"
							},

						];



				</script>
				</div>
				<div class="col-md-6" style="margin-top:0;">
					<div id="js-legend" class="chart-legend"></div>
					</div>
					<style type="text/css">
					.doughnut-legend {
						width: 100%;
						/*margin-top: 40px;*/
						padding-top: 0;
					}

						.chart-legend li {
							list-style: none;
							text-align: left;
							height: auto:;
						}
						.chart-legend li span{
						    /*display: inline-block;*/
						    width: 50px;
						    height: 12px;
						    margin-right: 5px;
						}
					</style>

				<div class="doughnut-legend hidden">
					<ul>
						<li><span style="background-color:#F7464A;"></span>
							<h6>Commerce</h6>
						</li>
						<li><span style="background-color:#46BFBD;"></span>
							<h6>Agriculture</h6>
						</li>
						<li><span style="background-color:#FDB45C;"></span>
							<h6>Home Industry</h6>
						</li>
						<li><span style="background-color:#949FB1;"></span>
							<h6>Service</h6>
						</li>
						<li><span style="background-color:#32ff4a;"></span>
							<h6>Housing</h6>
						</li>
					</ul><ul>
						<li><span style="background-color:#a64ca6;"></span>
							<h6>Water and sanitation</h6>
						</li>

						<li><span style="background-color:#ffc0cb"></span>
							<h6>Education</h6>
						</li>
						<li><span style="background-color:#abcc00;"></span>
							<h6>Healthcare</h6>
						</li>

						<li><span style="background-color:#4D5360;"></span>
							<h6>Other</h6>
						</li>
					</ul>
				</div>

			</div> <!-- DATA CHART 1 END -->
			</div>







</div>
</div>
<!--
<section class="">
	<div class="container startContent" style="margin-bottom: 50px;">
		<h2>Join millions of other business who signed on Amartha</h2>
		<button class="btn btn-default" type="submit">SIGN UP FOR AMARTHA</button>
	</div>
</section>
-->



<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/jquery.circliful.min.js"></script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/Chart.min.js"></script>




<script type="text/javascript">

var persen = {
				scaleLabel : "<%= Number(value) + '%'%>",
				multiTooltipTemplate: "<%%=datasetLabel%> : <%%= value %>",
				tooltipTemplate: "<%= label+':' %> <%= Number(value).toFixed(2).replace('.', ',')  + '%'%>",
				responsive: [true]
					    };

var pembiayaan = {
	tooltipTemplate: "<%= label+':' %> Financing (<%=value%>)",
				responsive: [true]
};

function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{2})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

$(document).ready(function(){

		//var ctx2 = document.getElementById("investChart2").getContext("2d");
		var ctx3 = document.getElementById("investChart3").getContext("2d");
		var ctx4 = document.getElementById("chart-area").getContext("2d");





		// window.myLine = new Chart(ctx2).Line(investData2, {responsive : true});
		//window.myLine = new Chart(ctx2).Bar(investData2, {responsive : true});
		window.myBar = new Chart(ctx3).Bar(investData3,pembiayaan, {responsive : true});
		window.myDoughnut = new Chart(ctx4).Doughnut(doughnutData, persen, {responsive : true});

		document.getElementById('js-legend').innerHTML = myDoughnut.generateLegend();


	});

</script>
