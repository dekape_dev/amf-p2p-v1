    <div id="headerImageStoryDuck" >
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo.png"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent singleHead  clearfix">

				<div class="container">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>

					<h1 class="text-center">Duck Breeding: Local Entrepreneurship Training</h1>

					<?php } else {?>

					<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Ternak Bebek: Pelatihan Kewirausahaan Lokal</h2>

					<?php }?>

			    	
	    	</div>
	    </div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="about_WhoWeAre  text-justify ">

					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<p style="padding-top: 20px; ">From 2012 to 2015, Amartha Microfinance (Amartha) and the CSR Team of Bank Mandiri have cooperated in implementing the Local Entrepreneurship Training on duck cultivation. The programme aims to improve the knowledge and skills of rural communities in duck cultivation and eventually to increase their income level. As a microfinance institution, besides providing affordable financial services, Amatha has continued to aim at strengthening the resilience of rural communities through conducting supporting programmes such as this training.</p>

					<p style="padding-top: 20px; ">This programme has targeted Amartha’s clients known for their duck breeding activities such as those living in Babakan, Putatnutug, and Cigelap villages, Ciseeng District, Bogor Regency. Client are invited to actively participate during the training which covers a range of aspects from raising egg-laying ducks until the marketing process.</p>

					<p style="padding-top: 20px; ">Taking example of the Local Entrepreneurship Training done in 2014, the duck breeding process began in May 2014 where clients were divided into groups and were supplied with ducks as initial assets. The two groups received a total of 300 female ducks and 21 male ducks. Clients were taught about running duck breeding business with profit sharing mechanism.</p>

					<p style="padding-top: 20px; ">Not only has the training assisted the communities who raise egg-laying ducks, it has also assisted the communities who do egg hatching and are salted-egg producers. The market potential for salted eggs is massive since its demand has doubled the supply. So far, the communities have only been able to produce 300-1,000 eggs per week while the demand is actually double than that.</p>

					<p style="padding-top: 20px; ">This training on duck breeding has given positive impact on the clients as they are able to increase their household income through duck breeding. Moreover, by doing this as groups, clients have learnt to cooperate more with each other, so that the cohesiveness of rural communities in terms of economic aspect can be established.  </p>

					<?php } else {?>

					<p style="padding-top: 20px; ">Sejak 2013 hingga 2015, Amartha Microfinance (Amartha) dan CSR Mandiri bekerja sama dalam melaksanakan Program Pelatihan Kewirausahaan Lokal, Ternak Bebek. Program bertujuan untuk memberikan dukungan bagi masyarakat pedesaan untuk meningkatkan kemampuan dalam usaha ternak bebek dan taraf ekonomi. Amartha merupakan Lembaga Keuangan Mikro (LKM) yang mendampingi dan melayani masyarakat pedesaan dengan jasa keuangan terjangkau. Selain itu Amartha mendampingi dan melayani masyarakat dengan program sosial berbasis penguatan komunitas, salah satunya Ternak Bebek: Pelatihan Kewiausahaan Lokal. </p>

					<p style="padding-top: 20px; ">Program menargetkan kelompok masyarakat yang telah memiliki tradisi ternak bebek sebelumnya, seperti di daerah Bababkan, BKS, dan Cigelap, Kecamatan Ciseeng, Kabupaten Bogor. Masyarakat diajak untuk terlibat dari hulu ke hilir, seperti terlibat dalam proses pemeliharaan bebek petelur hingga proses pemasaran. </p>

					<p style="padding-top: 20px; ">Proses pemeliharaan bebek dimulai pada Mei 2014, dua kelompok yang terbentuk diberikan modal berupa bebek. Dua kelompok yang terbentuk mendapatkan bebek betina hingga 300 ekor dan bebek jantan 21 ekor. Masyarakat di kelompok pemelihara bebek bertelur diajarkan bisnis kelompok dengan mekanisme bagi hasil. </p>

					<p style="padding-top: 20px; ">Pendampingan masyarakat tidak hanya dengan pemeliharaan bebek petelur, Amartha juga mendampingi kelompok penetasan telur dan produksi telur asin. Untuk kelmpok telur asin potensi pasarnya sangat besar mengingat daya serap pasar lokal di sekitar tempat produksi. Sejauh ini kelompok masyarakat yang kami dampingi baru bisa memenuhi 300 – 1000 butir telur/minggu dari daya serap dua kali lipat.</p>

					<p style="padding-top: 20px; ">Ternak Bebek telah memberikan dampak positif bagi target, masyarakat prasejahtera dapat memperoleh penghasilan tambahan melalui ternak bebek. Selain itu melalui usaha kelompok, masyarakat semakin terlatih bekerjasama satu sama lain, sehingga lahirnya kohesivitas masyarakat desa dalam sektor ekonomi.</p>

					<?php }?>

					

			</div>


			</div>
			<div class="col-md-4"></div>
		</div>
	</div>

	<!-- <div id="gallery">
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/vote_1.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/vote_2.jpg" />
        <img data-src="<?php echo $this->template->get_theme_path(); ?>img/stories/vote_3.jpg" />

	</div> -->








		<script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.easing.1.3.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.touchSwipe.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.imagesloaded.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/jquery.scrollTo-1.4.3.1-min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/spin.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/gallery/portfolio.js" type="text/javascript"></script>

    <script type="text/javascript">
    $(document).ready(function() {
            var p = $("#gallery").portfolio();
            var q = $("#gallery2").portfolio();
            p.init();

    });
    </script>
