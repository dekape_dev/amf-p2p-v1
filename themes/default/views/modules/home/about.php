
<!-- HEADER -->
	<div id="headerImageAbout">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="100px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

			<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<!-- Hero Text -->
			<div class="startContent singleHead clearfix" style="align-center">
		    	<!--<h2>We aim to contribute in evolving the financial services for </br> micro  and small businesses by providing access  to innovative financing solutions.</h3>-->


		    	<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2 class="opensansBold" style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">We choose to go the extra mile to open <br>access to financial services for the underprivileged</h2>

				<?php } else {?>
				<h2 class="opensansBold" style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Kami berinovasi untuk mengembangkan layanan keuangan yang efisien <br/>sehingga pengusaha mikro dan UKM dapat dilayani dengan solusi pembiayaan <br/>yang mudah dan praktis</h2>
				<?php }?>



	  		 </div>
	    </div>
	</div>
	<!-- END OF HEADER -->


	<!-- <div class="container">
		<div class="about_WhoWeAre  text-justify ">
			<h1 class="opensans text-left purple" style="margin-bottom:20px;">Overview</h1>
			<p>Amartha was born with a belief that everyone, including those who are not privileged with access to banking services, should have the opportunity to start and grow their businesses. Hence, as a peer-to-peer lending marketplace, we connect micro and small businesses in Indonesia seeking affordable working capital with investors wanting to make impact investing.</p>
			<p>We support the micro businesses to, for instance, open their grocery stalls or expand their tofu home industry, by offering them with loans, ranging from IDR 1 million to IDR 20 million. These amounts may seem miniscule, but with the right approach, we believe that it could serve as an economic opportunity that increases welfare.</p>
			<p>At the same time, we attract people who wish to make investments which are both socially and financially rewarding. We transform micro-entrepreneurs into credit-worthy borrowers through our group lending model in which borrowers within the same group guarantee each other’s loans. Our credit scoring system also allows investors to assess risk and expected return, enabling them to make a fully-informed investment decision.</p>
		</div>
	</div> -->


	<div class="container">
			<div class=" fs-14 about_WhoWeAre  text-center ">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>


				<h1 class="opensans text-center purple" style="margin-bottom:20px;">Overview</h1>
				<p class="opensans">Amartha was born with a belief that everyone, including those who are not privileged with access to banking services, should have the opportunity to start and grow their businesses. Hence, as a peer-to-peer lending marketplace, we connect micro and small businesses in Indonesia seeking affordable working capital with investors wanting to make impact investing.</p>
				<!--<p class="opensans text-center" style="font-size:25px"><i> “We choose to go the extra mile to open access to financial services for the underprivileged”</i></p>-->
				<p class="opensans">We support the micro businesses to, for instance, open their grocery stalls or expand their tofu home industry, by offering them with loans, ranging from IDR 1 million to IDR 20 million. These amounts may seem miniscule, but with the right approach, we believe that it could serve as an economic opportunity that increases welfare.</p>
				<p class="opensans">At the same time, we attract people who wish to make investments which are both socially and financially rewarding. We transform micro-entrepreneurs into credit-worthy borrowers through our group lending model in which borrowers within the same group guarantee each other’s loans. Our credit scoring system also allows investors to assess risk and expected return, enabling them to make a fully-informed investment decision.</p>


				<?php } else {?>
				<h1 class="opensans text-center purple" style="margin-bottom:20px;">Overview</h1>
				<p class="opensans">Amartha lahir dengan keyakinan bahwa setiap orang memiliki kesempatan yang sama untuk membuka atau mengembangkan usaha, termasuk bagi mereka yang kurang beruntung dan tidak memiliki kelayakan untuk mendapatkan layanan perbankan. Oleh karena itu, peer-to-peer (P2P) lending platform kami membantu menghubungkan usaha mikro dan kecil di Indonesia dengan modal pembiayaan yang terjangkau dari para investor yang mengedepankan impact dan makna dalam berinvestasi.</p>
				<!--<p class="opensans text-center" style="font-size:25px"><i> “We choose to go the extra mile to open access to financial services for the underprivileged”</i></p>-->
				<p class="opensans"><br/><i>Bagi Peminjam</i> : peminjam akan dikelompokkan menjadi dua kategori, yaitu dengan pengajuan pembiayaan antara IDR 1 juta hingga IDR 20 juta dan kelompok peminjam dengan pengajuan antara IDR 20 juta hingga IDR 200 juta.</p>
				<p class="opensans"><br/><i>Bagi Investor</i> : melalui pembiayaan berbasis kelompok (group lending), kami memitigasi risiko dengan cara tanggung renteng antar sesama anggota. Sistem penilaian kami juga memungkinkan investor untuk mengukur risiko dan memprediksi imbal hasil investasi yang sesuai. Kami membantu membangun peminjam menjadi pengusaha mikro dan kecil yang berkualitas.</p>
				<?php }?>
			</div>

	</div>

	<div class="container hidden">
		<div class=" fs-14 about_WhoWeAre  text-justify ">
			<!-- <div class="col-md-6"></div> -->

			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h1 class="opensans text-left purple" >Lending Principles</h1>
			<ul style="padding-top:20px;list-style-type: circle;">
				<li class="opensans">Prevention of over-indebtedness</li>
				<li class="opensans">Responsible margin/profit-sharing pricing</li>
				<li class="opensans">Transparency</li>
				<li class="opensans">Treating borrowers and investors fairly</li>
				<li class="opensans">Privacy and data security</li>
				<li class="opensans">Timely response for feedback and support</li>
			</ul>

			<?php } else {?>
			<h1 class="opensans text-left purple" >Prinsip Pembiayaan</h1>
			<ul style="padding-top:20px;list-style-type: circle;">
				<li class="opensans">Pencegahan kelebihan hutang</li>
				<li class="opensans">Margin yang dapat dipertanggungjawabkan</li>
				<li class="opensans">Keterbukaan</li>
				<li class="opensans">Memperlakukan investor dan peminjam secara berkeadilan</li>
				<li class="opensans">Kerahasiaan dan keamanan data terjamin</li>
				<li class="opensans">Respon dan dukungan yang cepat </li>
			</ul>
			<?php }?>
		</div>
	</div>
	<div class="container">
		<div class="row lendContent">

			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h1 class="opensans text-center purple" style="font-weight:300;" >Lending Principles</h1>
			<?php } else {?>
			<h1 class="opensans text-center purple" style="font-weight:300;" >Prinsip Pembiayaan</h1>
			<?php }?>


			<div class="col-md-2">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/lend/prevention.png">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<p class="p-t-10">Prevention of over-indebtedness</p>
				<?php } else {?>
				<p class="p-t-10">Pencegahan kelebihan hutang</p>
				<?php }?>
			</div>
			<div class="col-md-2">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/lend/profitmargin.png">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<p class="p-t-10">Responsible margin/profit-sharing pricing</p>
				<?php } else {?>
				<p class="p-t-10">Margin yang dapat dipertanggungjawabkan</p>
				<?php }?>
			</div>
			<div class="col-md-2">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/lend/tranparency.png">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<p class="p-t-10">Transparency</p>
				<?php } else {?>
				<p class="p-t-10">Keterbukaan</p>
				<?php }?>
			</div>
			<div class="col-md-2">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/lend/fair.png">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<p class="p-t-10">Treating borrowers and investors fairly</p>
				<?php } else {?>
				<p class="p-t-10">Memperlakukan investor dan peminjam secara berkeadilan</p>
				<?php }?>
			</div>
			<div class="col-md-2">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/lend/secure.png">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<p class="p-t-10">Privacy and data security</p>
				<?php } else {?>
				<p class="p-t-10">Kerahasiaan dan keamanan data terjamin</p>
				<?php }?>
			</div>
			<div class="col-md-2">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/lend/support.png">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<p class="p-t-10">Timely response for feedback and support</p>
				<?php } else {?>
				<p class="p-t-10">Respon dan dukungan yang cepat </p>
				<?php }?>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="about_WhoWeAre text-center">
		<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		<h1 class="opensans text-center purple" >Team</h1>
		<p class=" fs-14 opensans">Our team serve and work along with the micro and small businesses to unleash their potentials and seize opportunities or create them. Our people are passionate about creating an environment in which both investors and borrowers can inspire each other towards achieving a more meaningful life.</p>
		<p class=" fs-14 opensans">We aim to bridge millions of businesses with the funds they need to grow. Our work also focuses on assisting them to define success on their own terms, to make plans, and to translate them into actions. We believe that we grow and evolve for a greater good. </p>

		<?php } else {?>
		<h1 class="opensans text-center purple" >Tim</h1>
		<p class=" fs-14 opensans">Tim kami melayani dan bekerjasama dengan usaha mikro dan kecil untuk mendukung mereka menciptakan potensi dan meraih peluang usaha. Kami antusias menciptakan sebuah lingkungan dimana investor dan peminjam dapat berbagi gagasan untuk meraih kehidupan yang lebih bermakna.</p>
		<p class=" fs-14 opensans">Kami bekerja keras untuk menjembatani jutaan bisnis, dengan dana yang dibutuhkan, untuk terus tumbuh. Kami fokus terhadap investor dan peminjam dalam menentukan kesuksesaan yang ingin diraih, membuat perencanaan, dan mewujudkannya. Kami percaya bahwa kami tumbuh dan berkembang untuk kebaikan yang lebih besar.</p>
		<?php }?>
		</div>
	</div>

	<div class="container">
		<div class="about_WhoWeAre  text-justify ">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h1 class="opensans text-center purple" style="margin-bottom:20px;">Founder’s Story</h1>
				<p class=" fs-14 opensans"style="text-align:center">It was in 2009 when I met a middle-aged woman living in Ciseeng Village, Bogor, whose home-based business went bankrupt after spending all her money on medication for her ailing son. She needed IDR 500,000 to re-open her small shop and I lent her the money with agreement that she would repay the loan with some portion of her business profit, and yes, she could meet that agreement. Funny enough, not only has that experience changed her life, but also mine. It struck me how small amount of money could make a positive difference to someone’s life.</p>
				<p class="opensans text-center" style="font-size:21px"><i>"It struck me how small amount of money could make a positive difference to someone’s life."</i></p>
				<p class=" fs-14 opensans"style="text-align:center">I continued to support five other micro-businesses and then thought of really developing and expanding a microfinance model, aiming to empower more people and to create greater impacts. I decided to run my pilot project in Ciseeng Village, Bogor Regency, where the majority of the villagers had no access to banking services and were often prone to predatory money lending activities that charged double digit interest per month. Through this pilot project it became clear to me that providing capital to the rural communities and working alongside with multiple stakeholders may catalyse lasting change for the lives of the underprivileged communities. With this in mind, Amartha was founded in April 2010.</p>
				<p class=" fs-14 opensans"style="text-align:center">The invaluable experience of having to serve more than 15,000 borrowers has taught us that access to finance with a mechanism that is being well monitored and carefully evaluated can improve many lives. Also, it has taught us that bridging inequality should be a movement through which everyone can participate in empowering the underprivileged and this rationale has led to Amartha’s latest transformation into a peer-to-peer lending marketplace.</p>

				<?php } else {?>
				<h1 class="opensans text-center purple" style="margin-bottom:20px;">Tentang Pendiri</h1>
				<p class=" fs-14 opensans"style="text-align:center">Bermula pada 2009, saya bertemu dengan seorang ibu paruh baya yang tinggal di Desa Ciseeng, Bogor. Warung kelontongnya terpaksa tutup karena seluruh uang yang ia miliki habis untuk membayar pengobatan anaknya. Ibu tersebut membutuhkan Rp 500.000 untuk kembali membuka warung kecilnya. Saya meminjamkan dengan kesepakatan bahwa ia akan mengembalikan beserta bagi hasil dari keuntungan usaha. Ia setuju.</p>
				<p class="opensans text-center" style="font-size:21px"><i>"Cukup lucu, pengalaman tersebut mengubah hidupnya dan juga saya. Hal itu mengejutkan saya, bagaimana jumlah yang sedikit bisa memberi dampak positif bagi kehidupan orang lain."</i></p>
				<p class=" fs-14 opensans"style="text-align:center">Saya memutuskan untuk mengawali proyek di Desa Ciseeng dimana mayoritas penduduknya tidak memiliki akses layanan perbankan dan rentan pada praktik rentenir yang membebankan hutang hingga dua kali lipat. Proyek awal tersebut membuat saya semakin yakin bahwa dengan menyediakan modal bagi masyarakat pedesaan dan bekerja bersama dengan berbagai pemangku kepentingan dapat membuat sebuah perubahan berkelanjutan bagi kehidupan masyarakat prasejahtera. Berjalan dari pemikiran tersebut, Amartha resmi didirikan pada April 2010. </p>
				<p class=" fs-14 opensans"style="text-align:center">Pengalaman berharga dengan melayani lebih dari 20.000 anggota telah mengajarkan kami bahwa akses keuangan dengan mekanisme pengawasan dan evaluasi cermat dapat meningkatkan kehidupan banyak orang. Hal tersebut juga mengajarkan kami bahwa menjembatani kesenjangan sosial seharusnya menjadi sebuah gerakan dimana semua orang dapat berpartisipasi untuk memberdayakan masyarakat. Pemikiran ini yang mendorong Amartha bertransformasi dari lembaga keuangan mikro menjadi peer-to-peer lending.</p>
				<?php }?>
		</div>
		<div class="col-md-4 founderPic e-sign ">
				<h3 class="text-center">Andi Taufan Garuda Putra</h3>
		</div>

	</div>

	<div class="container partnerContent no-mobile">
		<div class="about_WhoWeAre  text-justify ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h1 class="opensans text-center purple" style="margin-bottom:20px;">Awards</h1>
			<?php } else {?>
			<h1 class="opensans text-center purple" style="margin-bottom:20px;">Penghargaan</h1>

			<?php }?>
		</div>
		<div class="row">
			<div class="col-md-12 ">
				<img width="100%" src="<?php echo $this->template->get_theme_path(); ?>/img/awards.jpg">
			</div>
		</div>
	</div>

	<div class="container partnerContent mobile">
		<div class="about_WhoWeAre  text-justify ">
			<h1 class="opensans text-center purple" style="margin-bottom:20px;">Awards</h1>
		</div>
		<div class="row">
			<div class="col-md-2 awardPic square logo_partners">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/award_1.png">
			</div>
			<div class="col-md-2 awardPic square logo_partners">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/award_2.png">
			</div>
			<div class="col-md-2 awardPic square logo_partners">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/award_3.png">
			</div>
			<div class="col-md-2 awardPic square logo_partners">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/award_4.png">
			</div>
			<!--
			<div class="col-md-2 awardPic square logo_partners">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/award_5.png">
			</div>
			<div class="col-md-2 awardPic logo_partners">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/award_6.png">
			</div>-->
			<div class="col-md-2 awardPic square logo_partners">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/award_7.png">
			</div>
			<div class="col-md-2 awardPic square1 logo_partners">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/award_8.png">
			</div>
		</div>
	</div>
</div>
