    <div id="headerImageStory" style="background: url(<?php echo $this->template->get_theme_path(); ?>/img/bg_homepage.jpg) no-repeat center bottom fixed;background-size:cover;">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="110px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

        	<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english'){ ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<div class="startContent clearfix">

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2>Amartha help business grow faster. We know because they tell us everyday</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">SIGN UP FOR AMARTHA</a>
				
				<?php } else {?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Amartha membantu bisnis tumbuh lebih cepat <br/>Kami tahu karena mereka menceritakannya setiap hari</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">Mulai Investasi Sekarang</a>
				
				<?php } ?>

		    	<h3>&nbsp;</h3>
		    	<!--<button class="btn btn-default" type="submit">SIGN UP FOR AMARTHA</button>-->
	    	</div>
		</div>
	</div>
	<div class="container text-center">
				<h2 class="text-center purple"><b>Nyai: Terus Mandiri dengan Usaha Tahu</b></h2>
				<p class="opensans" style="padding-bottom:20px;">Nyai Muhayati (48 tahun) memulai usaha tahu sejak 1984. Asam-manis usaha ia alami bersama almarhum suaminya. Pernah usahanya rugi sama sekali karena adonan (?) tahunya gagal. Modalnya habis. Ia cerdik, Nyai berhutang pada tukang kacang kedelai dan berjanji akan mengembalikannya sesegera dagangannya laris manis.
				“Yang penting mah kita dapet kepercayaan dari orang lain,” jelas Nyai. </p>

				<p class="opensans" style="padding-bottom:20px;">Asamnya usaha tidak menjadi halangan bagi Nyai, menurutnya selama ada kemauan dan kerja keras, modal akan datang. Seperti relasinya dengan Amartha yang berjalan empat tahun. Nyai telah mampu memproduksi tahu lebih kurang empat ribu potong perharinya, ia mampu meraih omset Rp 1.000.000/dua hari. Lebih lagi, ia mampu menggaji empat orang anak buah. </p>

				<h3 class="text-center purple"><b>Kerja Keras </b></h3>

				<p class="opensans" style="padding-bottom:20px;">Nyai pertama kali mengenal usaha tahu dari mertua. Dahulunya mertua Nyai adalah pedagang tahu yang ulet. Ia membuat dan menjual sendiri tahunya. Saat berkeluarga, Nyai dan suami memang tidak langsung usaha tahu karena modal usaha yang lumayan. Kedelai dan alat-alat untuk membuat tahu belum juga ia mampu beli. Usahanya pertama kali diawali dengan membuat oncom (makanan tradisional yang berasal dari ampas tahu). </p>

				<p class="opensans" stule="padding-bottom:20px;">Semakin lama tabungan Nyai dan suami semakin meningkat. Ia putuskan untuk memulai usaha tahu. Ia membuat dan menjual tahu bersama dengan suaminya. Nyai memiliki empat orang anak. Suaminya meninggal ketika anaknya masih remaja bahkan anak terakhirnya masih usia SD. Tidak ada pilihan lain, Nyai harus terus bekerja keras demi menghidupi diri dan anaknya. </p>

				<p class="opensans" stule="padding-bottom:20px;">Usaha tahu Nyai sekarang dijalankan bersama dua orang anaknya, karena satu orang anaknya meninggal sementara satu anaknya yang lain memilih untuk menjadi buruh bangunan. Mereka bekerja kerjas untuk menghasilkan 4.000 tahu/produksi, serta menjualnya di keesokan harinya. </p>

				<h3 class="text-center purple"><b>Asam Manis Usaha</b></h3>

				<p class="opensans" style="padding-bottom:20px;">“Saya pernah gak dapet untung sama sekali, boro-boro untung, orang tahunya gak jadi sama sekali. Kalau diuangin kedelainya aja abis Rp 300.000. Kuli kan tetep harus dibayar walaupun juga tahunya gagal,” ingat Nyai ketika usahanya sedang benar-benar payah. </p>

				<p class="opensans" style="padding-bottom:20px;">Akan tetapi, ia masih bersyukur, selama ini ia memiliki kepercayaan dari tukang kacang kedelai dimana ia berlangganan. Akhirnya, ia memutuskan berhutang. Jangan sampai besok tidak produksi karena tidak ada uang. Ia berpikir dapurnya harus tetap ngebul, karena dari sembilan orang yang hidup bersamanya di rumah (anak dan cucu), Nyailah yang selama ini berkontribusi besar. Nyai juga tidak pernah lupa, ada saat manis, dimana usahanya bisa memberinya banyak pemasukan. Hal tersebut ketika bulan Ramadhan dan setelah lebaran. Angka permintaan pasar meningkat, Nyai lincah menyambut momen tersebut. </p>

				<p class="opensans" style="padding-bottom:20px;">“Di bulan Ramadhan dan abis lebaran, alhamdulillah, itu permintaan tahu banyak. Seharinya saya bisa bikin sampe 50 – 80 KG, bahkan pernah juga bikin sampe 1 kuintal,” senyum terkembang selama Nyai bercerita. Bukan tanpa tantangan tentu, saat permintaan banyak pekerja Nyai yang mayoritas berusia muda justru memilih untuk tidak bekerja dulu karena main dengan sebayanya. Nyai segera mencari solusi lain, ia mengumpulkan orang-orang dengan usia tua untuk menjadi pekerja dadakan. Di masa itu ia mampu menggaji pekerjanya hingga dua kali lipat, yaitu Rp 200.000. </p>
				<p class="opensans" style="padding-bottom:20px;">“Alhamdulillah, karena permintaan emang ada dan rezekinya juga ada. Ketemu aja itu mah untuk bayar kuli,” imbuh Nyai. Di masa ini, Nyai bijak. Kelebihan keuntungan langsung ia belikan bahan modal untuk disimpan. Ia membeli kacang kedelai dengan bobot lebih dari biasanya. Ia juga memastikan kayu bakar (sebagai bahan masak utama) tersedia hingga satu – dua mobil kol. </p>


				<h3 class="text-center purple"><b>Modal, Halangan? </b></h3>
				<p class="opensans" style="padding-bottom:20px;">Sepanjang usahanya, Nyai merasa modal bukan menjadi halangan. Ia pertama kali membangun usaha dari hasil tabungan. Selain itu karena kemampuan Nyai menjaga amanah, ia dipercaya saja oleh tukang kacang kedelai untuk berhutang di masa darurat. Semenjak mengenal Amartha, Nyai pun merasa sangat senang. Bergabung selama empat tahun Nyai merasa Amartha sangat membantu usahanya untuk terus tumbuh. </p>

				<p class="opensans" style="padding-bottom:20px;">Di awal ia mendaptkan Rp 1.000.000 yang ia gunakan untuk membeli kacang kedelai. Di tahun kedua, dengan modal pembiayaan Rp 2.000.000 ia membeli kacang, kayu, garam, dan kebutuhan usaha lainnya. Dengan pembiayaan maksimal Rp 3.000.000 di tahun ketiga dirinya membeli dua karung kacang kedelai dan drum baru untuk memasak. </p>

				<p class="opensans" style="padding-bottom:20px;">Nyai adalah anggota yang menyadari betul bahwa pembiayaan dari Amartha adalah sebuah amanah. Ia mengaku tidak mau mengambil pembiayaan dari lembaga lain karena untuk usaha berkembang cukup satu saja tetapi terus meningkat. </p>

				<p class="opensans" style="padding-bottom:20px;">Di tahun terakhir ini Nyai mendapatkan pembiayaan maksimal Rp 4.500.000, ia gunakan untuk membeli mesin diesel. Selama empat tahun bersama Amartha, Nyai merasa modal bukanlah halangan untuk memajukan usaha. Lebih lagi menurut Nyai, Amartha sangat memperhatikan anggotanya. Ia senang dengan program seperti Sembako yang memungkinkan dirinya mendapatkan sembako dengan harga murah dan dapat dicicil. “Sangat meringankan!” imbuh Nyai. </p>

				<h3 class="text-center purple"><b>Cita-cita Kedepan </b></h3>

				<p class="opensans" style="padding-bottom:20px;">Dengan pembiayaan selanjutnya, Nyai masih memiliki cita-cita untuk mengembangkan usaha. Ia ingin memluaskan bengkel tahunya, menambah karyawan, dan menambah produksi tahu. Menurut Nyai pasar usaha tahu masih banyak, jadi masih bisa untuk meningkatkan jumlah produksi. Meski sudah tidak muda lagi, Nyai juga masih ingin produktif, dengan produktif ia bisa tetap mandiri, tidak bergantung pada orang lain. “Mudah-mudahan Allah terus tambahin umur saya, jadi saya bisa ngembangin usaha lagi,” tandas Nyai di akhir ceritanya. </p>


	</div>
