
<!-- HEADER -->
	<div id="headerImage">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="100px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

			<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<!-- Hero Text -->
			<div class="startContent clearfix">

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Creating opportunity for micro and small businesses to grow</h2>
		    	<a href="<?php echo site_url('investor');?>" class="button_main btn btn-default" type="submit">Start Investing Today</a>

				<?php } else {?>
				<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">IDR 30 miliar disalurkan, 20.000 peminjam, 0% NPL selama 5 tahun <br/>Melalui peer-to-peer (P2P) lending, kami membangun sarana untuk meraih kesempatan</h2>
		    	<a href="<?php echo site_url('register');?>" class="button_main btn btn-default" type="submit">Mulai Investasi Sekarang</a>
				<?php }?>



	    </div>
	    </div>
	</div>
	<!-- END OF HEADER -->

	<!-- AMARTHA -->
	<div class="container storiesSquare">
		<!-- GANTI IMAGESNYA DI CSS YAA.. JANGAN DIPAKSA DISNI -->
		<div class="row" style="padding-top:30px;">
			<div class="col-md-6" style="text-align:center;" >
				<div class=" picStory8 overlay black "style="width:100%;">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white justMe ">Free Glasses</h2>
					<h2 class="hd white justMe">Free Glasses to Increase Women Productivity</h2>
					<a href="<?php echo site_url('story/glasses'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white justMe ">Kacamata Gratis</h2>
					<h2 class="hd white justMe">Kacamata Gratis Untuk Produktivitas Perempuan</h2>
					<a href="<?php echo site_url('story/glasses'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>

				</div>
			</div>
			<div class="col-md-6" >
				<div class=" picStory1 overlay black " style="width:100%;">
					<?php if ($this->session->userdata('site_lang') == 'english') { ?>
					<h2 class="hs white justMe ">Enah</h2>
					<h2 class="hd white justMe">Micro-entrepreneur with a Big Vision</h2>
					<a href="<?php echo site_url('story/story'); ?>"><h5 class="hd white">See this story <i class=" fa fa-angle-right	"></i></h5></a>

					<?php } else {?>
					<h2 class="hs white justMe ">Enah</h2>
					<h2 class="hd white justMe">Pengusaha Kecil Bervisi Besar</h2>
					<a href="<?php echo site_url('story/story'); ?>"><h5 class="hd white">Simak cerita ini <i class=" fa fa-angle-right	"></i></h5></a>

					<?php }?>

				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row text-center" style="">
			<div class="col-md-6">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h3><strong>We empower <br>beyond financial services</strong></h3>
				<h4><a href="<?php echo site_url('story/glasses'); ?>">See Their Stories &gt;</a></h4>

				<?php } else {?>
				<h3><strong>Kami melakukan pemberdayaan, <br>melebihi sekedar layanan keuangan</strong></h3>
				<h4><a href="<?php echo site_url('story/glasses'); ?>">Simak cerita mereka &gt;</a></h4>

				<?php }?>

			</div>

			<div class="col-md-6">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h3 style=""><strong>We help more <br>entrepreneurs to grow their businesses</strong></h3>
				<h4><a href="<?php echo site_url('story/story'); ?>">See their stories &gt;</a></h4>

				<?php } else {?>
				<h3 style=""><strong>Kami membantu lebih banyak wirausahawan <br/>untuk memajukan usaha mereka</strong></h3>
				<h4><a href="<?php echo site_url('story/story'); ?>">Simak cerita mereka &gt;</a></h4>

				<?php }?>

			</div>
		</div>
	</div>
	<section class="">

		<div class="container">
		<div class=" careerContent  text-center ">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<p class="opensans fs-14 center-justified" style="padding-top: 20px; ">Departing from the traditional loans and investing options, we provide a peer-to-peer lending marketplace where micro and small businesses looking for working capital are connected with those who wish to invest and create positive impact.</p>
			<p class="opensans fs-14 center-justified" style="padding-top: 20px; ">We utilize technology to lower our operational cost. This way, we ensure that our borrowers enjoy better rates on their savings accounts and our investors receive the economic returns they desire.</p>

			<?php } else {?>
			<p class="opensans fs-14 center-justified" style="padding-top: 20px; ">Berbeda dengan cara pembiayaan tradisional, peer-to-peer (P2P) lending adalah teknologi marketplace untuk menghubungkan pengusaha mikro dan UKM yang memerlukan modal dengan para investor yang ingin mendanai usaha tersebut sesuai dengan profil risiko dan imbal hasil yang sesuai.</p>
			<p class="opensans fs-14 center-justified" style="padding-top: 20px; ">Kami memanfaatkan teknologi untuk mendekatkan peminjam dan investor serta untuk mengurangi biaya operasional. Dengan demikian, kami memastikan peminjam menikmati rate yang lebih baik serta investor mendapatkan imbal hasil sesuai harapan mereka.</p>

			<?php }?>

	</div>
	<div>
		<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		<h1 class="text-center bold" style="padding-top:20px;">Here’s how it works:</h1>
		
		<?php } else {?>
		<h1 class="text-center bold" style="padding-top:20px;">Cara Kerja Kami:</h1>

		<?php }?>

			<div class="row methodList">
				<div class="col-md-4 col-xs-4">
					<div class="row">
						<div class="col-md-3">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/iconHIW_1.png">

						</div>
						<div class="col-md-9	">
						<?php if ($this->session->userdata('site_lang') == 'english') { ?>
						<p class="fs-18">Borrowers select a loan amount, duration/tenor, and purpose that work best for them.</p>
						
						<?php } else {?>
						<p class="fs-18">Peminjam memilih jumlah pembiayaan, tenor cicilan serta tujuan pembiayaan yang sesuai.</p>
						
						<?php }?>

						</div>

					</div>
				</div>
				<div class="col-md-4 col-xs-4">

					<div class="row">
						<div class="col-md-3">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/iconHIW_2.png">

						</div>
						<div class="col-md-9	">
						<?php if ($this->session->userdata('site_lang') == 'english') { ?>
						<p class="fs-18">Investors examine the loan listings, pick those that meet their criteria, and invest accordingly.</p>

						<?php } else {?>
						<p class="fs-18">Pada marketplace kami, investor memilih daftar peminjam yang ingin mereka danai sesuai dengan kriteria, kemudian mulai menempatkan dananya pada pilihan tersebut.</p>

						<?php }?>
						</div>
					</div>

				</div>
				<div class="col-md-4 col-xs-4">

					<div class="row">
						<div class="col-md-3">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/iconHIW_3.png">

						</div>
						<div class="col-md-9	">
						<?php if ($this->session->userdata('site_lang') == 'english') { ?>
						<p class="fs-18">The investments made are followed with monthly payments from borrowers and a share of those payments being directly transferred to their respective Amartha accounts.</p>

						<?php } else {?>
						<p class="fs-18">Investasi yang dilakukan akan diikuti dengan pembayaran cicilan bulanan dari peminjam, yang akan ditransfer ke rekening investor di Amartha.</p>

						<?php }?>

						</div>
					</div>

				</div>
			</div>
	</div>
	</section>


	<section class="method hidden">
		<div class="container" style="border-bottom: 1px solid #eeeeee;">
			<div class="row Content1  text-center">
				<!--<h2 class="opensans purple">Expanding Outreach, Delivering Happiness</h2>-->
				<p class="hidden">We are a microfinance institution that provides financial services to the low-income people living in rural Indonesia who have limited access to affordable and quality financial services or even limited knowledge about such services. We works towards an empowering and sustainable banking system that is based on mutual trust, accountability, participation, and creativity.</p>
				<div class="row stories clearfix">
					<div class="col-md-6 storiesImage4">
							<img src="<?php echo $this->template->get_theme_path(); ?>/img/story_kacamata1.jpg">
						<h3><strong>We empower <br>beyond financial services</strong></h3>
						<h4><a href="<?php echo site_url('story/glasses'); ?>">See Their Stories ></a></h4>
						<div class="stories_title">
							<h2 class="opensans"><a href="<?php echo site_url('story/glasses'); ?>">Free Glasses to </br> Increase Women Productivity</a></h2>
							<small class="opensans"><a href="<?php echo site_url('story/glasses'); ?>">Read this story</a></small>
						</div>
					</div>
					<div class="col-md-6 storiesImage4">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/story_2.jpg">
						<h3 style="padding-right:80px;"><strong>We help more <br>entrepreneurs to grow their business</strong></h3>
						<h4><a href="<?php echo site_url('story/story'); ?>">See their stories ></a></h4>
						<div class="stories_title">
								<h2 class="opensans"><a href="<?php echo site_url('story/story'); ?>">Enah: Micro-entrepreneur <br>with a Big Vision</a></h2>
								<small class="opensans"><a href="<?php echo site_url('story/story'); ?>">Read this story</a></small>
						</div>
					</div>
						<div class="row stories clearfix hidden">
							<div class="col-md-6">
								<img src="<?php echo $this->template->get_theme_path(); ?>/img/stories1.jpg">
								<h3><strong>Social Project Story</strong></h3>
								<h4><a href="<?php echo site_url('story'); ?>">See Their Stories ></a></h4>
							</div>
							<div class="col-md-6">
								<img src="<?php echo $this->template->get_theme_path(); ?>/img/stories2.jpg">
								<h3><strong>People Story</strong></h3>
								<h4><a href="<?php echo site_url('story'); ?>">See Their Stories ></a></h4>
							</div>
						</div>

				</div><!-- <h4><a href="">See Their Stories</a></h4> -->
			</div>
	    </div>
	</section>
	<!-- END OF AMARTHA -->

	<!-- NEW METHOD -->
	<div class="container methodTitle">
		<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		<h1 class="text-center">We Make Investment Socially and Financially Rewarding</h1>

		<?php } else {?>
		<h1 class="text-center">Kami Menjadikan Berinvestasi Menguntungkan <br/>secara Finansial maupun Sosial</h1>

		<?php }?>

			<div class="row methodList">
				<div class="col-md-4 col-xs-4">
					<div class="row">
						<div class="col-md-3">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon1-new.png">

						</div>
						<div class="col-md-9	">
						<?php if ($this->session->userdata('site_lang') == 'english') { ?>
						<p class="fs-20">Impact investing – Invest in micro and small businesses</p>

						<?php } else {?>
						<p class="fs-20">Berinvestasi untuk impact pada usaha mikro dan UKM</p>

						<?php }?>

						</div>

					</div>
				</div>
				<div class="col-md-4 col-xs-4">

					<div class="row">
						<div class="col-md-3">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon2-new.png">

						</div>
						<div class="col-md-9	">
						<?php if ($this->session->userdata('site_lang') == 'english') { ?>
						<p class="fs-20">Proven – Consistent profitability from credit worthy borrowers</p>

						<?php } else {?>
						<p class="fs-20">Keuntungan yang konsisten dan terbukti, dari para peminjam berkualitas</p>

						<?php }?>

						</div>
					</div>

				</div>
				<div class="col-md-4 col-xs-4">

					<div class="row">
						<div class="col-md-3">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon3-new.png">

						</div>
						<div class="col-md-9	">
						<?php if ($this->session->userdata('site_lang') == 'english') { ?>
						<p class="fs-20">Investment diversification</p>

						<?php } else {?>
						<p class="fs-20">Diversifikasi investasi</p>

						<?php }?>

						</div>
					</div>

				</div>
			</div>
			<div class="methodButton">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<a href="<?php echo site_url();?>investor"><button> Learn about Investment</button></a>

				<?php } else {?>
				<a href="<?php echo site_url();?>investor"><button> Pelajari cara Berinvestasi</button></a>

				<?php }?>
			</div>
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h1 class="text-center">We Make Investment Socially and Financially Rewarding</h1>

			<?php } else {?>
			<h1 class="text-center">Kami Membantu Peminjam Mikro dan UKM menjadi Pengusaha yang Terpercaya dan Berkualitas</h1>

			<?php }?>

			<div class="row methodList">
				<div class="col-md-4 col-xs-4">
					<div class="row">
						<div class="col-md-3">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon4-new.png">

						</div>
						<div class="col-md-9	">
						<?php if ($this->session->userdata('site_lang') == 'english') { ?>
						<p class="fs-20">Receive loan with affordable rate</p>

						<?php } else {?>
						<p class="fs-20">Menerima pembiayaan dengan rate yang terjangkau</p>

						<?php }?>

						</div>

					</div>
				</div>
				<div class="col-md-4 col-xs-4">

					<div class="row">
						<div class="col-md-3">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon5-new.png">

						</div>
						<div class="col-md-9	">
						<?php if ($this->session->userdata('site_lang') == 'english') { ?>
						<p class="fs-20">Build rating to receive bigger loan size</p>

						<?php } else {?>
						<p class="fs-20">Membangun credit rating agar bisa mendapatkan pembiayaan yang lebih besar</p>

						<?php }?>

						</div>
					</div>

				</div>
				<div class="col-md-4 col-xs-4">

					<div class="row">
						<div class="col-md-3">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon6-new.png">

						</div>
						<div class="col-md-9	">
						<?php if ($this->session->userdata('site_lang') == 'english') { ?>
						<p class="fs-20">Secure process, quick, and easy</p>

						<?php } else {?>
						<p class="fs-20">Proses aman, cepat dan mudah</p>

						<?php }?>

						</div>
					</div>

				</div>
			</div>
			<div class="methodButton">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<a href="<?php echo site_url();?>borrow/under20mil"><button> Learn about Borrowing</button></a>

				<?php } else {?>
				<a href="<?php echo site_url();?>borrow/under20mil"><button> Pelajari cara Meminjam</button></a>

				<?php }?>

			</div>
		</div>
	</div>
	<!-- NEW METHOD END -->

	<!-- METHOD -->
	<!-- <section class="method">
	    <div class="container" style="border-bottom: 1px solid #eeeeee;">
	    	<div class="row Content2">
	    		<div class="col-md-6">
	    			<h2 class="opensans purple">We make investment socially and financially rewarding</h2>


	    			<div class="row ContentBox">
	    				<div class="col-md-4 icons">
	    					<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon1.png" style="width:100px;height:auto;">
	    				</div>
	    				<div class="col-md-8">
	    					<h3>Impact investing – Invest in micro and small businesses</h3>
	    				</div>
	    			</div>

	    			<div class="row ContentBox">
	    				<div class="col-md-4 icons">
	    					<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon2.png"style="width:100px;height:auto;">
	    				</div>
	    				<div class="col-md-8">
	    					<h3>Proven – Consistent profitability from credit worthy borrowers</h3>
	    				</div>
	    			</div>

	    			<div class="row ContentBox">
	    				<div class="col-md-4 icons">
	    					<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon3-new.png"style="width:100px;height:auto;">
	    				</div>
	    				<div class="col-md-8">
	    					<h3>Investment diversification</h3>
	    				</div>
	    			</div>
	    		</div>

	    		<div class="col-md-6">

	    			<h2 class="opensans purple" style="padding-bottom:20px;">We build bridge for growth</h2>

	    			<div class="row ContentBox ">
	    				<div class="col-md-4 icons">
	    					<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon4.png">
	    				</div>
	    				<div class="col-md-8 "><h3>Receive loan with affordable rate</h3></div>
	    			</div>

	    			<div class="row ContentBox">
	    				<div class="col-md-4 icons">
	    					<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon5.png">
	    				</div>
	    				<div class="col-md-8"><h3>Build rating to receive bigger loan size</h3></div>
	    			</div>

	    			<div class="row ContentBox">
	    				<div class="col-md-4 icons">
	    					<img src="<?php echo $this->template->get_theme_path(); ?>/img/icon6.png">
	    				</div>
	    				<div class="col-md-8"><h3>Secure process, quick, and easy</h3></div>
	    			</div>

	    		</div>
	    	</div>
		</div>
    </section> -->
	<!-- END OF METHOD -->

	<link href="<?php echo $this->template->get_theme_path(); ?>grid/css/plugins-99b2e167.css" rel="stylesheet">

    <div class="no-mobile" style="position:absolute; z-index:999; width:100%; margin-top:30px;">
		<div style="padding-top:43px; padding-bottom:22.001%;height:0px;" class="container p-b-100 maksa" >

        <!--<h5 class="text-white opensans">Our Impacts</h5>-->

          </div>
        </div>
		</div>
		</div>

	<div id="wrap" class="no-mobile">

        <main>


            <a class="anchor" id="culture-anchor"></a>
            <section class=" centered ">

                <div class="no-mobile ri-grid ri-grid-size-2" id="ri-grid">
                    <ul class="g">
                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/1.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/1.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/2.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/3.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/4.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/5.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/6.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/7.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/8.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/9.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/10.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/11.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/12.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/13.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/14.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/14.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/15.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/16.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/17.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/18.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/19.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/20.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/21.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/22.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/23.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/6.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/1.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/8.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/13.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/17.jpg"></a></li>

                        <li><a href="index.html#"><img alt="Amartha Impact"src="<?php echo $this->template->get_theme_path(); ?>grid/img/9.jpg"></a></li>
                    </ul>
                </div>

        </main>

        <script src="<?php echo $this->template->get_theme_path(); ?>grid/js/jquery-ui-41e1a9d0.js">
    </script>
    <script src="<?php echo $this->template->get_theme_path(); ?>grid/js/plugins-5f81aadd.js">
    </script>

    <script src="<?php echo $this->template->get_theme_path(); ?>grid/js/scripts-39fdf2e7.js">
    </script>




    </div>

	<!-- IMPACT MOBILE -->
	<section class="homeimpact p-t-100 opensans scroll-x-hidden mobile" data-pages-bg-image="<?php echo $this->template->get_theme_path(); ?>/img/bg_people.jpg" data-pages="parallax" style="background-image: url(&quot;<?php echo $this->template->get_theme_path(); ?>/img/bg_people.jpg&quot;); background-position: left 47.3398%; background-size:cover;height: 200px;">
      <div class="container p-b-100 hidden">
        <h5 class="text-white opensans">Our Impacts</h5>
        <div class="row">
          <div class="col-sm-6">
            <h1 class="m-t-5 text-white opensans">We take extra miles in order to arrive at the community improvement as a whole</h1>
          </div>
          <div class="col-sm-6">
            <div class="row m-t-15">

              <div class="col-sm-6">
                <div class="progress progress-small transparent-white m-t-15">
                  <div class="progress-bar progress-bar-white" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                  </div>
                </div>
                <h3 class="text-white no-margin opensans"><b>7091</b></h3>
                <p class="text-white opensans">Happy Families
                </p>
              </div>

              <div class="col-sm-6">
                <div class="progress progress-small transparent-white m-t-15">
                  <div class="progress-bar progress-bar-white" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                  </div>
                </div>
                <h3 class="text-white no-margin opensans"><b>7 billion</b></h3>
                <p class="text-white opensans">Loan Disbursed
                </p>
              </div>
			  <!--
              <div class="col-sm-6">
                <div class="progress progress-small transparent-white m-t-15">
                  <div class="progress-bar progress-bar-white" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                  </div>
                </div>
                <h3 class="text-white no-margin opensans"><b>255</b></h3>
                <p class="text-white opensans">Borrower Available
                </p>
              </div>

              <div class="col-sm-6">
                <div class="progress progress-small transparent-white m-t-15">
                  <div class="progress-bar progress-bar-white" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                  </div>
                </div>
                <h3 class="text-white no-margin opensans"><b>124</b></h3>
                <p class="text-white opensans">Villages Covered
                </p>
              </div>
            </div>
          </div>
        </div>
        <a href="<?php echo site_url("impact"); ?>" title="" class="text-white text-sm opensans "><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> Read Our Impact Stories</a>

      </div>
    <div class="bg-overlay" style="opacity: 0;"></div>
	</section>
	<!-- END OF IMPACT -->

	<!-- NEWSLETTER -->
	<!--
	<section class="p-b-30 p-t-30 bg-light">
      <div class="container">
		<form action="//amartha.us12.list-manage.com/subscribe/post?u=41044774ecb4f30e3d24bc764&amp;id=79ca2f30e0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

        <div class="row">
          <div class="col-sm-8 col-sm-offset-2">
            <h6 class="block-title text-center">Stay in touch</h6>
            <h1 class="text-center purple opensans">Subscribe to our newsletter</h1>
            <p class="small text-center ">We value your privacy. None of the details supplied will be shared with external parties</p>
            <div class="input-group text-center">

				<input type="email" name="EMAIL" id="mce-EMAIL" class="form-control" placeholder="Type Your Email Address" aria-describedby="basic-addon2" required />

				real people should not fill this in and expect good things - do not remove this or risk form bot signups
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_41044774ecb4f30e3d24bc764_79ca2f30e0" tabindex="-1" value=""></div>
	<span class="input-group-btn">
					<input class="btn btn-primary" type="submit" id="mc-embedded-subscribe">Subscribe</button>
				</span>

				-->

			</div>
          </div>
        </div>
		</form>
      </div>
    </section>
	<!-- END OF NEWSLETTER -->

    <div class="container">
     <div class="row">
          <div class="col-sm-6">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
            <h1 class="m-t-5 purple opensans">We take extra miles in order to arrive at the community improvement as a whole</h1>

			<?php } else {?>
            <h1 class="m-t-5 purple opensans">Membangun perekonomian yang inklusif, memeratakan kesejahteraan bagi Indonesia</h1>

 			<?php }?>

         </div>
          <div class="col-sm-6">
            <div class="row m-t-15">

              <div class="col-sm-6">
                <div class="progress progress-small transparent-white m-t-15">
                  <div class="progress-bar progress-bar-white" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                  </div>
                </div>
                <h3 class="no-margin opensans"><b>20.000 Anggota</b></h3>
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
                <p class=" opensans">Borrowers</p>

 				<?php } else {?>
                <p class=" opensans">telah kami bina melalui group lending, menjadikan mereka pelaku usaha mikro dan UKM terpercaya dan peminjam yang berkualitas</p>

				<?php }?>

             </div>

              <div class="col-sm-6">
                <div class="progress progress-small transparent-white m-t-15">
                  <div class="progress-bar progress-bar-white" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                  </div>
                </div>
 				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
     			<h3 class="no-margin opensans"><b>IDR 30 billion</b></h3>
				<p class="opensans">Loan Disbursed</p>

 				<?php } else {?>
                <h3 class="no-margin opensans"><b>IDR 30 Milyar</b></h3>
         		<p class="opensans">telah disalurkan untuk menggerakkan sektor-sektor ekonomi produktif di pedesaan, meningkatkan pendapatan keluarga anggota kami</p>

				<?php }?>
				
              </div>
              <!--
              <div class="col-sm-6">
                <div class="progress progress-small transparent-white m-t-15">
                  <div class="progress-bar progress-bar-white" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                  </div>
                </div>
                <h3 class="no-margin opensans"><b>255</b></h3>
                <p class="opensans">Borrower Available
                </p>
              </div>

              <div class="col-sm-6">
                <div class="progress progress-small transparent-white m-t-15">
                  <div class="progress-bar progress-bar-white" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                  </div>
                </div>
                <h3 class="no-margin opensans"><b>124</b></h3>
                <p class="opensans">Villages Covered
                </p>
              </div>-->

           	</div>
           </div>
       </div><br/><br/>
    </div>
    <div>
    	<div class="container hidden">
    		<div class="Content3">

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
    			<h2 class="opensans">We take extra miles in order to arrive at the community improvement as a whole </h2>

				<?php } else {?>
    			<h2 class="opensans">Membangun perekonomian yang inklusif, memeratakan kesejahteraan bagi Indonesia </h2>
				
				<?php }?>

    			<div class="grid">
	    			<div class="row">
	    				<div class="col-md-4"><img src="<?php echo $this->template->get_theme_path(); ?>/img/photo1.jpg"></div>
	    				<div class="col-md-4"><img src="<?php echo $this->template->get_theme_path(); ?>/img/photo1.jpg"></div>
	    				<div class="col-md-4"><img src="<?php echo $this->template->get_theme_path(); ?>/img/photo1.jpg"></div>

	    			</div>

					<div class="row">
	    				<div class="col-md-4"><img src="<?php echo $this->template->get_theme_path(); ?>/img/photo1.jpg"></div>
	    				<div class="col-md-4"><img src="<?php echo $this->template->get_theme_path(); ?>/img/photo1.jpg"></div>
	    				<div class="col-md-4"><img src="<?php echo $this->template->get_theme_path(); ?>/img/photo1.jpg"></div>

	    			</div>

	    			<div class="row">
	    				<div class="col-md-4"><img src="<?php echo $this->template->get_theme_path(); ?>/img/photo1.jpg"></div>
	    				<div class="col-md-4"><img src="<?php echo $this->template->get_theme_path(); ?>/img/photo1.jpg"></div>
	    				<div class="col-md-4"><img src="<?php echo $this->template->get_theme_path(); ?>/img/photo1.jpg"></div>

	    			</div>
				</div>

    		</div>
    	</div>
    </div>


