

			<!-- Personal Data -->
	<div class="container personalData">
		<div class="row perData">
			<div class="col-md-12">
				<h3 class="bold">Investasi Ibu Esih</h3>
				<!-- DATA TABLE -->
				<table class="table">
			
			    </thead>
			    <tbody>
			      
			      <tr><td class="bold">Dana match untuk pembiayaan:</td><td>Rp. 1.500.000  > masuk ke pembiayaan borrowers</td></tr>

			      <tr><td class="bold">Gross Profit-shared (20%):</td><td >Rp. 300,000 > per pembiayaan</td></tr>

			      <tr><td class="bold">Service fee (1%):</td><td >Rp. 18.000 > (Pokok+profit)*service fee</td></tr>

			      <tr><td class="bold">Net profit:</td><td >Rp. 282,000</td></tr>

			      <tr><td class="bold"></td><td>&nbsp;</td></tr>

			      <tr><td class="bold">Angsuran Pokok masuk per minggu:</td><td>Rp. 30.000</td></tr>

			      <tr><td class="bold">Angsuran Profit masuk per minggu:</td><td >Rp. 6.000 </td></tr>

			      <tr><td class="bold">Service fee (1%):</td><td >Rp. 360 </td></tr>
			      
			    </tbody>
			  </table>
			</div>
			</div>
			</div>
		
			<!-- <div class="col-md-6 chartPerData">
				<ul>
					<li>
						<?php 
							$ratio = number_format($borrower->data_plafond / ($borrower->data_pendapatan_total*12) * 100, 2);
						?>
						<div id="myStat" data-dimension="400" data-text="<?php echo $ratio;?>%" data-info="Debt to Income Ratio" data-width="30" data-fontsize="28" data-percent="<?php echo $ratio;?>" data-fgcolor="purple" data-bgcolor="#eee" data-fill="#faedff"></div>
					</li>
				</ul>
			</div> -->
		</div>



		<div class="row browseData" style="font-size:18px;">
			<form method="post" action="#">
			<div class="col-md-12">
				<div class="browseTable">
				<!-- TABLE START!! -->
				<table class="table table-striped table-bordered table-hover datatable tbl-responsive">

			    <!-- HEAD TABLE -->
			    <thead >
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Tanggal</th>
						<th class="text-center">Transaksi</th>
						<th class="text-center">Debet</th>
						<th class="text-center">Kredit</th>
						<th class="text-center">Saldo</th>
						<th class="text-center">Status</th>
					</tr>
			    </thead>

			    <tbody data-link="row" class="rowlink">
					
					<tr>
						<td class="text-center">1</td>
						<td class="text-center">12-01-2016</td>
						<td class="text-left">Top Up</td>
						<td class="text-center"></td>
						<td class="text-right">Rp 2.000.000,-</td>
						<td class="text-right">Rp 2.000.000,-</td>
						<td class="text-center">Done</td>
					</tr>

					<tr>
						<td class="text-center">2</td>
						<td class="text-center">11-01-2016</td>
						<td class="text-left">Pembiayaan</td>
						<td class="text-right">Rp 1.500.000,-</td>
						<td class="text-right"></td>
						<td class="text-right">Rp 500.000,-</td>
						<td class="text-center">Done</td>
					</tr>

					<tr>
						<td class="text-center">3</td>
						<td class="text-center">10-01-2016</td>
						<td class="text-left">Pembayaran Cicilan Aisah</td>
						<td class="text-right"></td>
						<td class="text-right">Rp 35.000</td>
						<td class="text-right"></td>
						<td class="text-center">Done</td>
					</tr>
					
					<tr>
						<td class="text-center">4</td>
						<td class="text-center">10-01-2016</td>
						<td class="text-left">Pembayaran Cicilan Aisah</td>
						<td class="text-right"></td>
						<td class="text-right">Rp 35.000</td>
						<td class="text-right"></td>
						<td class="text-center">Done</td>
					</tr>

					<tr>
						<td class="text-center">5</td>
						<td class="text-center">10-01-2016</td>
						<td class="text-left">Pembayaran Cicilan Aisah</td>
						<td class="text-right"></td>
						<td class="text-right">Rp 35.000</td>
						<td class="text-right"></td>
						<td class="text-center">Done</td>
					</tr>

					<tr>
						<td class="text-center">6</td>
						<td class="text-center">10-01-2016</td>
						<td class="text-left">Pembayaran Cicilan Aisah</td>
						<td class="text-right"></td>
						<td class="text-right">Rp 35.000</td>
						<td class="text-right"></td>
						<td class="text-center">Done</td>
					</tr>

					<tr>
						<td class="text-center">7</td>
						<td class="text-center">10-01-2016</td>
						<td class="text-left">Pembayaran Cicilan Aisah</td>
						<td class="text-right"></td>
						<td class="text-right">Rp 35.000</td>
						<td class="text-right"></td>
						<td class="text-center">Done</td>
					</tr>

					<tr>
						<td class="text-center">8</td>
						<td class="text-center">10-01-2016</td>
						<td class="text-left">Pembayaran Cicilan Aisah</td>
						<td class="text-right"></td>
						<td class="text-right">Rp 35.000</td>
						<td class="text-right"></td>
						<td class="text-center">Done</td>
					</tr>

					<tr>
						<td class="text-center">9</td>
						<td class="text-center">10-01-2016</td>
						<td class="text-left">Pembayaran Cicilan Aisah</td>
						<td class="text-right"></td>
						<td class="text-right">Rp 35.000</td>
						<td class="text-right"></td>
						<td class="text-center">Done</td>
					</tr>

					<tr>
						<td class="text-center">10</td>
						<td class="text-center">10-01-2016</td>
						<td class="text-left">Pembayaran Cicilan Aisah</td>
						<td class="text-right"></td>
						<td class="text-right">Rp 35.000</td>
						<td class="text-right"></td>
						<td class="text-center">Done</td>
					</tr>
					
				  </tbody>
			  </table>
			<!-- TABLE END!! -->
			  </div>
			</div>
		</div>

		<!-- MODULE BASE
		<div class="row browseData">
			<form method="post" action="<?php echo site_url('loan/addtocart'); ?>">
			<div class="col-md-12">
				<div class="browseTable">
			
				<table class="table table-striped table-bordered table-hover datatable tbl-responsive">

		
			    <thead >
					<tr>
						<th class="text-center">No</th>
						<th class="text-center">Tanggal</th>
						<th class="text-center">Transaksi</th>
						<th class="text-center">Debet</th>
						<th class="text-center">Kredit</th>
						<th class="text-center">Saldo</th>
						<th class="text-center">Status</th>
					</tr>
			    </thead>

			    <tbody data-link="row" class="rowlink">
					

					<?php $i=1; ?>
					<?php foreach($borrower as $row){ ?>					
						<?php 
						$startTimeStamp = strtotime(date('Y-m-d'));
						$endTimeStamp 	= strtotime($row->data_date_accept);
						$timeDiff 		= ($endTimeStamp - $startTimeStamp);
						$numberDays 	= $timeDiff/86400;
						$numberDays 	= intval($numberDays);
						?>
					<tr href="<?php echo site_url('borrower/profile/').$row->data_client; ?>">
			
				    	<td class="rowlink-skip checkIt">
							<input name="check_<?php echo $i; ?>" value="1" type="checkbox">
							<input name="loan_<?php echo $i; ?>" value="<?php echo $row->data_id; ?>" type="hidden" />
						</td>
						<td class="text-left">
							<a href="<?php echo site_url();?>loan/detail/<?php echo $row->data_id; ?>"><?php echo $row->client_fullname; ?></a>
							<input name="name_<?php echo $i; ?>" value="<?php echo $row->client_fullname; ?>" type="hidden" />
						</td>
						<td class="text-center">
							<?php echo $row->data_popi_total; ?>
							<input name="score_<?php echo $i; ?>" value="<?php echo $row->data_popi_total; ?>" type="hidden" />
						</td>
				    	<td class="text-center">
							<?php echo $row->data_popi_kategori; ?>
							<input name="grade_<?php echo $i; ?>" value="<?php echo $row->data_popi_kategori; ?>" type="hidden" />
							</td>
				    	<td class="text-right">
							<?php echo number_format($row->data_plafond); ?>
							<input name="plafond_<?php echo $i; ?>" value="<?php echo $row->data_plafond; ?>" type="hidden" />
						</td>
				    	<td class="text-center">
							<?php $grade=$row->data_margin/$row->data_plafond*100; echo number_format($grade); ?>
						</td>
				    	<td class="text-left"><?php echo $row->data_tujuan; ?></td>
				    	<td class="text-center">50</td>
				    	<td class="text-center">
							<?php if($numberDays > 0) { echo $numberDays." days";}else{ echo "expired"; }?>
							<input name="_days_<?php echo $i; ?>" value="<?php echo $numberDays; ?>" type="hidden" />
						</td>
					</tr>
					<?php $i++; } $no = $i-1; //end foreach ?>
			
					
				  </tbody>
			  </table>

			  </div>
			</div>
		</div> -->


		<div class="row">
			<div class="col-md-12 text-center">
				<div class="confirmButton">
					<a class="backButton" href="<?php echo site_url()?>invesment">< Back</a>
				</div>
			</div>
		</div>
		<div class="row">&nbsp;</div>
		</form>
	</div>