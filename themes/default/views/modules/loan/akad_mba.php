<div class="akad_template text-left">
<h1 class="akad_title purple text-center">AQAD PEMBIAYAAN<br/>"AL MURABAHA (JUAL - BELI)"</h1>
<p><b>No: <?php echo $this->uri->segment(4); ?></b></p>
<p><i>Bismillahirrahmanirrahim</i></p>
<p>Perjanjian Pembiayaan ini dibuat pada tanggal <b><?php echo date("d-m-Y"); ?></b>, oleh dan antara:</p>
<ol>
	<li><b><?php echo $profile['fullname']; ?></b>, sebagaimana terdata dalam sistem Amartha (selanjutnya disebut sebagai <b>"Pemberi Pembiayaan"</b>);</li>
	<li><b><?php echo $borrower->client_fullname; ?></b>, sebagaimana terdata dalam sistem Amartha (selanjutnya disebut sebagai <b>"Penerima Pembiayaan"</b>); dan</li>
	<li><b>PT Amartha Mikro Fintek</b>, suatu perusahaan yang didirikan  berdasarkan hukum Negara Republik Indonesia yang merupakan pengelola dari Amartha (selanjutnya disebut sebagai "Perantara").</li>
</ol>

<p>Pemberi Pembiayaan dan Penerima Pembiayaan sepakat untuk masuk ke dalam Perjanjian ini dengan ketentuan dan syarat sebagai berikut:</p>
<ol>
	<li>Penerima Pembiayaan telah memohon kepada Pemberi Pembiayaan melalui sistem yang dikelola oleh Perantara untuk membayar <?php echo $borrower->data_tujuan; ?>.</li>
	<li>Pemberi Pembiayaan mewakilkan kepada Penerima Pembiayaan untuk melakukan proses yang berkaitan dengan kepentingan sesuai butir 1 sebesar Rp <b><?php echo number_format($borrower->data_plafond); ?></b></li>
	<li>Jangka waktu pembiayaan yang diberikan oleh Pemberi Pembiayaan kepada Penerima Pembiayaan sebesar tersebut di atas telah disepakati kedua belah pihak selama <b><?php echo $borrower->data_jangkawaktu; ?></b> minggu.</li>
	<li>Terhadap pembiayaan ini Penerima Pembiayaan pada hakekatnya mengaku berhutang kepada Pemberi Pembiayaan dan semata-mata akan digunakan untuk keperluan sebagaimana yang tersebut dalam butir 1 (satu) di atas.</li>
	<li>Atas pemberian Fasilitas ini, Penerima Pembiayaan bersedia:
		<ol>
			<li>membayar Pokok kepada Pemberi Pembiayaan dari Fasilitas yakni sebesar Rp <?php echo number_format($borrower->data_plafond); ?></li>
			<li>membayar Ujrah kepada Pemberi Pembiayaan dari Fasilitas yakni sebesar Rp <?php echo number_format($borrower->data_plafond * 0.3); ?></li>
			<li>melunasi Utang tersebut dengan cara membayar angsuran setiap minggu sebesar Rp <?php echo number_format(($borrower->data_plafond + $borrower->data_plafond * 0.3) / $borrower->data_jangkawaktu); ?>.</li>
		</ol>
	</li>
	
	<li>Atas pemberian Fasilitas ini, Pemberi Pembiayaan bersedia membayar Biaya Jasa kepada Perantara yakni sebesar Rp <?php echo number_format( 1/100 * ($borrower->data_plafond / $borrower->data_jangkawaktu)); ?> yang akan dipotong dari setiap angsuran yang dibayarkan oleh Penerima Pembiayaan.</li>
	<li>Pemberi Pembiayaan, Penerima Pembiayaan, dan Perantara sepakat bahwa hal-hal lain terkait dengan Perjanjian ini akan diatur lebih lanjut dalam Syarat-Syarat Umum Perjanjian.</li>
	<li>Perjanjian ini dan Syarat-Syarat Umum Perjanjian merupakan bagian yang tidak terpisahkan sehingga harus dibaca dan diartikan secara bersama-sama.</li>
</ol>
<p><i>Walhamdulillaahirabbil'aalamiin</i></p>
</div>