
<div class=WordSection1>

<h3 align="center"><b>SYARAT SYARAT UMUM PERJANJIAN PEMBIAYAAN</b></h3>
<h3 align="center" style="padding-bottom:20px;"><b>(SSUPP)</b></h3>

<h3 align="center" style="padding-bottom:20px;"><b>Pasal 1<br/>DEFINISI DAN INTERPRETASI</b></h3>
<p class="opensans"><b>1.1 Definisi</b><br/>Dalam SSUPP ini, kecuali dinyatakan lain, kata-kata sebagai berikut mempunyai arti:</p>
<p class="opensans">"<b>Biaya Jasa</b>" adalah biaya yang dibayarkan dari Pemberi Pembiayaan kepada Perantara untuk jasa perantarana pemberian Fasilitas sebagaimana diatur lebih lanjut dalam Pasal 3.5 SSUPP ini.</p>
<p class="opensans">"<b>Biaya Perantaraan</b>" adalah biaya yang dibayarkan dari Penerima Pembiayaan kepada Perantara untuk pemberian Fasilitas dari Pemberi Pembiayaan sebagaimana diatur lebih lanjut dalam Pasal 3.3 SSUPP ini.</p>
<p class="opensans">"<b>Fasilitas</b>" adalah fasilitas pembiayaan yang diberikan oleh Pemberi Pembiayaan kepada Penerima Pembiayaan sebagaimana disebutkan dalam Perjanjian dan Pasal 2.1 SSUPP ini menurut dan tunduk pada ketentuan dan syarat SSUPP ini.</p>
<p class="opensans">"<b>Hari Kerja</b>" adalah suatu hari selain hari Sabtu, Minggu dan hari libur nasional, dimana bank-bank buka untuk melaksanakan usaha dan melakukan penyelesaian pembayaran satu sama lain di Jakarta.</p>
<p class="opensans">"<b>Jangka Waktu Fasilitas</b>" adalah jangka waktu sebagaimana disebut lebih lanjut dalam Perjanjian.</p>
<p class="opensans">"<b>Margin / <i>Ujrah</i></b>" adalah suatu biaya Fasilitas sebagaimana diatur lebih lanjut dalam Perjanjian dan Pasal 3.1 SSUPP ini.</p>
<p class="opensans">"<b>Peristiwa Cidera Janji</b>" adalah salah satu peristiwa yang ditetapkan dalam Pasal 9.1 SSUPP ini.</p>
<p class="opensans">"<b>SSUPP</b>" adalah syarat-syarat umum dari perjanjian pemberian Fasilitas antara Pemberi Pembiayaan, Penerima Pembiayaan, dan Perantara yang merupakan satu kesatuan yang tidak terpisahkan.</p>
<p class="opensans">"<b>Persetujuan</b>" adalah pemberian kewenangan, persetujuan, registrasi, perizinan, pembebasan dan lisensi dari pejabat atau badan yang berwenang, yang dilakukan oleh Penerima Pembiayaan sebagaimana dimaksud dalam Pasal 8.7 SSUPP ini.</p>
<p class="opensans">"<b>Rekening Perantara</b>" adalah rekening milik PT Amartha Mikro Fintek sebagai perantara antara Pemberi Pembiayaan dan Penerima Pembiayaan.</p>
<p class="opensans">"<b>Rupiah</b>" atau tanda “Rp” atau “IDR” adalah mata uang negara Republik Indonesia yang sah.</p>
<p class="opensans">"<b>Tanggal Jatuh Tempo</b>" adalah jangka waktu sebagaimana disebutkan dalam Perjanjian dan Pasal 9.1 SSUPP ini.</p>
<p class="opensans">Referensi dalam SSUPP ini kepada:</p>
<ol style="list-style-type: upper-roman;">
	<li><p>"Pemberi Pembiayaan" diartikan mencakup penggantinya dan/atau penerima pengalihannya sesuai dengan kepentingan mereka masing-masing.</p></li>
	<li><p>"pasal" diartikan sebagai penunjukan pada pasal dalam SSUPP ini.</p></li>
	<li><p>"utang" diartikan sehingga mencakup setiap kewajiban (baik yang ada sebagai pokok atau sebagai jaminan) untuk pembayaran atau pembayaran kembali uang, baik sekarang maupun yang akan datang, aktual atau yang kontinjen.</p></li>
	<li><p>“bulan” adalah penunjukan pada suatu periode pada hari pertama dalam bulan kalender dan berakhir pada hari yang sama dalam bulan kalender berikutnya kecuali, dimana periode tersebut akan berakhir pada suatu hari yang bukan Hari Kerja, akan berakhir pada Hari Kerja berikutnya, kecuali hari itu jatuh pada bulan kalender berikutnya yang didalamnya dinyatakan akan berakhir, dalam kasus tersebut, akan berakhir pada Hari Kerja sebelumnya dengan ketentuan bahwa, jika suatu periode mulai pada Hari Kerja terakhir dalam suatu bulan kalender atau jika tidak ada kesesuaian hari secara numerik dalam bulan dimana periode tersebut berakhir, periode tersebut akan berakhir pada Hari Kerja terakhir di bulan berikutnya (dan penunjukan pada “bulan” akan disesuaikan).</p></li>
</ol>
<p class="opensans" style="padding-bottom:20px;"><b>1.2 &nbsp;&nbsp;&nbsp;Judul</b><br/>Judul dalam SSUPP ini akan digunakan untuk kemudahan atau referensi saja dan tidak akan mempengaruhi interpretasi dari SSUPP ini.</p>

<h3 align="center" style="padding-bottom:20px;"><b>Pasal 2<br/>FASILITAS DAN PENGGUNAANNYA</b></h3>
<p class="opensans">2.1 &nbsp;&nbsp;&nbsp;Pemberi Pembiayaan akan menyediakan Fasilitas tersebut paling lambat 7 (tujuh) Hari Kerja dari tanggal Perjanjian disepakati di Rekening Perantara. Selanjutnya, paling lambat 7 (tujuh) Hari Kerja, Perantara akan mengirimkan Fasilitas kepada Rekening Penerima Pembiayaan sebagaimana terdata dalam sistem Amartha.</p>
<p class="opensans">2.2 &nbsp;&nbsp;&nbsp;Fasilitas akan digunakan oleh Penerima Pembiayaan untuk tujuan usaha maupun pribadi dan tidak melanggar undang-undang.</p>
<p class="opensans" style="padding-bottom:20px;">2.3 &nbsp;&nbsp;&nbsp;Penerima Pembiayaan bertanggung jawab atas kebenaran dari penggunaan Fasilitas. Pemberi Pembiayaan dan Perantara tidak berkewajiban untuk melakukan verifikasi atas kebenaran dari penggunaan Fasilitas oleh Penerima Pembiayaan.</p>


<h3 align="center" style="padding-bottom:20px;"><b>Pasal 3<br/>MARGIN DAN BIAYA</b></h3>
<p class="opensans">3.1 &nbsp;&nbsp;&nbsp;Untuk setiap jumlah utang pokok yang belum dilunasi oleh Penerima Pembiayaan kepada Pemberi Pembiayaan berdasarkan Perjanjian, Penerima Pembiayaan setuju untuk membayar Margin sebagaimana dirinci dalam Perjanjian.</p>
<p class="opensans">3.2 &nbsp;&nbsp;&nbsp;Margin wajib dibayarkan Penerima Fasilitas pada saat Tanggal Jatuh Tempo atau pembayaran lebih awal Fasilitas ke Rekening Perantara.</p>
<p class="opensans">3.3 &nbsp;&nbsp;&nbsp;Penerima Pembiayaan setuju untuk membayar Biaya Perantaraan kepada Perantara melalui Rekening Perantara sebagaimana dirinci dalam Perjanjian. Untuk menghindari keraguan, Para pihak sepakat bahwa Biaya Perantaraan akan sepenuhnya menjadi milik Perantara.</p>
<p class="opensans">3.4 &nbsp;&nbsp;&nbsp;Biaya Perantaraan wajib dibayarkan Penerima Pembiayaan pada saat Tanggal Jatuh Tempo atau pembayaran lebih awal Fasilitas ke Rekening Perantara.</span></p>
<p class="opensans">3.5 &nbsp;&nbsp;&nbsp;Pemberi Pembiayaan setuju untuk membayar Biaya Jasa kepada Perantara melalui Rekening Perantara sebagaimana dirinci dalam Perjanjian. Untuk menghindari keraguan, Para pihak sepakat bahwa Biaya Jasa akan sepenuhnya menjadi milik Perantara.</span></p>
<p class="opensans">3.6 &nbsp;&nbsp;&nbsp;Biaya Jasa wajib dibayarkan Pemberi Pembiayaan pada saat Perjanjian disetujui bersama.</span></p>
<p class="opensans" style="padding-bottom:20px;">3.7 &nbsp;&nbsp;&nbsp;Jangka waktu Fasilitas adalah waktu yang disepakati para pihak sebagaimana dirinci dalam Perjanjian, yakni sejak tanggal penyaluran Fasilitas ke rekening milik Penerima Pembiayaan ("<b>Jangka Waktu Fasilitas</b>").</p>

<h3 align="center" style="padding-bottom:20px;"><b>Pasal 4<br/>DENDA</b></h3>
<p class="opensans" style="padding-bottom:20px;">Jika Penerima Pembiayaan lalai untuk membayar tiap dan atau semua bagian atas utang pokok dan/atau Margin dan/atau Biaya Perantaraannya, berdasarkan Perjianjian saat jatuh tempo karena alasan apapun, Penerima Pembiayaan wajib membayar denda sebagaimana dirinci dalam Perjanjian, yang dibayarkan ke Rekening Perantara. Untuk menghindari keraguan, denda akan sepenuhnya menjadi milik dari Pemberi Pembiayaan.</p>

<h3 align="center" style="padding-bottom:20px;"><b>Pasal 5<br/>PEMBAYARAN</b></h3>
<p class="opensans">5.1 &nbsp;&nbsp;&nbsp;Penerima Pembiayaan wajib membayar seluruh jumlah Utang dari Fasilitas kepada Pemberi Pembiayaan pada akhir Jangka Waktu Fasilitas ("<b>Tanggal Jatuh Tempo Akhir</b>"), dalam mata uang yang sama dari Fasilitas.</p>
<p class="opensans">5.2 &nbsp;&nbsp;&nbsp;Para Pihak sepakat bahwa Pembayaran Fasilitas akan dilakukan secara bertahap sebagaimana dirinci dalam Perjanjian.</p>
<p class="opensans">5.3 &nbsp;&nbsp;&nbsp;Paling lambat 3 (tiga) Hari Kerja setelah penerimaan pembayaran dari Penerima Pembiayaan di Rekening Perantara, Perantara wajib mentransferkan pembayaran tersebut ke rekening milik Pemberi Pembiayaan sebagaimana terdata dalam sistem Amartha.</p>
<p class="opensans" style="padding-bottom:20px;">5.4 &nbsp;&nbsp;&nbsp;Pembayaran apapun baik atas Fasilitas, Margin, atau biaya lainnya yang timbul atas Perjanjian yang dilakukan oleh Penerima Pembiayaan kepada Pemberi Pembiayaan melalui Rekening Perantara adalah bebas dan bersih dari dan tanpa pemotongan apapun.</p>

<h3 align="center" style="padding-bottom:20px;"><b>Pasal 6<br/>BUKTI UTANG</b></h3>
<p class="opensans">6.1 &nbsp;&nbsp;&nbsp;Pembukuan dan/atau catatan-catatan yang dibuat dan akan dibuat oleh Pemberi Pembiayaan dalam sistem Amartha merupakan bukti <i>prima facie</i> atas jumlah Utang Penerima Pembiayaan kepada Pemberi Pembiayaan dalam SSUPP ini dan adalah final dan mengikat bagi Penerima Pembiayaan kecuali jika terdapat kesalahan yang nyata dan juga merupakan bukti <i>prima facie</i> dalam proses hukum apapun.</p>
<p class="opensans" style="padding-bottom:20px;">6.2 &nbsp;&nbsp;&nbsp;Jika ada ketidakcocokan apapun antara pembukuan dan/atau catatan-catatan Pemberi Pembiayaan sebagaimana disebutkan dalam Pasal 6.1 di atas dengan pembukuan dan/atau catatan-catatan Penerima Pembiayaan, Penerima Pembiayaan dapat meminta bukti atas jumlah yang wajib dilunasi yang harus dibayar oleh Penerima Pembiayaan kepada Pemberi Pembiayaan dan Pemberi Pembiayaan berdasarkan Perjanjian, dan Pemberi Pembiayaan wajib membuktikan saldo yang belum dilunasi tersebut dengan menunjukkan salinan dari pembukuan dan/atau catatan-catatan milik Pemberi Pembiayaan.</p>

<h3 align="center" style="padding-bottom:20px;"><b>Pasal 7<br/>PEMBAYARAN LEBIH AWAL</b></h3>
<p class="opensans" style="padding-bottom:20px;">Penerima Pembiayaan dapat membayar lebih awal secara sukarela seluruh jumlah utang Fasilitas, Margin dan Biaya Perantaraan kepada Pemberi Pembiayaan sebelum Tanggal Jatuh Tempo Akhir dengan ketentuan bahwa Penerima Pembiayaan wajib memberikan pemberitahuan paling tidak 5 (lima) Hari Kerja sebelum tanggal pembayaran lebih awal yang diusulkan.</p>

<h3 align="center" style="padding-bottom:20px;"><b>Pasal 8<br/>PERNYATAAN DAN JAMINAN</b></h3>
<p class="opensans">Penerima Pembiayaan, pada tanggal Perjanjian disepakati, menyatakan dan menjamin kepada Pemberi Pembiayaan, sebagai berikut:</p>
<p class="opensans">8.1 &nbsp;&nbsp;&nbsp;Pendirian. <b>[Jika Penerima Pembiayaan berbentuk badan hukum]</b></p>
<ol>
	<li>Penerima Pembiayaan adalah suatu badan hukum yang didirikan, disetujui, terdaftar dan sah keberadaannya berdasarkan peraturan perundang-undangan yang berlaku di Negara Republik Indonesia.</li>
	<li>Data-data yang dimuat oleh Penerima Pembiayaan dalam sistem Amartha adalah lengkap, benar dan betul dan sampai dengan tanggal ini dan tidak ada perubahan atau penggantian terkait data-data tersebut.</li>
</ol>

<p class="opensans">8.2 &nbsp;&nbsp;&nbsp;Kuasa dan Kewenangan. <b>[Jika Penerima Pembiayaan berbentuk badan hukum]</b></p>
<ol>
	<li>Penerima Pembiayaan mempunyai kuasa, kewenangan dan hak penuh untuk memikul utang dan kewajiban lain apapun yang disediakan dalam Perjanjian dan SSUPP, dan untuk membuat dan melaksanakan Perjanjian dan SSUPP ini.</li>
	<li>Penerima Pembiayaan mempunyai kuasa penuh dan kewenangan untuk memiliki asetnya sendiri dan untuk melakukan usaha yang dilakukannya dan/atau bermaksud untuk dilakukan.</li>
</ol>

<p class="opensans">8.3 &nbsp;&nbsp;&nbsp;Kewajiban yang Mengikat.</p>
<ul>
	Perjanjian dan SSUPP, saat disetujui oleh Penerima Pembiayaan, adalah legal, sah dan mengikat bagi Penerima Pembiayaan dan dapat dilaksanakan terhadap Penerima Pembiayaan sesuai dengan ketentuannya.
</ul>

<p class="opensans">8.4 &nbsp;&nbsp;&nbsp;Tindakan Penerima Pembiayaan.</p>
<ol>
	<b>[Jika Penerima Pembiayaan berbentuk badan hukum]</b><br/>
	Penerima Pembiayaan telah mengambil semua tindakan perusahaan yang dibutuhkan dan semua langkah dan tindakan yang disyaratkan dalam anggaran dasar dan peraturan terkait untuk membuat dan melaksanakan Perjanjian dan SSUPP ini.<br/>
	<br/>
	<b>[Jika Penerima Pembiayaan berbentuk orang perorangan dan telah menikah]</b><br/>
	Penerima Pembiayaan telah mendapatkan persetujuan dari istri/suami nya untuk membuat dan melaksanakan Perjanjian dan SSUPP ini dan semua dokumen lain yang disyaratkan dalam Perjanjian dan SSUPP ini, sebagaimana juga untuk memastikan keabsahan dan kinerja dokumen-dokumen tersebut (jika ada).</span></p>
</ol>

<p class="opensans">8.5 &nbsp;&nbsp;&nbsp;Tidak ada Cidera Janji</p>
<ol style="list-style-type: lower-latin">
	<li>Persetujuan dari Perjanjian dan SSUPP ini:</li>
	<ol tyle="list-style-type: lower-roman">
		<li>Tidak dan tidak akan melanggar ketentuan apapun dari hukum yang berlaku, peraturan atau pedoman pemerintah, kebijakan, petunjuk atau perintah, pernyataan apapun atau putusan atau perintah resmi pengadilan apapun, badan pengawas atau lembaga arbitrase, atau persetujuan apapun, atau dari akta pendirian atau anggaran dasar Penerima Pembiayaan; dan</li>
		<li>Tidak dan tidak akan mengakibatkan pelanggaran dari, atau merupakan cidera janji dari, atau membutuhkan persetujuan apapun dalam, perjanjian atau instrumen apapun dimana Penerima Pembiayaan merupakan pihak, ataupun dalam keadaan cidera janji  sehubungan dengan komitmen atau kewajiban keuangan apapun atau yang mana oleh karenanya ia atau asetnya dapat terikat atau terpengaruh.</li>
	</ol>
	<br/><li>Sampai pada tanggal disepakatinya Perjnajian ini Penerima Pembiayaan tidak sedang dalam keadaan cidera janji dalam perjanjian atau instrumen apapun dimana Penerima Pembiayaan merupakan pihak atau telah menyerahkan maupun cidera janji sehubungan dengan komitmen atau kewajiban keuangan apapun atau yang mana oleh karenanya ia dapat terikat atau terpengaruh, yang dapat mempengaruhi secara mendasar kemampuan Penerima Pembiayaan untuk melaksanakan kewajibannya berdasarkan Perjanjian dan SSUPP ini dan tidak ada peristiwa cidera janji tersebut yang terjadi dan/atau berlangsung terus menerus yang mana belum dikesampingkan atau diperbaiki seperti yang ditetapkan dalam Perjanjian dan SSUPP ini.</li>
</ol>

<p class="opensans">8.6 &nbsp;&nbsp;&nbsp;Proses Pengadilan</p>
<ol> Tidak ada tindakan, gugatan atau jalannya proses hukum atau oleh atau di pengadilan manapun atau badan pemerintah atau pihak berwenang atau lembaga arbitrase yang sekarang sedang tertunda atau, sepengetahuan Penerima Pembiayaan, baik dalam kasus perdata, niaga, sengketa pajak dan/atau sengketa lain yang mengancam atau mempengaruhi kemampuan Penerima Pembiayaan untuk memenuhi dan melaksanakan ketentuan dan syarat yang disebutkan dalam Perjanjian
dan SSUPP ini.</ol>

<p class="opensans">8.7 &nbsp;&nbsp;&nbsp;Persetujuan, Izin, Lisensi <b>[Jika Penerima Pembiayaan berbentuk badan hukum]</b></p>
<ul>
	Penerima Pembiayaan telah membuat atau memperoleh semua kewenangan, persetujuan, pendaftaran, perizinan, pembebasan, dan lisensi dari atau tindakan lain oleh pejabat atau badan pemerintah apapun yang diperlukan untuk menjalankan usahanya dan untuk membuat dan melaksanakan dan memberi kekuatan dan dampak penuh pada Perjanjian dna SSUPP ini dan semua dokumen lain (jika ada) yang disyaratkan berdasarkan Perjanjian dan SSUPP ini, yang dapat mempengaruhi kemampuan Penerima Pembiayaan untuk melaksanakan kewajibannya berdasarkan dokumen-dokumen tersebut (“<b>Persetujuan</b>”).
</ul>

<p class="opensans">8.8 &nbsp;&nbsp;&nbsp;Keterbukaan</p>
<ul>
	Semua informasi faktual yang dibuat oleh Penerima Pembiayaan kepada Pemberi Pembiayaan dan Perantara sehubungan dengan SSUPP ini benar, akurat dan tidak menyesatkan dalam setiap aspek mendasar. Tidak ada hal-hal yang telah terjadi dan tidak ada informasi yang telah diberikan atau disembunyikan yang mengakibatkan informasi yang diberikan oleh Penerima Pembiayaan tersebut menjadi tidak benar atau menyesatkan.</span></p>
</ul>

<p class="opensans">8.9 &nbsp;&nbsp;&nbsp;Tidak Ada Keadaan Pailit atau Pembubaran</p>
<ul>
	Tidak ada proses kepailitan dan/atau pembubaran yang timbul dan/atau akan timbul terhadap Penerima Pembiayaan dan tidak ada pengajuan pailit atau pembubaran yang telah dilakukan oleh Penerima Pembiayaan atau pihak lainnya.
</ul>

<p class="opensans">8.10 &nbsp;&nbsp;Tidak Ada Larangan</p>
<ul style="padding-bottom:20px;">
	Tidak ada larangan berdasarkan perjanjian-perjanjian fasilitas dan/atau dokumen-dokumen bagi Penerima Pembiayaan untuk mendapatkan fasilitas-fasilitas lain termasuk fasilitas ini.
</ul>

<h3 align="center" style="padding-bottom:20px;"><b>Pasal 9<br/>PERISTIWA CIDERA JANJI</b></h3>

<p class="opensans">9.1 &nbsp;&nbsp;&nbsp;Setiap peristiwa dan kejadian berikut ini merupakan peristiwa cidera janji (“<b>Peristiwa Cidera Janji</b>”) berdasarkan Perjanjian dan SSUPP ini:</p>
<ol style="list-style-type: lower-latin">
	<li>Kelalaian Pembayaran<br/><br/>
	<p>Penerima Pembiayaan lalai melakukan pembayaran saat jatuh tempo (baik pada saat jatuh tempo yang ditentukan, dengan percepatan atau lainnya) jumlah utang pokok, Margin, dan jumlah lain yang wajib dibayar oleh Penerima Pembiayaan berdasarkan Perjanjian.</p>
	</li>
	<li>Kepailitan dan Insolvensi<br/><br/>
	<p>Penerima Pembiayaan:</p>
	<ol style="list-style-type: lower-roman">
		<li>permohonan atas:<br/>
		- Kepailitan, reorganisasi, dan/atau penundaan kewajiban pembayaran utang;<br/>
		- Pembubaran atau likuidasi; <b>[Jika Penerima Pembiayaan berbentuk badan hukum]</b><br/>
		- Hal lain yang serupa dalam tujuan dan dampak;
		</li>
		<li>menjadi insolven.</li>
	</ol></li><br/>
	<li>Putusan<br/><br/>
	<p>Putusan arbitrase atau putusan akhir dan mengikat dari pengadilan atau pemerintah manapun yang memiliki dampak atas pelaksanaan dari Perjanjian.</p>
	</li>
	<li>Penyangkalan dan Pembatalan Perjanjian dan SSUPP<br/><br/>
	<p>Penerima Pembiayaan membatalkan atau bermaksud untuk membatalkan atau menyangkal Perjanjian dan SSUPP ini, atau membuktikan suatu niat untuk membatalkan atau menyangkal Perjanjian dan SSUPP ini atau dokumen lain yang berkaitan.</p>
	</li>
	Tidak ada Peristiwa Cidera Janji di atas yang akan timbul, jika kegagalan untuk patuh dapat diselesaikan, diselesaikan dalam jangka waktu paling lambat 30 (tiga puluh) hari kalender.
</ol>
</p>

<p class="opensans">9.2 &nbsp;&nbsp;&nbsp;Akibat Cidera Janji</p>
<ul style="padding-bottom:20px;">
	Jika dalam hal apapun salah satu dari Peristiwa Cidera Janji tersebut di atas terjadi, maka Pemberi Pembiayaan akan selalu dan dari waktu ke waktu setelah terjadinya Peristiwa Cidera Janji tersebut, tanpa memperhatikan suatu jangka waktu tertentu, tanpa adanya kewajiban melakukan suatu pemberitahuan, tuntutan, protes, permohonan apapun juga kepada siapapun juga, dan tanpa memperoleh persetujuan, putusan, penetapan atau kuasa dari manapun juga (termasuk namun tidak terbatas pada persetujuan, putusan, penetapan atau kuasa hakim sebagaimana disebutkan dalam Pasal 1266 dan 1267 Kitab Undang-Undang Hukum Perdata bagi pembatalan suatu perjanjian timbal balik), yang kesemuanya itu dikesampingkan oleh Penerima Pembiayaan, maka Pemberi Pembiayaan dapat melaksanakan haknya sebagai Pemberi Pembiayaan berdasarkan Perjanjian dan SSUPP ini.
</ul>

<h3 align="center" style="padding-bottom:20px;"><b>Pasal 10<br/>LAIN-LAIN</span></b></h3>
<p class="opensans">10.1 &nbsp;&nbsp;&nbsp;Keseluruhan Perjanjian</p>
<ul>
	Perjanjian dan SSUPP ini merupakan keseluruhan perjanjian oleh dan antara Para Pihak dalam Perjanjian dan SSUPP ini serta menggantikan semua pernyataan ataupun kesepakatan sebelumnya berkenaan dengan hal-hal yang diatur dalam perjanjian (jika ada).
</ul>

<p class="opensans">10.2 &nbsp;&nbsp;&nbsp;Perubahan</p>
<ul>
	Perjanjian dan SSUPP ini dapat diamandemen hanya dengan persetujuan dari Penerima Pembiayaan, Pemberi Pembiayaan, dan Perantara dengan suatu instrumen tertulis.
</ul>

<p class="opensans">10.3 &nbsp;&nbsp;&nbsp;Pengalihan</p>
<ol style="list-style-type: lower-latin">
	<li>Perjanjian dan SSUPP ini mengikat dan berlaku terhadap Penerima Pembiayaan, Pemberi Pembiayaan, Perantara dan masing-masing pengganti haknya dan penerima pengalihannya.</li>
	<li>Pemberi Pembiayaan dapat mengalihkan, memindahkan atau memberikan partisipasi hak dan kewajibannya dalam Perjanjian dan SSUPP ini, dengan syarat bahwa sebelum melakukan pengalihan, pemindahan tersebut, Pemberi Pembiayaan seharusnya memberitahukan kepada Penerima Pembiayaan 30 (tiga puluh) hari kalender. Pemberi Pembiayaan dapat melangsungkan pengalihan atau pemindahan tersebut jika Penerima Pembiayaan tidak merespon pemberitahuan Pemberi Pembiayaan dalam waktu 5 (lima) Hari Kerja.</li>
	<li>Penerima Pembiayaan tidak dapat mengalihkan dan/ atau memindahkan hak dan kewajibannya dalam Perjanjian dan SSUPP ini tanpa persetujuan terlebih dahulu dari Pemberi Pembiayaan.</li>
</ol>

<p class="opensans">10.4 &nbsp;&nbsp;&nbsp;Keterpisahan</p>
<ol style="list-style-type: lower-latin">
	<li>Jika salah satu ketentuan dari Perjanjian dan SSUPP ini menjadi dilarang atau tidak dapat dilaksanakan berdasarkan hukum yang berlaku, sebagaimana hukum tersebut berlaku, menjadi tidak efektif terbatas pada pelarangan atau ketidakberlakuan tanpa membuat tidak sahnya ketentuan lain dalam Perjanjian dan SSUPP.</li>
	<li>Penerima Pembiayaan berkewajiban menandatangani dokumen tambahan apapun yang secara wajar diminta oleh Pemberi Pembiayaan dalam rangka memungkinkan dapat dilaksanakannya ketentuan yang menjadi tidak sah, tidak berlaku atau tidak dapat dilaksanakan tersebut.</li>
</ol>

<p class="opensans">10.5 &nbsp;&nbsp;&nbsp;Keterbukaan</p>
<ul>
	Para Pihak harus menggunakan usaha terbaik mereka (termasuk untuk pengeluaran instruksi yang cocok untuk, dan masuk dalam perjanjian dengan, para karyawan mereka) untuk memastikan bahwa semua informasi rahasia yang diterima oleh mereka tidak dibuka kepada pihak ketiga manapun. Namun, Penerima Pembiayaan dengan ini setuju bahwa Pemberi Pembiayaan dapat membuka informasi tersebut terkait dengan SSUPP ini sebagaimana disyaratkan dalam kaitannya dengan pengalihan, pemindahan, partisipasi, atau pelepasan lainnya oleh Pemberi Pembiayaan atau sebagaimana disyaratkan oleh hukum atau peraturan yang relevan, aturan, atau investigasi dari institusi yang berwenang atas Penerima Pembiayaan.
</ul>

<p class="opensans">10.6 &nbsp;&nbsp;&nbsp;Hukum yang Berlaku.</p>
<ul>
	Perjanjian dan SUPP ini tunduk pada dan harus ditafsirkan menurut hukum Negara Republik Indonesia.
</ul>

<p class="opensans">10.7 &nbsp;&nbsp;&nbsp;Yurisdiksi</p>
<ul>
	Setiap dan semua sengketa, kontroversi atau tuntutan yang timbul dari atau terkait dengan Perjanjian dan SSUPP ini, termasuk segala pertanyaan terkait dengan keberadaannya, keabsahannya, interpretasinya, pelaksanaan atau pengakhirannya, akan diselesaikan dengan cara musyawarah mufakat yang dilandasi <i>ukhuwah islamiyyah</i>. Namun apabila dalam waktu 30 (tiga puluh) hari kalender sejak pertemuan musyawarah pertama belum juga dapat dicapai kata mufakat, maka penyelesaian akan diarahkan kepada dan akan diselesaikan oleh Pengadilan Negeri Jakarta Selatan. Penyerahan oleh para pihak untuk yurisdiksi tersebut tidak akan (dan tidak akan diartikan menjadi) membatasi hak dari Pemberi Pembiayaan untuk melakukan proses dengan cara lainnya yang diperbolehkan oleh hukum atau membawa tindakan atau proses hukum atau untuk mendapatkan eksekusi dari putusan dalam yurisdiksi lain yang cocok dan kompeten. Penerima Pembiayaan dengan ini mengesampingkan keberatan apapun yang sekarang atau setelahnya dapat Penerima Pembiayaan miliki terhadap tindakan tersebut dengan dasar forum yang tidak pantas atau tidak tepat.</span></p>
</ul>
<p class="opensans">10.8 &nbsp;&nbsp;&nbsp;Salinan</p>
<ul>
	Perjanjian dapat ditandatangani dengan jumlah salinan berapapun, dimana masing-masing dianggap sebagai aslinya, akan tetapi semua salinan-salinan tersebut merupakan instrumen yang satu dan sama.
</ul>

</div>