	<!-- TABLE DATA -->
	<div class="container historyData"><br/><br/>
		<div class="container">
		<div class="row titleTable ">
			<div class="col-md-6">

				<!-- DATA TABLE -->
				<table class="table">
			
			    
			    <tbody>
			      <tr><td>Borrower Name:</td><td ><strong><?php echo $data[0]->client_fullname; ?></strong></td></tr>
			      <tr><td>Loan Amount:</td><td>Rp <?php echo number_format($data[0]->data_plafond); ?></td></tr>
			      <tr><td>Loan Tenor:</td><td><?php echo $data[0]->data_jangkawaktu; ?></td></tr>
			      <tr><td>Weekly Principal Payment:</td><td>Rp <?php echo number_format($data[0]->data_plafond / $data[0]->data_jangkawaktu); ?></td></tr>
			    </tbody>
			  </table>
			</div>

		</div>
	</div>

		<div class="browseTable">
				<!-- TABLE START!! -->
				
				<table class="table table-striped table-bordered table-hover" width="100%">

	
			    <thead >
			       <tr>
					<th class="center" width="120px">Date</th>
			      	<th class="center">Terms</th>
			      	<th class="center">Freq</th>
			      	<th class="text-right">Principal Payment (IDR)</th>
			      	<th class="text-right">Profit Shared Payment (IDR)</th>
			      	<th class="text-right">Outstanding (IDR)</th>
			      	<th class="text-right">Investment (IDR)</th>
			      	<th class="text-right">Profit (IDR)</th>
			      </tr>
			    </thead>

			    <tbody >

			  
					<?php 
						if(count($data) > 0){
						foreach($data AS $row){ 
					?>
							<tr>
								<td class="text-center"><?php echo date("d M Y", strtotime($row->tr_date)); ?></td>
								<td class="text-center"><?php echo $row->tr_angsuranke; ?></td>
								<td class="text-center"><?php echo $row->tr_freq; ?></td>
								<td class="text-right"><?php echo number_format($row->tr_angsuranpokok); ?></td>
								<td class="text-right"><?php echo number_format($row->tr_profit); ?></td>
								<td class="text-right"><?php echo number_format(($row->data_jangkawaktu - $row->tr_angsuranke) * $row->tr_angsuranpokok); ?></td>
								<td class="text-right"><?php echo number_format(($row->tr_angsuranke) * $row->tr_angsuranpokok); ?></td>
								<td class="text-right"><?php echo number_format($row->tr_angsuranke * 20 /100 * $row->tr_angsuranpokok); ?></td>
								
							</tr>
					<?php } ?>
					<?php }else{ ?>
							<tr>
								<td class="text-center" colspan="10">No Data</td>
							</tr>
					<?php } ?>

				    
				  </tbody>
			  </table>
				<!-- TABLE END!! -->

			  </div>
			 

			</div>
	</div>

<!-- CALLING CHART SCRIPT -->
<script type="text/javascript">
	window.onload = function(){
		var ctx = document.getElementById("chart-area").getContext("2d");
		var ctx2 = document.getElementById("chart-area2").getContext("2d");

		window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});
		window.myDoughnut = new Chart(ctx2).Doughnut(doughnutData2, {responsive : true});
	};
</script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/Chart.min.js"></script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/datatables.min.js"></script>

