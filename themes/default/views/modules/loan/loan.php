

	<div class="container" style="width:1000px;">
		<div class="row">
			<div class="col-md-10 hidden">&nbsp;</div>
			<div class="col-md-2 filter hidden">
				<h3>FILTER</h3>
				<!-- DROPDOWN FILTER -->
				<div class="panel">
	               <div class="btn-group">
	                 <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
	                   <span data-bind="label">Filter Type</span>&nbsp;<span class="caret"></span>
	                 </button>
	                 <ul class="filterMenu dropdown-menu " role="menu">
	                   <li><a href="#">Location</a></li>
	                   <li><a href="#">Sector</a></li>
	                 </ul>
	               </div>
	             </div>
				<!-- DROPDOWN FILTER END -->
			</div>
		</div>
		<div class="row browseData">
			<form method="post" action="<?php echo site_url('loan/addtocart'); ?>">
			<div class="col-md-12 ">
			
				
				<div class="browseTable ">
				<?php if($this->session->flashdata('message')){ ?>
                   <div class="alert alert-warning"><?php echo print_message($this->session->flashdata('message')); ?></div>
                <?php } ?> 
				<!-- TABLE START!! -->
				<table class="table table-striped table-bordered table-hover datatable tbl-responsive">

			    <!-- HEAD TABLE -->
			    <thead >
					<tr>
						<th class="text-center">Choose</th>
						<th class="text-left">Name</th>
						<th class="text-left">Loan Sector</th>
						<th class="text-left">Loan Purpose</th>
						<th  class="text-center" width="30px">Tenor (Week)</th>
						<th class="text-right">Loan Ammount (IDR)</th>
						<th class="text-right">Projected Return (IDR)</th>
						<th class="text-center">Time Left</th>
					</tr>
			    </thead>

			    <tbody data-link="row" class="rowlink">
					
			    	<!-- table Data LOOP -->
					<?php $i=1; ?>
					<?php foreach($borrower as $row){ ?>					
						<?php 
						$startTimeStamp = strtotime(date('Y-m-d'));
						$endTimeStamp 	= strtotime($row->data_date_accept);
						$timeDiff 		= ($endTimeStamp - $startTimeStamp);
						$numberDays 	= $timeDiff/86400;
						$numberDays 	= intval($numberDays);
					?>
					<?php if($numberDays >0 ){ ?>
						<tr href="<?php echo site_url('borrower/profile/').$row->data_client; ?>">
							<!-- FIRST COULUMN -->
							<td class="rowlink-skip checkIt">
								<input name="check_<?php echo $i; ?>" value="1" type="checkbox">
								<input name="loan_<?php echo $i; ?>" value="<?php echo $row->data_id; ?>" type="hidden" />
							</td>
							<td class="text-left">
								<a href="<?php echo site_url();?>loan/detail/<?php echo $row->data_id; ?>"><?php echo $row->client_fullname; ?></a>
								<input name="name_<?php echo $i; ?>" value="<?php echo $row->client_fullname; ?>" type="hidden" />
							</td>
							<td class="text-left">
								<?php echo $row->sector_name; ?>
								<input name="score_<?php echo $i; ?>" value="<?php echo $row->sector_name; ?>" type="hidden" />
							</td>
							<td class="text-left">
								<?php echo $row->data_tujuan; ?>
								<input name="grade_<?php echo $i; ?>" value="<?php echo $row->data_tujuan; ?>" type="hidden" />
							</td>						
							<td class="text-center">50</td>
							<td class="text-right">
								<?php echo number_format($row->data_plafond); ?>
								<input name="plafond_<?php echo $i; ?>" value="<?php echo $row->data_plafond; ?>" type="hidden" />
							</td>
							<td class="text-right">
								<?php 
									$margin_investor = 20 ;
									$projected_return = $row->data_plafond + ($row->data_plafond * $margin_investor / 100); 
									echo number_format($projected_return); 
								?>
							</td>
							<td class="text-center">
								<?php if($numberDays > 0) { echo $numberDays." days";}else{ echo "expired"; }?>
								<input name="_days_<?php echo $i; ?>" value="<?php echo $numberDays; ?>" type="hidden" />
							</td>
						</tr>
					<?php $i++; ?>
					<?php } ?>
					<?php } $no = $i-1; //end foreach ?>
				    <!-- END Table Data LOOP -->
					
				  </tbody>
			  </table>
			<!-- TABLE END!! -->

			  </div>
			  

			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="confirmButton">
					<input name="no" value="<?php echo $no; ?>" type="hidden" />
					<button type="submit" class="btn btn-login btn-md">Continue to Confirmation</button>
				</div>
			</div>
		</div>
		<div class="row">&nbsp;</div>
		</form>
	</div>