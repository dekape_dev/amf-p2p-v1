<div class="container e-wallet">
	<script src="https://api.sandbox.veritrans.co.id/v2/assets/js/veritrans.min.js"></script>
    <script src="https://api.veritrans.co.id/v2/assets/js/veritrans.min.js"></script>
    <?php
    $wallet_balance = $this->session->userdata('wallet');
    if($wallet_balance < $this->cart->total())
    	{ $checkout_url = site_url('transaction/checkout/veritrans'); }
	  else
		  { $checkout_url = site_url('transaction/checkout/pay_with_wallet'); }

	?>
	<form action="<?php echo $checkout_url; ?>" method="post">
		<div class="row my_wallet">
				<div class="col-md-6 walletDetail">
					<h2>MY WALLET</h2>
					<table class="table">
				    <thead>
				      <tr>
				        <th></th><th></th><th></th>
				      </tr>
				    </thead>
				    <tbody>
				      <tr>
				        <td>MY WALLET DISBURSEMENT</td>
				        <td>Rp <?php echo number_format($wallet_disbursed); ?></td>
				      </tr>
				      <tr>
				        <td>MY WALLET AVAILABLE FUND</td>
				        <td>Rp <?php echo number_format($wallet_balance); ?></td>
				      </tr>
				      <tr>
				        <td><strong>MY WALLET ACCUMULATION</strong></td>
				        <td><strong>Rp <?php echo number_format($wallet_total); ?></strong></td>
				      </tr>
				    </tbody>
				  </table>
				</div>
				<div class="col-md-6 walletDetail">
					<h2>MY INVESTMENT</h2>
					<table class="table">
				    <thead>
				      <tr>
				        <th></th><th></th>
				      </tr>
				    </thead>
				    <tbody>
				      <tr>
				        <td>MY PROSPECTIVE BORROWERS</td>
				        <td><?php echo $this->cart->total_items(); ?></td>
				      </tr>
				      <tr>
				        <td><strong>MY INVESTMENT FUND<strong></td>
				        <td><strong>Rp <?php echo number_format($this->cart->total()); ?></strong></td>
				      </tr>
				    </tbody>
				  </table>
				</div>
		</div>
		<div class="row">
				<div class="col-md-6"></div>
				<div class="col-md-6">
					<div class="confirmButton">
						<button type="submit" class="btn btn-login">Fund Transfer</button>
					</div>
				</div>
		</div>
	</form>
</div>
