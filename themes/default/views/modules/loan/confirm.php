	<div class="container namaBorrower">
		<h2>Loan Confrmation</h2>
	</div>


	<div class="container confirmPage">
		<!-- ALERT NEW ITEM -->
		<div  class="hidden alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <p style="color:black;"><strong><?php echo $this->cart->total_items(); ?> Item(s)</strong> telah ditambahkan</p>
		</div>
		<!-- ALERT NEW ITEM END -->

		<!-- ALERT TIMEOUT -->
		<!--
		<div  class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <strong>Your Cart has been time out</strong> back to <a href="">Browse</a> Borrower now
		</div>
		-->
		<!-- ALERT TIMEOUT  -->

		<div>
		<form method="post" action="<?php echo site_url('loan/payment'); ?>">
			<div class="row">
				<div class="browseTable">
					<!-- TABLE START!! -->
					<table class="table table-striped table-hover borderless">
					<!-- HEAD TABLE -->
					<thead >
					  <tr>
						<th >Name</th>
						<th class="center">Loan Sector</th>
						<th class="center">Loan Purpose</th>
						<th class="center">Tenor<br/>(Week)</th>
						<th class="center">Loan Amount<br/>(IDR)</th>
						<th class="center">Projected Return<br/>(IDR)</th>
						<th class="center">Time Left</th>
						<th class="center">Akad</th>
						<th class="center"></th>
					  </tr>
					</thead>

					<tbody>

						<!-- Table Data LOOP -->

						<?php 
							if(count($this->cart->contents()) > 0){
								foreach ($this->cart->contents() as $items): ?>
						<?php
							$borrower = $this->loan_model->view_borrower_detail($items['id'])->result();
							$borrower = $borrower[0];

							$startTimeStamp = strtotime(date('Y-m-d'));
							$endTimeStamp 	= strtotime($borrower->data_date_accept);
							$timeDiff 		= ($endTimeStamp - $startTimeStamp);
							$numberDays 	= $timeDiff/86400;
							$numberDays 	= intval($numberDays);

						?>
						<tr>
							<td><b><?php echo $items['name'];?></b></td>
							<td class="center"><?php echo $borrower->sector_name; ?></td>
							<td class="center"><?php echo $borrower->data_tujuan; ?></td>
							<td class="center">50</td>
							<td class="center"><?php echo number_format($items['price']);?></td>
							<td class="center"><?php echo number_format($items['price'] + (20 / 100 * $items['price']));?></td>
							<td class="center"><?php if($numberDays > 0) { echo $numberDays." days";}else{ echo "expired"; }?></td>
							<td class="center"><input name="check_<?php echo $borrower->data_id; ?>" value="1" type="checkbox" required > <a href="<?php echo site_url();?>loan/akad/<?php echo $borrower->data_akad; ?>/<?php echo $borrower->data_id; ?>" title="View Akad" class="popup" ><?php echo $borrower->data_akad; ?></a></td>
							<td class="text-center">
								<a href="<?php echo site_url();?>loan/delcart/<?php echo $items['rowid']; ?>" class="delete" ><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
							</td>
						</tr>
						<?php endforeach; ?>
						<!-- END Table Data LOOP -->
					<?php }else{ ?>
							<tr>
								<td class="text-center" colspan="9">No Data</td>
							</tr>
					<?php } ?>

					  </tbody>
				  </table>
					<!-- TABLE END!! -->
					<strong>Dengan memberikan tanda centang/setuju pada Akad berarti Anda telah menyetujui isi dari Akad tersebut.</strong>

				  </div>
			</div>

			<div class="row">
				<div class="col-md-6 paymentTotal"></div>
				
				<div class="col-md-6 paymentTotal">
					<h2 class="pull-right">INVESTMENT FUNDS RECAP</h2>
					<table class="table">
						<thead>
						  <tr>
							<th></th>
							<th></th>
						  </tr>
						</thead>
						<tbody>
						  <tr>
							<td> Borrowers Total</td>
							<td><?php echo $this->cart->total_items(); ?></td>
						  </tr>
						  <tr>
							<td>Total Investments</td>
							<td><?php echo "Rp ".number_format($this->cart->total()); ?></td>
						  </tr>
						  <tr class="hidden">
							<td>Potongan Harga</td>
							<td><?php echo "Rp ".number_format($discount); ?></td>
						  </tr>
						  <tr class="hidden">
							<td>Tambahan Biaya</td>
							<td><?php echo "Rp ".number_format($fee); ?></td>
						  </tr>
						  <tr class="hidden">
							<td>Pajak</td>
							<td><?php echo "Rp ".number_format($tax); ?></td>
						  </tr>
						  <tr>
							<td><strong>GRAND TOTAL</strong></td>
							<td><strong><?php echo "Rp ".number_format( ($this->cart->total() + $fee + $tax) - $discount ); ?></strong></td>
						  </tr>
						  <tr>
							<td colspan="2"><input type="checkbox" name="" required /> &nbsp; Yes I agree with <a href="<?php echo site_url('termofservices'); ?>" target="_blank" title="Read Terms of Services"><u>Terms of Services</u></a> and <a  class="popup" href="<?php echo site_url('loan/ssupp'); ?>" target="_blank" title="Read SSUPP"><u>SSUPP</u></a></td>
						  </tr>
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-md-6 paymentTotal">&nbsp;</div>
					<div class="col-md-6 paymentTotal">
						<div class="confirmButton topUp">
							<!-- <button type="submit" class="btn btn-login">Top Up</button> -->
							<a href="<?php echo site_url('loan'); ?>" type="submit" class="btn" style="background-color:#704390; color:#fff;">Browse More Loans</a>
							<a href="<?php echo site_url('loan/payment'); ?>" type="submit" class="btn hidden" style="background-color:#704390; color:#fff;"> Continue to Transaction</a>
							<button type="submit" class="btn" style="margin-top:20px;background-color:#704390; color:#fff;">Continue to Transaction</button>
						</div>
					</div>
				</div>
				<div class="row">&nbsp;</div>
			</div>
		</form>
	</div>

	
	
	<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->