	

	<div class="container namaBorrower">
		<h2><?php echo $borrower->client_fullname; ?></h2>
	</div>


	<!-- Personal Data -->
	<div class="container personalData">
		<div class="row perData">
			<div class="col-md-6">
				<h3>Pesonal Data</h3>
				<!-- DATA TABLE -->
				<table class="table">
			
			    </thead>
			    <tbody>
			      <tr><td>Register Date</td><td><?php echo date("d M Y",strtotime($borrower->client_reg_date)); ?></td></tr>

			      <tr><td>Loan Submitted</td><td>Rp <?php echo number_format($borrower->data_plafond); ?></td></tr>

			       <tr><td>Loan Amount</td><td>Rp <?php echo number_format($borrower->data_plafond); ?></td></tr>

			      <tr><td>Loan Tenor</td><td><?php echo $borrower->data_jangkawaktu; ?> weeks</td></tr>

			      <tr><td>Projected Return</td><td>Rp <?php echo number_format($borrower->data_plafond + ($borrower->data_plafond * 20 / 100)); ?></td></tr>
			      
			      <tr><td>Weekly Principal Payment</td><td>Rp <?php echo number_format($borrower->data_plafond/$borrower->data_jangkawaktu); ?></td></tr>

			      <tr><td>Weekly Profit Shared Payment</td><td>Rp <?php echo number_format(($borrower->data_plafond * 20 / 100)/$borrower->data_jangkawaktu); ?></td></tr>

			      <tr><td>Monthly Income</td><td >Rp <?php echo number_format($borrower->data_pendapatan_total); ?></td></tr>

			      <tr><td>Employment</td><td><?php if($borrower->data_pendapatan_istrijenisusaha == ""){ echo "-"; }else{ echo $borrower->data_pendapatan_istrijenisusaha; } ?></td></tr>
			      <tr><td>Location</td><td><?php echo $borrower->branch_name; ?>, <?php echo $borrower->branch_location; ?></td></tr>

			      
			    </tbody>
			  </table>
			</div>

			<div class="col-md-6">
				<h3>Membership</h3>
				<!-- DATA TABLE -->
				<table class="table">
			
			    </thead>
			    <tbody>
			      
			      <tr><td>Loan Submitted</td><td><?php echo date("d M Y", strtotime($borrower->data_tgl)); ?></td></tr>

			      <tr><td>Review Status</td><td ><strong><?php if($borrower->data_status_pengajuan == "v"){echo "Approved"; } ?></strong></td></tr>

			      <tr><td>Member Loan#</td><td><?php echo $borrower->client_account;  ?></td></tr>
			      

			      
			    </tbody>
			  </table>
			</div>
			<!-- <div class="col-md-6 chartPerData">
				<ul>
					<li>
						<?php 
							$ratio = number_format($borrower->data_plafond / ($borrower->data_pendapatan_total*12) * 100, 2);
						?>
						<div id="myStat" data-dimension="400" data-text="<?php echo $ratio;?>%" data-info="Debt to Income Ratio" data-width="30" data-fontsize="28" data-percent="<?php echo $ratio;?>" data-fgcolor="purple" data-bgcolor="#eee" data-fill="#faedff"></div>
					</li>
				</ul>
			</div> -->
		</div>
	</div>

	<div class="container">
		<div class="row titleTable ">
			<h2 class="text-center " style="">Credit Profile</h2><br/>
			<div class="col-md-6">

				<!-- DATA TABLE -->
				<table class="table">
			
			    
			    <tbody>
			      <tr><td>Earliest Credit Line:</td><td ><strong><?php echo $borrower_detail['earliest_credit_line']; ?></strong></td></tr>
			      <tr><td>Open Credit Lines:</td><td><?php echo $borrower_detail['open_credit_line']; ?></td></tr>
			      <tr><td>Total Credit Lines:</td><td><?php echo $borrower_detail['total_credit_line']; ?></td></tr>
			      <tr><td>Revolving Credit Balance:</td><td>Rp <?php echo number_format($borrower_detail['revolving_credit_balance']); ?></td></tr>
			    </tbody>
			  </table>
			</div>

			<div class="col-md-6">

				<!-- DATA TABLE -->
				<table class="table ">
			
			    
			    <tbody>			      
			      <tr><td>Account Now Delinquent:</td><td><?php echo $borrower_detail['count_par']; ?></td></tr>
			      <tr><td>Delinquent Amount:</td><td ><strong>Rp <?php echo number_format($borrower_detail['count_par_amount']); ?></strong></td></tr>
			      <tr><td>Delinquencies (last 2 yrs):</td><td><?php echo $borrower_detail['count_par']; ?></td></tr>
			      <tr><td>Month Since Last Delinquencies:</td><td>n/a</td></tr>			      
			    </tbody>
			  </table>
			</div>

		</div>
	</div>



	<!-- TABLE DATA HISTORY -->
	<div class="container historyData">

		<h2>Credit History</h2>

		<div class="browseTable">
			
				<table class="table table-striped table-bordered table-hover" width="100%">

	
			    <thead >
			      <tr>
					<th class="text-center">Credit Line</th>
					<th class="text-center">Disburshment Date</th>
					<th class="text-center">Maturity Date</th>
					<th class="text-right">Loan Ammount (IDR)</th>
					<th class="text-right">Total Installment  (IDR)</th>
					<th class="text-right">Delinquencies</th>
				</tr>
			    </thead>

			    <tbody >

			  
					<?php foreach($history AS $h){ ?>
				    <tr>
				    	<td class="text-center"><?php echo $h->data_ke; ?></td>
				    	<td class="text-center"><?php echo date("d M Y",strtotime($h->data_date_accept)); ?></td>
				    	<td class="text-center"><?php echo date("d M Y",strtotime($h->data_jatuhtempo)); ?></td>
				    	<td class="text-right"><?php echo number_format($h->data_plafond); ?></td>
				    	<td class="text-right"><?php echo number_format(($h->data_plafond + $h->data_margin) / $h->data_jangkawaktu); ?></td>
				    	<td class="text-right"><?php echo $h->data_par; ?></td>
				    </tr>
					<?php } ?>

				    
				  </tbody>
			  </table>


			  </div>
	</div> 

	

	<div class="container navBack">
		<div class="row">
			<div class="col-lg-6 text-left">
				<a href="" class="addItem" onclick='window.history.back();'><span class="glyphicon glyphicon-chevron-left"></span> Back to Browse Loan</a>
				
			</div>
			<div class="col-lg-6 text-right">
				<form method="post" action="<?php echo site_url('loan/addtocart'); ?>">
				<input type="hidden" name="no" value="1" />
				<input type="hidden" name="check_1" value="1" />
				<input type="hidden" name="loan_1" value="<?php echo $borrower->data_id; ?>" />
				<input type="hidden" name="plafond_1" value="<?php echo $borrower->data_plafond; ?>" />
				<input type="hidden" name="name_1" value="<?php echo $borrower->client_fullname; ?>" />
				<input type="hidden" name="grade_1" value="12" />
				<input type="hidden" name="score_1" value="122" />
				<button type="submit" class="btn addItem">Select This Borrower</button>
				</form>
			</div>

		</div>
		<br/><br/>
	</div>

<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/jquery-latest.js"></script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/bootstrap.min.js"></script>
<script src="<?php echo $this->template->get_theme_path(); ?>/js/jasny-bootstrap.min.js"></script>
<script src="<?php echo $this->template->get_theme_path(); ?>/js/jquery.circliful.min.js"></script>
<script>
$( document ).ready(function() {
        $('#myStathalf').circliful();
		$('#myStat').circliful();
		$('#myStat2').circliful();
    });
</script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/main.js"></script>
