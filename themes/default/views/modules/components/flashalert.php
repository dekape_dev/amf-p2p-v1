<!-- ALERT NEW ITEM -->
		<div  class="alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <strong><?php echo $this->cart->total_items(); ?> Item(s)</strong> telah ditambahkan
		</div>
<!-- ALERT NEW ITEM END -->

<!-- ALERT TIMEOUT  -->

		<div  class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  <strong>Your Cart has been time out</strong> back to <a href="">Browse</a> Borrower now
		</div>

<!-- ALERT TIMEOUT  -->