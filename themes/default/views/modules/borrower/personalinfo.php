  <div class="headerSimple">
    <img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png">
  </div>
  <div class="container">
    <div class="row formRegister">
      <form method="post" action="">
        <div class="form-group">
          <input name="firstname" type="text" class="form-control" id="" placeholder="FirstName" value="<?php echo $borrower->borrower_first_name;  ?>">
        </div>

        <div class="form-group">
          <input name="lastname" type="text" class="form-control" id="" placeholder="LastName" value="<?php echo $borrower->borrower_last_name;  ?>">
        </div>

        <div class="form-group">
          <input name="birthday" type="text" class="form-control" id="" placeholder="Date of Birth (YYYY-MM-DD)" value="<?php echo $borrower->borrower_birth_date;  ?>">
        </div>

        <div class="form-group">
          <textarea name="address" class="form-control" rows="auto" placeholder="Street Address"><?php echo $borrower->borrower_address;  ?></textarea>
        </div>

        <div class="form-group">
          <input name="city" type="text" class="form-control" id="" placeholder="City" value="<?php echo $borrower->borrower_city;  ?>">
        </div>

        <div class="form-group">
          <input name="province" type="text" class="form-control" id="" placeholder="Province" value="<?php echo $borrower->borrower_province;  ?>">
        </div>

        <div class="form-group">
          <input name="zipcode" type="text" class="form-control" id="" placeholder="ZipCode" value="<?php echo $borrower->borrower_zipcode;  ?>">
        </div>

        <div class="form-group">
          <input name="phone" type="text" class="form-control" id="" placeholder="Primary Phone" value="<?php echo $borrower->borrower_phone;  ?>">
        </div>

        <div class="form-group">
          <input name="idcard" type="text" class="form-control" id="" placeholder="KTP" value="<?php echo $borrower->borrower_idcard;  ?>">
        </div>

        <div class="form-group">
          <input name="taxcard" type="text" class="form-control" id="" placeholder="NPWP" value="<?php echo $borrower->borrower_taxcard;  ?>">
        </div>
        <div class="form-group">
          <input name="securityquestion"  type="text" class="form-control" id="" placeholder="Security Question">
        </div>

        <h6>I have read and agree the following:</h6>

       <div class="checkbox">
          <label>
            <input name="agreement" type="checkbox" value="">
            investor agreement and propectus
          </label>
      </div>

      <div class="checkbox">
          <label>
            <input name="trust" type="checkbox" value="">
            trust agreement
          </label>
      </div>

      <div class="checkbox">
          <label>
            <input name="taxwithholding" name="" type="checkbox" value="">
            no tax withholding
          </label>
      </div>
          <button type="submit" class="btn btn-default">SAVE ></button>
      </form>
      <a href="<?php echo site_url('borrower'); ?>">
        <button class="btn btn-default" style="background-color:#704390;">< BACK</button>
      </a>
    </div>
  </div>
