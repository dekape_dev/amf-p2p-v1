  <div class="headerSimple">
    <a href="<?php echo site_url(); ?>" title="Amartha"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png"></a>
  </div>

  <div class="container">
    <div class="row">
     <div class="col-lg-8 col-md-8 col-xs-12 col-lg-offset-2">
      <br/><br/><br/><?php if($msg){ ?>
                <div class="alert alert-<?php echo $msg_style; ?> alert-dismissable"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo $msg; ?></div>
            <?php } ?>
    </div>
    </div>
    <div class="row formRegister">
      <form method="post" action="letmein_to_borrow">
        <?php if($this->session->flashdata('message')){ ?>
                   <?php echo print_message($this->session->flashdata('message')); ?>
                <?php } ?>

          <h3>BORROWER LOGIN</h3><br/>
          <div class="form-group">
            <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
          </div>

          <div class="form-group">
            <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-default">LOGIN</button>
      </form>
    </div>
  </div>
