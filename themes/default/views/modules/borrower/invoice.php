	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<div class="headerSimple">
		<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png">
	</div>
  <?php $num_notif    = $this->session->userdata('unread_notifications'); ?>
  <?php
        if($num_notif > 0 ) { $label = 'label-warning'; }
        else { $label = 'label-primary'; }
  ?>
  <?php $notification = '<span class="label '.$label.'">'.$num_notif.'</span>'; ?>
  <div class="headerUser">
    <div class="row userNavBorrow">
      <div class="col-md-2 "></div>
      <div class="col-md-2"><a href="">Hi, <?php echo $this->session->userdata('bfullname'); ?>.</a></div>
      <div class="col-md-2"><a href="<?php echo site_url('borrower/profile'); ?>">Account and Setting</a></div>
      <div class="col-md-2"><a href="<?php echo site_url('borrower/notification'); ?>">Notification <?php echo $notification; ?></a></div>
      <div class="col-md-2"><a href="<?php echo site_url('logout_to_borrow'); ?>">Sign Out</a></div>
      <div class="col-md-2"></div>
    </div>
  </div>

	<div style="border-bottom: 1px solid #919191;">
		<div class="container" >


			<div class="row userNav2">

				<div class="col-md-4"><a href="<?php echo site_url('borrower');?>">Overview</a></div>
				<div class="col-md-4 "><a href="<?php echo site_url('borrower/loanunder20mil'); ?>">Loan Request</a></div>
				<div class="col-md-4 active"><a href="<?php echo site_url('borrower/invoice'); ?>">Statement</a></div>
			</div>


		</div>
	</div>

	<div class="container">
    <div class="row tableInvoice">
      <!-- TABLE ! -->
      <table class="table table-striped table-bordered " >
        <thead>
          <tr>
            <th>Tanggal Cetak Tagihan</th>
            <th>Tanggal Jatuh Tempo</th>
            <th>Pembayaran Minimun yang tertunggak</th>
            <th>Total Tagihan</th>
          </tr>

        </thead>

        <tbody>
          <tr>
            <td>20/03/2015</td>
            <td>20/05/2015</td>
            <td>Rp. 234.567</td>
            <td>Rp. 1.234.567</td>
          </tr>

        </tbody>
      </table>
      <!-- TABLE END -->

      <!-- TABLE 2 -->
      <table class="table table-striped table-bordered " >
        <thead>
          <tr>
            <th>Nomor Kartu</th>
            <th>Batas Kredit</th>
            <th>Jumlah Tagihan</th>
            <th>Batas Tarik Tunai</th>
            <th>Sisa Batas Kredit</th>
            <th>Pembayaran Minimum</th>
          </tr>

        </thead>

        <tbody>
          <tr>
            <td>123 4324235 556</td>
            <td>Rp. 2.000.000,-</td>
            <td>Rp. 2.345.677,-</td>
            <td>Rp. 500.000,-</td>
            <td>Rp. 527.900,-</td>
            <td>Rp. 2.345.678,-</td>
          </tr>

        </tbody>
      </table>
      <!-- TABLE END -->

      <!-- TABLE 3 -->
      <h3>Rincian Transaksi</h3>
      <table class="table table-striped table-bordered " >
        <thead>
          <tr>
            <th>Tanggal Transaksi</th>
            <th>Tanggal Pencatatan</th>
            <th>Rincian Transaksi</th>
            <th>Jumlah Tagihan</th>
          </tr>

        </thead>

        <tbody>
          <tr>
            <td>20/03/2015</td>
            <td>20/03/2015-</td>
            <td style="text-align:left;">Loren ipsum Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle quora plaxo ideeli hulu weebly balihoo...</td>
            <td style="text-align:right;">Rp. 500.000,-</td>
          </tr>

          <tr>
            <td>20/03/2015</td>
            <td>20/03/2015-</td>
            <td style="text-align:left;">Loren ipsum Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle quora plaxo ideeli hulu weebly balihoo...</td>
            <td style="text-align:right;">Rp. 500.000,-</td>
          </tr>

          <tr>
            <td>20/03/2015</td>
            <td>20/03/2015-</td>
            <td style="text-align:left;">Loren ipsum Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle quora plaxo ideeli hulu weebly balihoo...</td>
            <td style="text-align:right;">Rp. 500.000,-</td>
          </tr>

        </tbody>
      </table>
      <!-- TABLE END -->


    </div>
  </div>
