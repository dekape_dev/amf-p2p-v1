	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<div class="headerSimple">
		<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png">
	</div>
	<?php $num_notif    = $this->session->userdata('unread_notifications'); ?>
  <?php
        if($num_notif > 0 ) { $label = 'label-warning'; }
        else { $label = 'label-primary'; }
  ?>
  <?php $notification = '<span class="label '.$label.'">'.$num_notif.'</span>'; ?>
  <div class="headerUser">
    <div class="row userNavBorrow">
      <div class="col-md-2 "></div>
      <div class="col-md-2"><a href="">Hi, <?php echo $this->session->userdata('bfullname'); ?>.</a></div>
      <div class="col-md-2"><a href="<?php echo site_url('borrower/profile'); ?>">Account and Setting</a></div>
      <div class="col-md-2"><a href="<?php echo site_url('borrower/notification'); ?>">Notification <?php echo $notification; ?></a></div>
      <div class="col-md-2"><a href="<?php echo site_url('logout_to_borrow'); ?>">Sign Out</a></div>
      <div class="col-md-2"></div>
    </div>
  </div>

	<div style="border-bottom: 1px solid #919191;">
		<div class="container" >


			<div class="row userNav2">

				<div class="col-md-4 active"><a href="<?php echo site_url('borrower');?>">Overview</a></div>
				<div class="col-md-4 "><a href="<?php echo site_url('borrower/loanunder20mil'); ?>">Loan Request</a></div>
				<div class="col-md-4"><a href="<?php echo site_url('borrower/invoice'); ?>">Statement</a></div>


				<!-- <div class="col-md-3 right active"><a href="<?php echo site_url('dashboard'); ?>">Account</a></div>
				<div class="col-md-3 right"><a href="<?php echo site_url('investor/portfolio'); ?>">Portfolio</a></div>
				<div class="col-md-3 right"><a href="<?php echo site_url('investor/history'); ?>">History</a></div>
				<div class="col-md-3 left dropdown">
					<a class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="marketperfomance.html">
					Market Perfomance</a>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					    <li><a href="<?php echo site_url('marketperformance/statistics'); ?>">Statistic</a></li>
					    <li><a href="<?php echo site_url('marketperformance/investor'); ?>">Investor</a></li>
					    <li><a href="<?php echo site_url('marketperformance/borrower'); ?>">Borrower</a></li>
					</ul>
				</div> -->
			</div>

			<!-- <div class="stepNav">
				<ul>
					<li class="active">
						<a href="<?php echo site_url('dashboard'); ?>">Overview</a>
					</li>
					<li>></li>
					<li><a href="<?php echo site_url('loan'); ?>">Browse Loan</a></li>
					<li>></li>
					<li><a href="<?php echo site_url('loan/confirmation'); ?>">Confirmation</a></li>
					<li>></li>
					<li><a href="<?php echo site_url('loan/payment'); ?>">Transfer Fund</a></li>
				</ul>
			</div> -->

		</div>
	</div>

	<div class="container">
		<div class="row data1">
			<div class="col-md-4"><h1>Rp. 8.500.000,-</h1> <h3>Outstanding principal</h3></div>
			<div class="col-md-4"><h1>Rp. 10.000.00,-</h1> <h3>Total loan</h3></div>
			<div class="col-md-4"><h1>85%</h1> <h3>Complete</h3></div>

		</div>

		<div class="row data2">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="progress">
			  <div class="progress-bar" role="progressbar" aria-valuenow="15"
			  aria-valuemin="0" aria-valuemax="100" style="width:85%">
			    85%
			  </div>
			</div>

			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
	<div class="container buttonLoan" style="text-align:center;">
		<a href="<?php echo site_url('borrower/loanunder20mil'); ?>" class="button_main btn btn-default" type="submit">Loan Request</a>
	</div>
	<div class="container">
		<div class="row data4">
			<div class="col-md-12">
				<div class="infoBox">
					<h5>Profit-shared</h5>
					<h1>RP. 150.000,-</h1>
					<h5 class="bottom">Total payment+profit-shared Rp. 1.650.000</h5>
					<button type="submit" class="btn btn-login">CASH OUT</button>

				</div>
			</div>
			<!-- <div class="col-md-6">
				<div class="infoBox">
					<h5>Projected Annualized Return</h5>
					<h1>10.23%</h1>
					<h5 class="bottom">Average nisbah for Investor 52%</h5>
				</div>
			</div> -->

		</div>

	</div>

	<div class="container">
		<div class="row data6">
	        <div class="col-md-6">
	        	<table class="table">
			    <thead>
			      <tr>
			        <th>My Account #1500001</th>
			        <th></th>

			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td>Available cash</td>
			        <td>Rp. 1.500.000</td>

			      </tr>
			      <tr>
			        <td>Outstanding principal</td>
			        <td>Rp. 8.500.000</td>

			      </tr>
			      <tr>
			        <td class="summary">Account value</td>
			        <td class="summary">Rp. 10.000.000</td>

			      </tr>
			    </tbody>
			  </table>

	        </div>

	        <div class="col-md-6">
	        	<table class="table">
			    <thead>
			      <tr>
			        <th></br></th>
			        <th></th>

			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td>My notes at a glance</td>
			        <td>12</td>

			      </tr>
			      <tr>
			        <td>Issued and current</td>
			        <td>5</td>

			      </tr>
			       <tr>
			        <td>Fully paid</td>
			        <td>4</td>

			      </tr>
			      <tr>
			        <td>Late 1-14 days</td>
			        <td>1</td>

			      </tr>

			      <tr>
			        <td>Late 15-30 days</td>
			        <td>0</td>

			      </tr><tr>
			        <td>Late 30+ days</td>
			        <td>1</td>

			      </tr><tr>
			        <td>Write-off</td>
			        <td>1</td>

			      </tr>

			    </tbody>
			  </table>
			  <div  style="float:right;">
			  		<h5 style="float:right; padding-left:5px;"><a href=""> Amount</a></h5>
				  <h5 class="active" style="float:right;">Display By <a href="">Number</a>   |  </h5>

			</div>
	        </div>
		</div>
	</div>
