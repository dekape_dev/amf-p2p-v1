	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<div class="headerSimple">
		<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png">
	</div>
	<?php $num_notif    = $this->session->userdata('unread_notifications'); ?>
  <?php
        if($num_notif > 0 ) { $label = 'label-warning'; }
        else { $label = 'label-primary'; }
  ?>
  <?php $notification = '<span class="label '.$label.'">'.$num_notif.'</span>'; ?>
  <div class="headerUser">
    <div class="row userNavBorrow">
      <div class="col-md-2 "></div>
      <div class="col-md-2"><a href="">Hi, <?php echo $this->session->userdata('bfullname'); ?>.</a></div>
      <div class="col-md-2"><a href="<?php echo site_url('borrower/profile'); ?>">Account and Setting</a></div>
      <div class="col-md-2"><a href="<?php echo site_url('borrower/notification'); ?>">Notification <?php echo $notification; ?></a></div>
      <div class="col-md-2"><a href="<?php echo site_url('logout_to_borrow'); ?>">Sign Out</a></div>
      <div class="col-md-2"></div>
    </div>
  </div>

	<div style="border-bottom: 1px solid #919191;">
		<div class="container" >


			<div class="row userNav2">

				<div class="col-md-4"><a href="<?php echo site_url('borrower');?>">Overview</a></div>
				<div class="col-md-4 active"><a href="<?php echo site_url('borrower/loanunder20mil'); ?>">Loan Request</a></div>
				<div class="col-md-4"><a href="<?php echo site_url('borrower/invoice'); ?>">Statement</a></div>
			</div>


		</div>
	</div>

	<div class="container">
		<div class="row loanNav">
			<div class="col-md-2"></div>
			<div class="col-md-4"><a href="<?php echo site_url('borrower/loanunder20mil'); ?>"><h3>Below 20 Million</h3></a></div>
			<div class="col-md-4 active"><a href="<?php echo site_url('borrower/loanabove20mil'); ?>"><h3>Above 20 Million</h3></a></div>
			<div class="col-md-2"></div>

		</div>
	</div>

	<div class="container">
		<div class="row loanForm">
			<div class="col-md-12 col-sm-12">
				<h2>Please Complete Detail Below</h2>
				<form>
				  <!-- <div class="form-group">
				    <input type="text" class="form-control" id="" placeholder="Name">
				  </div> -->
				  <div class="form-group">
				  	<div class="input-group">
				      <div class="input-group-addon">IDR</div>
				      	<input type="text" class="form-control" id="exampleInputAmount" placeholder="Amount">
				      <div class="input-group-addon">.000</div>
				    </div>
				  </div>

					  <div class="form-group">

						<select class="form-control">
						  <option>18 Month</option>
						  <option>24 Month</option>
						  <option>3</option>
						  <option>4</option>
						  <option>5</option>
						</select>

					</div>
					<div class="form-group">
						<select class="form-control">
						  <option>Select purpose</option>
						  <option>2</option>
						  <option>3</option>
						  <option>4</option>
						  <option>5</option>
						</select>
					</div>

					<div class="form-group">
					    <input type="email" class="form-control" id="" placeholder="Email">
					</div>
				</form>

				<div class="row">
					<div class="col-md-2">
						<form>
							<div class="form-group">
								<select class="form-control">
								  <option>Title</option>
								  <option>Mr</option>
								  <option>Mrs</option>
								 </select>
							</div>
						</form>
					</div>
					<div class="col-md-5">
						<div class="form-group">
				    		<input type="text" class="form-control" id="" placeholder="First Name">
				  		</div>
					</div>
					<div class="col-md-5">
						<div class="form-group">
				    		<input type="text" class="form-control" id="" placeholder="Last Name">
				  		</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12">
						<h3>Date of Birth</h3>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<form>
							<div class="form-group">
								<select class="form-control">
								  <option>Date</option>
								  <option></option>
								  <option>Mrs</option>
								 </select>
							</div>
						</form>
					</div>
					<div class="col-md-4">
						<form>
							<div class="form-group">
								<select class="form-control">
								  <option>Month</option>
								  <option>Mr</option>
								  <option>Mrs</option>
								 </select>
							</div>
						</form>
					</div>
					<div class="col-md-4">
						<form>
							<div class="form-group">
								<select class="form-control">
								  <option>Year</option>
								  <option>Mr</option>
								  <option>Mrs</option>
								 </select>
							</div>
						</form>
					</div>
				</div>
				<form>
					<div class="form-group">
				    <input type="number" class="form-control" id="" placeholder="Phone Number">
				  </div>
				  <label>Address</label>
				  	<div class="form-group">
				    	<textarea class="form-control" rows="3"></textarea>
				  	</div>

				  	<div class="form-group">
				    	<div class="form-group">
								<select class="form-control">
								  <option>City</option>
								  <option>1</option>
								  <option>2</option>
								 </select>
							</div>
				  	</div>

				  	<div class="form-group">
				    	<div class="form-group">
								<select class="form-control">
								  <option>Province/State</option>
								  <option>1</option>
								  <option>2</option>
								 </select>
						</div>
				  	</div>
				  	<div class="row">
				  		<div class="col-md-8">
				  		<div class="form-group">
					    	<div class="form-group">
									<select class="form-control">
									  <option>Country</option>
									  <option>1</option>
									  <option>2</option>
									 </select>
							</div>
				  		</div>

				  		</div>
				  		<div class="col-md-4">
				  			<div class="form-group">
					    		<input type="text" class="form-control" id="" placeholder="Postal Code">
					  		</div>
				  		</div>
				  	</div>
				  	<div class="form-group">
				    	<div class="form-group">
								<select class="form-control">
								  <option>How long you have lived there?</option>
								  <option>1</option>
								  <option>2</option>
								 </select>
						</div>
				  	</div>

				  	<div class="checkbox term">
					    <label>
					      <input type="checkbox"> Please click here to confirm you agree to Our <a href="#" style="color: purple;">Website &amp; Exchange Term of Use</a> and <a href="#" style="color: purple;">Our Privacy and Policy.</a>
					    </label>
					</div>

					<div class="checkbox term">
					    <label>
					      <input type="checkbox"> I don’t want spam from 3rd parties but I am happy to receive RateSetter news and special offers. You can unsubscribe at any time.
					    </label>
					</div>

					<div class="checkbox term">
					    <label>
					      <input type="checkbox">If we can’t help with a loan, one of our carefully selected partners may be able to. If you are happy to be contacted by one of them, please tick this box. RateSetter, acting as a Credit Broker, will receive a commission for loans arranged by partners.
					    </label>
					</div>

					<div class="container startContent" style="padding-top:0px;">
		<button class="btn btn-default btn-save" type="submit">SUBMIT NOW</button>
	</div>

				</form>



			</div>
		</div>
	</div>
