
<!-- HEADER -->
  <div id="headerContact">
    <div class="headerNav">
      <div class="logo">
        <a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
      </div>
      <div class="menu_mobile">
        <div>
          <button id="mobile_toggle" >
            <img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="100px">
            <h2>MENU</h2>
          </button>
        </div>
      </div>

      <div class="row navi shadowNavi">
        <div class="col-md-1 "></div>

        <?php if ($this->session->userdata('site_lang') == 'english') { ?>
        <div class="col-md-2">
          <a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
        </div>
        <?php } else {?>
        <div class="col-md-2">
          <a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
        </div>
        <?php }?>


        <?php if ($this->session->userdata('site_lang') == 'english') { ?>
        <div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
        </div>
        <?php } else {?>
        <div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
        </div>
        <?php }?>


        <div class="col-md-2"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>

        <?php if ($this->session->userdata('site_lang') == 'english') { ?>
        <div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
        </div>
        <?php } else {?>
        <div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
        </div>
        <?php }?>

        <?php if ($this->session->userdata('site_lang') == 'english') { ?>
        <div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
        </div>
        <?php } else {?>
        <div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
        </div>
        <?php }?>

        <?php if ($this->session->userdata('site_lang') == 'english') { ?>
        <div class="col-md-1 res">
          <?php
            if($this->session->userdata('logged_in_user')) {
              echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
            }else{
          ?>
          <a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
          <div class="loginPop">
            <form method="post" action="letmein">
              <div class="form-group">
                <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
              </div>
              <div class="form-group">
                <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
              </div>
              <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
              <button class="btn btn-login">Login</button>
            </form>
          </div>
          <?php } ?>
        </div>
        <?php } else {?>
        <div class="col-md-1 res">
          <?php
            if($this->session->userdata('logged_in_user')) {
              echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
            }else{
          ?>
          <a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
          <div class="loginPop">
            <form method="post" action="letmein">
              <div class="form-group">
                <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
              </div>
              <div class="form-group">
                <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
              </div>
              <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
              <button class="btn btn-login">MASUK</button>
            </form>
          </div>
          <?php } ?>
        </div>
        <?php }?>
      </div>

      <!-- Hero Text -->
      <div class="startContent clearfix">
          <?php if ($this->session->userdata('site_lang') == 'english') { ?>
          <h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Expanding Outreach, Delivering Happiness</h2>
          <h3 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">To empower low-income families in rural areas with affordable financial services, enabling them to pursue life for greater purposes</h3>

          <?php } else {?>
          <h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Kami mendengarkan untuk memberi solusi terbaik</h2>
          <h3 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Melalui pendekatan human-centered, kami berusaha terbaik untuk menggali insight <br/>dari masyarakat demi pengembangan layanan dan organisasi kami</h3>

        <?php }?>

      </div>
      </div>
  </div>
  <!-- END OF HEADER -->


  <section id="" >
  <div class="container container-fixed-lg p-t-50 p-b-100  " >

<?php
  /* Server-Side Form, perlu dipindahkan ke controller? */
  if ($_SERVER['REQUEST_METHOD'] == 'POST'){
  $nama = isset($_POST['c_name']) ? trim(htmlentities($_POST['c_name'])):'';
  $email = isset($_POST['c_email']) ? trim(htmlentities($_POST['c_email'])):'';
  $phone = isset($_POST['c_phone']) ? trim(htmlentities($_POST['c_phone'])):'';
  $plafond = isset($_POST['c_plafond']) ? trim(htmlentities($_POST['c_plafond'])):'';
  $usaha = isset($_POST['c_usaha']) ? trim(htmlentities($_POST['c_usaha'])):'';
  $tujuan = isset($_POST['c_tujuan']) ? trim(htmlentities($_POST['c_tujuan'])):'';
  $nilai = isset($_POST['c_nilai']) ? trim(htmlentities($_POST['c_nilai'])):'';
  $sumber = isset($_POST['c_sumber']) ? trim(htmlentities($_POST['c_sumber'])):'';
  $tenor = isset($_POST['c_tenor']) ? trim(htmlentities($_POST['c_tenor'])):'';
  /* untuk menampung variabel post dari captcha google adalah g-recaptcha-reponse */
  $captcha = isset($_POST['g-recaptcha-response']) ? $_POST['g-recaptcha-response']:'';

  $secret_key = '6LcWEhwTAAAAAOEJlY9kBFcU92JJEoLeOzrS-eEm'; //masukkan secret key-nya berdasarkan secret key masig-masing saat create api key nya
  $error = 'Mohon maaf, Periksa kembali kelengkapan informasi dan pastikan memberikan centang pada reCAPTCHA';
  if ($captcha != '' && $nama != '' && $email != '' && $phone != '' && $plafond != '' && $usaha != '' && $tujuan != '') {
     $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secret_key) . '&response=' . $captcha;   
     $recaptcha = file_get_contents($url);
     $recaptcha = json_decode($recaptcha, true);
     if (!$recaptcha['success']) {
      echo $error;

     } else {
      $to = "info@amartha.com";
      $subject = "Prospect Borrower from ".$nama;
      $txt = "Nama : ".$nama."\r\n";
      $txt .= "Email : ".$email."\r\n";
      $txt .= "Phone : ".$phone."\r\n";
      $txt .= "Plafond : ".$plafond."\r\n";
      $txt .= "Usaha : ".$usaha."\r\n";
      $txt .= "Tujuan : ".$tujuan."\r\n";
      $txt .= "Nilai : ".$nilai."\r\n";
      $txt .= "Sumber : ".$sumber."\r\n";
      $txt .= "Tenor : ".$tenor;

      $headers = "From: NoReply <noreply@amartha.com>" . "\r\n" .
      "CC: medi@amartha.co.id";

      mail($to,$subject,$txt,$headers);
      echo "Pesan terkirim, Terimakasih atas informasinya.<br/>";

     }
  } else {
     echo $error;
  }
}
?>

        <?php if ($this->session->userdata('site_lang') == 'english') { ?>
        <div class="row">
          <div class="col-md-8">
            <h2 class="opensans purple">Contact us</h2>
            <div class="">
              <br>
              <p class="no-margin">Fill up this form to contact us if you have any futher questions</p>
              <form role="form" class="m-t-15" method="post" action="">
        <?php if($this->session->flashdata('message')){ ?>
          <div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
        <?php } ?>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group form-group-default">
                      <label>Title (Mr/Ms)</label>
                      <input type="text" name="c_name" class="form-control" placeholder="Title (Mr/Ms)" required />
                    </div>
                  </div>
                </div>
                <div class="form-group form-group-default">
                  <label>First Name</label>
                  <input type="text" name="c_name"  class="form-control" placeholder="First Name" required />
                </div>
                <div class="form-group form-group-default">
                  <label>Last Name</label>
                  <input type="text"  name="c_lname"  class="form-control" placeholder="Last Name" required />
                </div>
                <div class="form-group form-group-default">
                  <label>Company Name</label>
                  <input type="text" name="c_cpn" class="form-control" placeholder="Company" required/>
                </div>
                <div class="form-group form-group-default">
                  <label>Position</label>
                  <input type="text" name="c_position" class="form-control" placeholder="Position" required/>
                </div>
                 <div class="form-group form-group-default">
                  <label>Email Address</label>
                  <input type="email" name="c_email" class="form-control" placeholder="Email Address" required/>
                </div>
                <div class="form-group form-group-default">
                  <label>Phone Number</label>
                  <input type="text" name="c_phone" class="form-control" placeholder="Phone Number" required/>
                </div>
                <div class="form-group form-group-default">
                  <label>Type of institution</label>
                  <select class="form-control">
                    <option value="Cooperative">Cooperative</option>
                    <option value="bpr">BPR</option>
                    <option value="tani">Kelompok Usaha Tani</option>
                    <option value="">Other (please specify)</option>
                  </select>
                </div>
                <div class="form-group form-group-default">
                  <label>Website(s)</label>
                  <input type="text" name="c_website" class="form-control" placeholder="website" required/>
                </div>

                <div class="sm-p-t-10 clearfix">
                  <button class="btn btn-primary btn-purple">Send Message</button>
                </div>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
          <div class="col-md-4">
            <div class="visible-xs visible-sm b-b b-grey-light m-t-35 m-b-30"></div>
            <div class="p-l-40 sm-p-l-0 sm-p-t-10">
             <h2 class="opensans purple">Address &amp; Contact</h2>
              <br>
              <div class="row">
                <div class="col-sm-12">
                  <h5 class="block-title">OFFICE </h5>
                  <address class="m-t-10">
          Bukit Indraprasta D3/1,
                    <br/> Telaga Kahuripan, Parung,
                    <br/> Kab. Bogor 16330,
          <br/> Indonesia
          </address>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-sm-12">
                  <p class="fs-12 font-montserrat bold all-caps no-margin hint-text"><h5 class="block-title">HOTLINE</h5> </p>
                  <p class="hint-text no-margin fs-14">+62 251 861 8111</p>
                </div>

                <div class="col-sm-12">
                  <br/><p class="fs-12 font-montserrat bold all-caps no-margin hint-text"><h5 class="block-title">EMAIL</h5> </p>
                  <p class="hint-text no-margin fs-14"><a href="mailto:info@amartha.com" title="Email us">info@amartha.com</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <?php } else {?>
        <div class="row">
          <div class="col-md-8">
            <h2 class="opensans purple">Daftar sebagai Peminjam</h2>
            <div class="">
              <br>
              <p class="no-margin">Silakan isi formulir ini dan kami segera menghubungi Anda untuk perkembangan lebih lanjut.</p>
              <form role="form" class="m-t-15" method="post" action="<?php echo site_url('register_to_borrow'); ?>">
        <?php if($this->session->flashdata('message')){ ?>
          <div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
        <?php } ?>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group form-group-default">
                      <label>Nama Lengkap</label>
                      <input type="text" name="c_name" class="form-control" placeholder="Nama Lengkap" required />
                    </div>
                  </div>
                </div>
                 <div class="form-group form-group-default">
                  <label>Alamat Email</label>
                  <input type="email" name="c_email" class="form-control" placeholder="Alamat Email" required/>
                </div>
                <div class="form-group form-group-default">
                  <label>Nomor Telepon</label>
                  <input type="text" name="c_phone" class="form-control" placeholder="Contoh: 08123456789" required/>
                </div>
                <div class="form-group form-group-default">
                  <label>Plafond Pembiayaan (dalam Rupiah)</label>
                  <input type="text" name="c_plafond" class="form-control" placeholder="Contoh: 10.000.000" required/>
                </div>
                <div class="form-group form-group-default">
                  <label>Usaha yang Dijalankan</label>
                  <input type="text" name="c_usaha" class="form-control" placeholder="Usaha yang Dijalankan" required/>
                </div>
                <div class="form-group form-group-default">
                  <label>Tujuan Pinjaman</label>
                  <input type="text" name="c_tujuan" class="form-control" placeholder="Tujuan Pinjaman" required/>
                </div>
                <div class="form-group form-group-default">
                  <label><br/>Isi data berikut apabila telah memiliki pinjaman dari pihak lain:</label>
                </div>
                <div class="form-group form-group-default">
                  <label>Nilai pinjaman (dalam Rupiah)</label>
                  <input type="text" name="c_nilai" class="form-control" placeholder="Contoh: 5.000.000" />
                </div>
                <div class="form-group form-group-default">
                  <label>Sumber Pinjaman</label>
                  <input type="text" name="c_sumber" class="form-control" placeholder="Contoh: Nama Bank" />
                </div>
                <div class="form-group form-group-default">
                  <label>Tenor</label>
                  <input type="text" name="c_tenor" class="form-control" placeholder="Contoh : 6 bulan" />
                </div>

                <div class="sm-p-t-10 clearfix">
                  <div class="g-recaptcha" data-sitekey="6LcWEhwTAAAAAPfi757s8JNO8TPVufoDzTc9EsGS"></div><br/>
                  <button class="btn btn-primary btn-purple">Kirim</button>
                </div>
                <div class="clearfix"></div>
              </form>
            </div>
          </div>
          <div class="col-md-4">
            <div class="visible-xs visible-sm b-b b-grey-light m-t-35 m-b-30"></div>
            <div class="p-l-40 sm-p-l-0 sm-p-t-10">
             <h2 class="opensans purple">Alamat &amp; Kontak</h2>
              <br>
              <div class="row">
                <div class="col-sm-12">
                  <h5 class="block-title">KANTOR </h5>
                  <address class="m-t-10">
          Bukit Indraprasta D3/1,
                    <br/> Telaga Kahuripan, Parung,
                    <br/> Kab. Bogor 16330,
          <br/> Indonesia
          </address>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-sm-12">
                  <p class="fs-12 font-montserrat bold all-caps no-margin hint-text"><h5 class="block-title">HOTLINE</h5> </p>
                  <p class="hint-text no-margin fs-14">+62 251 861 8111</p>
                </div>

                <div class="col-sm-12">
                  <br/><p class="fs-12 font-montserrat bold all-caps no-margin hint-text"><h5 class="block-title">EMAIL</h5> </p>
                  <p class="hint-text no-margin fs-14"><a href="mailto:info@amartha.com" title="Email us">info@amartha.com</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <?php } ?>

        </div>
      </section>

<!--
#BACKUP FORM REGISTER BORROWER
  <div class="headerSimple">
    <img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png">
  </div>
  <div class="container">
    <div class="row formRegister">
      <form method="post" action="">
          <div class="form-group">
            <input name="register_first_name" type="text" class="form-control" id="" placeholder="FirstName">
          </div>

          <div class="form-group">
            <input name="register_last_name" type="text" class="form-control" id="" placeholder="LastName">
          </div>

          <div class="form-group">
            <input name="register_email" type="email" class="form-control" id="" placeholder="Email">
          </div>

          <div class="form-group">
            <input name="register_passwd" type="password" class="form-control" id="" placeholder="Password">
          </div>

          <div class="form-group">
            <input name="register_passwd_confirm" type="password" class="form-control" id="" placeholder="Confirm Password">
          </div>

          <h6>By continuing, I agree to the terms of use and </br>privacy policy</h5>
          <button type="submit" class="btn btn-default">NEXT</button>
      </form>
          <a href="<?php echo site_url('borrowing'); ?>"><button class="btn btn-default" style="background-color:#704390;">< BACK</button></a>

    </div>
  </div>
-->