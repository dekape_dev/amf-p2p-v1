  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

  <div class="headerSimple">
    <img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png">
  </div>
  <?php $num_notif    = $this->session->userdata('unread_notifications'); ?>
  <?php
        if($num_notif > 0 ) { $label = 'label-warning'; }
        else { $label = 'label-primary'; }
  ?>
  <?php $notification = '<span class="label '.$label.'">'.$num_notif.'</span>'; ?>
  <div class="headerUser">
    <div class="row userNavBorrow">
      <div class="col-md-2 "></div>
      <div class="col-md-2"><a href="">Hi, <?php echo $this->session->userdata('bfullname'); ?>.</a></div>
      <div class="col-md-2"><a href="<?php echo site_url('borrower/profile'); ?>">Account and Setting</a></div>
      <div class="col-md-2"><a href="<?php echo site_url('borrower/notification'); ?>">Notification <?php echo $notification; ?></a></div>
      <div class="col-md-2"><a href="<?php echo site_url('logout_to_borrow'); ?>">Sign Out</a></div>
      <div class="col-md-2"></div>
    </div>
  </div>

  <div style="border-bottom: 1px solid #919191;">
    <div class="container" >


      <div class="row userNav2">

        <div class="col-md-4 active"><a href="<?php echo site_url('borrower');?>">Overview</a></div>
        <div class="col-md-4 "><a href="<?php echo site_url('borrower/loanunder20mil'); ?>">Loan Request</a></div>
        <div class="col-md-4"><a href="<?php echo site_url('borrower/invoice'); ?>">Statement</a></div>
      </div>


    </div>
  </div>

  <div style="background-color:#f0def4;">
  <div class="container" style="background-color:#f0def4;">
  <section class="contentTimeline">

      <!-- row -->
      <div class="row">

        <div class="col-md-12">
          <!-- The time line -->
          <ul class="timeline">
          <?php  ?>
          <?php
          foreach ($date_notifications as $d) {
                      //<!-- timeline time label -->
                      echo '<li class="time-label">
                              <span class="bg-purple">'.date('d M Y', strtotime($d->created_date)).'</span>
                            </li>';
                      //echo '<!-- <span class="bg-green">'.$d.'</span> -->';

                      //<!-- /.timeline-label -->
                     foreach ($notifications as $n) {
                        if($n->created_date != $d->created_date) { continue; }
                        //<!-- timeline item -->
                        //echo '<!-- <i class="fa fa-envelope bg-blue"></i> -->';
                        //echo '<!-- <i class="fa fa-envelope bg-red"></i> -->';
                        //echo '<!-- <i class="fa fa-envelope bg-green"></i> -->';
                        //echo '<!-- <i class="fa fa-user bg-aqua"></i> -->';
                        //echo '<!-- <i class="fa fa-comments bg-yellow"></i> -->';
                        //echo '<!-- <i class="fa fa-info-circle bg-purple"></i> -->';
                        $envelope = '<i class="fa fa-envelope bg-green"></i>';
                        if($n->type == 'APR'){ $envelope = '<i class="fa fa-envelope bg-green"></i>';}
                        else if($n->type == 'FIN'){ $envelope = '<i class="fa fa-envelope bg-red"></i>';}

                        $datetime  = explode(" ", $n->created_on);
                        $time      = date('h:i A', strtotime($datetime[1]));

                        echo
                          '<li>'.$envelope.
                            '<div class="timeline-item">
                              <span class="time"><i class="fa fa-clock-o"></i> '.$time.'</span>
                              <h3 class="timeline-header"><a href="'.$n->from.'">Admin: </a>'.$n->title.'</h3>
                              <div class="timeline-body">'.$n->content.'</div>';
                        //echo '<div class="timeline-footer">
                        //        <a class="btn btn-primary btn-xs">Read more</a>
                        //        <a class="btn btn-success btn-xs">Approve Offer</a>
                        //        <a class="btn btn-danger btn-xs">Denied Offer</a>
                        //        <a class="btn btn-danger btn-xs">Delete</a>
                        //      </div>';
                        echo
                            '</div>
                          </li>';
                        //<!-- END timeline item -->
                }
          }
          ?>
          <?php  ?>
            <li>
              <i class="fa fa-clock-o bg-gray"></i>
            </li>
          </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    </div>
    </div>
