	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<div class="headerSimple">
		<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png">
	</div>
  <?php $num_notif    = $this->session->userdata('unread_notifications'); ?>
  <?php
        if($num_notif > 0 ) { $label = 'label-warning'; }
        else { $label = 'label-primary'; }
  ?>
  <?php $notification = '<span class="label '.$label.'">'.$num_notif.'</span>'; ?>
  <div class="headerUser">
    <div class="row userNavBorrow">
      <div class="col-md-2 "></div>
      <div class="col-md-2"><a href="">Hi, <?php echo $this->session->userdata('bfullname'); ?>.</a></div>
      <div class="col-md-2"><a href="<?php echo site_url('borrower/profile'); ?>">Account and Setting</a></div>
      <div class="col-md-2"><a href="<?php echo site_url('borrower/notification'); ?>">Notification <?php echo $notification; ?></a></div>
      <div class="col-md-2"><a href="<?php echo site_url('logout_to_borrow'); ?>">Sign Out</a></div>
      <div class="col-md-2"></div>
    </div>
  </div>

	<div style="border-bottom: 1px solid #919191;">
		<div class="container" >


			<div class="row userNav2">

				<div class="col-md-4 active"><a href="<?php echo site_url('borrower');?>">Overview</a></div>
				<div class="col-md-4 "><a href="<?php echo site_url('borrower/loanunder20mil'); ?>">Loan Request</a></div>
				<div class="col-md-4"><a href="<?php echo site_url('borrower/invoice'); ?>">Statement</a></div>
			</div>


		</div>
	</div>

	<div style="background-color:#f0def4;">
  <div class="container" style="background-color:#f0def4;">
  <section class="contentTimeline">

      <!-- row -->
      <div class="row">
        <div class="col-md-12">
          <!-- The time line -->
          <ul class="timeline">
            <!-- timeline time label -->
            <li class="time-label">
                  <span class="bg-red">
                    10 Feb. 2014
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-envelope bg-green"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                <h3 class="timeline-header"><a href="#">Admin:</a> Loan Request Approved</h3>

                <div class="timeline-body">
                  Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                  weebly ning heekya handango imeem plugg dopplr jibjab, movity
                  jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                  quora plaxo ideeli hulu weebly balihoo...
                </div>
                <div class="timeline-footer">
                  <a class="btn btn-primary btn-xs">Read more</a>
                  <a class="btn btn-danger btn-xs">Delete</a>
                </div>
              </div>
            </li>
            <!-- END timeline item -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-envelope bg-blue"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                <h3 class="timeline-header"><a href="#">Admin:</a> Loan Request Recieve</h3>

                <div class="timeline-body">
                  Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                  weebly ning heekya handango imeem plugg dopplr jibjab, movity
                  jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                  quora plaxo ideeli hulu weebly balihoo...
                </div>

              </div>
            </li>
            <!-- END timeline item -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-user bg-aqua"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

                <h3 class="timeline-header no-border"><a href="#">20 Investor</a> See Your Request</h3>
              </div>
            </li>
            <!-- END timeline item -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-comments bg-yellow"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                <h3 class="timeline-header"><a href="#">Jay White</a> Offering</h3>

                <div class="timeline-body">
                  Take me to your leader!
                  Switzerland is small and neutral!
                  We are more like Germany, ambitious and misunderstood!
                </div>
                <div class="timeline-footer">
                  <a class="btn btn-success btn-xs">Approve Offer</a>
                  <a class="btn btn-danger btn-xs">Denied Offer</a>
                </div>
              </div>
            </li>
            <!-- END timeline item -->
            <!-- timeline time label -->
            <li class="time-label">
                  <span class="bg-green">
                    3 Jan. 2014
                  </span>
            </li>
            <!-- /.timeline-label -->
            <!-- timeline item -->
            <li>
              <i class="fa fa-envelope bg-blue"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                <h3 class="timeline-header"><a href="#">Admin:</a> Loan Request Process</h3>

                <div class="timeline-body">
                  Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                  weebly ning heekya handango imeem plugg dopplr jibjab, movity
                  jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                  quora plaxo ideeli hulu weebly balihoo...
                </div>

              </div>
            </li>
            <!-- END timeline item -->

             <!-- timeline item -->
            <li>
              <i class="fa fa-info-circle bg-purple"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                <h3 class="timeline-header"><a href="#">Info:</a> Loan Disbursement</h3>

                <div class="timeline-body">
                  Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                  weebly ning heekya handango imeem plugg dopplr jibjab, movity
                  jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                  quora plaxo ideeli hulu weebly balihoo...
                </div>

              </div>
            </li>


            <!-- timeline item -->
            <li>
              <i class="fa fa-envelope bg-red"></i>

              <div class="timeline-item">
                <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                <h3 class="timeline-header"><a href="#">Admin:</a> Loan Request Denied</h3>

                <div class="timeline-body">
                  Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                  weebly ning heekya handango imeem plugg dopplr jibjab, movity
                  jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                  quora plaxo ideeli hulu weebly balihoo...
                </div>
                <div class="timeline-footer">
                  <a class="btn btn-primary btn-xs">Read more</a>
                  <a class="btn btn-danger btn-xs">Delete</a>
                </div>
              </div>
            </li>
            <!-- END timeline item -->
            <li>
              <i class="fa fa-clock-o bg-gray"></i>
            </li>
          </ul>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->



    </section>
    </div>
    </div>
