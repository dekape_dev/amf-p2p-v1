
	<!-- Photo Profile -->
	<!-- <div class="container photoProfile">
		<h3>BORROWER DETAILS</h3>
		<div class="photoBox" style="background: url(../amartha/img/bg_homepage.jpg) no-repeat center center;"></div>
	</div> -->


	<div class="container namaBorrower">
		<h2>Nama Borrower</h2>
	</div>


	<!-- Personal Data -->
	<div class="container personalData">
		<div class="row perData">
			<div class="col-md-6">
				<h3>Personal Data</h3>
				<!-- DATA TABLE -->
				<table class="table">

			    </thead>
			    <tbody>
			      <tr><td>Register Date</td><td>11 November 2014</td></tr>

			      <tr><td>Loan Submitted</td><td>Rp 500.000</td></tr>

			       <tr><td>Loan Amount</td><td>Rp 500.000</td></tr>

			      <tr><td>Loan Tenor</td><td>Rp 500.000</td></tr>

			      <tr><td>Projected Return</td><td>Rp 500.000</td></tr>

			      <tr><td>Monthly Principal Payment</td><td>Rp 500.000</td></tr>

			      <tr><td>Monthly Profit Shared Payment</td><td>Rp 500.000</td></tr>

			      <tr><td>Income</td><td >Rp 500.000</td></tr>

			      <tr><td>Employment</td><td>Pedagang Tahu</td></tr>

			      <tr><td>Location</td><td>Bogor</td></tr>


			    </tbody>
			  </table>


			</div>
			<div class="col-md-6 chartPerData">
				<ul>
					<li>
						<div id="myStat" data-dimension="200" data-text="58%" data-info="Debt to Income Ratio" data-width="30" data-fontsize="28" data-percent="58" data-fgcolor="purple" data-bgcolor="#eee" data-fill="#faedff"></div>
					</li>

					<li>
						<div id="myStat2" data-dimension="200" data-text="58%" data-info="Debt to Income Ratio" data-width="30" data-fontsize="28" data-percent="58" data-fgcolor="purple" data-bgcolor="#eee" data-fill="#faedff"></div>
					</li>
				</ul>
			</div>
		</div>
	</div>

	<!-- TABLE DATA HISTORY -->
	<div class="container historyData">

		<h2>HISTORY</h2>

		<div class="browseTable">
				<!-- TABLE START!! -->
				<table class="table table-striped table-bordered table-hover">

			    <!-- HEAD TABLE -->
			    <thead >
			      <tr><th>Date</th><th>Total Credit Line</th><th>Revolving</th><th>Credit Balance</th><th>Account Now Delinquent</th><th> Delinquent Ammount</th><th>Month Since Last Delinquent</th></tr>
			    </thead>

			    <tbody data-link="row" class="rowlink">

			    	<!-- table Data LOOP -->
				    <tr>
				    	<!-- FIRST COULUMN -->
				    	<td class="rowlink-skip">
				    		1
				    	</td>
				    	<!-- NEXT COULUMN -->

				    	<td><a href="#LINKTODETAILS">Input mask</a></td>
				    	<td>test</td>
				    	<td>test</td>
				    	<td>test</td>
				    	<td>test</td>
				    	<td>test</td>



				    </tr>
				    <!-- END Table Data LOOP -->

				  </tbody>
			  </table>
				<!-- TABLE END!! -->

			  </div>
			  <!-- Navi for Table -->
			  <div class="tableNav">
			  	<div class="row">
			  		<div class="col-sm-5">
			  			<div class="dataTables_info" id="example_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries
			  			</div>
			  		</div>

			  		<div class="col-sm-7">
			  			<div class="dataTables_paginate paging_simple_numbers" id="example_paginate">
			  				<ul class="pagination">

			  					<li class="paginate_button previous disabled" id="example_previous"><a href="#" aria-controls="example" data-dt-idx="0" tabindex="0">Previous</a>
			  					</li>

			  					<li class="paginate_button active"><a href="#" aria-controls="example" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="2" tabindex="0">2</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="3" tabindex="0">3</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="4" tabindex="0">4</a></li><li class="paginate_button "><a href="#" aria-controls="example" data-dt-idx="5" tabindex="0">5</a></li>

			  					<li class="paginate_button next" id="example_next"><a href="#" aria-controls="example" data-dt-idx="7" tabindex="0">Next</a></li>

			  				</ul>
			  			</div>
			  		</div>
			  	</div>


			  </div>
	</div>

	<div class="container navBack">
			<button class="back" onclick='window.history.back();' class="btn btn-login">back</button>

			<button class="addItem" onclick='' class="btn btn-login">Choose This Person</button>

	</div>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>js/jquery.circliful.min.js"></script>
<script>
$( document ).ready(function() {
        $('#myStathalf').circliful();
		$('#myStat').circliful();
		$('#myStathalf2').circliful();
		$('#myStat2').circliful();
    $('#myStat3').circliful();
    $('#myStat4').circliful();
    $('#myStathalf3').circliful();
    });
</script>
