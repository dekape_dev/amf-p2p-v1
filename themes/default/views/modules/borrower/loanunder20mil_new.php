	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

	<div class="headerSimple">
		<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png">
	</div>
	<?php $num_notif    = $this->session->userdata('unread_notifications'); ?>
  <?php
        if($num_notif > 0 ) { $label = 'label-warning'; }
        else { $label = 'label-primary'; }
  ?>
  <?php $notification = '<span class="label '.$label.'">'.$num_notif.'</span>'; ?>
  <div class="headerUser">
    <div class="row userNavBorrow">
      <div class="col-md-2 "></div>
      <div class="col-md-2"><a href="">Hi, <?php echo $this->session->userdata('bfullname'); ?>.</a></div>
      <div class="col-md-2"><a href="<?php echo site_url('borrower/profile'); ?>">Account and Setting</a></div>
      <div class="col-md-2"><a href="<?php echo site_url('borrower/notification'); ?>">Notification <?php echo $notification; ?></a></div>
      <div class="col-md-2"><a href="<?php echo site_url('logout_to_borrow'); ?>">Sign Out</a></div>
      <div class="col-md-2"></div>
    </div>
  </div>

	<div style="border-bottom: 1px solid #919191;">
		<div class="container" >


			<div class="row userNav2">

				<div class="col-md-4"><a href="<?php echo site_url('borrower');?>">Overview</a></div>
				<div class="col-md-4 active"><a href="<?php echo site_url('borrower/loanunder20mil'); ?>">Loan Request</a></div>
				<div class="col-md-4"><a href="<?php echo site_url('borrower/invoice'); ?>">Statement</a></div>


				<!-- <div class="col-md-3 right active"><a href="<?php echo site_url('dashboard'); ?>">Account</a></div>
				<div class="col-md-3 right"><a href="<?php echo site_url('investor/portfolio'); ?>">Portfolio</a></div>
				<div class="col-md-3 right"><a href="<?php echo site_url('investor/history'); ?>">History</a></div>
				<div class="col-md-3 left dropdown">
					<a class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" href="marketperfomance.html">
					Market Perfomance</a>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					    <li><a href="<?php echo site_url('marketperformance/statistics'); ?>">Statistic</a></li>
					    <li><a href="<?php echo site_url('marketperformance/investor'); ?>">Investor</a></li>
					    <li><a href="<?php echo site_url('marketperformance/borrower'); ?>">Borrower</a></li>
					</ul>
				</div> -->
			</div>

			<!-- <div class="stepNav">
				<ul>
					<li class="active">
						<a href="<?php echo site_url('dashboard'); ?>">Overview</a>
					</li>
					<li>></li>
					<li><a href="<?php echo site_url('loan'); ?>">Browse Loan</a></li>
					<li>></li>
					<li><a href="<?php echo site_url('loan/confirmation'); ?>">Confirmation</a></li>
					<li>></li>
					<li><a href="<?php echo site_url('loan/payment'); ?>">Transfer Fund</a></li>
				</ul>
			</div> -->

		</div>
	</div>

	<div class="container">
		<div class="row loanNav">
			<div class="col-md-2"></div>
			<div class="col-md-4 active"><a href="<?php echo site_url('borrower/loanunder20mil'); ?>"><h3>Below 20 Million</h3></a></div>
			<div class="col-md-4"><a href="<?php echo site_url('borrower/loanabove20mil'); ?>"><h3>Above 20 Million</h3></a></div>
			<div class="col-md-2"></div>

		</div>
	</div>

	<div class="container">
		<div class="row loanForm">
			<form method="post" action="">
				<!-- FIRSNAME-LASTNAME -->

				<?php if($this->session->flashdata('message')){ ?>
				<div class="alert alert-success" ><?php echo print_message($this->session->flashdata('message')); ?></div>
				<?php } ?>

			  <div class="row form-group">
			  	<div class="col-sm-12">
			  	<div class="col-md-6">
			  		<input name="borrower_below_firstname" type="text" class="form-control"  placeholder="First Name" >
			  	</div>
			  	<div class="col-md-6 resp">
			  		<input name="borrower_below_lastname" type="text" class="form-control"  placeholder="Last Name">
			  	</div>
			  </div>
			  </div>

			  <div class="col-md-12 col-sm-12">

			  <!-- EMAIL -->
			  <div class="form-group">
			    <input name="borrower_below_email" type="email" class="form-control" id="" placeholder="Email" >
			  </div>

			  <!-- NO KTP -->
			  <div class="form-group">
			    <input name="borrower_below_ktp" type="text" class="form-control" id="" placeholder="*No KTP" >
			  </div>

			  <!-- NO HP -->
			  <div class="form-group">
			    <input name="borrower_below_hp" type="number" class="form-control" id="" placeholder="*Phone Number" >
			  </div>



			  <div class="form-group">
			    <input name="borrower_below_borndate" class="form-control" id="datepicker"  placeholder="dd/mm/yyyy" >
			  </div>

			  <!-- Plafond -->
			  <div class="form-group">
			    <input name="borrower_below_plafond" type="text" class="form-control" id="" placeholder="Plafond" >
			  </div>

			  <!-- sector -->
			  <div class="form-group">
			    <input name="borrower_below_sector" type="text" class="form-control" id="" placeholder="Sector" >
			  </div>

			  <!-- Plafond -->
			  <div class="form-group">
			    <input name="borrower_below_desa" type="text" class="form-control" id="" placeholder="Desa" >
			  </div>

			  <!-- LOan Period -->

			  <div class="form-group">
				  <select name="borrower_below_tenor" class="form-control" id="sel1" >
				    <option style="color:red;">Select your Tenor/Loan Period</option>
				    <option value="1">1 years</option>
				    <option value="2">2 years</option>
				    <option  value="3">4 years</option>
				  </select>
				</div>


			  <!-- Location -->

			  <div class="form-group">
				  <select name="borrower_below_location" class="form-control" id="sel1" >
				    <option style="color:red;">Select your location</option>
				    <option  value="1">2</option>
				    <option  value="2">3</option>
				    <option  value="3">4</option>
				  </select>
				</div>

				<p>*Ketentuan Berlaku</p>

				<!-- TERM AND CONDITION -->

			  <div class="checkbox">
			    <label>
			      <input type="checkbox"> <a href="#test-popup" class="open-popup-link">I agree to the terms and conditions</a>
			    </label>
			  </div>

			  <!-- BUTTON -->

			  <button type="submit" class="btn btn-default btn-submit">DAFTAR SEKARANG</button>
				</div>
			</form>
		</div>
	</div>
