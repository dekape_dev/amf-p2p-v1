	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<div class="container">
		<div class="row contentFirst">
			<h1 class="text-center bold">INVESTOR</h1>

			<div class="col-md-6">
				<div class="small-box bg-green">
		            <div class="inner">
		              <h3>IDR 2.000.000.000</h3>

		              <p>Total amount of investments to date </p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-stats-bars"></i>
		            </div>
          		</div>
			</div>

			<div class="col-md-6">
				<div class="small-box bg-yellow">
		            <div class="inner">
		              <h3>IDR 203.880.600</h3>

		              <p>Amount of investment last month</p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-stats-bars"></i>
		            </div>
          		</div>


			</div>
		</div>
	</div>
				


	<div class="container">
		<div class="row chartDataGraph">

			<div class="col-md-6">
				<div class="small-box bg-aqua">
		            <div class="inner">
		              <h3>IDR 1.000.304.567</h3>

		              <p>Number of investment originated</p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-stats-bars"></i>
		            </div>
          		</div>

          		<div class="small-box bg-velvet">
		            <div class="inner">
		              <h3>IDR 205.067.080</h3>

		              <p>Investor balances</p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-stats-bars"></i>
		            </div>
          		</div>

          		

			</div>
			<div class="col-md-6">
				<h3 class="padReset">Profit-shared to investors</h3>
				<canvas id="investChart2" ></canvas>

			</div>


		</div>

		<script type="text/javascript">
			var investData2 = {
			    labels: ["January", "February", "March", "April", "May", "June", "July"],
			    datasets: [

			        {
			            label: "Profit-shared to investors",
			            fillColor: "rgba(255,82,82,1)",
			            strokeColor: "rgba(151,187,205,1)",
			            pointColor: "rgba(86,87,94,1)",
			            pointStrokeColor: "#fff",
			            pointHighlightFill: "#fff",
			            pointHighlightStroke: "rgba(191,101,115,1)",
			            data: [28, 48, 40, 19, 86, 27, 90]
			        }
			    ]
			};

		</script>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="small-box bg-purple">
		            <div class="inner">
		              <h3>IDR 802.037.356</h3>

		              <p>On Loan</p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-stats-bars"></i>
		            </div>
          		</div>
			</div>

			<div class="col-md-6">
				<div class="small-box bg-softbrown">
		            <div class="inner">
		              <h3>IDR 102.257.539</h3>

		              <p>Average amount invested</p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-stats-bars"></i>
		            </div>
          		</div>


			</div>
		</div>
	</div>



	<div class="container">
		<!-- <div class="row chartDataBar">
			<div class="col-md-6">
				<h3>amount of investment last month </h3>

				<canvas id="investChart3" height="450" width="600"></canvas>

				<script type="text/javascript">
					var investData3 = {
					    labels: ["January", "February", "March", "April", "May", "June", "July"],
					    datasets: [
					        {
					            label: "amount of investment last month",
					            fillColor: "#cd8cec",
					            strokeColor: "#cd8cec",
					            pointColor: "#cd8cec",
					            pointStrokeColor: "#fff",
					            pointHighlightFill: "#fff",
					            pointHighlightStroke: "rgba(220,220,220,1)",
					            data: [65, 59, 80, 81, 56, 55, 40]
					        }
					    ]
					};

					
				</script>


			</div>
			<div class="col-md-6">
				<h3>number of investment originated </h3>

				<canvas id="investChart4" height="450" width="600"></canvas>

				<script type="text/javascript">
					var investData4 = {
					    labels: ["January", "February", "March", "April", "May", "June", "July"],
					    datasets: [

					        {
					            label: "number of investment originated",
					            fillColor: "rgba(151,187,205,1)",
					            strokeColor: "rgba(151,187,205,1)",
					            pointColor: "rgba(151,187,205,1)",
					            pointStrokeColor: "#fff",
					            pointHighlightFill: "#fff",
					            pointHighlightStroke: "rgba(151,187,205,1)",
					            data: [28, 48, 40, 19, 86, 27, 90]
					        }
					    ]
					};
				</script>

			</div>
		</div> -->
	</div>


	<div class="container">
		<div class="row chartData">
			<h1 class="bold">INVESTOR PROFILE</h1>
			<div class="col-md-4">
				<div class="donut">
					<h3>Age</h3>
					<canvas id="investChart5" width="500" height="500"/>
				</div>

				<script>
						var investData5 = [
								{
									value: 300,
									color:"#F7464A",
									highlight: "#FF5A5E",
									label: "purple"
								},
								{
									value: 50,
									color: "#46BFBD",
									highlight: "#5AD3D1",
									label: "blue"
								},
								{
									value: 100,
									color: "#FDB45C",
									highlight: "#FFC870",
									label: "Yellow"
								},
								{
									value: 40,
									color: "#949FB1",
									highlight: "#A8B3C5",
									label: "Grey"
								},
								{
									value: 120,
									color: "#4D5360",
									highlight: "#616774",
									label: "Dark Grey"
								}

							];



				</script>
				<div class="doughnut-legend">
					<ul>
						<li><span style="background-color:#F7464A;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#46BFBD;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#FDB45C;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#949FB1;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#4D5360;"></span>
							<h6>test</h6>
						</li>
					</ul>
				</div>
			</div>

			<div class="col-md-4">
				<div class="donut">
					<h3>Gender</h3>
					<canvas id="investChart6" width="500" height="500"/>
				</div>

				<script>
						var investData6 = [
								{
									value: 300,
									color:"#F7464A",
									highlight: "#FF5A5E",
									label: "purple"
								},
								{
									value: 50,
									color: "#46BFBD",
									highlight: "#5AD3D1",
									label: "blue"
								},
								{
									value: 100,
									color: "#FDB45C",
									highlight: "#FFC870",
									label: "Yellow"
								},
								{
									value: 40,
									color: "#949FB1",
									highlight: "#A8B3C5",
									label: "Grey"
								},
								{
									value: 120,
									color: "#4D5360",
									highlight: "#616774",
									label: "Dark Grey"
								}

							];



				</script>
				<div class="doughnut-legend">
					<ul>
						<li><span style="background-color:#F7464A;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#46BFBD;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#FDB45C;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#949FB1;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#4D5360;"></span>
							<h6>test</h6>
						</li>
					</ul>
				</div>
			</div>

			<div class="col-md-4">
				<h3>Investable Assets</h3>

				<canvas id="investChart7" height="450" width="600"></canvas>

				<script type="text/javascript">
					var investData7 = {
					    labels: ["January", "February", "March", "April", "May", "June", "July"],
					    datasets: [
					        {
					            label: "amount of investment last month",
					            fillColor: "#cd8cec",
					            strokeColor: "#cd8cec",
					            pointColor: "#cd8cec",
					            pointStrokeColor: "#fff",
					            pointHighlightFill: "#fff",
					            pointHighlightStroke: "rgba(220,220,220,1)",
					            data: [65, 59, 80, 81, 56, 55, 40]
					        }
					    ]
					};
				</script>


			</div>



			</div>
		</div>
	</div>

	<!-- BORROWER DATA -->



	<div class="container" style="padding-top:100px;">
		<div class="row contentFirst">
			<h1 class="text-center bold">BORROWER</h1>

			<div class="col-md-6">
				<div class="small-box bg-green">
		            <div class="inner">
		              <h3>IDR 802.037.356</h3>

		              <p>Loans funded to date </p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-stats-bars"></i>
		            </div>
          		</div>
			</div>

			<div class="col-md-6">
				<div class="small-box bg-yellow">
		            <div class="inner">
		              <h3>IDR 23.880.600</h3>

		              <p>Loans funded last month</p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-stats-bars"></i>
		            </div>
          		</div>


			</div>
		</div>
	</div>

	<div class="container">
		<div class="row chartDataGraph">
			<div class="col-md-6">
				<h3 class="padReset">Profit-shared to investors</h3>
				<canvas id="borrowChart2" ></canvas>

			</div>

			<div class="col-md-6">
				<h3 class="padReset">PAR</h3>
				<canvas id="borrowChart3" ></canvas>

			</div>
		</div>
		</div>
	<div class="container">

		<div class="row chartDataGraph" >
			<div class="col-md-4">

				<h3 >Total demand per month</h3>
				<canvas id="borrowChart4"></canvas>

			</div>

			<div class="col-md-4">
				<h3 >Approved borrower demand </h3>

				<canvas id="borrowChart9" ></canvas>

			</div>

			<div class="col-md-4">
				<h3 >Pending borrower demand</h3>
				
				<canvas id="borrowChart10" ></canvas>

			</div>
		</div>

		
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="small-box bg-purple">
		            <div class="inner">
		              <h3>IDR 802.037.356</h3>

		              <p>Average loan size </p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-stats-bars"></i>
		            </div>
          		</div>
			</div>

			<div class="col-md-6">
				<div class="small-box bg-softbrown">
		            <div class="inner">
		              <h3>IDR 102.257.539</h3>

		              <p>Total number of loan origination</p>
		            </div>
		            <div class="icon">
		              <i class="ion ion-stats-bars"></i>
		            </div>
          		</div>


			</div>
		</div>
	</div>

	

	<div class="container">
		<div class="row chartData">
			<h1 class="bold">BORROWER PROFILE</h1>
			<div class="col-md-6">
				<div class="donut">
					<h3>Age</h3>
					<canvas id="borrowChart5" width="500" height="500"/>
				</div>

				<script>
						var borrowData5 = [
								{
									value: 300,
									color:"#F7464A",
									highlight: "#FF5A5E",
									label: "purple"
								},
								{
									value: 50,
									color: "#46BFBD",
									highlight: "#5AD3D1",
									label: "blue"
								},
								{
									value: 100,
									color: "#FDB45C",
									highlight: "#FFC870",
									label: "Yellow"
								},
								{
									value: 40,
									color: "#949FB1",
									highlight: "#A8B3C5",
									label: "Grey"
								},
								{
									value: 120,
									color: "#4D5360",
									highlight: "#616774",
									label: "Dark Grey"
								}

							];

				</script>
				<div class="doughnut-legend">
					<ul>
						<li><span style="background-color:#F7464A;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#46BFBD;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#FDB45C;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#949FB1;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#4D5360;"></span>
							<h6>test</h6>
						</li>
					</ul>
				</div>
			</div>

		<div class="row chartData">
			<div class="col-md-6">
				<div class="donut">
					<h3>Gender</h3>
					<canvas id="borrowChart6" width="500" height="500"/>
				</div>

				<script>
						var borrowData6 = [
								{
									value: 300,
									color:"#F7464A",
									highlight: "#FF5A5E",
									label: "purple"
								},
								{
									value: 50,
									color: "#46BFBD",
									highlight: "#5AD3D1",
									label: "blue"
								},
								{
									value: 100,
									color: "#FDB45C",
									highlight: "#FFC870",
									label: "Yellow"
								},
								{
									value: 40,
									color: "#949FB1",
									highlight: "#A8B3C5",
									label: "Grey"
								},
								{
									value: 120,
									color: "#4D5360",
									highlight: "#616774",
									label: "Dark Grey"
								}

							];



				</script>
				<div class="doughnut-legend">
					<ul>
						<li><span style="background-color:#F7464A;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#46BFBD;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#FDB45C;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#949FB1;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#4D5360;"></span>
							<h6>test</h6>
						</li>
					</ul>
				</div>
			</div>


			<div class="col-md-6">
				<div class="donut">
					<h3>Income profile</h3>
					<canvas id="borrowChart7" width="500" height="500"/>
				</div>

				<script>
						var borrowData7 = [
								{
									value: 300,
									color:"#F7464A",
									highlight: "#FF5A5E",
									label: "purple"
								},
								{
									value: 50,
									color: "#46BFBD",
									highlight: "#5AD3D1",
									label: "blue"
								},
								{
									value: 100,
									color: "#FDB45C",
									highlight: "#FFC870",
									label: "Yellow"
								},
								{
									value: 40,
									color: "#949FB1",
									highlight: "#A8B3C5",
									label: "Grey"
								},
								{
									value: 120,
									color: "#4D5360",
									highlight: "#616774",
									label: "Dark Grey"
								}

							];



				</script>
				<div class="doughnut-legend">
					<ul>
						<li><span style="background-color:#F7464A;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#46BFBD;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#FDB45C;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#949FB1;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#4D5360;"></span>
							<h6>test</h6>
						</li>
					</ul>
				</div>
			</div>

			<div class="col-md-6">
				<div class="donut">
					<h3>Industry</h3>
					<canvas id="borrowChart8" width="500" height="500"/>
				</div>

				<script>
						var borrowData8 = [
								{
									value: 300,
									color:"#F7464A",
									highlight: "#FF5A5E",
									label: "purple"
								},
								{
									value: 50,
									color: "#46BFBD",
									highlight: "#5AD3D1",
									label: "blue"
								},
								{
									value: 100,
									color: "#FDB45C",
									highlight: "#FFC870",
									label: "Yellow"
								},
								{
									value: 40,
									color: "#949FB1",
									highlight: "#A8B3C5",
									label: "Grey"
								},
								{
									value: 120,
									color: "#4D5360",
									highlight: "#616774",
									label: "Dark Grey"
								}

							];



				</script>
				<div class="doughnut-legend">
					<ul>
						<li><span style="background-color:#F7464A;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#46BFBD;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#FDB45C;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#949FB1;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#4D5360;"></span>
							<h6>test</h6>
						</li>
					</ul>
				</div>
			</div>
</div>




</div>

			</div>
		</div>
	</div>



<script type="text/javascript">
	window.onload = function(){
		var borrowData2 = {
			    labels: ["January", "February", "March", "April", "May", "June", "July"],
			    datasets: [

			        {
			            label: "Profit-shared to investors",
			            fillColor: "rgba(255,82,82,1)",
			            strokeColor: "rgba(255,82,82,,1)",
			            pointColor: "rgba(86,87,94,1)",
			            pointStrokeColor: "#fff",
			            pointHighlightFill: "#fff",
			            pointHighlightStroke: "rgba(191,101,115,1)",
			            data: [28, 48, 40, 19, 86, 27, 90]
			        }
			    ]
			};

			var borrowData3 = {
			    labels: ["January", "February", "March", "April", "May", "June", "July"],
			    datasets: [

			        {
			            label: "Profit-shared to investors",
			            fillColor: "rgba(0,150,136,1)",
			            strokeColor: "rgba(255,82,82,,1)",
			            pointColor: "rgba(0,121,107,1)",
			            pointStrokeColor: "#fff",
			            pointHighlightFill: "#fff",
			            pointHighlightStroke: "rgba(191,101,115,1)",
			            data: [19, 90, 40, 86, 26, 27, 48]
			        }
			    ]
			};

			var borrowData4 = {
			    labels: ["January", "February", "March", "April", "May", "June", "July"],
			    datasets: [

			        {
			            label: "Profit-shared to investors",
			            fillColor: "rgba(0,150,136,1)",
			            strokeColor: "rgba(255,82,82,,1)",
			            pointColor: "rgba(0,121,107,1)",
			            pointStrokeColor: "#fff",
			            pointHighlightFill: "#fff",
			            pointHighlightStroke: "rgba(191,101,115,1)",
			            data: [19, 90, 40, 86, 26, 27, 48]
			        }
			    ]
			};

			var borrowData9 = {
			    labels: ["January", "February", "March", "April", "May", "June", "July"],
			    datasets: [

			        {
			            label: "Profit-shared to investors",
			            fillColor: "rgba(0,150,136,1)",
			            strokeColor: "rgba(255,82,82,,1)",
			            pointColor: "rgba(0,121,107,1)",
			            pointStrokeColor: "#fff",
			            pointHighlightFill: "#fff",
			            pointHighlightStroke: "rgba(191,101,115,1)",
			            data: [19, 90, 40, 86, 26, 27, 48]
			        }
			    ]
			};

			var borrowData10 = {
			    labels: ["January", "February", "March", "April", "May", "June", "July"],
			    datasets: [

			        {
			            label: "Profit-shared to investors",
			            fillColor: "rgba(0,150,136,1)",
			            strokeColor: "rgba(255,82,82,,1)",
			            pointColor: "rgba(0,121,107,1)",
			            pointStrokeColor: "#fff",
			            pointHighlightFill: "#fff",
			            pointHighlightStroke: "rgba(191,101,115,1)",
			            data: [19, 90, 40, 86, 26, 27, 48]
			        }
			    ]
			};





		var money = {scaleLabel : "<%= Number(value).toFixed(3).replace('.', ',') + 'k'%>",responsive: [true]};
		
		//Investor
		var ctx2 = document.getElementById("investChart2").getContext("2d");
		// var ctx3 = document.getElementById("investChart3").getContext("2d");
		// var ctx4 = document.getElementById("investChart4").getContext("2d");
		var ctx5 = document.getElementById("investChart5").getContext("2d");
		var ctx6 = document.getElementById("investChart6").getContext("2d");
		var ctx7 = document.getElementById("investChart7").getContext("2d");

		//borrower
		var ctx8 = document.getElementById("borrowChart2").getContext("2d");
		var ctx9 = document.getElementById("borrowChart3").getContext("2d");
		var ctx10 = document.getElementById("borrowChart4").getContext("2d");

		var ctx11 = document.getElementById("borrowChart5").getContext("2d");
		var ctx12 = document.getElementById("borrowChart6").getContext("2d");
		var ctx13 = document.getElementById("borrowChart7").getContext("2d");
		var ctx14 = document.getElementById("borrowChart8").getContext("2d");
		var ctx15 = document.getElementById("borrowChart9").getContext("2d");
		var ctx16 = document.getElementById("borrowChart10").getContext("2d");




		
		window.myLine = new Chart(ctx2).Line(investData2, money);
		// window.myBar  = new Chart(ctx3).Bar(investData3, {responsive : true});
		// window.myBar  = new Chart(ctx4).Bar(investData4, {responsive : true});
		window.myDoughnut = new Chart(ctx5).Doughnut(investData5, {responsive : true});
		window.myDoughnut = new Chart(ctx6).Doughnut(investData6, {responsive : true});
		window.myBar = new Chart(ctx7).Bar(investData7, {responsive : true});

		//borrower
		window.myLine = new Chart(ctx8).Bar(borrowData2, {responsive : true});
		window.myBar  = new Chart(ctx9).Bar(borrowData3, {responsive : true});
		window.myBar  = new Chart(ctx10).Bar(borrowData4, {responsive : true});
		window.myDoughnut = new Chart(ctx11).Doughnut(borrowData5, {responsive : true});
		window.myDoughnut = new Chart(ctx12).Doughnut(borrowData6, {responsive : true});
		window.myDoughnut = new Chart(ctx13).Doughnut(borrowData7, {responsive : true});
		window.myDoughnut = new Chart(ctx14).Doughnut(borrowData8, {responsive : true});
		window.myBar  = new Chart(ctx15).Bar(borrowData9, {responsive : true});
		window.myBar  = new Chart(ctx16).Bar(borrowData10, {responsive : true});



	};
</script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/Chart.min.js"></script>
