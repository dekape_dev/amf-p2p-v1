<div class="container">
	<div class="row my_wallet">
	
			<?php if($this->session->flashdata('message')){ ?><br/>
                   <div class="alert alert-warning"><?php echo print_message($this->session->flashdata('message')); ?></div>
            <?php } ?> 
			
			<div class="col-md-6"></div>
			<div class="col-md-6 walletDetail">
				<h2 class="pull-right">MY WALLET</h2>
				<table class="table">
			    <thead>
			      <tr>
			        <th></br></th>
			        <th></th>
			       
			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td>Saldo My Wallet</td>
			        <td>Rp <?php echo number_format($this->session->userdata('wallet')); ?></td>
			       
			      </tr><tr>
			        <td><strong>TOTAL MY WALLET</strong></td>
			        <td><strong>Rp <?php echo number_format($this->session->userdata('wallet')); ?></strong></td>
			       
			      </tr>
			      
			    </tbody>
			  </table>
			</div>
	</div>
	<div class="row">
		
			<div class="col-md-6"></div>
			<div class="col-md-6">
				<div class="confirmButton">
					<button type="submit" class="btn btn-login"  data-toggle="modal" data-target="#myModal" >Top Up My Wallet</button>
					<button type="submit" class="btn btn-login"  data-toggle="modal" data-target="#Cashout" >Cash Out</button>
				</div>
			</div>
			
			
	</div>
	<h2>HISTORY OF E-WALLET TRANSACTIONS</h2>
	<div class="row">
				
			<div class="browseTable col-md-12">
				<!-- TABLE START!! -->
				<table class="table table-striped table-bordered table-hover">
			    <!-- HEAD TABLE -->
			    <thead >
			      <tr>
			      	<th>No</th>
			      	<th>Debet</th>
			      	<th>Credit</th>
			      	<th>Balance</th>
			      	<th>Remark</th>
			      	<th>Date</th>
			      	<th>Status</th>
			      	<th>Payment Channel</th>
			    </thead>
			    <tbody data-link="row" class="rowlink">
			    	<!-- table Data LOOP -->
			    	<?php $n = 1; ?>
			    	<?php 
						if(count($history) > 0){
							foreach ($history as $row) { ?>
				    <tr>
				    	<!-- FIRST COULUMN -->
				    	<td class="rowlink-skip"><?php echo $n; $n++; ?></td>
				    	<!-- NEXT COULUMN -->
				    	<td>Rp <?php echo number_format($row->wallet_debet); ?></td>
				    	<td>Rp <?php echo number_format($row->wallet_credit); ?></td>
				    	<td>Rp <?php echo number_format($row->wallet_saldo); ?></td>
				    	<td><?php echo $row->wallet_remark; ?></td>
				    	<td><?php echo date("d M Y H:i:s",strtotime($row->modified_on)); ?></td>
				    	<td><?php echo $row->wallet_status; ?></td>
				    	<td><?php echo str_replace("_"," ",$row->wallet_payment_type); ?></td>
				    </tr>
				    <?php } ?>
				    <!-- END Table Data LOOP -->
					<?php }else{ ?>
							<tr>
								<td class="text-center" colspan="8">No Data</td>
							</tr>
					<?php } ?>
				</tbody>
				</table>
			</div>
	</div>

</div>


				<!-- TOP UP E WALLET -->
				<script type="text/javascript">

				//add commas
				function add_commas(number){
					//remove any existing commas...
					number=number.replace(/,/g, "");
					//start putting in new commas...
					number = '' + number;
					if (number.length > 3) {
						var mod = number.length % 3;
						var output = (mod > 0 ? (number.substring(0,mod)) : '');
						for (i=0 ; i < Math.floor(number.length / 3); i++) {
							if ((mod == 0) && (i == 0))
								output += number.substring(mod+ 3 * i, mod + 3 * i + 3);
							else
								output+= ',' + number.substring(mod + 3 * i, mod + 3 * i + 3);
						}
						return (output);
					}
					else return number;
				}

				//number only
				$(document).ready(function() {
				    $('.number-only').keypress(function(e) {
				            var verified = (e.which == 8 || e.which == undefined || e.which == 0) ? null : String.fromCharCode(e.which).match(/[^0-9]/);
				            if (verified) {e.preventDefault();}
				    });
				});

				// Trim commas on submit
				// HOW?
				//$(document).ready(function(){
				//var **stripString** = $('.topup_ammount').text().replace(/,/g, '');
				//$('.*topup_ammount*').text(**stripString**);
				//});

				</script>

				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<form class="form-horizontal" role="form" method="post" id="form_cash_out" action="<?php echo site_url("transaction/checkout/e_wallet")?>">
								
  					<div class="modal-dialog" role="document">
   						 <div class="modal-content">
    						  <div class="modal-header">
       								 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       								<h4 class="modal-title" id="myModalLabel">Top Up E Wallet</h4>
    						  </div>
     						 <div class="modal-body">
								<div class="form-group">
                        			<label for="email" class="col-md-4">Top Up Amount (IDR)</label>
                        				<div class="col-md-8">
                            				<input type="text" name="topup_ammount"class="number-only form-control" onkeyup="this.value=add_commas(this.value);" name="" id="" required />
                        				</div>
								</div>
							 </div>
   						     <div class="modal-footer">
    					    	<button type="submit" class="btn text-center" style="background-color:#704390; color:#fff;" >
                					<span class="glyphicon glyphicon-floppy-disk"></span> Submit
               					 </button>
    						  </div>
   						 </div>
 					 </div>
					
    			</form>
				</div>
				
				<?php 
							
					$userdata  = $this->session->userdata('logged_in_user');
					$lender_id = $userdata['uid'];
				?>
				<div class="modal fade" id="Cashout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<form class="form-horizontal" role="form" method="post" id="form_cash_out" action="<?php echo site_url("transaction/checkout/cashout")?>">
								
  					<div class="modal-dialog" role="document">
   						 <div class="modal-content">
    						  <div class="modal-header">
       								 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       								<h4 class="modal-title" id="myModalLabel">Cashout Wallet</h4>
    						  </div>
     						 <div class="modal-body">
								<div class="form-group">
                        			<label for="email" class="col-md-4">Amount (IDR)</label>
                        				<div class="col-md-8">
                            				<input type="text" name="ammount" class="number-only form-control" onkeyup="this.value=add_commas(this.value);" id="" required />
											<input type="hidden" name="lender" value="<?php echo $lender_id; ?>" />
                        				</div>
								</div>
							 </div>
   						     <div class="modal-footer">
    					    	<button type="submit" class="btn text-center" style="background-color:#704390; color:#fff;" >
                					<span class="glyphicon glyphicon-floppy-disk"></span> Submit
               					 </button>
    						  </div>
   						 </div>
 					 </div>
					
    			</form>
				</div>