	<div class="headerSimple">
		<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png">
	</div>

	<div class="container">
		<div class="row formRegister">
			<form method="post" action="">
				<?php if($this->session->flashdata('message')){ ?>
                   <b><?php echo print_message($this->session->flashdata('message')); ?></b><br/>
                <?php } ?>
				  <div class="form-group">
				    <input name="register_first_name" type="text" class="form-control" id="" placeholder="FirstName">
				  </div>

				  <div class="form-group">
				    <input name="register_last_name" type="text" class="form-control" id="" placeholder="LastName">
				  </div>
				  <br/>	
				  <div class="form-group">
				    <input name="register_email" type="email" class="form-control" id="" placeholder="Email">
				  </div>

				  <div class="form-group">
				    <input name="register_passwd" type="password" class="form-control" id="" placeholder="Password">
				  </div>

				  <div class="form-group">
				    <input name="register_passwd_confirm" type="password" class="form-control" id="" placeholder="Confirm Password">
				  </div>
				  <br/>				  
				  
				  <div class="form-group">
				    <input name="register_phone" type="text" class="form-control number-only" id="" placeholder="Phone">
				  </div>
				  <div class="form-group hidden">
				    <textarea name="register_address" type="text" class="form-control" id="" placeholder="Address" ></textarea>
				  </div>
				  <div class="form-group hidden">
				    <input name="register_city" type="text" class="form-control" id="" placeholder="City">
				  </div>
				  <div class="form-group hidden">
				    <input name="register_province" type="text" class="form-control" id="" placeholder="Province">
				  </div>
				  <div class="form-group hidden">
				    <input name="register_zipcode" type="text" class="form-control" id="" placeholder="Zipcode">
				  </div>
				  <br/><h6>By continuing, I agree to the terms of use and <br/>privacy policy</h6>
				  <button type="submit" class="btn btn-default">REGISTER</button>
			</form>
		</div>
		<div class="row formRegisterNew">
		  	<h6>Already own an account? <br/>Login to start investing.</h6>
	          	<a href="<?php echo site_url() ?>"><button class="btn btn-default" style="background-color:#704390;">&nbsp;<  B A C K</button></a>
			  	<a href="<?php echo site_url('login') ?>"><button class="btn btn-default" style="background-color:#704390;"> L O G I N  ></button></a>
	    </div>  
	</div>


<script type="text/javascript">
	//number only
	$(document).ready(function() {
	    $('.number-only').keypress(function(e) {
            var verified = (e.which == 8 || e.which == undefined || e.which == 0) ? null : String.fromCharCode(e.which).match(/[^0-9]/);
            if (verified) {e.preventDefault();}
		});
	});
</script>