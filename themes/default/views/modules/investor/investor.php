
<!-- HEADER -->
	<div id="headerImageInvest">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="100px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

			<div class="row navi shadowNavi">
				<div class="col-md-1 "></div>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
				</div>
				<?php } else {?>
				<div class="col-md-2">
					<a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
				</div>
				<?php }?>


				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>	
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
				</div>	
				<?php }?>


				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-1"></div>
				
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMBIAYAAN</a>
				</div>
				<?php }?>

				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVEST</a>
				</div>
				<?php } else {?>
				<div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTASI</a>
				</div>
				<?php }?>
				
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
						  <button class="btn btn-login">Login</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php } else {?>
				<div class="col-md-1 res">
					<?php
						if($this->session->userdata('logged_in_user')) {
							echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
						}else{
					?>
					<a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
					<div class="loginPop">
						<form method="post" action="letmein">
						  <div class="form-group">
						    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
						  </div>
						  <div class="form-group">
						    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
						  </div>
						  <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
						  <button class="btn btn-login">MASUK</button>
						</form>
					</div>
					<?php } ?>
				</div>
				<?php }?>
			</div>

			<!-- Hero Text -->
			<div class="startContent clearfix ">
		    	<!--<h2>With peer to peer lending we cut the cost of</br> the highstreet and share the savings with you</h2>-->
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		    	<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Enjoy that fulfilling experience when you get <br>your desired economic return as well as create a positive impact</h2>
		    	<?php
						if($this->session->userdata('logged_in_user')) {
							$investor_navigation = site_url('dashboard');
						}else{
							$investor_navigation = site_url('register');
						}
				?>
				<a href="<?php echo $investor_navigation; ?>" class="button_main btn btn-default" type="submit">Start Investing</a>
				<?php } else {?>
				<h2 style="letter-spacing: 0.5px;font-weight:700;text-shadow:0.1px 1px 0.1px #181818; ">Menyelesaikan kendala finansial <br>yang dihadapi usaha mikro dan kecil dengan nilai pengembalian yang seimbang.</h2>
		    	<?php
						if($this->session->userdata('logged_in_user')) {
							$investor_navigation = site_url('dashboard');
						}else{
							$investor_navigation = site_url('register');
						}
				?>
				<a href="<?php echo $investor_navigation; ?>" class="button_main btn btn-default" type="submit">Mulai Investasi</a>
				<?php }?>
				
			</div>
	    </div>
		
	</div>
	<!-- END OF HEADER -->
	
	

	<section class="bg-white">
	<div class="container">
		<div class="row investCont1">
			<div class="col-md-6 chartDataGraph">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h2 class="opensans text-center bold purple">Loan Disbursement</h2>
				<?php } else {?>
				<h2 class="opensans text-center bold purple">Penyaluran Kredit</h2>
				<?php }?>
				<div class="graph">
					<canvas id="investChart1" ></canvas>
					<script type="text/javascript">

						var investData1 = {
						    labels: ["Dec-10",
						    		 "Dec-11", 
						    		 "Dec-12", 
						    		 "Dec-13", 
						    		 "Dec-14", 
						    		 "Dec-15"],
						    datasets: [
						        {
						            label: "Loan Disbursement",
						            fillColor: "rgba(151,187,205,1)",
						            strokeColor: "rgba(151,187,205,1)",
						            pointColor: "rgba(151,187,205,1)",
						            pointStrokeColor: "#fff",
						            pointHighlightFill: "#fff",
						            pointHighlightStroke: "rgba(151,187,205,1)",

						            data: [0.07095,
						            	   0.648, 
						            	   3.164, 
						            	   8.943, 
						            	   19.278, 
						            	   33.42]
						        }

						    ]
						};
					</script>
				</div>
			</div>

					
			<div class="col-md-6 ">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h2 class="opensans text-center bold purple">Our Growth</h2>
				<h4 class="fs-14 opensans">Since our inception as a microfinance institution in 2010, Amartha has disbursed more than IDR 30 billion.</h4>
				<h4 class="fs-14 opensans">When we make a decision on whether to finance or not, we go beyond employing a credit scoring to recognising other aspects, including schools attended, area of study, academic performance, and work history.</h4>
				<h4 class="fs-14 opensans">Despite having limited assets, limited formal working experience, and a lack of credit history; our robust underwriting model allows high-quality borrowers to be identified from those micro and small businesses. With continuous training, character building, and trust building that equipped with reliable monitoring system, we can transform micro and small businesses into creditworthy borrowers.</h4>
				<?php } else {?>
				<h2 class="opensans text-center bold purple">Pertumbuhan</h2>
				<h4 class="fs-14 opensans">Sejak awal Amartha berdiri sebagai Lembaga Keuangan Mikro (LKM) pada 2010, kami telah mengucurkan dana lebih dari Rp 30 miliar (USD 3 juta).</h4>
				<h4 class="fs-14 opensans">Ketika kami membuat keputusan apakah memberikan pembiayaan atau tidak, kami tidak lagi menggunakan sudut pandang tradisional kenali-konsumen-Anda. Kami memanfaatkan nilai pembiayaan untuk menghitung aspek lain, seperti kohesivitas sosial dan kepercayaan masyarakat lokal.</h4>
				<h4 class="fs-14 opensans">Model pembiayaan yang kami terapkan telah berhasil mentransformasi pengusaha mikro menjadi peminjam berkualitas meskipun dengan aset dan pengalaman pekerjaan formal yang terbatas. Mereka yang kami sebut dengan peminjam-utama di masa depan.</h4>
				<?php }?>
			</div>
			
			
		</div>
	</div>
	<!--
	<div class="container">
		<div class="row investCont2" style="margin-top:-100px;">
			<div class="about_WhoWeAre  text-justify ">
				<h2 class="opensans text-center purple">Value proposition</h2>
				<p class="opensans fs-14">To continue being a responsible microfinance institution, we follow a rigorous initial assessment and undertake continuous monitoring in order to protect our clients.
											We firmly hold onto the principles of Appropriate Product Design and Delivery, Prevention Over-indebtedness and Transparency.
									</p>
			</div>
		</div>
	</div>-->
	</section>


	<!-- <section class="">
	<div class="container">
		<div class="row investCont2">
			<div class="col-md-6 text-justify opensans">
				<h2 class="purple  opensans">How we reduce your risk</h2>
				<h4 class="fs-14 opensans">We do thorough credit and affordability checks on all potential borrowers. Once they become the borrowers, we continue to assess and monitor their behaviour through: </h4>

				<ul style="list-style-type: circle;">
					<li ><h4 class="fs-14 opensans">Discipline in attending weekly group meeting: no short-cuts, no late attendance</h4></li>
					<li ><h4 class="fs-14 opensans">Loan utilization checks</h4></li>
					<li ><h4 class="fs-14 opensans">Clear and strict disciplinary procedure</h4></li>
					<li ><h4 class="fs-14 opensans">Control environment</h4></li>
					<li ><h4 class="fs-14 opensans">Risk assessment</h4></li>
					<li ><h4 class="fs-14 opensans">Control activities</h4></li>
				</ul> 

				
			</div>

			
			<div class="col-md-6">
				<div class="info bg-purpleAmartha">
					<img src="<?php echo $this->template->get_theme_path(); ?>/img/dhapati.png" width="100%"> 
					<div class="row">
						<div class="col-md-6">
							<h4>1. All borrowers will pay a risk-adjusted fee into the fund</h4>
						</div>
						<div class="col-md-6">
							<h4>2. If a borrower misses a payment, the Dhanapati Fund repays investors; if the loan goes into default, the Dhanapati Fund takes over the loan and repays the principal investment of the investors.</h4>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
	</section> -->

	<section class="">
	<div class="container">
		<div class="row listReduce">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h2 class="purple  opensans">How We Reduce Your Risk</h2>
			<?php } else {?>
			<h2 class="purple  opensans">Bagaimana kami mengelola risiko</h2>
			<?php }?>

			<div class="col-md-4 col-xs-4">
				<div class="row">
					<div class="col-md-3">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/reduce/icon1.png">
					</div>

					<div class="col-md-9 left">
						<?php if ($this->session->userdata('site_lang') == 'english') { ?>
						<h3>Robust Underwriting:</h3>
						<p>we do thorough credit and affordability checks on all potential borrowers. Once they become the borrowers, we continue to assess and monitor their behaviour through: </p>
						<ul style="padding-left:20px;">
							<li ><p >Discipline in attending weekly group meeting: no short-cuts, no late attendance</p></li>
							<li ><p >Loan utilization checks</p></li>
							<li ><p>Clear and strict disciplinary procedure</p></li>
							<li ><p >Control environment</p></li>
							<li ><p >Risk assessment</p></li>
							<li ><p >Control activities</p></li>
						</ul> 
						<?php } else {?>
						<h3>Penjaminan (yang) Kuat:</h3>
						<p>Kami menerapkan monitoring pembiayaan dan nilai kelayakan untuk semua calon peminjam. Setelah mereka resmi sebagai peminjam, kami terus menilai dan melakukan pengawasan melalui: </p>
						<ul style="padding-left:20px;">
							<li ><p >Disiplin kehadiaran dalam pertemuan kelompok mingguan: tidak ada keterlambatan</p></li>
							<li ><p >Monitoring pembiayaan</p></li>
							<li ><p>Penerapan disiplin yang ketat dan jelas</p></li>
							<li ><p >Mengenali lingkungan</p></li>
							<li ><p >Perkiraan risiko</p></li>
							<li ><p >Mengedepankan pengawasan</p></li>
						</ul> 
						<?php }?>
					</div>
				</div>

			</div>

			<div class="col-md-4 col-xs-4">
				<div class="row">
					<div class="col-md-3">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/reduce/icon2.png">
					</div>

					<div class="col-md-9 left">
						<?php if ($this->session->userdata('site_lang') == 'english') { ?>
						<h3>Portfolio Management:</h3>
						<p>Our loan book is diversified across many borrowers with different purposes of loan, different platforms, margins, and tenors.</p>
						<?php } else {?>
						<h3>Manajemen Portofolio:</h3>
						<p>Buku angsuran kami bervariasi untuk setiap peminjam sesuai dengan tujuan dan besar pembiayaan, margin, dan lama angsuran.</p>
						<?php }?>
					</div>
				</div>

			</div>

			<div class="col-md-4 col-xs-4">
				<div class="row">
					<div class="col-md-3">
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/reduce/icon3.png">
					</div>

					<div class="col-md-9 left">
						<?php if ($this->session->userdata('site_lang') == 'english') { ?>
						<h3>The Dhanapati Fund:</h3>
						<p>if a borrower misses a payment, the Dhanapati Fund repays investors; if the loan goes into default, the Dhanapati Fund takes over the loan and repays outstanding capital to investors.</p>
						<?php } else {?>
						<h3>Dana Dhanapati:</h3>
						<p>Kami merancang sistem investasi aman bagi investor. Semua peminjam akan membayar biaya risiko yang didesuaikan dengan total dana pembiayaan. Jika seorang peminjam mengalami gagal bayar, Dana Dhanapati akan digunakan untuk membayar investor. Jika proses peminjaman berjalan semestinya, Dana Dhanapati dapat mengambil alih pembiayaan dan melunasi investasi utama dari investor.</p>
						<?php }?>
					</div>
				</div>

			</div>

	</div>
	</div>
	</section>
	
	<div class="container">
		<div class="row investCont3">
			<!-- <h2 style="margin-bottom:80px;">Repayment and Portfolio status</h2> -->
			<div class="col-md-6">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h2 class="purple" style="margin-bottom:20px;font-weight:700;">Repayment</h2>

			<?php } else {?>
			<h2 class="purple" style="margin-bottom:20px;font-weight:700;">Pembayaran Kembali</h2>

			<?php }?>

				<div class="info2">
					<div class="graph">
							<canvas id="investChart2" ></canvas>
						</div>
					</div>

					<script type="text/javascript">

						var investData2 = {
						    labels: ["Feb-15", "Apr-15",  "Jun-15", "Aug-15","Oct-15","Dec-15"],
						    datasets: [
						        {
						            label: "Repayment",
						            fillColor: "rgba(151,187,205,1)",
						            strokeColor: "rgba(151,187,205,1)",
						            pointColor: "rgba(151,187,205,1)",
						            pointStrokeColor: "#fff",
						            pointHighlightFill: "#fff",
						            pointHighlightStroke: "rgba(151,187,205,1)",
						            data: [1.787, 3.651, 5.529, 7.262, 9.576, 12.103],

						        }

						    ]
						};
						var money = {
					        scaleLabel : "<%= Number(value).toFixed(3).replace() + '.000.000 IDR'%>",
					        responsive: [true],
							tooltipTemplate: "<%= label+':' %>  IDR (<%=Number(value).toFixed(3).replace('.',',')%>) billion"

					    };

					    var money2 = {
					        scaleLabel : "<%= Number(value) + ',000,000,000 IDR'%>",
					        responsive: [true],
							tooltipTemplate: "<%= label+':' %> IDR (<%=Number(value).toFixed(3).replace('.',',')%>) million"

					    };

					    var persen = {
					        scaleLabel : "<%= Number(value) + '%'%>",
					        responsive: [true],
	tooltipTemplate: "<%= label+':' %> (<%=value%>%)"

					    };


						

					</script>


				</div>


			<div class="col-md-6">
			<?php if ($this->session->userdata('site_lang') == 'english') { ?>
			<h2 class="purple" style="margin-bottom:20px;font-weight:700;">Portfolio status</h2>

			<?php } else {?>
			<h2 class="purple" style="margin-bottom:20px;font-weight:700;">Status Portofolio</h2>

			<?php }?>

				<div class="info3"style="height: 350px;">
					<div class="graph">
					<canvas id="investChart3" height="230px"></canvas>
					</div>

				<script type="text/javascript">
					var investData3 = {
					    labels: ["Current on paid in full", "1-7 days late", "8-14 day late", "15-30 day late", "30day+ late", "Charged off"],
					    datasets: [
					        {
					            label: "amount of investment last month",
					            fillColor: "#cd8cec",
					            strokeColor: "#cd8cec",
					            pointColor: "#cd8cec",
					            pointStrokeColor: "#fff",
					            pointHighlightFill: "#fff",
					            pointHighlightStroke: "rgba(151,187,205,1)",
					            data: [99.92, 0.076, 0, 0, 0, 0]
					        }
					    ]
					};
				</script>

				</div>
			</div>

		</div>
	</div>

	<!-- <div class="container">
		<div class="row investCont2" style="margin-top:-100px;">
			<div class="about_WhoWeAre  text-justify ">
				<h1 class="opensans text-left purple">Whom we lend to</h1>
				<p class="opensans">We mainly lend to those with:</p>
				<ul style="list-style-type: circle;">
					<li ><h4 class="fs-14 opensans">Education: have completed or have not completed primary school</h4></li>
					<li ><h4 class="fs-14 opensans">Occupation: self-employed, small groceries owners, landless labourers</h4></li>
					<li ><h4 class="fs-14 opensans">Family: with average of 3-4 children</h4></li>
					<li ><h4 class="fs-14 opensans">Age: women in their productive age between 31-40 years old</h4></li>
					<li ><h4 class="fs-14 opensans">Housing conditions: mostly woven bamboo walls, earth flooring, and outside closet</h4></li>
				</ul>

			</div>
		</div>
	</div> -->

	<div class="container">
		<div class="row lendContent">
		<?php if ($this->session->userdata('site_lang') == 'english') { ?>
		<h1 class="purple">Whom We Lend To</h1>	
		<?php } else {?>
		<h1 class="purple">Siapakah Peminjam Kami</h1>	
		<?php }?>

			<div class="col-md-4">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/lend/education.png">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h2>EDUCATION:</h2>
				<p>have completed or have not completed primary school</p>
				<?php } else {?>
				<h2>PENDIDIKAN:</h2>
				<p>lulus atau tidak lulus SD</p>
				<?php }?>
			</div>
			<div class="col-md-4">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/lend/occupation.png">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h2>OCCUPATION:</h2>
				<p>self-employed, small groceries owners, landless labourers</p>
				<?php } else {?>
				<h2>PEKERJAAN:</h2>
				<p>wirausaha, pemilik warung kelontongan, buruh tani.</p>
				<?php }?>
			</div>
			<div class="col-md-4">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/lend/family.png">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h2>FAMILY:</h2>
				<p>with average of 3-4 children</p>
				<?php } else {?>
				<h2>KELUARGA:</h2>
				<p>rata-rata 3 – 4 orang anak</p>
				<?php }?>
			</div>
		</div>
		<div class="row lendContent">
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/lend/age.png">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h2>AGE:</h2>
				<p>women in their productive age between 31-40 years old</p>
				<?php } else {?>
				<h2>UMUR:</h2>
				<p>perempuan di usia produktif 31 – 40 tahun</p>
				<?php }?>
			</div>
			<div class="col-md-4">
				<img src="<?php echo $this->template->get_theme_path(); ?>/img/lend/housing.png">
				<?php if ($this->session->userdata('site_lang') == 'english') { ?>
				<h2>HOUSING CONDITION:</h2>
				<p>mostly woven bamboo walls, earth flooring, and outside closet</p>
				<?php } else {?>
				<h2>KONDISI RUMAH:</h2>
				<p>bangunan dengan dinding bambu, lantai tanah, dan kamar mandi di luar.</p>
				<?php }?>
			</div>
			<div class="col-md-2"></div>

		</div>
	</div>



	<div class="container hidden">
		<div class="row investCont4">
			<h2>A closer look at our borrower</h2>

			<div class="col-md-4">
				<div class="info3">
					<div class="row">
						<div class="col-md-6 left"><img src="<?php echo $this->template->get_theme_path(); ?>/img/user.png"></div>
						<div class="col-md-6 right">
							<h2>693</h2>
							<h5>Average Score</h5>
						</div>

					</div>

					<div class="row">
						<div class="col-md-6 left"><img src="<?php echo $this->template->get_theme_path(); ?>/img/user.png"></div>
						<div class="col-md-6 right">
							<h2>Rp. 345.987</h2>
							<h5>Average Income</h5>
						</div>

					</div>
				</div>

			</div>
			<div class="col-md-4">
				<div class="info4">
					<div class="donut">
					<h3>Age</h3>
					<canvas id="investChart4" width="500" height="500"/>
						</div>

						<script>
								var investData4 = [
										{
											value: 300,
											color:"#F7464A",
											highlight: "#FF5A5E",
											label: "purple"
										},
										{
											value: 50,
											color: "#46BFBD",
											highlight: "#5AD3D1",
											label: "blue"
										},
										{
											value: 100,
											color: "#FDB45C",
											highlight: "#FFC870",
											label: "Yellow"
										},
										{
											value: 40,
											color: "#949FB1",
											highlight: "#A8B3C5",
											label: "Grey"
										},
										{
											value: 120,
											color: "#4D5360",
											highlight: "#616774",
											label: "Dark Grey"
										}

									];



						</script>



				</div>
			</div>

			<div class="col-md-4">
				<div class="info4">
					<div class="donut">
					<h3>Industry</h3>
					<canvas id="investChart5" width="500" height="500"/>
						</div>

						<script>
								var investData5 = [
										{
											value: 300,
											color:"#F7464A",
											highlight: "#FF5A5E",
											label: "purple"
										},
										{
											value: 50,
											color: "#46BFBD",
											highlight: "#5AD3D1",
											label: "blue"
										},
										{
											value: 100,
											color: "#FDB45C",
											highlight: "#FFC870",
											label: "Yellow"
										},
										{
											value: 40,
											color: "#949FB1",
											highlight: "#A8B3C5",
											label: "Grey"
										},
										{
											value: 120,
											color: "#4D5360",
											highlight: "#616774",
											label: "Dark Grey"
										}

									];



						</script>
				</div>
			</div>
		</div>
	</div>

	<?php if ($this->session->userdata('site_lang') == 'english') { ?>
	<div class="container" style="padding-top:30px;">
		<h1 class="opensans text-center bold purple">Institutional Investors</h1>
		<p class="opensans fs-14 text-center" style="padding-top:20px;">Amartha partners with institutional investors such as funds , banks, and other financial institutions, which lend the necessary funds to finance creditworthy borrowers. In return, not only can institutional investors a competitive financial return but they can also create an impact at the grassroots level.</p>

		<p class="opensans fs-14 text-center" style="padding-top:20px;">As a lending marketplace, Amartha enables institutional investors to purchase loans originated through the Amartha technology platform. Through this platform, institutional investors can leverage the underwriting capabilities of Amartha and gain access to this emerging asset class.</p>

		<p class="opensans fs-14 text-center" style="padding-top:20px; padding-bottom:20px;;"> If your organisation is interested in partnering with Amartha, please send your email to <u>institutional@amartha.com</u>. We will contact you soon to schedule a time to speak with our partnership team.</p>
	</div>
	<?php } else {?>
	<div class="container" style="padding-top:30px;">
		<h1 class="opensans text-center bold purple">Investor Institutional</h1>
		<p class="opensans fs-14 text-center" style="padding-top:20px;">Amartha bermitra dengan institutional investor seperti bank, pengelola dana, dan lembaga keuangan lainnya yang menyalurkan dananya kepada para peminjam berkualitas. Sebagai hasilnya institutional investor tersebut tidak hanya memperoleh imbal hasil yang kompetitif tetapi juga menciptakan impact pada masyarakat kelas terbawah.</p>

		<p class="opensans fs-14 text-center" style="padding-top:20px;">Sebagai P2P lending marketplace, Amartha memungkinkan investor institutional untuk mendanai pembiayaan melalui teknologi pada platform kami. Dengan demikian, investor dapat memanfaatkan kemampuan kami dalam melakukan underwriting dan mereka pun dapat menjangkau level aset yang sedang berkembang.</p>

		<p class="opensans fs-14 text-center" style="padding-top:20px; padding-bottom:20px;;">Jika institusi Anda tertarik untuk bermitra dengan Amartha, silakan kirimkan email ke <u>hello@amartha.com</u>. Kami dengan senang hati akan menghubungi Anda dan mendiskusikan bagaimana kita bisa bekerja bersama-sama.</p>
	</div>
	
	<?php }?>



<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/jquery.circliful.min.js"></script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/Chart.min.js"></script>




<script type="text/javascript">

$(document).ready(function(){
		var ctx = document.getElementById("investChart1").getContext("2d");
		var ctx2 = document.getElementById("investChart2").getContext("2d");
		var ctx3 = document.getElementById("investChart3").getContext("2d");
		var ctx4 = document.getElementById("investChart4").getContext("2d");
		var ctx5 = document.getElementById("investChart5").getContext("2d");

		window.myLine = new Chart(ctx).Line(investData1, money, {responsive : true});
		window.myLine = new Chart(ctx2).Line(investData2,money2, {responsive : true});
		window.myBar = new Chart(ctx3).Bar(investData3, persen, {responsive : true});
		window.myDoughnut = new Chart(ctx4).Doughnut(investData4, {responsive : true});
		window.myDoughnut = new Chart(ctx5).Doughnut(investData5, {responsive : true});
		
	});

</script>

