	<div class="container">
		<div class="row formRegister">
			<h3 class="purple">Account Information</h3>
			<form method="post" action="">
			  <div class="form-group">
			    <input name="firstname" type="text" class="form-control" id="" placeholder="FirstName" value="<?php echo $lender->lender_first_name;  ?>">
			  </div>

			  <div class="form-group">
			    <input name="lastname" type="text" class="form-control" id="" placeholder="LastName" value="<?php echo $lender->lender_last_name;  ?>">
			  </div>

			  <div class="form-group">
			    <input name="birthday" type="text" class="form-control" id="" placeholder="Date of Birth (YYYY-MM-DD)" value="<?php echo $lender->lender_birth_date;  ?>">
			  </div>

			  <div class="form-group">
			    <textarea name="address" class="form-control" rows="auto" placeholder="Street Address" ><?php echo $lender->lender_address;  ?></textarea>
			  </div>

			  <div class="form-group">
			    <input name="city" type="text" class="form-control" id="" placeholder="City" value="<?php echo $lender->lender_city;  ?>">
			  </div>

			  <div class="form-group">
			    <input name="province" type="text" class="form-control" id="" placeholder="Province" value="<?php echo $lender->lender_province;  ?>">
			  </div>

			  <div class="form-group">
			    <input name="zipcode" type="text" class="form-control" id="" placeholder="ZipCode" value="<?php echo $lender->lender_zipcode;  ?>">
			  </div>

			  <div class="form-group">
			    <input name="phone" type="text" class="form-control" id="" placeholder="Primary Phone" value="<?php echo $lender->lender_phone;  ?>">
			  </div>
			  <select name=""  class="form-control" >
				  <option>1.000.000 – 10.000.000</option>
				  <option>>10juta – 50 juta</option>
				  <option>>50juta – 100juta</option>
				  <option>>100juta – 250 juta</option>
				  <option>>250juta – 500 juta</option>
			</select>
			  <div class="form-group">
			    <input name="idcard" type="text" class="form-control" id="" placeholder="KTP" value="<?php echo $lender->lender_idcard;  ?>">
			  </div>

			  <div class="form-group">
			    <input name="taxcard" type="text" class="form-control" id="" placeholder="NPWP" value="<?php echo $lender->lender_taxcard;  ?>">
			  </div>
			  <div class="form-group">
			    <input name="securityquestion"  type="text" class="form-control" id="" placeholder="Security Question">
			  </div>

			  <h6>I have read and agree the following:</h6>

			 <div class="checkbox">
				  <label>
				    <input name="agreement" type="checkbox" value="">
				    investor agreement and propectus
				  </label>
			</div>

			<div class="checkbox">
				  <label>
				    <input name="trust" type="checkbox" value="">
				    trust agreement
				  </label>
			</div>

			<div class="checkbox">
				  <label>
				    <input name="taxwithholding" name="" type="checkbox" value="">
				    no tax withholding
				  </label>
			</div>

			<div class="checkbox">
				  <label>
				    <input name="taxwithholding" name="" type="checkbox" value="">
				    <a href="#">Term and service</a>
				  </label>
			</div>

			<div class="checkbox">
				  <label>
				    <input name="taxwithholding" name="" type="checkbox" value="">
				    <a href="#">SSUPP</a>
				  </label>
			</div>

			<div class="checkbox">
				  <label>
				    <input name="taxwithholding" name="" type="checkbox" value="">
				    <a href="#">Privacy policy</a>
				  </label>
			</div>

			<div style="">
				  	  <button type="submit" class="btn btn-default">SAVE ></button>
					  <a href="<?php echo site_url('dashboard') ?>">
					  	<button class="btn btn-default" style="background-color:#704390;">< BACK</button>
					  </a>
			</div>
			</form>
		</div>
	</div>
