
<!--	<div class="container">
		<div class="row data1">
			<div class="col-md-4"><h1>Rp <?php echo number_format($deposit['outstanding']); ?></h1> <h3>Outstanding principal</h3></div>
			<div class="col-md-4"><h1>Rp <?php echo number_format($deposit['total']); ?></h1> <h3>Total loan</h3></div>
			<div class="col-md-4"><h1><?php echo number_format($deposit['percentage']); ?>%</h1> <h3>Complete</h3></div>
		</div>

		<div class="row data2">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="progress">
			  <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo number_format($deposit['percentage']); ?>"
			  		 aria-valuemin="0" aria-valuemax="100" style="width:<?php echo number_format($deposit['percentage']); ?>%">
			    <?php echo number_format($deposit['percentage']); ?>%
			  </div>
			</div>

			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
-->
	<div class="container text-center">
		<div class="row data3">
			<div class="col-md-12"><p>&nbsp;</p></div>
			<div class="col-md-12"><h5>Lihat daftar usaha yang ingin dibiayai</h5></div>
			<div class="col-md-12">
				<?php if($deposit['total'] >= 0 ){ ?>
				<a href="<?php echo site_url('loan'); ?>" class="btn text-center" style="background-color:#704390; color:#fff;" type="submit">LIHAT DAFTAR</a>
				<?php }else{ ?>
				<a href="<?php echo site_url('loan/noloan'); ?>" class="btn text-center popupsmall" style="background-color:#704390; color:#fff;" >LIHAT DAFTAR</a>
				<?php }?>
			</div>
		</div>
	</div>
	<br/>
	<div class="container">
		<div class="row data4">
			<div class="col-md-6">
				<!--<div class="infoBox">
					<h5>Profit-shared</h5>
					<h1>Rp 0</h1>
					<h5 class="bottom">Total payment+profit-shared Rp 0</h5>
					<button type="button" class="btn text-center" style="background-color:#704390; color:#fff;" data-toggle="modal" data-target="#myModal">
  					Cash Out
					</button>
				</div>-->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  					<div class="modal-dialog" role="document">
   						 <div class="modal-content">
    						  <div class="modal-header">
       								 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       								<h4 class="modal-title" id="myModalLabel">Cash Out</h4>
    						  </div>
     						 <div class="modal-body">
								<form class="form-horizontal" role="form" method="post" id="form_cash_out">
									<div class="form-group">
                        			<label for="email" class="col-md-4">My Fund</label>
                        				<div class="col-md-8">
                            				<input type="text" class="form-control" name="" id="" required />
                        				</div>
                    				</div>

                    				<div class="form-group">
                        			<label for="email" class="col-md-4">Cash Out</label>
                        				<div class="col-md-8">
                            				<input type="text" class="form-control" name="" id="" required />
                        				</div>
                    				</div>

                    				<div class="form-group">
                        			<label for="email" class="col-md-4">Cash Out Cost</label>
                        				<div class="col-md-8">
                            				<input type="text" class="form-control" name="" id="" required />
                        				</div>
                    				</div>

                    				<div class="form-group">
                        			<label for="email" class="col-md-4">Total Cash Out</label>
                        				<div class="col-md-8">
                            				<input type="text" class="form-control" name="" id="" required />
                        				</div>
                    				</div>
    						    </form>
     						 </div>
   						     <div class="modal-footer">
    					    	<button type="submit" class="btn text-center" style="background-color:#704390; color:#fff;" id="save" form="form_kontak">
                					<span class="glyphicon glyphicon-floppy-disk"></span> Submit
               					 </button>
    						  </div>
   						 </div>
 					 </div>
				</div>
			</div>
<!--			<div class="col-md-6">
				<div class="infoBox">
					<h5>Projected Annualized Return</h5>
					<h1>20%</h1>
				</div>
			</div>
-->		</div>
	</div>
	<div class="container">
		<div class="row data5">
	        <div class="col-md-6">
	        <table class="table">
				    <thead>
				      <tr>
				        <th>My Account #<?php echo $lender->lender_number;?></th>
				        <th></th>
				      </tr>
				    </thead>
				    <tbody>
				      <tr>
				        <td>Available cash</td>
				        <td>Rp <?php echo number_format($deposit['available']); ?></td>
				      </tr>
				      <tr>
				        <td>Outstanding principal</td>
				        <td>Rp <?php echo number_format($deposit['outstanding']); ?></td>
				      </tr>
				      <tr>
				        <td class="summary">Account value</td>
				        <td class="summary">Rp <?php echo number_format($deposit['total']); ?></td>
				      </tr>
				    </tbody>
			    </table>
			    <!-- <a href="<?php echo site_url('invesment'); ?>" class="btn text-center" style="background-color:#704390; color:#fff;" type="submit">My Invesment</a> -->
	        </div>

	        <div class="col-md-6">
	        	<table class="table">
						    <thead>
						      <tr>
						        <th></br></th>
						        <th></th>

						      </tr>
						    </thead>
						    <tbody>
						      <tr>
						        <td>My notes at a glance</td>
						        <td><?php echo $lender_notes['all']; ?></td>

						      </tr>
						      <tr>
						        <td>Issued and current</td>
						        <td><?php echo $lender_notes['current']; ?></td>

						      </tr>
						       <tr>
						        <td>Fully paid</td>
						        <td><?php echo $lender_notes['finished']; ?></td>

						      </tr>
						      <tr>
						        <td>Late 1-14 days</td>
						        <td><?php echo $lender_par['1']; ?></td>

						      </tr>

						      <tr>
						        <td>Late 15-30 days</td>
						        <td><?php echo $lender_par['2']; ?></td>

						      </tr><tr>
						        <td>Late 30+ days</td>
						        <td><?php echo $lender_par['3']; ?></td>

						      </tr><tr>
						        <td>Write-off</td>
						        <td>0</td>

						      </tr>

						    </tbody>
			 			</table>
			  <!--<div  style="float:right;">
			  		<h5 style="float:right; padding-left:5px;"><a href=""> Amount</a></h5>
				    <h5 class="active" style="float:right;">Display By <a href="">Number</a>   |  </h5>
			  </div>-->
	      </div>
		</div>
</div>
