	<!-- TABLE DATA -->
	<div class="container historyData">
		
		<div class="browseTable">
				<!-- TABLE START!! -->
				<table class="table table-striped table-bordered table-hover" width="100%">	
			    <thead >
			       <tr>
					<th width="120px">Name</th>
			      	<th class="left">Loan Sector</th>
			      	<th class="center hidden">Credit Score</th>
			      	<th class="center hidden">Credit Grade</th>
			      	<th class="left">Purpose</th>
			      	<th class="center">Tenor<br/>(Week)</th>
			      	<th class="text-right">Loan Ammount (IDR)</th>
			      	<th class="text-right">Outstanding Principal (IDR)</th>
			      	<th class="text-right">On-going Profit-shared (IDR)</th>
			      	<th class="center">Repayment Status</th>
			      </tr>
			    </thead>

			    <tbody >

			  
					<?php 
						if(count($borrower) > 0){
						foreach($borrower AS $row){ 
					?>
							<tr>
								<td class="text-left"><a href="<?php echo site_url('loan/view/'.$row->data_id); ?>" title="View Borrower"><?php echo $row->client_fullname; ?></a></td>
								<td class="text-left"><?php echo $row->sector_name; ?></td>
								<td class="text-center hidden"><?php echo $row->data_popi_score; ?></td>
								<td class="text-center hidden"><?php echo $row->data_popi_grade; ?></td>
								<td class="text-left"><?php echo $row->data_tujuan;; ?></td>
								<td class="text-center"><?php echo $row->data_jangkawaktu;; ?></td>
								<td class="text-right"><?php echo number_format($row->data_plafond); ?></td>
								<td class="text-right"><?php echo number_format(($row->data_jangkawaktu - $row->data_angsuranke)*($row->data_plafond / $row->data_jangkawaktu)); ?></td>
								<td class="text-right"><?php echo number_format(20/100*($row->data_angsuranke)*($row->data_plafond / $row->data_jangkawaktu)); ?></td>
								<td class="text-center"><?php if($row->data_par > 0){ echo "TERLAMBAT ($row->data_par)"; }else{ echo "LANCAR"; }; ?></td>
							</tr>
					<?php } ?>
					<?php }else{ ?>
							<tr>
								<td class="text-center" colspan="10">No Data</td>
							</tr>
					<?php } ?>

				    
				  </tbody>
			  </table>
				<!-- TABLE END!! -->

			  </div>
			 

			</div>
	</div>

<!-- CALLING CHART SCRIPT -->
<script type="text/javascript">
	window.onload = function(){
		var ctx = document.getElementById("chart-area").getContext("2d");
		var ctx2 = document.getElementById("chart-area2").getContext("2d");

		window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});
		window.myDoughnut = new Chart(ctx2).Doughnut(doughnutData2, {responsive : true});
	};
</script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/Chart.min.js"></script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/datatables.min.js"></script>

