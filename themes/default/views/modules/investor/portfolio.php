	<div class="container hidden"><br/>
		<div class="row chartData hidden">
			<!-- DATA CHART 1 -->
			<div class="col-md-6">
				<h3>TESTING</h3>
				<div id="canvas-holder">
					<canvas id="chart-area" width="500" height="500"/>
				</div>

				<script>
					var doughnutData = [
							{
								value: 300,
								color:"#F7464A",
								highlight: "#FF5A5E",
								label: "Red"
							},
							{
								value: 50,
								color: "#46BFBD",
								highlight: "#5AD3D1",
								label: "Green"
							},
							{
								value: 100,
								color: "#FDB45C",
								highlight: "#FFC870",
								label: "Yellow"
							},
							{
								value: 40,
								color: "#949FB1",
								highlight: "#A8B3C5",
								label: "Grey"
							},
							{
								value: 120,
								color: "#4D5360",
								highlight: "#616774",
								label: "Dark Grey"
							}

						];



				</script>

				<div class="doughnut-legend">
					<ul>
						<li><span style="background-color:#F7464A;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#46BFBD;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#FDB45C;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#949FB1;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#4D5360;"></span>
							<h6>test</h6>
						</li>
					</ul>
				</div>

			</div> <!-- DATA CHART 1 END -->



			<!-- DATA CHART 2 -->
			<div class="col-md-6">
				<h3>Testing</h3>
				<div id="canvas-holder">
					<canvas id="chart-area2" width="500" height="500"/>
				</div>

				<script>
					var doughnutData2 = [
							{
								value: 300,
								color:"#F7464A",
								highlight: "#FF5A5E",
								label: "purple"
							},
							{
								value: 50,
								color: "#46BFBD",
								highlight: "#5AD3D1",
								label: "blue"
							},
							{
								value: 100,
								color: "#FDB45C",
								highlight: "#FFC870",
								label: "Yellow"
							},
							{
								value: 40,
								color: "#949FB1",
								highlight: "#A8B3C5",
								label: "Grey"
							},
							{
								value: 120,
								color: "#4D5360",
								highlight: "#616774",
								label: "Dark Grey"
							}

						];



				</script>
				<div class="doughnut-legend">
					<ul>
						<li><span style="background-color:#F7464A;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#46BFBD;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#FDB45C;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#949FB1;"></span>
							<h6>test</h6>
						</li>
						<li><span style="background-color:#4D5360;"></span>
							<h6>test</h6>
						</li>
					</ul>
				</div>
			</div>
			<!-- DATA CHART 2 END -->
		</div>
		<!-- CHART END -->
	</div>

	<!-- TABLE DATA -->
	<div class="container historyData">

		<!-- <h2>HISTORY</h2> -->

		<div class="browseTable">
				<!-- TABLE START!! -->
				<table class="table table-striped table-bordered table-hover" width="100%">

	
			    <thead >
			       <tr>
					<th width="120px">Name</th>
			      	<th class="left">Loan Sector</th>
			      	<th class="left">Purpose</th>
			      	<th class="center">Tenor<br/>(Week)</th>
			      	<th class="text-right">Loan Ammount (IDR)</th>
			      	<th class="text-right">Outstanding Principal (IDR)</th>
			      	<th class="text-right">On-going Profit-shared (IDR)</th>
			      	<th class="center">Repayment Status</th>
			      	<th class="center">View Repayment</th>
			      </tr>
			    </thead>

			    <tbody >

					
					<?php 
						if(count($borrower) > 0){
							foreach($borrower AS $row){ ?>
				    <tr>
				    	<td class="text-left"><a href="<?php echo site_url('loan/view/'.$row->data_id); ?>" title="View Borrower"><?php echo $row->client_fullname; ?></a></td>
				    	<td class="text-left"><?php echo $row->sector_name; ?></td>
				    	<td class="text-left"><?php echo $row->data_tujuan;; ?></td>
				    	<td class="text-center"><?php echo $row->data_jangkawaktu;; ?></td>
				    	<td class="text-right"><?php echo number_format($row->data_plafond); ?></td>
				    	<td class="text-right"><?php echo number_format(($row->data_jangkawaktu - $row->data_angsuranke)*($row->data_plafond / $row->data_jangkawaktu)); ?></td>
				    	<td class="text-right"><?php echo number_format(($row->data_angsuranke)*($row->data_plafond / $row->data_jangkawaktu * 20/100)); ?></td>
						<td class="text-center">
							<?php 
								if($row->data_status == 2){
									echo "PENGAJUAN";
								}elseif($row->data_status == 1){
									if($row->data_par > 0){ 
										echo "TERLAMBAT ($row->data_par)"; 
									}else{ 
										echo "LANCAR"; 
									}									
								} 
							?>
						</td>
						<td class="text-center">
							<a href="<?php echo site_url('loan/repayment/'.$row->data_id); ?>" title="View Repayment"><span class="glyphicon glyphicon-file" aria-hidden="true"></span></a>
						</td>
				    </tr>
					<?php } ?>
					<?php }else{ ?>
							<tr>
								<td class="text-center" colspan="11">No Data</td>
							</tr>
					<?php } ?>
				    
				  </tbody>
			  </table>
				<!-- TABLE END!! -->

			  </div>
			  

			</div>
	</div>

<!-- CALLING CHART SCRIPT -->
<script type="text/javascript">
	window.onload = function(){
		var ctx = document.getElementById("chart-area").getContext("2d");
		var ctx2 = document.getElementById("chart-area2").getContext("2d");

		window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});
		window.myDoughnut = new Chart(ctx2).Doughnut(doughnutData2, {responsive : true});
	};
</script>
<script type="text/javascript" src="<?php echo $this->template->get_theme_path(); ?>/js/Chart.min.js"></script>