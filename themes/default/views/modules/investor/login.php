	<div class="headerSimple">
		<a href="<?php echo site_url(); ?>" title="Amartha"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png"></a>
	</div>

	<div class="container">


		<div class="row formRegister">
			<form method="post" action="letmein">
				<?php if($this->session->flashdata('message')){ ?>
                   <?php echo print_message($this->session->flashdata('message')); ?>
                <?php } ?>

				  <h3>INVESTOR LOGIN</h3><br/>
				  <div class="form-group">
				    <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
				  </div>

				  <div class="form-group">
				    <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
				  </div>
				  <div style="">
				  	  <button type="submit" class="btn btn-default">LOGIN &gt;</button>
					  <a href="<?php echo site_url() ?>" class="btn hidden btn-default" style="background-color:#704390;"> &lt; BACK  </a>
				  </div>
				  <div class="form-group">
				    <small>Forgot password? <a href="<?php echo site_url("forget") ?>" title="Forgot Password" ><u>Click here</u></a></small>
				  </div>
			</form>
          	
			
		</div>
	</div>
