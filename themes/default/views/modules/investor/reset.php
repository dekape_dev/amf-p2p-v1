	<div class="headerSimple">
		<a href="<?php echo site_url(); ?>" title="Amartha"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png"></a>
	</div>

	<div class="container">


		<div class="row formRegister">
			<form method="post" action="">
				<?php if($this->session->flashdata('message')){ ?>
                   <?php echo print_message($this->session->flashdata('message')); ?>
                <?php } ?>

				  <h3>RESET PASSWORD</h3><br/>
				  <div class="form-group">
					<p>New Password<p>
				    <input name="password" type="password" class="form-control" id="" placeholder="" required />
				  </div>
				  <div class="form-group">
					<p>Confirm Password<p>
				    <input name="confirmpassword" type="password" class="form-control" id="" placeholder="" required />
				  </div>

				  <div style="">
				    <input name="resetcode" type="hidden" class="form-control" value="<?php echo $this->uri->segment(2); ?>" />
				  	  <button type="submit" class="btn btn-default">RESET</button>
				  </div>
			</form>
          	
			
		</div>
	</div>
