<section id="" >
	<div class="container container-fixed-lg p-t-50 p-b-100  " >
	<form role="form" class="m-t-15" method="post" action="">
		
		<div class="row">		
			<h1 class="opensans purple" align="center">PROFIL INVESTOR</h1>
			<?php if($this->session->flashdata('message')){ ?>
				<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
			<?php } ?>
		</div>
		
		<div class="row">	
			<div class="col-sm-12">
				<h2 class="opensans purple">Data Personal Investor</h2>
			</div>
		</div>
		
		<div class="row">		
			<div class="col-md-6 col-sm-12">				
					<div class="row">
					  <div class="col-sm-12">
						<div class="form-group form-group-default">
						  <label>Nama Depan*</label>
						  <input type="text" name="lender_first_name"  class="form-control" placeholder="" value="<?php echo set_value('lender_first_name', isset($data->lender_first_name) ? $data->lender_first_name : ''); ?>" required />
						</div>
					  </div>
					</div>
					<div class="row">
					  <div class="col-sm-12">
						<div class="form-group form-group-default">
					  <label>Nama Belakang*</label>
					  <input type="text" name="lender_last_name"  class="form-control" placeholder="" value="<?php echo set_value('lender_last_name', isset($data->lender_last_name) ? $data->lender_last_name : ''); ?>" required />
						</div>
					  </div>
					</div>					
					<div class="form-group form-group-default">
					  <label>Alamat Email*</label>
					  <input type="text" name="lender_email"  class="form-control" placeholder="" readonly required value="<?php echo set_value('lender_email', isset($data->lender_email) ? $data->lender_email : ''); ?>" />
					</div>					
					<div class="form-group form-group-default">
					  <label>Nomor Telepon*</label>
					  <input type="text" name="lender_phone"  class="form-control" placeholder="" value="<?php echo set_value('lender_phone', isset($data->lender_phone) ? $data->lender_phone : ''); ?>" required />
					</div>
					<div class="form-group form-group-default">
					  <label>Jenis Kelamin*</label>
					  <select name="lender_sex" class="form-control" placeholder="" required >
						  <option value="LAKI-LAKI" <?php if($data->lender_sex == "LAKI-LAKI"){ echo "selected";} ?> >Laki-laki</option>
						  <option value="PEREMPUAN" <?php if($data->lender_sex == "PEREMPUAN"){ echo "selected";} ?> >Perempuan</option>
					  </select>
					</div>
					<div class="form-group form-group-default">
					  <label>Warga Negara</label>
					  <input type="text" name="lender_nationality" class="form-control" placeholder="" value="<?php echo set_value('lender_nationality', isset($data->lender_nationality) ? $data->lender_nationality : ''); ?>" />
					</div>
			</div>
			<div class="col-md-6 col-sm-12">
				<div class="row">
                  <div class="col-sm-12">
                    <div class="form-group form-group-default">
                  <label>Nomor Identitas (KTP/SIM/Passport)*</label>
                  <input type="text" name="lender_idcard"  class="form-control" placeholder="" value="<?php echo set_value('lender_idcard', isset($data->lender_idcard) ? $data->lender_idcard : ''); ?>" required />
                    </div>
                  </div>
                </div>
                 <div class="form-group form-group-default">
                  <label>NPWP</label>
                  <input type="text" name="lender_taxcard"  class="form-control" placeholder="" value="<?php echo set_value('lender_taxcard', isset($data->lender_taxcard) ? $data->lender_taxcard : ''); ?>" />
                </div>
				<div class="form-group form-group-default">
					  <label>Tempat Lahir*</label>
					  <input type="text" name="lender_birth_place"  class="form-control" placeholder="" value="<?php echo set_value('lender_birth_place', isset($data->lender_birth_place) ? $data->lender_birth_place : ''); ?>" required />
				</div>
				<div class="form-group form-group-default">
					  <label>Tanggal Lahir*</label>
					  <input type="text" name="lender_birth_date"  class="form-control datepicker" data-date-format="yyyy-mm-dd" placeholder="yyyy-mm-dd" value="<?php echo set_value('lender_birth_date', isset($data->lender_birth_date) ? $data->lender_birth_date : ''); ?>" required />
				</div>
                <div class="form-group form-group-default">
                  <label>Status Perkawinan*</label>
					<select name="lender_martial" class="form-control" required >
						  <option value="SINGLE" <?php if($data->lender_martial == "SINGLE"){ echo "selected";} ?> >Belum Menikah</option>
						  <option value="KAWIN"  <?php if($data->lender_martial == "KAWIN"){ echo "selected";} ?> >Menikah</option>
						  <option value="CERAI"  <?php if($data->lender_martial == "CERAI"){ echo "selected";} ?> >Cerai</option>
					</select>
                </div>
			</div>
	</div>
	
	<br/>
	<div class="row">		
		<div class="col-md-6 col-sm-12">
            <h2 class="opensans purple">Informasi Alamat Investor</h2>
                <div class="form-group form-group-default">
                  <label>Alamat*</label>
                  <textarea name="lender_address" class="form-control" placeholder="" required><?php echo set_value('lender_address', isset($data->lender_address) ? $data->lender_address : ''); ?></textarea>
                </div>
                <div class="form-group form-group-default">
                  <label>Provinsi*</label>
					<select name="lender_province" id="prop"  class="form-control"  onchange="ajaxkota(this.value)" required>
						<?php if($data->lender_province){ ?>
						<option value="<?php echo $data->lender_province; ?>"><?php echo $location['province']; ?></option>
						<option value="">-------------------------------</option>
						<?php }else{ ?>
						<option value="">Pilih Provinsi</option>
						<?php } ?>
						<?php foreach($province AS $prop): ?>
						<option value="<?php echo $prop->lokasi_propinsi; ?>"><?php echo $prop->lokasi_nama; ?></option>
						<?php endforeach; ?>
					</select>
                </div>
                <div class="form-group form-group-default">
                  <label>Kota*</label>
					<select name="lender_city" id="kota"  class="form-control"  onchange="ajaxkec(this.value)" required>
					<?php if($data->lender_city){ ?>
						<option value="<?php echo $data->lender_city; ?>"><?php echo $location['city']; ?></option>
					<?php } ?>
					</select>
                </div>
                <div class="form-group form-group-default">
                  <label>Kecamatan*</label>
					<select name="lender_kecamatan" id="kec"  class="form-control"  onchange="ajaxkel(this.value)" required >
					<?php if($data->lender_kecamatan){ ?>
						<option value="<?php echo $data->lender_kecamatan; ?>"><?php echo $location['kec']; ?></option>
					<?php } ?>
					</select>
                </div>
                <div class="form-group form-group-default">
                  <label>Kelurahan*</label>
					<select name="lender_kelurahan" id="kel"  class="form-control" >
					<?php if($data->lender_kelurahan){ ?>
						<option value="<?php echo $data->lender_kelurahan; ?>"><?php echo $location['kel']; ?></option>
					<?php } ?>
					</select>
                </div>
                <div class="form-group form-group-default">
                  <label>Kode Pos*</label>
                  <input type="text" name="lender_zipcode"  class="form-control" placeholder="" value="<?php echo set_value('lender_zipcode', isset($data->lender_zipcode) ? $data->lender_zipcode : ''); ?>" required />
                </div>
		</div>
		<div class="col-md-6 col-sm-12">
			<h2 class="opensans purple">Informasi Alamat Domisili</h2>
                <div class="form-group form-group-default">
                  <label>Alamat</label>
                  <textarea name="lender_address_domisili" class="form-control" placeholder="" ><?php echo set_value('lender_address_domisili', isset($data->lender_address_domisili) ? $data->lender_address_domisili : ''); ?></textarea>
                </div>
                <div class="form-group form-group-default">
                  <label>Provinsi</label>
				  <select name="lender_province_domisili" id="prop"  class="form-control"  onchange="ajaxkota2(this.value)">
					
						<?php if($data->lender_province_domisili){ ?>
						<option value="<?php echo $data->lender_province_domisili; ?>"><?php echo $location['province_2']; ?></option>
						<option value="">-------------------------------</option>
						<?php }else{ ?>
						<option value="">Pilih Provinsi</option>
						<?php } ?>
						
						<?php foreach($province AS $prop): ?>
						<option value="<?php echo $prop->lokasi_propinsi; ?>"><?php echo $prop->lokasi_nama; ?></option>
						<?php endforeach; ?>
					</select>
				  
                </div>
                <div class="form-group form-group-default">
                  <label>Kota</label>                 
					<select name="lender_city_domisili" id="kota2"  class="form-control"  onchange="ajaxkec2(this.value)">
					<?php if($data->lender_city_domisili){ ?>
						<option value="<?php echo $data->lender_city_domisili; ?>"><?php echo $location['city_2']; ?></option>
					<?php } ?>
					</select>
                </div>
                <div class="form-group form-group-default">
                  <label>Kecamatan</label>
					<select name="lender_kecamatan_domisili" id="kec2"  class="form-control"  onchange="ajaxkel2(this.value)">
					<?php if($data->lender_kecamatan_domisili){ ?>
						<option value="<?php echo $data->lender_kecamatan_domisili; ?>"><?php echo $location['kec_2']; ?></option>
					<?php } ?>
					</select>
                </div>
                <div class="form-group form-group-default">
                  <label>Kelurahan</label>
					<select name="lender_kelurahan_domisili" id="kel2"  class="form-control" >
					<?php if($data->lender_kelurahan_domisili){ ?>
						<option value="<?php echo $data->lender_kelurahan_domisili; ?>"><?php echo $location['kel_2']; ?></option>
					<?php } ?>
					</select>
                </div>
                <div class="form-group form-group-default">
                  <label>Kode Pos</label>
                  <input type="text" name="lender_zipcode_domisili"  class="form-control" placeholder=""  value="<?php echo set_value('lender_zipcode_domisili', isset($data->lender_zipcode_domisili) ? $data->lender_zipcode_domisili : ''); ?>"  />
                </div>
		</div>
	</div>
	<br/>
	<div class="row">		
		<div class="col-md-6 col-sm-12">
            <h2 class="opensans purple">Data Pekerjaan dan Penghasilan</h2>
                 <div class="form-group form-group-default">
                  <label>Pekerjaan*</label>
                  <select name="lender_job_name" class="form-control" placeholder="" required >
                      <option value="Karyawan Swasta" 		<?php if($data->lender_job_name == "Karyawan Swasta"){ echo "selected";} ?> 	>Karyawan Swasta</option>
                      <option value="Pegawai Negeri Sipil"	<?php if($data->lender_job_name == "Pegawai Negeri Sipil"){ echo "selected";}?> >Pegawai Negeri Sipil</option>
                      <option value="Pengusaha"				<?php if($data->lender_job_name == "Pengusaha"){ echo "selected";} ?> 			>Pengusaha</option>
                      <option value="TNI / Polisi"			<?php if($data->lender_job_name == "TNI / Polisi"){ echo "selected";} ?> 		>TNI / Polisi</option>
                      <option value="Karyawan BUMN"			<?php if($data->lender_job_name == "Karyawan BUMN"){ echo "selected";} ?> 		>Karyawan BUMN</option>
                      <option value="Pensiun"				<?php if($data->lender_job_name == "Pensiun"){ echo "selected";} ?> 			>Pensiun</option>
                      <option value="Ibu Rumah Tangga"		<?php if($data->lender_job_name == "Ibu Rumah Tangga"){ echo "selected";} ?> 	>Ibu Rumah Tangga</option>
                      <option value="Pelajar"				<?php if($data->lender_job_name == "Pelajar"){ echo "selected";} ?> 			>Pelajar</option>
                      <option value="Profesional"			<?php if($data->lender_job_name == "Profesional"){ echo "selected";} ?> 		>Profesional</option>
                      <option value="Lainnya"				<?php if($data->lender_job_name == "Lainnya"){ echo "selected";} ?>				>Lainnya</option>
                  </select>
                </div>
                 <div class="form-group form-group-default">
                  <label>Nama Perusahaan*</label>
                  <input type="text" name="lender_job_company"  class="form-control" placeholder=""  value="<?php echo set_value('lender_job_company', isset($data->lender_job_company) ? $data->lender_job_company : ''); ?>"  />
                  </select>
                </div>
               <div class="form-group form-group-default">
                  <label>Alamat Perusahaan</label>
                  <input type="text" name="lender_job_company_address"  class="form-control" placeholder=""  value="<?php echo set_value('lender_job_company_address', isset($data->lender_job_company_address) ? $data->lender_job_company_address : ''); ?>" />
                </div>
		</div>
		<div class="col-md-6 col-sm-12">
			<h2 class="opensans purple">&nbsp;</h2>
                 <div class="form-group form-group-default">
                  <label>Total Pendapatan (IDR/bulan)*</label>
                  <select name="lender_income" class="form-control" placeholder="" required >
                      <option value="5000000"	<?php if($data->lender_income == "5000000"){ echo "selected";} ?> >Dibawah 5 Juta - 5 Juta</option>
                      <option value="15000000"	<?php if($data->lender_income == "15000000"){ echo "selected";} ?> >Diatas 5 Juta - 15 Juta</option>
                      <option value="30000000"	<?php if($data->lender_income == "30000000"){ echo "selected";} ?> >Diatas 15 juta - 30 Juta</option>
                      <option value="50000000"	<?php if($data->lender_income == "50000000"){ echo "selected";} ?> >Diatas 30 Juta - 50 Juta</option>
                      <option value="100000000"	<?php if($data->lender_income == "100000000"){ echo "selected";} ?> >Diatas 50 Juta - 100 Juta</option>
                      <option value="100000001"	<?php if($data->lender_income == "100000001"){ echo "selected";} ?> >Diatas 100 Juta</option>
                  </select>
                </div>
                <div class="form-group form-group-default">
                  <label>Sumber Dana*</label>
                  <select name="lender_income_source_fund" class="form-control" placeholder="" required >
                      <option value="salary"	<?php if($data->lender_income_source_fund == "salary"){ echo "selected";} ?> >Penghasilan Rutin / Gaji</option>
                      <option value="saving"	<?php if($data->lender_income_source_fund == "saving"){ echo "selected";} ?>>Tabungan / Hasil Investasi</option>
                      <option value="bisnis"	<?php if($data->lender_income_source_fund == "bisnis"){ echo "selected";} ?> >Usaha / Bisnis</option>
                  </select>
                </div>
                  <div class="form-group form-group-default">
                  <label>Negara Asal Dana*</label>
                  <input type="text" name="lender_income_source_country"  class="form-control" placeholder=""  value="<?php echo set_value('lender_income_source_country', isset($data->lender_income_source_country) ? $data->lender_income_source_country : ''); ?>" required />
                </div>
		</div>
	</div>
	<br/>
	<div class="row">		
		<div class="col-md-6 col-sm-12">
            <h2 class="opensans purple">Referensi Bank</h2>
                 <div class="form-group form-group-default">
                  <label>Nomor Rekening*</label>
                  <input type="text" name="lender_bank_account"  class="form-control" placeholder=""  value="<?php echo set_value('lender_bank_account', isset($data->lender_bank_account) ? $data->lender_bank_account : ''); ?>" required />
                </div>
                <div class="form-group form-group-default">
                  <label>Pemilik Rekening*</label>
                  <input type="text" name="lender_bank_holder"  class="form-control" placeholder="" value="<?php echo set_value('lender_bank_holder', isset($data->lender_bank_holder) ? $data->lender_bank_holder : ''); ?>" required />
                </div>
		</div>
		<div class="col-md-6 col-sm-12">
			 <h2 class="opensans purple">&nbsp;</h2>
                 <div class="form-group form-group-default">
                  <label>Nama Bank*</label>
                  <input type="text" name="lender_bank_name"  class="form-control" placeholder="" value="<?php echo set_value('lender_bank_name', isset($data->lender_bank_name) ? $data->lender_bank_name : ''); ?>" required />
                </div>
                <div class="form-group form-group-default">
                  <label>Cabang Bank*</label>
                  <input type="text" name="lender_bank_branch"  class="form-control" placeholder="" value="<?php echo set_value('lender_bank_branch', isset($data->lender_bank_branch) ? $data->lender_bank_branch : ''); ?>" required />
                </div>
		</div>
	</div>	
	<br/>
	<div class="row">		
		<div class="col-md-12 col-sm-12 text-center">
			<input type="hidden" name="lender_id"  value="<?php echo set_value('lender_id', isset($data->lender_id) ? $data->lender_id : ''); ?>" />
            <button class="btn btn-primary btn-purple">Save Profile</button>
            
		</div>
	</div>
    
	</form>
    </div>
</section>