<div class="container">
    <div class="row">		
        <div class="col-lg-8 col-md-8 col-xs-12">
            <form id="f_user" role="form" action="" method="post">
                <?php if($this->session->flashdata('message')){ ?>
                    <div class="alert alert-success alert-dismissable"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
                <?php } ?>
			  <div class="form-group">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-xs-12"><label for="">First Name</label><input type="text" name="register_first_name" class="form-control" id="" placeholder="" required /></div>
					<div class="col-lg-6 col-md-6 col-xs-12"><label for="">Last Name</label><input type="text" name="register_last_name" class="form-control" id="" placeholder=""></div>
				</div>
			  </div>
			  <div class="form-group">
				<label for="">Email</label>
				<input type="email" name="register_email" class="form-control" id="" placeholder="" required />
			  </div>
			  <div class="form-group">
				<label for="">Password</label>
				<input type="password" name="register_passwd" class="form-control" id="pass" placeholder="" required />
			  </div>
			  <div class="form-group">
				<label for="">Confirm Password</label>
				<input type="password" name="register_passwd_confirm" class="form-control" id="repass" placeholder="" />
			  </div>
			  <div class="form-group">
				<input type="checkbox" name="register_terms" value="1" required /> Terms of Use
			  </div>
			  <div class="form-group">
				<input type="checkbox" name="register_policy" value="1" required /> Privacy Policy
			  </div>			  
			  
			  <div class="form-group">
				<button type="submit" onclick="beforeSubmitUser()" class="btn btn-success  btn-sm ">REGISTER</button>
			  </div>
			  
			  
			</form>
        </div>
        
		<div class="col-lg-4 col-md-4 col-xs-12">
				
		</div>
   </div>
</div> 