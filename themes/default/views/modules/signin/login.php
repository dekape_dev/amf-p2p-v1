<div class="container">
    <div class="row">		
        <div class="col-lg-8 col-md-8 col-xs-12">
            <form  role="form" action="VerifyLogin" method="post">
                <?php if($this->session->flashdata('message')){ ?>
                    <div class="alert alert-success alert-dismissable"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
                <?php } ?>
			 
			  <div class="form-group">
				<label for="">Email</label>
				<input type="email" name="login_email" class="form-control" id="" placeholder="" required />
			  </div>
			  <div class="form-group">
				<label for="">Password</label>
				<input type="password" name="login_passwd" class="form-control" id="pass" placeholder="" required />
			  </div>
				<div class="form-group">
				<button type="submit" class="btn btn-success  btn-sm ">LOGIN</button>
			  </div>
			  
			  
			</form>
        </div>
        
		<div class="col-lg-4 col-md-4 col-xs-12">
				
		</div>
   </div>
</div> 