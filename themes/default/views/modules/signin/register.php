	<div class="headerSimple">
		<img src="<?php echo $this->template->get_theme_path(); ?>/img/logo_only_white.png">
	</div>

	<div class="container">
		<div class="row formRegister">
			<form method="post" action="">
				  <div class="form-group">
				    <input name="register_first_name" type="text" class="form-control" id="" placeholder="FirstName">
				  </div>

				  <div class="form-group">
				    <input name="register_last_name" type="text" class="form-control" id="" placeholder="LastName">
				  </div>

				  <div class="form-group">
				    <input name="register_email" type="email" class="form-control" id="" placeholder="Email">
				  </div>

				  <div class="form-group">
				    <input name="register_passwd" type="password" class="form-control" id="" placeholder="Password">
				  </div>

				  <div class="form-group">
				    <input name="register_passwd_confirm" type="password" class="form-control" id="" placeholder="Confirm Password">
				  </div>
				  
				  <div class="form-group">
				    <select name="register_type" class="">
						<option value="Investor">Investor</option>
						<option value="Borrower">Borrower</option>
					</select>
					<hr/>
				  </div>

				  <h6>By continuing, I agree to the terms of use and </br>privacy policy</h5>
				  <button type="submit" class="btn btn-default">NEXT</button>
			</form>
		</div>
	</div>
