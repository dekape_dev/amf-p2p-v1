<div id="headerImageAbout">
		<div class="headerNav">
			<div class="logo">
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $this->template->get_theme_path(); ?>/img/logo2.png" height="100px"></a>
			</div>
			<div class="menu_mobile">
				<div>
					<button id="mobile_toggle" >
						<img src="<?php echo $this->template->get_theme_path(); ?>/img/menu-mobile.png" height="100px">
						<h2>MENU</h2>
					</button>
				</div>
			</div>

			<div class="row navi shadowNavi">
        <div class="col-md-1 "></div>

        <?php if ($this->session->userdata('site_lang') == 'english') { ?>
        <div class="col-md-2">
          <a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">ABOUT</a>
        </div>
        <?php } else {?>
        <div class="col-md-2">
          <a href="<?php echo site_url('about'); ?>" class="<?php echo $menu_about; ?>">TENTANG KAMI</a>
        </div>
        <?php }?>


        <?php if ($this->session->userdata('site_lang') == 'english') { ?>
        <div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
        </div>
        <?php } else {?>
        <div class="col-md-1 res"><a href="<?php echo site_url('impact'); ?>" class="<?php echo $menu_impact; ?>">IMPACT</a>
        </div>
        <?php }?>


        <div class="col-md-2"></div>
        <div class="col-md-1"></div>
        <div class="col-md-1"></div>

        <?php if ($this->session->userdata('site_lang') == 'english') { ?>
        <div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">BORROW</a>
        </div>
        <?php } else {?>
        <div class="col-md-1 res"><a href="<?php echo site_url('borrow/under20mil'); ?>" class="<?php echo $menu_borrow; ?>">PEMINJAM</a>
        </div>
        <?php }?>

        <?php if ($this->session->userdata('site_lang') == 'english') { ?>
        <div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTOR</a>
        </div>
        <?php } else {?>
        <div class="col-md-1 res"><a href="<?php echo site_url('investor'); ?>" class="<?php echo $menu_invest; ?>">INVESTOR</a>
        </div>
        <?php }?>

        <?php if ($this->session->userdata('site_lang') == 'english') { ?>
        <div class="col-md-1 res">
          <?php
            if($this->session->userdata('logged_in_user')) {
              echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
            }else{
          ?>
          <a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> LOGIN</a>
          <div class="loginPop">
            <form method="post" action="letmein">
              <div class="form-group">
                <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
              </div>
              <div class="form-group">
                <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
              </div>
              <h5>Not Yet Register? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">SIGN UP HERE</a></h5>
              <button class="btn btn-login">Login</button>
            </form>
          </div>
          <?php } ?>
        </div>
        <?php } else {?>
        <div class="col-md-1 res">
          <?php
            if($this->session->userdata('logged_in_user')) {
              echo '<a href="'.site_url('dashboard').'" class="">DASHBOARD</a>';
            }else{
          ?>
          <a id="loginBox" href="#"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span> MASUK</a>
          <div class="loginPop">
            <form method="post" action="letmein">
              <div class="form-group">
                <input name="login_email" type="email" class="form-control" id="" placeholder="Email">
              </div>
              <div class="form-group">
                <input name="login_passwd" type="password" class="form-control" id="" placeholder="Password">
              </div>
              <h5>Belum Daftar? </br><a href="<?php echo site_url('register'); ?>" style="color:purple;">DAFTAR DISINI</a></h5>
              <button class="btn btn-login">MASUK</button>
            </form>
          </div>
          <?php } ?>
        </div>
        <?php }?>
      </div>
		</div>
	</div>
	<div class="container text-justify">
				<h2 class="opensans text-center purple"><b>SYARAT DAN KETENTUAN</b></h2>
				<h2 class="opensans text-center purple"><b>AMARTHA</b></h2>
				<p class="opensans" style="padding-bottom:20px;">Halaman ini berisi tentang ketentuan dan prasyarat dari penggunaan situs dan layanan kami oleh anda (“Ketentuan  Penggunaan”). Ketentuan Penggunaan ini wajib anda baca secara hati-hati sebelum menggunakan situs dan layanan kami. Apabila anda tidak menyetujui Ketentuan Penggunaan ini, harap untuk tidak menggunakan Situs Web dan Layanan kami.</p>
				<h3 class="opensans purple"><b>I.	Tentang Kami</b></h3>
				<p class="opensans" style="padding-bottom:20px;">Amartha didirikan didirikan oleh PT Amartha Mikro Fintek, sebuah perusahaan yang didirikan berdasarkan hukum yang berlaku di Indonesia dan beralamat di Bukit Indraprasta D3/No.1 Telaga Kahuripan, Kabupaten Bogor 16330.</p>
				
				<p class="opensans" style="padding-bottom:20px;">Ketentuan Penggunaan yang tercantum dalam halaman ini dilindungi oleh hak cipta. Segala penggunaan sebagian atau seluruh Ketentuan Penggunaan ini oleh pihak ketiga tidak diperbolehkan, kecuali seizin kami dan diperbolehkan berdasarkan peraturan perundang-undangan yang berlaku di Indonesia. </p>

				<h3 class="opensans purple"><b>II.	Definisi</b></h3>

				<p class="opensans" style="padding-bottom:20px;">a.	“Akun” adalah identitas unik yang digunakan untuk mengklasifikasikan Anda dan mencatat kegiatan Anda dalam Situs Web dan Layanan kami yang dilakukan dengan cara pendaftaran pada Situs Web Kami; </p>
			
				<p class="opensans" style="padding-bottom:20px;">b.	“Anda” dengan variasi tata bahasa dan ungkapan yang serumpun, merujuk pada Peminjam, Pemberi Pinjaman atau orang lain yang menggunakan Situs Web dan Layanan terlepas dari apakah orang tersebut terdaftar dalam Situs Web atau tidak;</p>
				
				<p class="opensans" style="padding-bottom:20px;">c.	“Data Pribadi” berarti informasi atau data yang dapat digunakan untuk mengidentifikasi secara langsung atau tidak langsung seorang individu yang merupakan orang perseorangan.</p>

				<p class="opensans" style="padding-bottom:20px;">d.	“Kebijakan Privasi” mengacu pada Kebijakan Privasi di Situs Web ini.</p>

				<p class="opensans" style="padding-bottom:20px;">e.	“Kami” dengan variasi tata bahasa dan ungkapan yang serumpun, merujuk pada Amartha beserta kuasa yang sah dari Amartha. </p>

				<p class="opensans" style="padding-bottom:20px;">f.	“Konten“ memiliki arti semua konten pada Situs Web dan/atau Layanan kami, secara keseluruhan atau setiap bagian, termasuk tetapi tidak terbatas pada desain, teks, gambar grafis, foto, gambar, citra, video, perangkat lunak, musik, suara dan file lain, peringkat kredit, tarif, biaya, kuotasi, data historis, grafik, statistik, artikel, informasi kontak kami, setiap informasi lain, dan pemilihan dan pengaturannya</p>

				<p class="opensans" style="padding-bottom:20px;">g.	“Keadaan Memaksa” adalah sebagaimana dimaksud dalam Pasal [__] Ketentuan Penggunaan ini</p>

				<p class="opensans" style="padding-bottom:20px;">h.	“Layanan” memiliki arti yang sama sebagaimana ditetapkan dalam Pasal [__] dibawah.</p>

				<p class="opensans" style="padding-bottom:20px;">i.	“Pemberi Pinjaman” adalah orang perorangan atau badan hukum yang berniat untuk menjadi investor dan memberikan pinjaman kepada Peminjam melalui Layanan kami’</p>

				<p class="opensans" style="padding-bottom:20px;">j.	“Peminjam” adalah orang perorangan atau badan hukum yang berniat untuk mencari pinjaman sejumlah uang melalui Layanan kami.</p>

				<p class="opensans" style="padding-bottom:20px;">k.	“Perjanjian Kredit” merupakan Perjanjian Kredit antara Kami, Peminjam dan Pemberi Pinjaman dimana Peminjam sepakat untuk meminjam dan Pemberi Pinjaman sepakat untuk member pinjaman dan Kami berperan sebagai perantara;</p>

				<p class="opensans" style="padding-bottom:20px;">l.	“Perjanjian” merupakan keseluruhan dokumen yang terdiri dari:</p>
				<ol style="list-style-type: upper-roman;">
					<li><p>Ketentuan Penggunaan;</p></li>
					<li><p>Kebijakan Privasi;</p></li>
					<li><p>Perjanjian Kredit</p></li>
					<li><p>Perjanjian lainnya antara Kami dengan Anda, apabila ada.</p></li>
					<li><p>Ketentuan Penggunaan;</p></li>
				</ol>
<!-- 				<h3>(i)	Ketentuan Penggunaan;</h3>
<h3>(ii)	Kebijakan Privasi;</h3>
(iii)	Perjanjian Kredit;
(iv)	Perjanjian lainnya antara Kami dengan Anda, apabila ada.
 -->

				<p class="opensans" style="padding-bottom:20px;">m. “Situs Web” mengacu pada website Amartha [_], termasuk semua domain dan sub-domainnya</p>

				<p class="opensans" style="padding-bottom:0px;"></p>

				<p class="opensans" style="padding-bottom:20px;">Untuk keperluan Ketentuan Penggunaan ini, Anda dan Kami secara bersama-sama disebut dengan “Para Pihak” dan secara masing-masing disebut dengan “Pihak”. </p>

				<h3 class="opensans purple"><b>III.	Perubahan dari Waktu ke Waktu</b></h3>



				<p class="opensans" style="padding-bottom:20px;">Kami dapat melakukan perubahan Ketentuan Penggunaan dari waktu ke waktu. Apabila Kami melakukan perubahan Ketentuan Penggunaan, Kami akan menempatkannya pada Situs Web Kami dan menandai dalam halaman ini mengenai tanggal perubahan terkahir dari Ketentuan Penggunaan. Anda dengan ini setuju bahwa penggunaan kelanjutan dari Anda terhadap Situs Web dan Layanan dan Layanan Tambahan kami setelah perubahan Ketentuan Penggunaan oleh kami merupakan tanda bahwa Anda menyetujui Ketentuan Penggunaan yang baru tersebut. </p>

				<h3 class="opensans purple"><b>IV.	Persyaratan Umur</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Untuk dapat menggunakan Layanan dan Situs Web Kami, Anda harus lebih dari 21 tahun. Apabila Anda berada berada di bawah 21 tahun, Anda dapat menggunakan Situs Web, Layanan Tambahan dan Layanan kami hanya dengan keterlibatan orang tua atau wali.</p>

				<h3 class="opensans purple"><b>V.	Layanan Kami</b></h3>
				<ol>
					<li style="padding-top:20px;"><p>Dalam Situs Web, Kami menyediakan Layanan berupa:</p>
						<ol class="listPad" Style="list-style-type: lower-alpha;">
							<li>Menyediakan wadah bagi calon Peminjam untuk mencari pinjaman dan calon Pemberi Pinjaman untuk berinvestasi dengan memberikan pinjaman;</li>

							<li>Menyediakan ruang eksklusif dan mempertemukan antara calon Peminjam dan Pemberi Pinjaman dalam Situs Web agar kerjasama pinjam meminjam dapat terlaksana;</li>

							<li>Menyeleksi, menganalisa dan menyetujui aplikasi pinjaman yang diajukan oleh calon Peminjam agar investor dapat memperoleh investasi yang berkualitas.</li>

							<li>Menyediakan rekening penampungan yang berisi dana pinjaman dari Pemberi Pinjaman kepada Peminjam untuk menjembatani hubungan antara Pemberi Pinjaman dengan Peminjam <b> [Ketentuan mengenai service fee dan rekening penampungan akan diatur dalam Perjanjian Kredit]</b></li>

						</ol>
					</li>

					<li style="padding-top:20px;"><p>Dalam menyediakan Layanan, Kami menegaskan sebagai berikut:</p>
						<ol class="listPad" style="list-style-type: lower-alpha;">
							<li>Kami tidak menjamin bahwa Peminjam akan mengembalikan pinjaman kepada Pemberi Pinjaman sesuai dengan ketentuan dalam Perjanjian Kredit</li>

							<li>Kami tidak diatur dan/atau dalam pengawasan Otoritas Jasa Keuangan (“<b>OJK</b>”) di Indonesia;</li>

							<li>Kami hanya bertindak sebagai perantara antara Peminjam dan Pemberi Pinajaman, dan dalam memberikan Layanan kami tidak dijamin oleh otoritas terkait, termasuk namun tidak terbatas kepada Lembaga Penjamin Simpanan (LPS) OJK.</li>

							<li>Kami tidak memberikan segala bentuk saran investasi atau rekomendasi investasi terkait pilihan-pilihan dalam Situs Web dan Layanan ini;</li>

							<li>Isi, informasi dan materi yang kami berikan dalam Situs Web dan Layanan ini hanya dimaksudkan untuk memberikan informasi dan tidak dianggap sebagai sebuah penawaran, permohonan, undangan, saran atau rekomendasi untuk membeli atau menjual investasi, sekuritas atau produk pasar modal atau jasa keuangan lainnya;</li>

							<li>Kami hanya memberikan Layanan berupa fungsi administratif saja dan tidak dapat dianggap sebagai namun tidak terbatas pada konsultan investasi atau manajer investasi dan Kami tidak dan tidak akan memberikan saran, memberi kewajiban atau kewajiban lainnya untuk Layanan selain dari yang ditegaskan di Ketentuan Penggunaan ini, Kebijakan Privasi dan Perjanjian Kredit;</li>

							<li>Dana yang ditempatkan di rekening Kami tidak akan dianggap sebagai simpanan yang diselenggarakan oleh perusahaan seperti yang diatur dalam peraturan perundang-undangan tentang perbankan di Indonesia.</li>

							<li>Baik perusahaan Kami atau setiap Direktur, pegawai, karyawan, wakil, afiliasi-afiliasi atau agen-agennya tidak memiliki tanggung jawab terkait setiap gangguan atau masalah yang terjadi atau dianggap terjadi, yang disebabkan atau karena kekurangan dari persiapan atau publikasi dari materi dan informasi yang tercantum dalam Situs Web Kami.</li>

							<li>Semua informasi dan data yang didapatkan dari Situs Web Kami akan disimpan oleh Kami sesuai dengan ketentuan Undang-Undang Nomor 11 Tahun 2008 tentang Informasi dan Transaksi Elektronik beserta peraturan turunannya.</li>
						</ol>
					</li>
				</ol>

				<h3 class="opensans purple"><b>VI.	Pernyataan dan Jaminan dari Anda</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Anda dengan tanpa syarat dan tidak dapat ditarik kembali menyatakan dan menjamin bahwa:</p>
				<ol class="listPad">
					<li>Anda telah membaca dan menyetujui Ketentuan Penggunaan ini dan Kebijakan Privasi Kami;</li>

					<li>Anda memiliki hak hukum penuh, kapasitas dan kewenangan secara hukum untuk mengakses Situs Web dan menggunakan Layanan Kami;</li>

					<li>Anda memiliki itikad baik dalam menggunakan Situs Web dan Layanan kami dan akan menggunakan Situs Web dan Layanan Kami untuk tujuan yang sah dan tidak bertentangan dengan peraturan perundang-undangan;</li>

					<li>Penggunaan Situs Web dan Layanan Kami oleh Anda tidak melanggar kewajiban-kewajiban Anda yang telah ada dan yang akan ada, termasuk namun tidak terbatas pada kewajiban yang terdapat pada perjanjian Anda dengan pihak ketiga lainnya;</li>

					<li>Semua informasi dan data-data yang Anda berikan atau akan berikan kepada Kami adalah akurat dan lengkap dan tidak <i>misleading;</i></li>

					<li>Tidak ada materi atau informasi atau data apapun yang disampaikan melalui akun Anda atau yang diposting atau dibagikan oleh Anda melalui Situs Web, Layanan kami akan melanggar atau menyalahi hak-hak dari pihak ketiga manapun, termasuk hak privasi, hak cipta, merek dagang, publisitas atau hak-hak kepemilikan atau pribadi lainnya; atau mengandung fitnah, pencemaran nama baik atau materi yang melanggar hukum;</li>

					<li>Setiap pernyataan dan jaminan tersebut di atas dibuat dengan sebenar-benarnya, tanpa menyembunyikan fakta dan hal material apapun, dan dengan demikian Anda akan bertanggung jawab sepenuhnya atas kebenaran dari hal-hal yang telah dinyatakan di atas, demikian pula akan bersedia bertanggung jawab baik secara perdata maupun pidana, apabila pernyataan dan jaminan ini tidak sesuai dengan keadaan sebenarnya.</li>
				</ol>

				<h3 class="opensans purple"><b>VII.	Kewajiban Anda Dalam Penggunaan Situs Web dan Layanan</b></h3>

				<ol>
					<li style="padding-top:20px;"><p>Dalam menggukan Situs Web dan Layanan, Anda dengan ini setuju untuk:</p>
						<ol class="listPad" class="listPad" style="list-style-type: lower-alpha;">
							<li>Menggunakan Situs Web dan Layanan sesuai dengan Ketentuan Penggunaan, Kebijakan Privasi, Perjanjian Kredit dan peraturan perundang-undangan yang berlaku;</li>
							<li>Tidak melakukan penipuan yang menyebabkan pihak yang menyediakan pinjaman kepada Anda atau pihak yang meminjam kepada Anda atau Kami dalam Perjanjian Kredit mengalami kerugian;</li>

							<li>Tidak menggunakan informasi dan data yang anda terima terkait dengan Layanan untuk tujuan selain dari yang ditentukan dalam Ketentuan Penggunaan, Kebijakan Privasi dan perjanjian penyediaan kredit Anda.</li>

							<li>tidak memposting materi dan data apapun yang mengandung virus, Trojan horse, worm, time-bomb, keystroke logger, spyware, adware atau kode komputer berbahaya lain atau yang serupa, file atau program yang dirancang untuk menginterupsi, mempengaruhi, merusak atau membatasi fungsionalitas setiap perangkat lunak atau perangkat keras komputer atau peralatan telekomunikasi;</li>

							<li>tidak mengakses tanpa wewenang, meretas (hack), menghalangi, mengganggu, menonaktifkan, membebani dengan berlebihan atau mengganggu kerja atau tampilan yang layak dari Situs Web atau Layanan, yang termasuk tetapi tidak terbatas pada serangan penolakan layanan (denial-of-service), serangan spoof, sesi peretasan, gangguan rekayasa terbalik, pemrograman ulang, atau pemanfaatan setiap teknik framing untuk melampirkan setiap konten atau informasi kepemilikan lainnya;</li>

							<li>Tidak memposting informasi pribadi apapun dari pihak ketiga manapun pada Situs Web, termasuk tetapi tidak terbatas pada, alamat, nomor telepon, alamat email, nomor kartu identitas atau jaminan sosial dan nomor kartu kredit;</li>

							<li>Tidak menggunakan konten atau informasi yang apabila diposting, dibagikan, digunakan atau diunggah melaui Situs Web dan Layanan oleh Anda akan menyebabkan dilanggarnya hak pihak ketiga lainnya;</li>

							<li>Tidak memposting, atau memastikan pengiriman iklan, permintaan, materi promosi, “junk mail”, “spam”, “chain letters”. “pyramid schemes” yang tidak diminta atau tidak sah atau bentuk permintaan apapun lainnya;</li>

							<li>tidak menggunakan script otomatis, terlibat dalam praktek “screen scraping”, “database scraping” atau aktivitas lain dengan tujuan untuk mendapatkan persetujuan atau informasi lain dari Situs Web, Konten dan Layanan atau untuk tujuan lain tanpa seizin Kami;</li>

							<li>Tidak berpura-pura memiliki identitas yang bukan merupakan identitas diri Anda sebenarnya atau merepresentasikan diri Anda tidak sesuai dengan kondisi dan identitas Anda sebenarnya; </li>

							<li>Menggunakan koneksi internet dan perangkat-perangkat yang aman dan terpercaya sehingga keamanan dan kelancaran Situs Web dan Layanan tidak terganggu;</li>

							<li>Tetap mendaftar dan menggunakan 1 (satu) Akun, kecuali dengan seizin Kami terlebih dahulu;</li>

							<li>Tidak mengelakkan atau mencoba mengelakkan, cara pengamanan apapun dari Situs Web; </li>

							<li>Menjaga keamanan akun Anda, termasuk kata sandi unik, password dan keterangan unik lainnya yang identik dengan Akun anda dan tetap bertanggung jawab atas penggunaan akun oleh orang lain selain dari Anda;</li>

							<li>Mengizinkan pihak ketiga lainnya untuk melakukan hal yang dilarang dalam Ketentuan Penggunaan, Kebijakan Privasi dan Perjanjian Kredit.</li>
						</ol>
					</li>
					<li  style="padding-top:20px;"><p>Kami memiliki hak untuk menonaktifkan Ajyb Anda setiap saat, jika menurut Kami Anda telah gagal untuk mematuhi mana saja ketetapan dari persyaratan ini.</p></li>
				</ol>

				<h3 class="opensans purple"><b>VIII.	Hubungan Kami dengan Anda</b></h3>
				<ol class="listPad">
					<li>Dalam kaitannya dengan Layanan, Kami hanyalah sebatas Pihak yang menyediakan Layanan, perantara dan Pihak yang mempertemukan Anda dengan pihak lainnya untuk melakukan pinjam meminjam uang. Kami tidak bertanggung jawab atas kerugian yang timbul terkait dengan perikatan Anda dengan pihak Peminjam atau Pemberi Pinjaman atau pihak ketiga lainnya.</li>

					<li>Tidak ada Ketentuan dalam Perjanjian ini akan membuat atau dianggap menciptakan hubungan kemitraan, agen atau hubungan pemberi kerja dan pekerja antara Anda dan Kami. </li>
				</ol>

				<h3 class="opensans purple"><b>IX.	Hak Atas Kekayaan Intelektual</b></h3>

				<ol class="listPad">
					<li>Hak cipta, paten, merek dagang, desain terdaftar dan semua hak atas kekayaan intelektual pada Situs Web, Layanan dan Konten, termasuk namun tidak terbatas pada hak cipta dalam gabungan dari semua Konten adalah milik dan tetap merupakan hak Kami.</li>

					<li>Amartha, gambar grafis, logo, desain, header halaman, ikon, tulisan dan nama layanan milik Amartha (secara kolektif disebut “Merek”) yang ditampilkan pada Situs Web ini adalah aset dari Amartha dan semua Merek yang secara tegas dilindungi oleh Amartha atau pihak ketiga yang relevan. Akses Anda pada dan/atau penggunaan Situs Web, Konten dan Layanan tidak dapat dianggap sebagai pemberian. Setiap lisensi atau hak untuk menggunakan merek dagang apapun yang ada pada Situs Web tanpa persetujuan tertulis terlebih dahulu dari Kami atau para pihak ketiga yang relevan.</li>

					<li>Anda tidak diizinkan untuk menggunakan Merek apapun tanpa persetujuan terlebih dahulu dari Kami atau pihak ketiga tersebut. Kami secara agresif menggunakan hak kekayaan intelektual kami sepanjang diizinkan oleh hukum sepenuhnya. Nama Amartha dan Merek lain tidak dapat digunakan dengan cara apapun, termasuk dalam setiap iklan atau publisitas, atau sebagai hyperlink tanpa izin tertulis terlebih dahulu dari Kami.</li>

					<li>Nama domain di mana Situs Web berada adalah milik mutlak Kami dan Anda tidak dapat menggunakan atau mengadopsi nama serupa untuk penggunaan Anda sendiri.</li>

					<li>Dengan ketentuan bahwa Anda berhak menggunakan Situs Web, Anda diberikan lisensi terbatas untuk mengakses dan menggunakan Situs Web dan untuk mengunduh atau mencetak salinan dari bagian manapun dari Konten semata-mata untuk penggunaan Anda pribadi sehubungan dengan penggunaan Anda atas Situs Web, Konten atau Layanan, dengan ketentuan bahwa Anda memelihara semua hak cipta atau pemberitahuan kepemilikan lengkap lainnya. Status kami (dan bahwa dari setiap kontributor yang diidentifikasi) sebagai penulis konten pada Situs Web kami harus selalu diakui. Namun, Anda tidak harus mengubah salinan kertas atau digital dari setiap materi yang Anda telah cetak atau unduh dengan cara apapun, dan Anda tidak harus menggunakan setiap ilustrasi, foto, video atau audio secara berurutan atau gambar grafis secara terpisah dari teks apapun yang menyertainya.</li>

					<li>Lisensi ini dapat kami batalkan, setiap saat tanpa pemberitahun atau alasan. Setiap penggunaan Situs Web atau Konten tanpa izin tertulis terlebih dahulu dari Kami, selain yang secara khusus diberikan wewenang dalam Ketentuan Penggunaan ini, sangat dilarang dan lisensi yang diberikan dalam Ketentuan Penggunaan ini harus diakhiri. Penggunaan tidak sah tersebut juga dapat melanggar hukum yang berlaku  termasuk tetapi tidak terbatas, undang-undang hak cipta dan merek, dan dapat mengakibatkan diajukannya tuntutan hukum terhadap Anda.</li>

					<li>Jika Anda menggunakan Merek, Situs Web ini, Konten dan Layanan dengan melanggar Ketentuan Penggunaan ini, hak Anda untuk menggunakan Merek kami, Situs Web ini, Konten dan Layanan akan diberhentikan segera dan Anda harus, menurut kebijaksanaan kami, mengembalikan atau memusnahkan setiap salinan materi yang Anda telah buat.</li>
				</ol>

				<h3 class="opensans purple"><b>X.	Penggunaan Data Pribadi</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Anda diwajibkan untuk membaca Kebijakan Privasi kami dalam menggunakan Situs Web dan Layanan kami yang menjelaskan penggunaan data dan kebijakan penerapan privasi kami secara detil. Harap Anda tidak mengirimkan setiap Data Pribadi atau data tanpa membaca terlebih dahulu Kebijakan Privasi Kami.  Kebijakan Privasi kami merupakan satu kesatuan yang tidak terpisahkan dengan Ketentuan Penggunaan ini. </p>

				<h3 class="opensans purple"><b>XI.	Akses ke Situs Web Kami</b></h3>

				<ol class="listPad">
					<li>Situs Web kami dapat diakses dengan tanpa biaya apapun. Kami tidak menjamin Situs Web dan Layanan kami dapat selalu tersedia dan tidak terganggu. Kami tidak bertanggungjawab apabila Situs Web dan Layanan Kami menjadi tidak dapat tersedia pada waktu dan periode tertentu.</li>

					<li>Anda bertanggungjawab untuk melakukan pengurusan-pengurusan yang diperlukan bagi Anda untuk dapat mengakses Situs Web dan Layanan Kami. Anda juga bertanggungjawab untuk memastikan setiap orang yang mengakses Situs Web dan Layanan Kami melalui koneksi internet Anda mengetahui Ketentuan Penggunaan ini, Kebijakan Privasi dan ketentuan lainnya dan orang tersebut sepakat untuk tunduk pada ketentuan-ketentuan tersebut. </li>

					<li>Situs Web dan Layanan Kami ditujukan untuk orang-orang yang berdomisili di Indonesia. Kami tidak menjamin ketersediaan konten melalui Situs Web Kami cukup dan dapat tersedia di wilayah lainnya. Kami dapat membatasi ketersediaan Situs Web dan Layanan kami untuk orang tertentu atau pada wilayah tertentu. Apabila Anda mengakses Situs Web dan Layanan Kami dari luar Indonesia, Anda dengan ini mengetahui bahwa Anda mengaksesnya dengan resiko-resiko Anda sendiri. </li>
				</ol>

				<h3 class="opensans purple"><b>XII.	Virus</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Kami tidak menjamin bahwa Situs Web kami bebas dari segala virus atau kerusakan. Anda bertanggung jawab untuk melakukan konfigurasi teknologi informasi, program komputer dan platform Anda sendiri dalam rangka mengakses Situs Web kami. Anda harus menggunakan perangkat lunak anti-virus milik Anda sendiri. </p>

				<h3 class="opensans purple"><b>XIII.	Penyalahgunaan Situs</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Anda tidak boleh menyalahgunakan Situs Web ini dikarenakan oleh sebab apapun. Anda tidak akan melakukan atau mendukung tindak pidana, mengirimkan atau mendistribusikan virus termasuk Trojan horse, worm, logic bomb atau mengirimkan materi berbahaya lainnya pada Situs Web, teknologi berbahaya, melanggar kepercayaan atau dengan cara apapun yang bersifat ofensif atau melecehkan; memasuki setiap aspek dari Layanan; merusak data; menyebabkan gangguan terhadap pengguna lain; melanggar hak kesopanan orang lain; mengirim iklan atau materi promosi yang tidak diminta; atau mencoba untuk mempengaruhi kinerja atau fungsi dari setiap fasilitas komputer atau akses terhadap seluruh Situs Web. Setiap pelanggaran ketentuan ini merupakan tindak pidana di bawah Undang-Undang Nomor 11 Tahun 2008 tentang Internet dan Transaksi Elektronik (ITE) beserta peraturan turunannya. Jika hal tersebut terjadi, Kami akan melaporkan pelanggaran kepada pihak penegak hukum yang berwenang dan akan diambil tindakan hukum yang tepat.</p>

				<h3 class="opensans purple"><b>XIV.	Situs Web Pihak Ketiga</b></h3>

				<ol class="listPad">
					<li>Situs Web ini adakalanya terkait dengan Situs Web lain. Keterkaitan tersebut diluar kendali dan tanggung jawab kami. Kami tidak menerima jaminan atas isi atau ketersediaan situs terkait yang tidak dioperasikan oleh kami. Kaitan pada Situs Web kami, sediakan hanya untuk kenyamanan Anda dan kami tidak mengindikasikan kepercayaan atau persetujuan atas situs terkait. Untuk itu, Anda sebaiknya, selalu merujuk kepada syarat dan ketentuan yang ada pada situs terkait sebelum Anda menggunakan situs tersebut dan ajukanlah pertanyaan atau komentar Anda langsung kepada penyedia Situs web tersebut.</li>

					<li>Anda tidak diizinkan (dan juga Anda tidak diizinkan membantu orang lain) untuk membuat link dari Situs Web Anda ke Situs Web kami (dengan cara apapun) tanpa persetujuan tertulis, yang kami bisa berikan atau tolak sesuai dengan kebijakan kami. Anda tidak diizinkan untuk membuat tautan langsung ("Hot Link") terhadap konten atau gambar tanpa izin tertulis dari kami terlebih dulu.</li>
				</ol>

				<h3 class="opensans purple"><b>XV.	Perubahan Konten dan Situs Web</b></h3>

				<ol class="listPad">
					<li>Kami dapat merubah dari waktu ke waktu Situs Web beserta kontennya. Harap dicatat bahwa Konten dalam Situs Web kami dapat menjadi kadaluwarsa dan Kami tidak memiliki tanggungjawab untuk memperbaharui konten tersebut. </li>

					<li>Kami tidak menjamin bahwa Konten dan Situs Web Kami terbebas dari segala kesalahan dan kelalaian. </li>
				</ol>

				<h3 class="opensans purple"><b>XVI.	Penggunaan Cookies</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Situs Web ini menggunakan cookie, yang harus Anda setujui penggunaannya untuk menikmati fungsionalitas penuh dari Situs Web ini. Cookie adalah file yang digunakan oleh peladen kami untuk mengidentifikasi komputer Anda. Cookie yang kami gunakan akan merekam bagian mana dari Situs Web ini yang Anda kunjungi dan berapa lama. Anda berhak untuk menolak penggunaan cookie dengan cara mengkonfigurasi web jelajah Anda. Mohon untuk diingat, bahwa konfigurasi tersebut bisa saja mengganggu beberapa fungsi dari Situs Web ini. Untuk informasi lebih lanjut terkait penggunaan cookie ini, silahkan melihat bagian Kebijakan Privasi kami.</p>

				<h3 class="opensans purple"><b>XVII.	Akun dan Kata Sandi Anda</b></h3>

				<ol class="listPad">
					<li>Apabila Anda memilih sendiri atau diberikan kode unik, kata sandi atau informasi lainnya dalam kaitannya dengan prosedur keamanan Kami, Anda harus memperlakukan informasi tersebut dengan rahasia dan hati-hati. Anda tidak dapat memberikan informasi tersebut kepada pihak ketiga. </li>

					<li>Kami memiliki hak untuk mengnonaktifkan kode unik atau kata sandi apapun, baik yang anda pilih sendiri atau yang kami buatkan, dalam waktu kapanpun, jika menurut pandangan Kami Anda telah gagal untuk melaksanakan Ketentuan Penggunaan ini. </li>

					<li>Apabila nda mengetahui atau mencurigai adanya pihak lain yang mengetahui kode unik atau kata sandi anda, Anda wajib untuk memberitahukan dan menghubungi Kami. </li>
				</ol>

				<h3 class="opensans purple"><b>XVIII.	Menggungah Konten ke dalam Situs Web Kami</b></h3>

				<ol class="listPad">
					<li>Apabila terdapat fitur dalam Situs Web kami yang memperbolehkan Anda menggugah konten ke dalam Situs Web kami, konten tersebut wajib tunduk sesuai dengan ketentuan dalam Ketentuan Penggunaan ini dan ketentuan-ketentuan peraturan perundang-undangan yang berlaku. Anda menjamin bahwa apabila konten tersebut tidak sesuai dengan ketentuan yang disyaratkan, maka Anda bertanggung jawab kepada Kami dan wajib mengganti rugi Kami atas pelanggaran ketentuan ini. </li>

					<li>Konten yang anda unggah kedalam Situs Web Kami akan Kami anggap sebagai bukan rahasia dan tidak menjadi milik siapapun. Anda memiliki hak milik terhadap konten tersebut, namun Anda dengan ini memberikan Kami kuasa terbatas untuk menggunakan, menyimpan dan menyalin konten tersebut dan untuk mendistribusikannya kepada pihak lain. </li>

					<li>Kami juga memiliki hak untuk memberitahukan identitas Anda kepada pihak ketiga yang mengklaim bahwa konten yang diunggah oleh Anda pada Situs Web Kami menyababkan timbulnya pelanggaran hak kekayaan intelektual pihak tersebut atau hak privasi mereka. </li>

					<li>Kami tidak bertanggung jawab atau wajib mengganti rugi pihak ketiga atas akurasi konten yang diunggah oleh Anda atau pihak lainnya dalam Situs Web Kami. </li>

					<li>Kami memiliki hak untuk menyingkirkan unggahan atau konten yang anda pasang dalam Situs Web Kami, jika menurut pandangan Kami unggahan atau konten tersebut tidak sesuai dengan Ketentuan  Penggunaan ini dan ketentuan-ketentuan peraturan perundang-undangan yang berlaku. </li>
				</ol>

				<h3 class="opensans purple"><b>XIX.	Hak Pihak Ketiga</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Kecuali sebagaimana diatur secara tegas dalam Ketentuan Penggunaan ini atau perjanjian lain antara kami dan Anda atau berdasarkan peraturan perundang-undangan yang berlaku, orang atau entitas yang bukan suatu pihak pada Perjanjian tidak memiliki hak berdasarkan setiap hukum yang berlaku untuk menuntut pelaksanaan setiap ketentuan Perjanjian ini, terlepas apakah orang atau entitas tersebut telah diidentifikasi dengan namanya, sebagai anggota dari suatu kelas atau sebagaimana sesuai dengan suatu deskripsi tertentu.</p>

				<h3 class="opensans purple"><b>XX.	Pembatasan Tanggung Jawab dan Ganti Kerugian</b></h3>

				<ol class="listPad">
					<li>Dalam menyediakan Situs Web dan Layanan, Kami berupaya dan berkomitmen untuk tetap tunduk pada peraturan perundang-undangan yang berlaku, termasuk namun tidak terbatas pada Undang-Undang Nomor 11 Tahun 2008 tentang transaksi elektronik beserta peraturan turunannya dan Undang-Undang [__] Perlindungan Konsumen.</li>

					<li>Kami tidak bertanggung jawab atas segala kerugian yang timbul atas gagalnya pemenuhan atau kelalaian Anda untuk melaksanakan ketentuan-ketentuan dalam Ketentuan Penggunaan, Kebijakan Privasi dan Perjanjian Kredit antara Anda dengan pihak ketiga lainnya. </li>

					<li>Kami tidak bertanggungjawab atas hubungan hukum antara Peminjam dan Pemberi Pinjaman dalam Perjanjian Kredit, termasuk namun tidak terbatas apabila terdapatnya kerugian Anda atau pihak ketiga lainnya yang timbul berdasarkan Perjanjian Kredit tersebut.</li>

					<li>Kami tidak bertanggungjawab apabila setelah aplikasi Peminjam kami terima namun ternyata dikemudian hari Peminjam gagal melaksanakan kewajibannya berdasarkan Perjanjian Kredit kepada Pemberi Pinajaman.</li>

					<li>Tampa mengesampingkan ketentuan-ketentuan lainnya dalam Ketentuan Penggunaan, Perjanjian Kredit dan Kebijakan Privasi, Kami tidak bertanggungjawab atas setiap kerugian yang timbul sehubungan dengan:
						<ol class="listPad" style="list-style-type: lower-alpha;">
							<li>Tidak tersedianya akses dari dan ke Situs Web, Layanan, Konten dikarenakan alasan dan sebab apapun;</li>

							<li>Perubahan, modifikasi, penghapusan, penambahan, penghentian dari Situs Web, Layanan kami, Konten, Ketentuan Penggunaan, Kebijakan Privasi dan Perjanjian Kredit. </li>

							<li>Kegagalan kinerja sistem, jaringan, server, koneksi Kami sehingga menyebabkan tidak tersedianya Situs Web dan Layanan Kami, baik yang terjadi secara sengaja, atau tidak sengaja, atau akibat dari perbuatan pihak ketiga di luar kuasa kami.</li>

							<li>Ketidaklayakan, kurangnya, ketidaksempurnaan, kesesuaian, ketidakakuratan, kesalahan, ketidaklengkapan dan kekeliruan dari Situs Web, Layanan dan Konten.</li>

							<li> Hilangnya keuntungan, peluang atau data yang timbul dari penggunaan Anda atas Situs Web, Konten, Layanan.</li>

							<li>Dampak merugikan yang Anda alami akibat mengakses Situs Web dan Layanan kami, termasuk namun tidak terbatas pada hilangnya keuntungan, gangguan bisnis, peluang bisnis;</li>

							<li>Setiap kerugian yang timbul diakibatkan oleh Keadaan Memaksa;</li>

							<li>Setiap pelanggaran atas pengesampingan Ketentuan Penggunaan dan Kebijakan Privasi yang diwajibkan oleh peraturan perundang-undangan yang berlaku, perintah otoritas yang berwajib atau  putusan pengadilan.</li>
						</ol>
					</li>

					<li>Anda setuju untuk mengganti kerugian dan melindungi Kami dari kerugian terhadap semua tuntutan, kewajiban (termasuk kewajiban berdasarkan undang-undang dan kewajiban kepada para pihak ketiga), biaya, pengeluaran, denda, ongkos (termasuk tetapi tidak terbatas pada biaya hukum atas dasar ganti rugi penuh), ganti rugi, keputusan dan/atau kerugian yang diderita atau ditimbulkan oleh Kami, termasuk keuntungan dan peluang (yang potensial atau sebenarnya) yang dapat diambil darinya atau hilang, yang disebabkan atau timbul karena:

						<ol class="listPad" style="list-style-type: lower-alpha;">
							<li>Pelanggaran Anda terhadap Ketentuan Penggunaan, Kebijakan Privasi, Perjanjian Kredit dan peraturan perundang-undangan yang berlaku;</li>

							<li>Pelanggaran terhadap pernyataan dan jaminan sebagaimana disebutkan diatas;</li>

							<li>Perbuatan dari pihak ketiga menggunakan akun Anda yang menyebabkan kerugian bagi Kami dan/atau pihak lainnya;</li>

							<li>Kealfaan dari Anda yang menyebabkan kerugian dari pihak ketiga. </li>
						</ol>	
					</li>
				</ol>

				<h3 class="opensans purple"><b>XXI.	Pengakhiran</b></h3>

				<ol class="listPad">
					<li>Kami dapat setiap saat secara sepihak menonaktifkan dan menutup Akun Anda, termasuk untuk menghapus seluruh informasi, data dan konten Anda, baik untuk sementara atau permanen dengan tanpa pemberitahuan terlebih dahulu apabila menurut pertimbangan kami hal tersebut perlu dilakukan.</li>

					<li>Pengakhiran sebagaimana dimaksud dalam Pasal ini tidak menghilangkan kewajiban Anda atas segala pembayaran ganti rugi yang wajib dibayarkan sebagaimana diatur dalam Ketentuan Penggunaan dan Kebijakan Privasi ini.</li>

					<li>Sehubungan dengan pengakhiran, Para Pihak sepakat untuk mengesampingkan Pasal 1266 dan Pasal 1267 Kitab Undang-Undang Hukum Perdata.</li>
				</ol>

				<h3 class="opensans purple"><b>XXII.	Keterpisahan</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Jika ada syarat atau ketentuan dalam Ketentuan Penggunaan ini secara keseluruhan atau sebagian dinyatakan sampai batas apapun sebagai tidak sah atau tidak dapat dilaksanakan berdasarkan undang-undang atau peraturan hukum, syarat atau ketentuan atau bagian itu hingga batas tersebut dianggap bukan merupakan bagian dari Ketentuan Penggunaan ini dan keberlakuan dari syarat dan ketentuan lainnya di dalam Ketentuan Penggunaan tidak akan terpengaruh. Kegagalan kami dalam melaksanakan atau menjalankan setiap hak atau ketentuan dari Ketentuan Penggunaan ini bukan merupakan pengesampingan hak atau ketentuan tersebut keadaan tersebut atau setiap keadaan lainnya.</p>

				<h3 class="opensans purple"><b>XXIII.	Keseluruhan Perjanjian</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Ketentuan Penggunaan, Kebijakan Privasi dan Perjanjian Kredit merupakan keseluruhan perjanjian antara Anda dan Kami dan menggantikan setiap dan semua perjanjian sebelumnya dan kontemporer antara Anda. Setiap pengesampingan Ketentuan Penggunaan akan efektif jika secara tertulis dan ditandatangani oleh penandatangan resmi oleh Kami.</p>
				<p class="opensans" style="padding-bottom:20px;">Anda mengakui bahwa, dalam memasuki Perjanjian ini, baik Anda maupun kami tidak mengandalkan representasi apapun, melakukan atau janji yang diberikan oleh yang lain atau tersirat dari kata-kata apapun atau tertulis antara Anda dan kami sebelum Perjanjian, kecuali secara tegas dinyatakan dalam Perjanjian. </p>

				<h3 class="opensans purple"><b>XXIV.	Keadaan Memaksa</b></h3>

				<ol class="listPad">
					<li>Yang dimaksud dengan Keadaan Memaksa adalah kejadian-kejadian di luar kekuasaan dan di luar kemampuan yang wajar dari Kami yang mengakibatkan terjadinya keterlambatan pemenuhan kewajiban di dalam Ketentuan Penggunaan ini, yang antara lain meliputi bencana alam seperti gempa bumi, angin taufan, banjir, letusan gunung berapi, epidemi, kebakaran, pemogokan massal, perang, huru-hara, revolusi, kekacauan yang disebabkan keadaan ekonomi, politik, sosial, pemberontakan, perubahan pemerintah secara inkonstitusional, perubahan peraturan perundang-undangan dan perubahan kebijakan Pemerintah di bidang ekonomi dan moneter yang secara langsung mempengaruhi Kami.</li>

					<li>Kami tidak akan dipersalahkan bila terjadi keterlambatan pemenuhan kewajiban di dalam Perjanjian ini yang disebabkan oleh Keadaan Kahar. </li>

					<li>Anda setuju untuk tidak menuntut hak apapun sehubungan dengan tidak dapat terlaksananya kewajiban Kami di dalam Perjanjian ini yang disebabkan oleh terjadinya Keadaan Kahar.</li>
				</ol>

				<h3 class="opensans purple"><b>XXV.	Pajak dan Biaya-Biaya Lainnya</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Pajak dan biaya-biaya lainnya yang timbul sehubungan dengan Layanan dan Situs Web akan menjadi beban dan tanggung jawab masing-masing Pihak sesuai peraturan yang berlaku.</p>

				<h3 class="opensans purple"><b>XXVI.	Pemberitahuan</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Setiap pemberitahuan dan notifikasi berdasarkan Perjanjian akan diberikan secara tertulis baik melalui surat, faksimili atau email kepada Anda di alamat atau alamat e-mail sesuai dengan yang diinformasikan oleh Anda.</p>

				<h3 class="opensans purple"><b>XXVII.	Pengalihan</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Anda sepakat bahwa hak dan kewajiban masing-masing Anda dalam Ketentuan Penggunaan ini tidak dapat dialihkan kepada pihak ketiga lainnya, selain ketentuan-ketentuan yang telah disebutkan di dalam Ketentuan Penggunaan ini.</p>

				<h3 class="opensans purple"><b>XXVIII.	Hukum Yang Berlaku dan Penyelesaian Perselisihan</b></h3>

				<p class="opensans" style="padding-bottom:20px;">Ketentuan Penggunaan ini diatur dan ditafsirkan berdasarkan hukum yang berlaku di Negara Republik Indonesia. Baik Kami dan Anda dengan ini tunduk kepada yurisdiksi non-eksklusif dari Pengadilan Indonesia. </p>

				<h3 class="opensans purple"><b>XXIX.	Pertentangan</b></h3>


				<p class="opensans" style="padding-bottom:0px;">Apabila terdapat pertentangan antara Ketentuan Penggunaan ini dengan Kebijakan Privasi, Perjanjian Kredit ataupun perikatan-perikatan yang timbul terkait dengan Situs Web dan Layanan antara Anda dengan Kami, maka urutan keberlakuan dari Perjanjian adalah sebagai berikut:

				</p>

				<ol class="listPad">
						<li>Ketentuan Penggunaan;</li>
						<li>Kebijakan Privasi;</li>
						<li>Perjanjian Kredit;</li>
						<li>Perikatan-perikatan lainnya antara Kami dengan Anda, apabila ada.</li>
					</ol>
				<p class="opensans" style="padding-bottom:20px;">Batal demi hukumnya salah satu perjanjian tersebut diatas tidak menyebabkan menjadi batalnya perjanjian lainnya. </p>
				<p class="opensans" style="padding-bottom:20px;"></p>
				<p class="opensans" style="padding-bottom:20px;"></p>
				<p class="opensans" style="padding-bottom:20px;"></p>
				<p class="opensans" style="padding-bottom:20px;"></p>
				<p class="opensans" style="padding-bottom:20px;"></p>
				<p class="opensans" style="padding-bottom:20px;"></p>
				
	</div>
