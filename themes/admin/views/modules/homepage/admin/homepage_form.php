
				<header class="header bg-dark bg-gradient">
					<ul class="nav nav-tabs">
						<li class="active"><a href="<?php echo site_url('admin/homepage/add');?>/<?php echo $projectid;?>">Add New</a></li>
						<li class="pull-right"><a href="<?php echo site_url('admin/homepage');?>"><i class="icon-double-angle-left"></i> Back to Homepage</a></li>
					</ul>
				</header>
				<section class="scrollable wrapper">
					<div class="tab-content">
						<section class="tab-pane active" id="basic">
							<br/>
							<form action="" method="post" class="form-horizontal" id="form_a" data-validate="parsley"  novalidate="novalidate" enctype="multipart/form-data">
								<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
								<div class="form-group">
									<label class="col-sm-3 control-label"></label>
									<div class="col-sm-4">
										<?php echo "<h3>".$menu_title."</h3>"; ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Upload Photo</label>
									<div class="col-sm-5">
										<?php echo form_upload('image'); ?>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Title</label>
									<div class="col-sm-5">
										<input type="text" name="home_title" placeholder="" data-required="true" class="form-control" value="<?php echo set_value('home_title', isset($data->home_title) ? $data->home_title : ''); ?>"  />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label">Sub Title</label>
									<div class="col-sm-5">
										<input type="text" name="home_subtitle" placeholder="" data-required="true" class="form-control" value="<?php echo set_value('home_subtitle', isset($data->home_subtitle) ? $data->home_subtitle : ''); ?>"  />
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-4 col-sm-offset-3">
										<input type="hidden" name="flag" value="add" />
										<input type="hidden" name="home_id" value="<?php echo $home_id; ?>" />
										<button type="submit" class="btn btn-primary">Save Changes</button>
										<a href="project" class="btn btn-white">Cancel</a>
									</div>
								</div>
							</form>
						</section>
					</div>
				</section>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
	$('#datepicker').datepicker({
	dateFormat: ("yy-mm-dd")
	});
	</script>
