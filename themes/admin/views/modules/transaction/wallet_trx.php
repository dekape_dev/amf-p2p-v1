<header class="header bg-primary dker bg-gradient">
	<ul class="nav nav-tabs">
		<li class="active">
            <a href="<?php echo site_url(); ?>/admin/transaction/wallet" > Transaction List</a>
        </li>
    </ul>
</header>

<section class="scrollable wrapper">            
	
	<div class="tab-pane active" id="transaction_list">
		<?php if($this->session->flashdata('message')){ ?>
		<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
		<?php } ?>
			
		<section class="panel">
		  
			<header class="panel-heading"><?php echo $menu_title; ?></header>
			
			<div class="table-responsive">            
			  <table class="table table-striped m-b-none tbl text-normal" data-ride="">                
				<thead>                  
				  <tr>
					<th width="30px">No</th>
					<th>Date</th>
					<th>Investor Name</th>
					<th width="120px" class="text-right">Debet (Rp)</th>
					<th width="120px" class="text-right">Credit (Rp)</th>
					<th width="130px" class="text-right">Balance (Rp)</th>
					<th>Remark</th>
					<th>Status</th>
					<th>Payment Channel</th>
				  </tr>                  
				</thead> 
				<tbody>	
					<?php $no=1;?>
					<?php foreach($data as $row):  ?>
					<tr>     
						<td align="center"><?php echo $no; ?></td> 
				    	<td><?php echo date("d M Y H:i:s",strtotime($row->modified_on)); ?></td>
						<td><?php echo $row->lender_first_name." ".$row->lender_last_name; ?></td>   
						<td class="text-right"><?php echo number_format($row->wallet_debet); ?></td>
				    	<td class="text-right"><?php echo number_format($row->wallet_credit); ?></td>
				    	<td class="text-right"><?php echo number_format($row->wallet_saldo); ?></td>
				    	<td><?php echo $row->wallet_remark; ?></td>
				    	<td><?php echo $row->wallet_status; ?></td>
				    	<td><?php echo str_replace("_"," ",$row->wallet_payment_type); ?></td>
						
					</tr>
					<?php $no++; ?>
					<?php endforeach; ?>
				</tbody>			
			  </table>		  
			</div>		
		  </section>	  
		</div>	
	</div>		  
</section>
