<section class="vbox">
    <header class="header bg-white b-b"><p class="pull-left"><b>DASHBOARD</b></p><p class="pull-right"><?php echo date("D, d M Y");?></p></header>        
	<section class="scrollable wrapper">
		<div class="row">
			<div class="col-lg-12">
            
				<div class="row">
					
					<div class="col-lg-4 col-sm-12">
						<section class="panel no-borders">
							<header class="panel-heading bg-success lter">
								<span class="pull-right"><a href="<?php echo site_url("admin/borrower"); ?>">view details</a></span>
								<span class="h4">Borrower <br>    <small class="text-muted"></small>  </span>
							
							</header>
							<section class="panel-body">
								<div class="text-center">
									<span class="text-muted">TOTAL</span>
									<span class="h3 block"><?php echo $borrower['total']; ?> </span>
									<br/>
								</div>
								
									<ul class="list-group list-group-flush no-radius alt">
										<li class="list-group-item">
											<span class="label bg-primary lt"><?php echo $borrower['funding']; ?></span> Funding <span class="pull-right"><?php echo number_format($borrower['sum_funding']/1000); ?> k</span>
										</li>
										<li class="list-group-item">
											<span class="label bg-success"><?php echo $borrower['funded']; ?></span> Funded <span class="pull-right"><?php echo number_format($borrower['sum_funded']/1000); ?> k</span>
										</li>
										<li class="list-group-item">
											<span class="label bg-danger"><?php echo $borrower['notfunded']; ?></span> Not Funded <span class="pull-right"><?php echo number_format($borrower['sum_notfunded']/1000); ?> k</span>
										</li>
									</ul>
								</section>
						</section>
						
						
					</div>
					
					<div class="col-lg-4 col-sm-12">
						<section class="panel no-borders">
							<header class="panel-heading bg-primary lter">
								<span class="pull-right"><a href="<?php echo site_url("admin/investor"); ?>">view details</a></span>
								<span class="h4">Investor <br>    <small class="text-muted"></small>  </span>
							
							</header>
							<section class="panel-body">
								<div class="text-center">
									<span class="text-muted">  TOTAL </span>
									<span class="h3 block">   <?php echo $lender['register']; ?> </span>
									<br/>
								</div>
								
									<ul class="list-group list-group-flush no-radius alt">
										<li class="list-group-item">
											Activation <span class="pull-right"><?php echo $lender['register_enable']; ?></span>
										</li>
										<li class="list-group-item">
											Not Activation <span class="pull-right"><?php echo $lender['register_disable']; ?></span>
										</li>
										<li class="list-group-item">
											Invest <span class="pull-right"><?php echo $lender['invest']; ?></span>
										</li>
									</ul>
								</section>
						</section>
					</div>
					
					<div class="col-lg-4 col-sm-12">
						<section class="panel no-borders">
							<header class="panel-heading bg-info lter">
								<span class="pull-right"><a href="<?php echo site_url("admin/trx"); ?>">view details</a></span>
								<span class="h4">Deposit <br>    <small class="text-muted"></small>  </span>
							
							</header>
							<section class="panel-body">
								<div class="text-center">
									<span class="text-muted">  TOTAL </span>
									<span class="h3 block">   <?php echo $deposit['total']; ?> </span>
									<br/>
								</div>
								
									<ul class="list-group list-group-flush no-radius alt">
										<li class="list-group-item">
											Success <span class="pull-right"><?php echo $deposit['success']; ?></span>
										</li>
										<li class="list-group-item">
											Verification <span class="pull-right"><?php echo $deposit['verification']; ?></span>
										</li>
									</ul>
								</section>
						</section>
					</div>
					
					
					<div class="col-lg-3 col-sm-12">
					</div>
					
				</div>
            
            </div> 
		</div>
    </section>
</section>