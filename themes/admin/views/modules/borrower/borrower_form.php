<header class="header bg-primary dker bg-gradient">
	<ul class="nav nav-tabs">
		<li>
            <a href="<?php echo site_url(); ?>admin/borrower" >Borrower List</a>
    </li>
    <li class="active">
            <a href="<?php echo site_url(); ?>admin/borrower/candidate" >Add Borrower</a>
    </li>
    <li class="">
            <a href="<?php echo site_url(); ?>admin/borrower/mis_connection" >MIS Connection Borrower</a>
    </li>
  </ul>
</header>

				<section class="scrollable wrapper">
					<div class="tab-content">
						<section class="tab-pane active" id="basic">
							<br/>
							<form action="" method="post" class="form-horizontal" id="form_a" data-validate="parsley"  novalidate="novalidate" enctype="multipart/form-data">
								<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
									<label class="col-sm-3 control-label"></label>
									<?php echo "<h3>".$menu_."</h3>"; ?>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Tanggal</label>
										<input name="borrower_date_timestamp" type="text" id="datepicker"  placeholder="Date YYYY-MM-DD" rows="10"  class="form-control" value="<?php echo set_value('borrower_date_timestamp', isset($data->borrower_date_timestamp) ? $data->borrower_date_timestamp : date('Y-m-d')); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">No Rekening</label>
										<input name="borrower_client_account" type="text" placeholder="" rows="10"  class="form-control" value="<?php echo set_value('borrower_client_account', isset($data->borrower_client_account) ? $data->borrower_client_account : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Nama</label>
										<input name="borrower_client_name" type="text" placeholder="" class="form-control" value="<?php echo set_value('borrower_client_name', isset($data->borrower_client_name) ? $data->borrower_client_name : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Cabang</label>
										<input name="borrower_branch_name" type="text" placeholder="" class="form-control" value="<?php echo set_value('borrower_branch_name', isset($data->borrower_branch_name) ? $data->borrower_branch_name : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Majelis</label>
										<input name="borrower_group_name" type="text" placeholder=""  class="form-control" value="<?php echo set_value('borrower_group_name', isset($data->borrower_group_name) ? $data->borrower_group_name : ''); ?>" >
									</div>
								</div>
								<hr/>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Plafond</label>
										<input name="borrower_plafond" type="text" placeholder=""  class="form-control" value="<?php echo set_value('borrower_plafond', isset($data->borrower_plafond) ? $data->borrower_plafond : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Profit Shared</label>
										<input name="borrower_profit" type="text" placeholder=""   class="form-control" value="<?php echo set_value('borrower_profit', isset($data->borrower_profit) ? $data->borrower_profit : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Tenor</label>
										<input name="borrower_tenor" type="text" placeholder=""   class="form-control" value="<?php echo set_value('borrower_tenor', isset($data->borrower_tenor) ? $data->borrower_tenor : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Tanggal Pengajuan</label>
										<input name="borrower_date_pengajuan" type="text" id="datepicker"  placeholder="Date YYYY-MM-DD" rows="10"  class="form-control" value="<?php echo set_value('borrower_date_pengajuan', isset($data->borrower_date_pengajuan) ? $data->borrower_date_pengajuan : date('Y-m-d')); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Tanggal Pencairan</label>
										<input name="borrower_date_pencairan" type="text" id="datepicker"  placeholder="Date YYYY-MM-DD" rows="10"  class="form-control" value="<?php echo set_value('borrower_date_pencairan', isset($data->borrower_date_pencairan) ? $data->borrower_date_pencairan : date('Y-m-d')); ?>" >
									</div>
								</div>
								<hr/>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
									<label class="control-label">Foto</label>
										<?php echo form_upload('image'); ?>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Profile</label>
										<textarea name="borrower_overview" class="textarea" placeholder="" rows="10"  class="form-control" ><?php echo set_value('borrower_overview', isset($data->borrower_overview) ? $data->borrower_overview : ''); ?></textarea>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Sector</label>
										<input name="borrower_sector" type="text" placeholder=""  class="form-control" value="<?php echo set_value('borrower_sector', isset($data->borrower_sector) ? $data->borrower_sector : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Monthly Income</label>
										<input name="borrower_monthly_income" type="text" placeholder="" class="form-control" value="<?php echo set_value('borrower_monthly_income', isset($data->borrower_monthly_income) ? $data->borrower_monthly_income : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Employment</label>
										<input name="borrower_employment" type="text" placeholder="" class="form-control" value="<?php echo set_value('borrower_employment', isset($data->borrower_employment) ? $data->borrower_employment : ''); ?>" >
									</div>
								</div>
								<hr/>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Status Pendanaan</label>
										<select name="borrower_fund_status" class="form-control">
											<option value="Funding" <?php if($data->borrower_fund_status == "Funding"){ echo "selected";} ?>>Funding</option>
											<option value="Funded" <?php if($data->borrower_fund_status == "Funded"){ echo "selected";} ?>>Funded</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Status Borrower</label>
										<select name="borrower_status" class="form-control">
											<option value="Enable" <?php if($data->borrower_status == "Enable"){ echo "selected";} ?>>Enable</option>
											<option value="Disable" <?php if($data->borrower_status == "Disable"){ echo "selected";} ?>>Disable</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<input type="hidden" name="flag" value="add" />
										<input type="hidden" name="borrower_id" value="<?php echo $data->borrower_id; ?>" />
										<input type="hidden" name="borrower_disbursh_id" value="1" />
										<button type="submit" class="btn btn-primary">Save Changes</button>
										<a href="project" class="btn btn-white">Cancel</a>
									</div>
								</div>
							</form>
						</section>
					</div>
				</section>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
	$('#datepicker').datepicker({
	dateFormat: ("yy-mm-dd")
	});
	</script>
