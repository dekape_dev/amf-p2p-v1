<header class="header bg-primary dker bg-gradient">
	<ul class="nav nav-tabs">
		<li>
            <a href="<?php echo site_url(); ?>admin/borrower" >Borrower List</a>
        </li>
        <li class="">
            <a href="<?php echo site_url(); ?>admin/borrower/candidate" >Add Borrower</a>
        </li>
        <li class="">
            <a href="<?php echo site_url(); ?>admin/borrower/mis_connection" >MIS Connection Borrower</a>
        </li>
    </ul>
</header>

				<section class="scrollable wrapper">
					<div class="tab-content">
						<section class="tab-pane active" id="basic">
							<br/>


							<form action="" method="post" class="form-horizontal col-lg-6" id="form_a" data-validate="parsley"  novalidate="novalidate" enctype="multipart/form-data">
								<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>

								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Nama</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo $data->client_fullname; ?>" />
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">No Rekening</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo $data->client_account; ?>" />
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Majelis</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo $data->group_name; ?>" />
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Cabang</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo $data->branch_name; ?>" />
									</div>
								</div>
								<hr/>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Tgl Pengajuan</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo date("d M Y",strtotime($data->data_tgl)); ?>" />
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Tgl Pencairan</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo date("d M Y",strtotime($data->data_date_accept)); ?>" />
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Plafond</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo number_format($data->data_plafond); ?>" />
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Pembiayaan Ke</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo number_format($data->data_ke); ?>" />
									</div>
								</div>
								<hr/>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Tujuan Pembiayaan</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo $data->data_tujuan; ?>" />
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Keterangan</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo $data->data_keterangan; ?>" />
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">PPI Score</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo $data->data_popi_total." (".$data->data_popi_kategori.")"; ?>" />
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">CHI Score</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo $data->data_rmc_total." (".$data->data_rmc_kategori.")"; ?>" />
									</div>
								</div>
								<hr/>

								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Pekerjaan Suami</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo $data->data_suami_pekerjaan; ?>" />
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Pekerjaan Istri</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo $data->data_pendapatan_istrijenisusaha; ?>" />
									</div>
								</div>
								<div class="form-group">
									<label for="" class="col-sm-3 control-label">Saving Power</label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="" placeholder="" value="<?php echo $data->data_savingpower; ?>" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-3 col-sm-5">
										<input type="hidden" name="data_id" value="<?php echo $data->data_id; ?>" />
										<button type="submit" class="btn btn-primary">Add to P2P</button>
										<a href="<?php echo site_url(); ?>admin/borrower/candidate" class="btn btn-white">Back</a>
									</div>
								</div>
							</form>
						</section>
					</div>
				</section>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script>
	$('#datepicker').datepicker({
	dateFormat: ("yy-mm-dd")
	});
	</script>
