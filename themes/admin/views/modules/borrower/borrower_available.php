<header class="header bg-primary dker bg-gradient">
	<ul class="nav nav-tabs">
		<li class="">
            <a href="<?php echo site_url(); ?>admin/borrower" >Borrower List</a>
        </li>
        <li class="active">
            <a href="<?php echo site_url(); ?>admin/borrower/candidate" >Add Borrower</a>
        </li>
        <li class="">
            <a href="<?php echo site_url(); ?>admin/borrower/mis_connection" >MIS Connection Borrower</a>
        </li>
    </ul>
</header>

<section class="scrollable wrapper">

	<div class="tab-pane active" id="borrower_list">
		<?php if($this->session->flashdata('message')){ ?>
		<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
		<?php } ?>

		<section class="panel">

			<header class="panel-heading"><?php echo $menu_title; ?></header>

			<div class="table-responsive">
			  <table class="table table-striped m-b-none tbl text-normal" data-ride="">
				<thead>
				  <tr>
					<th width="30px">No</th>
					<th>Nama</th>
					<th>No Rekening</th>
					<th>Cabang</th>
					<th>Majelis</th>
					<th class="text-center">Ke</th>
					<th class="text-right">Plafond (Rp)</th>
					<th width="120px">Tgl Pengajuan</th>
					<th width="120px">Tgl Pencairan</th>
					<th width="30px" class="text-center">PPI</th>
					<th width="30px" class="text-center">CHI</th>
					<th width="30px" class="text-center">Manage</th>
				  </tr>
				</thead>
				<tbody>
					<?php $no=1;?>
					<?php foreach($data as $row):  ?>
					<tr>
						<td align="center"><?php echo $no; ?></td>
						<td><?php echo $row->client_fullname; ?></td>
						<td><?php echo $row->client_account; ?></td>
						<td><?php echo $row->branch_name; ?></td>
						<td><?php echo $row->group_name; ?></td>
						<td class="text-center"><?php echo $row->data_ke; ?></td>
						<td class="text-right"><?php echo number_format($row->data_plafond); ?></td>
						<td><?php echo $row->data_tgl; ?></td>
						<td><?php echo $row->data_date_accept; ?></td>
						<td><?php echo $row->data_popi_kategori; ?></td>
						<td><?php echo $row->data_rmc_kategori; ?></td>
						<td class="text-center">
							<a href="<?php echo site_url()."admin/borrower/detail/".$row->data_id; ?>" title="detail"><i class="icon-search"></i></a>
							<a href="<?php echo site_url()."admin/borrower/add/".$row->data_id; ?>" title="Add" onclick="return confirmAdd();"><i class="icon-plus"></i></a>
						</td>
					</tr>
					<?php $no++; ?>
					<?php endforeach; ?>
				</tbody>
			  </table>
			</div>
		  </section>
		</div>
	</div>
</section>
