<header class="header bg-primary dker bg-gradient">
	<ul class="nav nav-tabs">
		<li class="active">
            <a href="<?php echo site_url(); ?>admin/borrower" >Borrower List</a>
        </li>
        <li class="">
            <a href="<?php echo site_url(); ?>admin/borrower/candidate" >Add Borrower</a>
        </li>
        <li class="">
            <a href="<?php echo site_url(); ?>admin/borrower/mis_connection" >MIS Connection Borrower</a>
        </li>
    </ul>
</header>

<section class="scrollable wrapper">

	<div class="tab-pane active" id="borrower_list">
		<?php if($this->session->flashdata('message')){ ?>
		<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
		<?php } ?>

		<section class="panel">

			<header class="panel-heading"><?php echo $menu_title; ?></header>

			<div class="table-responsive">
			  <table class="table table-striped m-b-none tbl text-normal" data-ride="">
				<thead>
				  <tr>
					<th width="30px">No</th>
					<th>Nama</th>
					<th>No Rekening</th>
					<th>Cabang</th>
					<th>Majelis</th>
					<th class="text-right">Plafond (Rp)</th>
					<th class="text-center">Ke</th>
					<th width="150px">Tgl Pengajuan</th>
					<th class="text-center">Days Left</th>
					<th class="text-center">State</th>
					<th width="50px" class="text-center">Status</th>
					<th width="80px"  class="">Manage</th>
				  </tr>
				</thead>
				<tbody>
					<?php $no=1;?>
					<?php foreach($data as $row):  ?>
					<?php
						$startTimeStamp = strtotime(date('Y-m-d'));
						$endTimeStamp 	= strtotime($row->data_date_accept);
						$timeDiff 		= ($endTimeStamp - $startTimeStamp);
						$numberDays 	= $timeDiff/86400;
						$numberDays 	= intval($numberDays);
						?>
					<tr>
						<td align="center"><?php echo $no; ?></td>
						<td><?php echo $row->client_fullname; ?></td>
						<td><?php echo $row->client_account; ?></td>
						<td><?php echo $row->branch_name; ?></td>
						<td><?php echo $row->group_name; ?></td>
						<td class="text-right"><?php echo number_format($row->data_plafond); ?></td>
						<td class="text-center"><?php echo number_format($row->data_ke); ?></td>
						<td><?php echo date("d M Y",strtotime($row->data_tgl)); ?></td>
						<td class="text-center"><?php echo $numberDays; ?></td>
						<td class="text-center"><?php if($row->data_status == 1){ echo "Berjalan";}elseif($row->data_status == 2){ echo "Pengajuan";}elseif($row->data_status == 3){ echo "Selesai";}; ?></td>
						<td class="text-center">
							<?php if($row->data_p2p == 1){ ?>
								<?php if($row->data_p2p_lender != "0"){ ?>
									<span class="label bg-success">Funded</span>
								<?php }else{ ?>
									<?php if($numberDays >= 0){ ?>
									<span class="label bg-primary">Funding</span>
									<?php }else{ ?>
									<span class="label bg-warning">Expired</span>
									<?php } ?>
								<?php } ?>
							<?php }elseif($row->data_p2p == 2){ ?>
									<span class="label bg-danger">Not Funded</span>
							<?php }?>
						</td>
						<td class="text-center">
							<a href="<?php echo site_url()."admin/borrower/detail/".$row->data_id; ?>" title="Borrower Detail"><i class="icon-search"></i></a>
							<?php if($row->data_p2p == 1 AND $row->data_p2p_lender == "0"){ ?>
							<a href="<?php echo site_url()."admin/borrower/remove/".$row->data_id; ?>" title="Remove from P2P" onclick="return confirmDialog();"><i class="icon-trash"></i></a>
							<?php } ?>
							<a href="<?php echo site_url()."/admin/borrower/edit/".$row->borrower_id; ?>" title="Edit" class="hidden"><i class="icon-pencil"></i></a>
							<a href="<?php echo site_url()."/admin/borrower/del/".$row->borrower_id; ?>" title="Delete" class="hidden" onclick="return confirmDialog();" ><i class="icon-trash"></i></a>
						</td>
					</tr>
					<?php $no++; ?>
					<?php endforeach; ?>
				</tbody>
			  </table>
			</div>
		  </section>
		</div>
	</div>
</section>
