<header class="header bg-primary dker bg-gradient">
	<ul class="nav nav-tabs">
		<li class="">
            <a href="<?php echo site_url(); ?>admin/borrower" >Borrower List</a>
        </li>
        <li class="">
            <a href="<?php echo site_url(); ?>admin/borrower/candidate" >Add Borrower</a>
        </li>
        <li class="active">
            <a href="<?php echo site_url(); ?>admin/borrower/mis_connection" >MIS Connection Borrower</a>
        </li>
    </ul>
</header>

<section class="scrollable wrapper">

	<div class="tab-pane active" id="borrower_list">
		<?php if($this->session->flashdata('message')){ ?>
		<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
		<?php } ?>

		<section class="panel">

			<header class="panel-heading"><?php echo $menu_title; ?></header>

			<div class="table-responsive">
			  <table class="table table-striped m-b-none tbl text-normal" data-ride="">
				<thead>
				  <tr>
					<th width="30px">No</th>
					<th width="150px">Nama P2P</th>
					<th width="150px">Nama MIS</th>
					<th width="150px">No Rekening</th>
					<th width="150px">Majelis</th>
					<th width="30px">Email</th>
					<th width="10px" class="text-center">Ke</th>
					<th width="50px" class="text-right">Plafond (Rp)</th>
					<th width="120px">Tgl Pengajuan</th>
					<th width="120px">Tgl Pencairan</th>
					<th width="30px" class="text-center">Manage</th>
				  </tr>
				</thead>
				<tbody>
				<?php ?>
					<?php $no=1; $acc_no = null;?>
					<?php foreach($data as $row):  ?>
					<?php if($acc_no == $row->borrower_account) { continue; } else { $acc_no = $row->borrower_account; } ?>
					<tr>
						<td align="center"><?php echo $no; ?></td>
						<td><?php echo $row->borrower_first_name.' '.$row->borrower_last_name; ?></td>
						<td>
							<?php if($row->client_fullname != null) { echo $row->client_fullname; } else { echo 'N/A'; } ?>
						</td>
						<form action="<?php echo site_url()."admin/borrower/connect_to_mis/".$row->borrower_id; ?>" method="post">
							<td><input name="b_account" type="text" id=""  placeholder="Date YYYY-MM-DD" value="<?php echo $row->borrower_account; ?>" style='width:100%;'></td>
							<td class="text-center">
									<select name="b_group" style='width:100%;'>
									<?php
											echo '<option value="0">Not connected to MIS</option>';
											foreach ($groups as $g) {
												if($row->borrower_majelis == $g->group_id) { $selected = 'selected="selected"'; }
												else { $selected = ''; }
												echo '<option value="'.$g->group_id.'" '.$selected.'>'.$g->group_name.'</option>';
											}
									?>
									</select>
							</td>
							<input name="b_id" type="hidden" value="<?php echo $row->borrower_id ?>">
							<td><?php echo $row->borrower_email; ?></td>
							<td class="text-center">
								<?php if($row->data_ke != null) { echo $row->data_ke; } else { echo 'N/A'; } ?>
							</td>
							<td class="text-right">
								<?php if($row->data_plafond != null) { echo number_format($row->data_plafond); }
											else { echo 'N/A'; } ?>
							</td>
							<td>
								<?php if($row->data_tgl != null) { echo date('d M Y', strtotime($row->data_tgl)); }
							        else { echo 'N/A'; } ?>
							</td>
							<td>
								<?php if($row->data_date_accept != null) { echo date('d M Y', strtotime($row->data_date_accept)); }
										  else echo 'N/A'; ?>
							</td>
							<td class="text-center">
								<?php if($row->client_id != null) echo '<a class="btn btn-xs btn-primary" href="'.site_url().'admin/borrower/detail/'.$row->client_id.'" title="Profile"><i class="icon-search"></i></a>'; ?>
								<?php if($row->client_id == null) echo '<button type="submit" class="btn btn-xs btn-primary"><i class="icon-plus"></i></button>'; ?>
								<?php if($row->client_id != null) echo '<a class="btn btn-xs btn-primary" href="'.site_url().'admin/borrower/disconnect_from_mis/'.$row->borrower_id.'" title="Disconnect"><i class="icon-minus"></i></a>'; ?>
							</td>
						</form>
					</tr>
					<?php $no++; ?>
					<?php endforeach; ?>
				<?php ?>
				</tbody>
			  </table>
			</div>
		  </section>
		</div>
	</div>
</section>
