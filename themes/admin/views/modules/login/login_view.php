


<section id="content" class="m-t-lg wrapper-md animated fadeInUp" style="margin-top: 100px;">
	<a class="nav-brand" href="#"><h2>Amartha.co.id</h2></a><br/>

		<div class="row m-n">

			<div class="col-md-4 col-md-offset-4 m-t-lg">
				<?php if(validation_errors()){ ?>
					<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo validation_errors(); ?></div>
				<?php } ?>
				<?php if($this->session->flashdata('message')){ ?>
					<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo $this->session->flashdata('message'); ?></div>
				<?php } ?>
				<section class="panel">
					<header class="panel-heading text-center">Sign in</header>
					<form action="<?php echo site_url('admin/login/verifylogin'); ?>" method="post" class="panel-body">
						<div class="form-group">
							<label class="control-label">Username</label>
							<input type="text" name="username" placeholder="Username" class="form-control">
						</div>
						<div class="form-group">
							<label class="control-label">Password</label>
							<input type="password" name="password" id="inputPassword" placeholder="Password" class="form-control">
						</div>
						<button type="submit" class="btn btn-info">Sign in</button>

					</form>
				</section>
			</div>
		</div>
	</section>
