<header class="header bg-primary dker bg-gradient">
	<ul class="nav nav-tabs">
		<li class="active">
            <a href="#static" data-toggle="tab">News</a>
        </li>
        <li class="">
            <a href="#static" data-toggle="tab">Static table</a>
        </li>
    </ul>
</header>

<section class="scrollable wrapper">            
	<div class="tab-content"><h3><?php echo $menu_title; ?></h3>
		<div class="tab-pane active" id="svc_architecture">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
			<?php } ?>
		  <section class="panel">
		  
			<header class="panel-heading"><a href="news/add_news" class="btn btn-default btn-xs">Add New News</a></header>
			
			<div class="table-responsive">            
			  <table class="table table-striped m-b-none tbl" data-ride="">                
				<thead>                  
				  <tr>
					<th width="30px">No</th>
					<th width="200px">News Title</th>
					<th width="200px">News Date</th>
					<th width="200px">Image</th>
					<th>Content</th>
					<th width="130px">Manage</th>
				  </tr>                  
				</thead> 
				<tbody>	
					<?php $no=1;?>
					<?php foreach($project as $p):  ?>
					<tr>     
						<td align="center"><?php echo $no; ?></td>               
						<td><b><?php echo $p->news_title; ?></b></td>
						<td><b><?php echo $p->news_date; ?></b></td>
						<td><img src="<?php echo base_url().'/files/'.$p->news_picture; ?>" alt="" width="200px"/></td>
						<td><b align="justify"><?php echo substr($p->news_content,0,200)?></b></td>
						<td>
							<a href="<?php echo site_url()."/admin/news/edit_news/".$p->news_id; ?>" title="Edit"><i class="icon-pencil"></i></a>
							<a href="<?php echo site_url()."/admin/news/delete_news/".$p->news_id; ?>" title="Delete" onclick="return confirmDialog();" ><i class="icon-trash"></i></a></td>
					</tr>
					<?php $no++; ?>
					<?php endforeach; ?>
				</tbody>			
			  </table>		  
			</div>		
		  </section>	  
		</div>	
	</div>		  
</section>

<script>
	function confirmDialog() {
		return confirm("Are you sure you want to delete this News")
	}
</script>