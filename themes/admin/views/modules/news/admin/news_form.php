
				<header class="header bg-dark bg-gradient">
					<ul class="nav nav-tabs">
						<li class="active"><a href="<?php echo site_url();?>/admin/news/add_news/<?php echo $projectid;?>">Add New News</a></li>
						<li class="pull-right"><a href="<?php echo site_url();?>/admin/news"><i class="icon-double-angle-left"></i> Back to News</a></li>
					</ul>
		  		</header>
				<section class="scrollable wrapper">
					<div class="tab-content">
						<section class="tab-pane active" id="basic">							
							<br/>
							<form action="" method="post" class="form-horizontal" id="form_a" data-validate="parsley"  novalidate="novalidate" enctype="multipart/form-data">
								<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
									<label class="col-sm-3 control-label"></label> 
									<?php echo "<h3>".$menu_title."</h3>"; ?>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">News Title</label>
										<input name="news_title" type="text" placeholder="Title" rows="10"  class="form-control" value="<?php echo set_value('news_title', isset($data->news_title) ? $data->news_title : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">News Date</label>
										<input name="news_date" type="text" id="datepicker"  placeholder="Date" rows="10"  class="form-control" value="<?php echo set_value('news_date', isset($data->news_date) ? $data->news_date : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
									<label class="control-label">Upload Photo</label>
										<?php echo form_upload('image'); ?>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">News Content</label>
										<textarea name="news_content" class="textarea" placeholder="" rows="10"  class="form-control" ><?php echo set_value('news_content', isset($data->news_content) ? $data->news_content : ''); ?></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<input type="hidden" name="flag" value="add" />
										<input type="hidden" name="news_id" value="<?php echo $news_id; ?>" />
										<button type="submit" class="btn btn-primary">Save Changes</button>
										<a href="project" class="btn btn-white">Cancel</a>
									</div>
								</div>
							</form>
						</section>
					</div>
				</section>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script> 
	$('#datepicker').datepicker({
	dateFormat: ("yy-mm-dd")
	});
	</script>