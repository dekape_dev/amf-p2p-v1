
				<section class="scrollable wrapper">
					<div class="tab-content"><h3><?php echo $menu_title; ?></h3>
						<div class="tab-pane active" id="svc_architecture">
							<?php if($this->session->flashdata('message')){ ?>
							<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
							<?php } ?>
						  <section class="panel">

							<header class="panel-heading"><a href="homepage/add_photo" class="btn btn-default btn-xs">Add New Homepage</a></header>

							<div class="table-responsive">
							  <table class="table table-striped m-b-none tbl" data-ride="">
								<thead>
								  <tr>
									<th width="30px">No</th>
									<th width="200px">Tittle</th>
									<th>Caption</th>
									<th width="130px">Manage</th>
								  </tr>
								</thead>
								<tbody>
									<?php $no=1;?>
									<?php foreach($get_content as $p):  ?>
									<tr>
										<td align="center"><?php echo $no; ?></td>
										<td><img src="<?php echo $p->title; ?>" alt="" width="200px"/></td>
										<td><b><?php echo $p->content_title_ind; ?> <?php echo $p->content_title_eng; ?></td>
										<td>
											<a href="<?php echo site_url('admin/homepage/edit_photo')."/".$p->home_id; ?>" title="Edit"><i class="icon-pencil"></i></a>
											<a href="<?php echo site_url('admin/homepage/delete_photo')."/".$p->home_id; ?>" title="Delete" onclick="return confirmDialog();" ><i class="icon-trash"></i></a></td>
									</tr>
									<?php $no++; ?>
									<?php endforeach; ?>
								</tbody>
							  </table>
							</div>
						  </section>
						</div>
					</div>
				</section><script>
function confirmDialog() {
    return confirm("Are you sure you want to delete this photo?")
}
</script>
