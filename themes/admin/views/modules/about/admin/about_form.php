
				<section class="scrollable wrapper">
					<div class="tab-content">
						<section class="tab-pane active" id="basic">							
							<br/>
							<form action="" method="post" class="form-horizontal" id="form_a" data-validate="parsley"  novalidate="novalidate" enctype="multipart/form-data">
								<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
								
	
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-10">
									<?php if($this->session->flashdata('message')){ ?>
												<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
										<?php } ?>
										<?php echo "<h3>".$menu_title."</h3>"; ?>
										
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Title</label>
										<input name="tittle" type="text" placeholder="Title" rows="10"  class="form-control" value="<?php echo set_value('content_title', isset($content_title) ? $content_title : ''); ?>" readonly />
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Content ENG</label>
										<textarea name="content_eng" class="textarea" placeholder="" rows="10"  class="form-control" ><?php echo set_value('content_eng', isset($content_eng) ? $content_eng : ''); ?></textarea>
									</div>
									<div class="col-sm-5">
										<label class="control-label">Content IND</label>
										<textarea name="content_ind" class="textarea" placeholder="" rows="10"  class="form-control" ><?php echo set_value('content_ind', isset($content_ind) ? $content_ind : ''); ?></textarea>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-sm-4 col-sm-offset-1">
										<input type="hidden" name="content_id" value="<?php echo $content_id; ?>">
										<button type="submit" class="btn btn-primary">Save Changes</button>
										<a href="about" class="btn btn-white">Cancel</a>
									</div>
								</div>
							</form>
						</section>
					</div>
				</section>
