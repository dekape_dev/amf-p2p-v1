<header class="header bg-primary dker bg-gradient">
	<ul class="nav nav-tabs">
		<li>
            <a href="<?php echo site_url(); ?>/admin/lender" >Lender List</a>
        </li>
        <li class="active">
            <a href="<?php echo site_url(); ?>/admin/lender/create" >Add Lender</a>
        </li>
    </ul>
</header>

				<section class="scrollable wrapper">
					<div class="tab-content">
						<section class="tab-pane active" id="basic">							
							<br/>
							<form action="" method="post" class="form-horizontal" id="form_a" data-validate="parsley"  novalidate="novalidate" enctype="multipart/form-data">
								<?php echo validation_errors('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button>', '</div>'); ?>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
									<label class="col-sm-3 control-label"></label> 
									<?php echo "<h3>".$menu_."</h3>"; ?>
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Name</label>
										<input name="lender_name" type="text" placeholder="" rows="10"  class="form-control" value="<?php echo set_value('lender_name', isset($data->lender_name) ? $data->lender_name : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Email</label>
										<input name="lender_email" type="text" placeholder="" class="form-control" value="<?php echo set_value('lender_email', isset($data->lender_email) ? $data->lender_email : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Phone</label>
										<input name="lender_phone" type="text" placeholder=""   class="form-control" value="<?php echo set_value('lender_phone', isset($data->lender_phone) ? $data->lender_phone : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Birth Date</label>
										<input name="lender_birth_date" type="text" id="datepicker"  placeholder="Date YYYY-MM-DD" class="form-control" value="<?php echo set_value('lender_birth_date', isset($data->lender_birth_date) ? $data->lender_birth_date : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Address</label>
										<input name="lender_address" type="text" placeholder=""  class="form-control" value="<?php echo set_value('lender_address', isset($data->lender_address) ? $data->lender_address : ''); ?>" >
									</div>
								</div>
								<hr/>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Bank Account No</label>
										<input name="lender_bank_account" type="text" placeholder="" class="form-control" value="<?php echo set_value('lender_bank_account', isset($data->lender_bank_account) ? $data->lender_bank_account : ''); ?>" >
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Bank Name</label>
										<input name="lender_bank_name" type="text" placeholder="" class="form-control" value="<?php echo set_value('lender_bank_name', isset($data->lender_bank_name) ? $data->lender_bank_name : ''); ?>" >
									</div>
								</div>
								
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<label class="control-label">Status lender</label>
										<select name="lender_status" class="form-control">	
											<option value="Enable" <?php if($data->lender_status == "Enable"){ echo "selected";} ?>>Enable</option>
											<option value="Disable" <?php if($data->lender_status == "Disable"){ echo "selected";} ?>>Disable</option>
										</select>	
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-1 col-sm-5">
										<input type="hidden" name="flag" value="add" />
										<input type="hidden" name="lender_id" value="<?php echo $data->lender_id; ?>" />
										<button type="submit" class="btn btn-primary">Save Changes</button>
										<a href="project" class="btn btn-white">Cancel</a>
									</div>
								</div>
							</form>
						</section>
					</div>
				</section>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	<script> 
	$('#datepicker').datepicker({
	dateFormat: ("yy-mm-dd")
	});
	</script>