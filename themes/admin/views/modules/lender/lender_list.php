<header class="header bg-primary dker bg-gradient">
	<ul class="nav nav-tabs">
		<li class="active">
            <a href="<?php echo site_url(); ?>/admin/lender" > Lender List</a>
        </li>
        <li class="">
            <a href="<?php echo site_url(); ?>/admin/lender/create" >Add Lender</a>
        </li>
    </ul>
</header>

<section class="scrollable wrapper">            
	
	<div class="tab-pane active" id="lender_list">
		<?php if($this->session->flashdata('message')){ ?>
		<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
		<?php } ?>
			
		<section class="panel">
		  
			<header class="panel-heading"><?php echo $menu_title; ?></header>
			
			<div class="table-responsive">            
			  <table class="table table-striped m-b-none tbl text-normal" data-ride="">                
				<thead>                  
				  <tr>
					<th width="30px">No</th>
					<th>Nama</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Lending</th>
					<th width="50px">Status</th>
					<th width="80px">Manage</th>
				  </tr>                  
				</thead> 
				<tbody>	
					<?php $no=1;?>
					<?php foreach($data as $row):  ?>
					<tr>     
						<td align="center"><?php echo $no; ?></td> 
						<td><?php echo $row->lender_name; ?></td>   
						<td><?php echo $row->lender_email; ?></td>   
						<td><?php echo $row->lender_phone; ?></td>   
						<td>10</td> 
						<td><span class="label <?php if($row->lender_status == "Enable"){ echo "bg-success"; }elseif($row->lender_status == "Disable"){ echo "bg-danger"; } ?>"><?php echo $row->lender_status; ?></span></td>
						<td>
							<a href="<?php echo site_url()."/admin/lender/edit/".$row->lender_id; ?>" title="Edit"><i class="icon-pencil"></i></a>
							<a href="<?php echo site_url()."/admin/lender/del/".$row->lender_id; ?>" title="Delete" onclick="return confirmDialog();" ><i class="icon-trash"></i></a></td>
					</tr>
					<?php $no++; ?>
					<?php endforeach; ?>
				</tbody>			
			  </table>		  
			</div>		
		  </section>	  
		</div>	
	</div>		  
</section>
