<header class="header bg-primary dker bg-gradient">
	<ul class="nav nav-tabs">
		<li class="active">
            <a href="<?php echo site_url(); ?>/admin/trx" > Transaction List</a>
        </li>
        <li class="">
            <a href="<?php echo site_url(); ?>/admin/trx/create" >New Transaction</a>
        </li>
    </ul>
</header>

<section class="scrollable wrapper">            
	
	<div class="tab-pane active" id="transaction_list">
		<?php if($this->session->flashdata('message')){ ?>
		<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert"><i class="icon-remove"></i></button> <?php echo print_message($this->session->flashdata('message')); ?></div>
		<?php } ?>
			
		<section class="panel">
		  
			<header class="panel-heading"><?php echo $menu_title; ?></header>
			
			<div class="table-responsive">            
			  <table class="table table-striped m-b-none tbl text-normal" data-ride="">                
				<thead>                  
				  <tr>
					<th width="30px">No</th>
					<th>Transaction No</th>
					<th>Date</th>
					<th>Lender Name</th>
					<th>Borrower Name</th>
					<th>Borrower Account</th>
					<th>Plafond</th>
					<th>Margin</th>
					<th>Tenor</th>
					<th width="50px">Status</th>
					<th width="80px">Manage</th>
				  </tr>                  
				</thead> 
				<tbody>	
					<?php $no=1;?>
					<?php foreach($data as $row):  ?>
					<tr>     
						<td align="center"><?php echo $no; ?></td> 
						<td><?php echo $row->trx_order_no; ?></td> 
						<td><?php echo $row->trx_date; ?></td> 
						<td><?php echo $row->lender_first_name." ".$row->lender_last_name; ?></td>   
						<td><?php echo $row->client_fullname; ?></td> 
						<td><?php echo $row->client_account; ?></td>   
						<td><?php echo number_format($row->data_plafond); ?></td> 
						<td><?php echo number_format($row->data_margin); ?></td> 
						<td><?php echo number_format($row->data_jangkawaktu); ?></td> 
						<td><span class="label <?php if($row->trx_status == "Success"){ echo "bg-success"; }elseif($row->trx_status == "Verification"){ echo "bg-warning"; } ?>"><?php echo $row->trx_status; ?></span></td>
						<td>
							<a href="<?php echo site_url()."/admin/trx/edit/".$row->trx_id; ?>" title="Edit"><i class="icon-pencil"></i></a>
							<a href="<?php echo site_url()."/admin/trx/del/".$row->trx_id; ?>" title="Delete" onclick="return confirmDialog();" ><i class="icon-trash"></i></a></td>
					</tr>
					<?php $no++; ?>
					<?php endforeach; ?>
				</tbody>			
			  </table>		  
			</div>		
		  </section>	  
		</div>	
	</div>		  
</section>
