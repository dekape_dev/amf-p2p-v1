<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<title><?php if($menu_title){ echo $menu_title. " | "; } ?>Administrator Area</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>css/app.v1.css">
	<link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>css/font.css" cache="false">
	<link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>css/style.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

  </head>
  <body>
	<section class="hbox stretch">

	<!-- .aside -->
	<aside class="bg-primary aside-sm" id="nav">
		<section class="vbox">
			<header class="dker nav-bar">
				<a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="body"><i class="icon-reorder"></i></a>
				<a href="#" class="nav-brand" data-toggle="fullscreen">Amartha P2P</a><a class="btn btn-link visible-xs" data-toggle="class:show" data-target=".nav-user">  <i class="icon-comment-alt"></i></a>
			</header>
			<footer class="footer bg-gradient hidden-xs">
					<a href="<?php echo site_url('admin/login/logout'); ?>" class="btn btn-sm btn-link m-r-n-xs pull-right" title="Logout">  <i class="icon-off"></i></a>
					<a href="#nav" data-toggle="class:nav-vertical" class="btn btn-sm btn-link m-l-n-sm">  <i class="icon-reorder"></i></a>
			</footer>

				<section>
					<!-- user -->
					<div class="bg-success nav-user hidden-xs pos-rlt">
						<div class="nav-avatar pos-rlt">
							<a href="#" class="thumb-sm avatar animated rollIn" data-toggle="dropdown">
								<img src="<?php echo $this->template->get_theme_path(); ?>images/avatar.jpg" alt="" class=""><span class="caret caret-white"></span>
							</a>
							<ul class="dropdown-menu m-t-sm animated fadeInLeft">
								<span class="arrow top"></span>
								<li><a href="#">Settings</a></li>
								<li><a href="<?php echo site_url('admin/login/logout'); ?>">Logout</a></li>
							</ul>
							<!-- Mobile -->
							<div class="visible-xs m-t m-b">
								<a href="#" class="h3">John.Smith</a>
								<p><i class="icon-map-marker"></i>London, UK</p>
							</div>
						</div>
						<div class="nav-msg">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<b class="badge badge-white count-n">Admin</b>
							</a>
					  </div>
					</div>
					<!-- / user -->


					<!-- nav -->
					<nav class="nav-primary hidden-xs">
						<ul class="nav">
							<li><a href="<?php echo site_url('admin/home'); ?>"><i class="icon-dashboard"></i><span>Dashboard</span></a></li>
							<li class="dropdown-submenu">
								<a href="<?php echo site_url('admin/borrower'); ?>"><i class="icon-group"></i><span>Borrowers</span></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo site_url('admin/borrower'); ?>">Borrowers List</a></li>
									<li><a href="<?php echo site_url('admin/borrower/candidate'); ?>">Add Borrower</a></li>
									<li><a href="<?php echo site_url('admin/borrower/mis_connection'); ?>">Connect Borrower to MIS</a></li>
								</ul>
							</li>

							<li class="dropdown-submenu">
								<a href="<?php echo site_url('admin/lender'); ?>"><i class="icon-briefcase"></i><span>Investors</span></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo site_url('admin/lender'); ?>">Investor List</a></li>
									<li><a href="<?php echo site_url('admin/lender/create'); ?>">Add New Investor</a></li>
								</ul>
							</li>

							<li class="dropdown-submenu">
								<a href="<?php echo site_url('admin/trx'); ?>"><i class="icon-money"></i><span>Transaction</span></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo site_url('admin/trx'); ?>">Transaction List</a></li>
									<li><a href="<?php echo site_url('admin/transaction/wallet'); ?>">Wallet Transaction</a></li>
									<!--<li><a href="<?php echo site_url('admin/trx/create'); ?>">New Transaction</a></li>-->
								</ul>
							</li>

							<li class="dropdown-submenu">
								<a href="<?php echo site_url('admin/content'); ?>"><i class="icon-file"></i><span> Contents</span></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo site_url('admin/about'); ?>">About</a></li>
								</ul>
							</li>

							<li class="dropdown-submenu">
								<a href="<?php echo site_url('admin/borrower'); ?>"><i class="icon-gear"></i><span>Settings</span></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo site_url('admin/borrower'); ?>">Users</a></li>
								</ul>
							</li>


						</ul>
					</nav><!-- / nav -->
				</section>
		</section>
	</aside>
	<!-- /.aside -->

	<section id="content">
		<section class="vbox">
			<?php echo $template['body']; ?>
		</section>
	</section>


    </section>

	<script src="<?php echo $this->template->get_theme_path(); ?>css/app.v1.js" />
	<script src="<?php echo $this->template->get_theme_path(); ?>js/jquery.js"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/tinymce/tinymce.min.js"></script>

	<script>
	$(document).ready(function(){
		$('.tbl').dataTable({
			"bSort": false
		  } );

		 tinymce.init({selector:'.textarea'});


	});
	function confirmDialog() {
		return confirm("Are you sure you want to delete this data")
	}
	function confirmAdd() {
		return confirm("Are you sure you want to activate this data")
	}
	</script>


  </body>
</html>
