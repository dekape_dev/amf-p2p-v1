<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title><?php echo $menu_title; ?> | Administrator Area</title>
	<meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>css/app.v1.css">
	<link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>css/font.css" cache="false">
	<link rel="stylesheet" href="<?php echo $this->template->get_theme_path(); ?>css/style.css">
	<!--[if lt IE 9]> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/html5.js" cache="false"></script> <script src="js/ie/fix.js" cache="false"></script> <![endif]-->
</head>

<body>
	<?php echo $template['body']; ?>

	<!-- footer -->
	<footer id="footer">
		<div class="text-center padder clearfix">
			<p> <small>&copy; <?php echo date('Y');?>. Amartha.co.id. All Rights Reserved</small>
			</p>
		</div>
	</footer>
	<script src="<?php echo $this->template->get_theme_path(); ?>js/jquery.js"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo $this->template->get_theme_path(); ?>js/jquery.dataTables.min.js"></script>
	<script>
	$(document).ready(function(){
		$('.tbl').dataTable({
			"bSort": false
		  } );
	});
	</script>
</body>

</html>
