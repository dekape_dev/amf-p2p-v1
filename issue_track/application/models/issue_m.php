<?PHP
	class Issue_M extends CI_Model
	{
		//Property
		
		private $issue_id;
		private $issue_cat;
		private $issue_title;
		private $issue_desc;
		private $issue_level;
		private $issue_status;
		private $issue_user;
		private $issue_image;
		private $issue_responds;
		private $issue_date_new;
		private $issue_dev_clear;
		private $issue_date_clear;
		//Methode
		
		public function set_issue_id($value)
		{
			$this->issue_id = $value;
		}
		
		public function get_issue_id()
		{
			return $this->issue_id;
		}
		
		public function set_issue_cat($value)
		{
			$this->issue_cat = $value;
		}
		
		public function get_issue_cat()
		{
			return $this->issue_cat;
		}
		
		public function set_issue_title($value)
		{
			$this->issue_title = $value;
		}
		
		public function get_issue_title()
		{
			return $this->issue_title;
		}
		
		public function set_issue_desc($value)
		{
			$this->issue_desc = $value;
		}
		
		public function get_issue_desc()
		{
			return $this->issue_desc;
		}
		
		public function set_issue_level($value)
		{
			$this->issue_level = $value;
		}
		
		public function get_issue_level()
		{
			return $this->issue_level;
		}
		
		public function set_issue_status($value)
		{
			$this->issue_status = $value;
		}
		
		public function get_issue_status()
		{
			return $this->issue_status;
		}
		
		public function set_issue_user($value)
		{
			$this->issue_user = $value;
		}
		
		public function get_issue_user()
		{
			return $this->issue_user;
		}
		
		public function set_issue_image($value)
		{
			$this->issue_image = $value;
		}
		
		public function get_issue_image()
		{
			return $this->issue_image;
		}
		
		
		public function set_issue_responds($value)
		{
			$this->issue_responds = $value;
		}
		
		public function get_issue_responds()
		{
			return $this->issue_responds;
		}
		
		
		public function set_issue_date_new($value)
		{
			$this->issue_date_new = $value;
		}
		
		public function get_issue_date_new()
		{
			return $this->issue_date_new;
		}
		
		public function set_issue_dev_clear($value)
		{
			$this->issue_dev_clear = $value;
		}
		
		public function get_issue_dev_clear()
		{
			return $this->issue_dev_clear;
		}
		
		public function set_issue_date_clear($value)
		{
			$this->issue_date_clear = $value;
		}
		
		public function get_issue_date_clear()
		{
			return $this->issue_date_clear;
		}
		
		
		public function view()
		{
			$sql = "SELECT * FROM tbl_issue,tbl_cat,tbl_level,tbl_stat
					WHERE tbl_issue.issue_cat=tbl_cat.cat_id and tbl_issue.issue_level=tbl_level.level_id and tbl_issue.issue_status=tbl_stat.stat_id";
				
			
			return $this->db->query($sql);
		}
		
		public function view_by_issue_id()
		{
			$sql = "SELECT * FROM tbl_issue
					WHERE issue_id='".$this->get_issue_id()."'";
			
			return $this->db->query($sql);
		}
		
		public function insert()
		{
			$sql = "INSERT INTO tbl_issue
					( issue_cat, 
					issue_title,
					issue_desc,
					issue_level,
					issue_status,
					issue_user,
					issue_image,
					issue_responds,
					issue_date_new,
					issue_dev_clear,
					issue_date_clear) 
					VALUES ( '".$this->get_issue_cat()."',
					'".$this->get_issue_title()."',
					'".$this->get_issue_desc()."',
					'".$this->get_issue_level()."',
					'".$this->get_issue_status()."',
					'".$this->get_issue_user()."',
					'".$this->get_issue_image()."',
					'".$this->get_issue_responds()."',
					'".$this->get_issue_date_new()."',
					'".$this->get_issue_dev_clear()."', 
					'".$this->get_issue_date_clear()."')";
			
			$this->db->query($sql);
		}
		
		public function update()
		{
			$sql = "UPDATE tbl_issue 
					SET issue_cat='".$this->get_issue_cat()."', 
					issue_title='".$this->get_issue_title()."',
					issue_desc='".$this->get_issue_desc()."',
					issue_level='".$this->get_issue_level()."',
					issue_status='".$this->get_issue_status()."',
					issue_user='".$this->get_issue_user()."',
					issue_image='".$this->get_issue_image()."',
					issue_responds='".$this->get_issue_responds()."',
					issue_date_new='".$this->get_issue_date_new()."',
					issue_dev_clear='".$this->get_issue_dev_clear()."',
					issue_date_clear='".$this->get_issue_date_clear()."'
					WHERE issue_id='".$this->get_issue_id()."'";
			
			$this->db->query($sql);
		}
		
		//CATEGORY
		public function view_jenis()
		{
			$sql = "SELECT * FROM tbl_cat";
			
			return $this->db->query($sql);
		}
		
		public function view_by_id()
		{
			$sql = "SELECT * FROM tbl_issue, tbl_cat
					WHERE tbl_issue.issue_cat=tbl_cat.cat_id AND tbl_issue.issue_id='".$this->get_issue_id()."'";
			
			return $this->db->query($sql);
		}
		
		public function view_by_id_jenis()
		{
			$sql = "SELECT * FROM tbl_issue, tbl_cat
					WHERE tbl_issue.issue_cat=tbl_cat.cat_id AND tbl_issue.issue_cat='".$this->get_issue_cat()."' AND tbl_issue.issue_id='".$this->get_issue_id()."'";
			
			return $this->db->query($sql);
		}
		
		//LEVEL
		
		public function view_level()
		{
			$sql = "SELECT * FROM tbl_level";
			
			return $this->db->query($sql);
		}
		
		public function view_by_idlevel()
		{
			$sql = "SELECT * FROM tbl_issue, tbl_level
					WHERE tbl_issue.issue_level=tbl_level.level_id AND tbl_issue.issue_id='".$this->get_issue_id()."'";
			
			return $this->db->query($sql);
		}
		
		public function view_by_id_level()
		{
			$sql = "SELECT * FROM tbl_issue, tbl_level
					WHERE tbl_issue.issue_level=tbl_level.level_id AND tbl_issue.issue_level='".$this->get_issue_level()."' AND tbl_issue.issue_id='".$this->get_issue_id()."'";
			
			return $this->db->query($sql);
		}
		
		
		//STATUS
		
		public function view_status()
		{
			$sql = "SELECT * FROM tbl_stat";
			
			return $this->db->query($sql);
		}
		
		public function view_by_idstatus()
		{
			$sql = "SELECT * FROM tbl_issue, tbl_stat
					WHERE tbl_issue.issue_stat=tbl_stat.stat_id AND tbl_issue.issue_id='".$this->get_issue_id()."'";
			
			return $this->db->query($sql);
		}
		
		public function view_by_id_status()
		{
			$sql = "SELECT * FROM tbl_issue, tbl_stat
					WHERE tbl_issue.issue_stat=tbl_stat.stat_id AND tbl_issue.issue_stat='".$this->get_issue_stat()."' AND tbl_issue.issue_id='".$this->get_issue_id()."'";
			
			return $this->db->query($sql);
		}
		
		//STATUS 2
		
		
		public function view_statuss()
		{
			$sql = "SELECT * FROM tbl_stat WHERE stat_id='1'";
			
			return $this->db->query($sql);
		}
		
		public function view_by_idstatuss()
		{
			$sql = "SELECT * FROM tbl_issue, tbl_stat
					WHERE tbl_issue.issue_stat=tbl_stat.stat_id AND tbl_issue.issue_id='".$this->get_issue_id()."'";
			
			return $this->db->query($sql);
		}
		
		public function view_by_id_statuss()
		{
			$sql = "SELECT * FROM tbl_issue, tbl_stat
					WHERE tbl_issue.issue_stat=tbl_stat.stat_id AND tbl_issue.issue_stat='".$this->get_issue_stat()."' AND tbl_issue.issue_id='".$this->get_issue_id()."'";
			
			return $this->db->query($sql);
		}
		
		
		public function delete()
		{
			$sql = "DELETE FROM tbl_issue 
					WHERE issue_id='".$this->get_issue_id()."'";
			
			$this->db->query($sql);
		}
		
		public function delete_all()
		{
			$sql = "TRUNCATE TABLE tbl_issue";
			
			$this->db->query($sql);
		}
	}
?>