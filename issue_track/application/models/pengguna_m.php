<?PHP
	class Pengguna_M extends CI_Model
	{
		//Property
		
		private $user_id;
		private $user_name;
		private $user_pass;
		private $user_priv;
		
		//Methode
		
		public function set_user_id($value)
		{
			$this->user_id = $value;
		}
		
		public function get_user_id()
		{
			return $this->user_id;
		}
		
		public function set_user_name($value)
		{
			$this->user_name = $value;
		}
		
		public function get_user_name()
		{
			return $this->user_name;
		}
		
		public function set_user_pass($value)
		{
			$this->user_pass = $value;		
		}
		
		public function get_user_pass()
		{
			return $this->user_pass;	
		}
		
		public function set_user_priv($value)
		{
			$this->user_priv = $value;	
		}
		
		public function get_user_priv()
		{
			return $this->user_priv;	
		}
		
		public function view_by_username()
		{
			$sql = "SELECT * FROM tbl_user 
					WHERE user_name='".$this->get_user_name()."'";
			
			return $this->db->query($sql);
		}
		
		public function view_by_userpass()
		{
			$sql = "SELECT * FROM tbl_user 
					WHERE user_name='".$this->get_user_name()."' AND 
					user_pass='".md5($this->get_user_pass())."'";
			
			return $this->db->query($sql);
		}
		
		public function view_by_areaadmin()
		{
			$sql = "SELECT * FROM tbl_user
					WHERE user_name='".$this->get_user_name()."' AND user_pass='".$this->get_user_pass()."' AND
					user_priv='superuser'";
			
			return $this->db->query($sql);
		}
		
		public function view_by_areauser()
		{
			$sql = "SELECT * FROM tbl_user
					WHERE user_name='".$this->get_user_name()."' AND user_pass='".$this->get_user_pass()."' AND
					user_priv='user'";
			
			return $this->db->query($sql);
		}
		
		public function view_by_areadev()
		{
			$sql = "SELECT * FROM tbl_user
					WHERE user_name='".$this->get_user_name()."' AND user_pass='".$this->get_user_pass()."' AND
					user_priv='dev'";
			
			return $this->db->query($sql);
		}
		
		public function view_cek_id(){
			$sql="SELECT user_priv FROM tbl_user
					WHERE user_name='".$this->get_user_name()."' AND 
					user_pass='".md5($this->get_user_pass())."'";
					
			return $this->db->query($sql);
		}
		
		public function view_superuser()
		{
			$sql = "SELECT user_name,user_pass,user_priv FROM tbl_user
					WHERE tbl_user.user_priv='superuser'";
			
			return $this->db->query($sql);
		}
		
		public function view_user()
		{
			$sql = $sql = "SELECT user_name,user_pass,user_priv FROM tbl_user
					WHERE tbl_user.user_priv='user'";
			
			return $this->db->query($sql);
		}
		
		public function insert()
		{
			$sql = "INSERT INTO tbl_user 
					(user_name, 
					user_pass) 
					VALUES ('".$this->get_user_name()."', 
					'".md5($this->get_user_pass())."')";
			
			$this->db->query($sql);
		}
		
	}
?>