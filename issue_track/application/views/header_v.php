<!DOCTYPE html>
<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
		
        
    	<title>Online Issue Tracking | Amartha</title>
        
         <link href="<?PHP echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="<?PHP echo base_url(); ?>assets/css/datepicker.css" rel="stylesheet">
        <link href="<?PHP echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet">
         <link href="<?PHP echo base_url(); ?>assets/source/jquery.fancybox.css?v=2.1.5" media="screen">
        
        <script src="<?PHP echo base_url(); ?>assets/lib/jquery-1.10.1.min.js"></script> 
        <script src="<?PHP echo base_url(); ?>assets/source/jquery.fancybox.js?v=2.1.5"></script>        
        <script src="<?PHP echo base_url(); ?>assets/js/jquery-1.10.2.min.js"></script>            
        <script src="<?PHP echo base_url(); ?>assets/js/jquery.js"></script>
        <script src="<?PHP echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?PHP echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?PHP echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
        <script src="<?PHP echo base_url(); ?>assets/js/jquery.dataTables.js"></script>
        <script src="<?PHP echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
        <script src="<?PHP echo base_url(); ?>assets/js/countdown.js"></script>

		<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();
			
		</script>

    </head>
    <body>
    	<nav class="navbar navbar-default" role="navigation" style="margin-bottom: 0;">
        	<div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
						<img src="<?PHP echo base_url(); ?>assets/amartha/logo_amartha_white.png" alt="amartha" style="width: 50px">
    		
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <?PHP
						if($this->session->userdata("user_priv")=="user")
						{
						?>
                        <li<?PHP if($this->uri->segment(1) == "beranda" || $this->uri->segment(1) == "beranda") { echo ' class="active"'; } ?>>
                            <a href="<?PHP echo site_url(); ?>beranda">Dashboard</a>
                        </li>
                        <li<?PHP if($this->uri->segment(1) == "issue") { echo ' class="active"'; } ?>>
                            <a href="<?PHP echo site_url(); ?>issue"><span class="badge"><!--<?php echo 6 ?>--></span>Issue Report</a></li>
                        </li>
                        
                        <?PHP
						}
						else
						{
						
						?>
                        
                        <li<?PHP if($this->uri->segment(1) == "beranda" || $this->uri->segment(1) == "beranda") { echo ' class="active"'; } ?>>
                            <a href="<?PHP echo site_url(); ?>beranda">Dashboard</a>
                        </li>
                        <li<?PHP if($this->uri->segment(1) == "issue") { echo ' class="active"'; } ?>>
                            <a href="<?PHP echo site_url(); ?>issue"><span class="badge"><!--<?php echo 6 ?>--></span>Issue Report</a></li>
                        </li>
                         <li<?PHP if($this->uri->segment(1) == "issue_admin") { echo ' class="active"'; } ?>>
                            <a href="<?PHP echo site_url(); ?>issue_admin"><span class="badge"><!--<?php echo 6 ?>--></span>Issue Admin Report</a></li>
                        </li>
						<?PHP
						}
						?>
                       
                    </ul>
                    <div class="navbar-right">
                    	
                        <?PHP
							if($this->session->userdata("user_name") == "")
							{
						?>
						
						
                        
                        <a href="<?PHP echo site_url(); ?>masuk" class="btn btn-primary navbar-btn">
                            <span class="glyphicon glyphicon-log-in"></span> Log in
                        </a>
                        
                        <?PHP
							}
							else
							{
						?>
                        
                        <a href="<?PHP echo site_url(); ?>masuk/logout" class="btn btn-primary navbar-btn">
                            <span class="glyphicon glyphicon-log-out"></span> Log Out
                        </a>
                        
                        <span class="glyphicon glyphicon-user" style="color:#FFF"></span><h style="color:#FFF"> <?php echo $this->session->userdata("user_name") ?></h>
						
						<?PHP
							}
						?>
                        
						
                        
                    </div>
                </div>
            </div>
        </nav>
    	
