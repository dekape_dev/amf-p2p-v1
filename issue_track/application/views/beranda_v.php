<?php 
$this->load->view("header_v");
?>

<div class="container">
	<div class="panel panel-default">
    	<div class="panel-body">
            <div class="carousel slide" id="banner">
                <ol class="carousel-indicators">
                    <li data-target="#banner" data-slide-to="0" class="active"></li>
                    <li data-target="#banner" data-slide-to="1"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="<?PHP echo base_url(); ?>assets/img/bg_homepage-old.jpg" alt="">
                        <div class="carousel-caption">
                            <h2>Amartha Mikro Fintek</h2>
                            <!--<p>Mencari dan Memberi yang Terbaik</p-->
                        </div>
                    </div>
                    <div class="item">
                        <img src="<?PHP echo base_url(); ?>assets/img/bg_homepage-2.jpg" alt="">
                        <div class="carousel-caption">
                            <!--h3>Fintek 2</h3>
                            <p></p-->
                        </div>
                    </div>
                </div>
            </div>
    	</div>
    </div>
</div>
  
            

 <?PHP
 $this->load->view("footer_v");
 ?>           
             
 <script type="text/javascript">
	$(document).ready(function(e) {
		$('.carousel').carousel();
    });
</script>
                
