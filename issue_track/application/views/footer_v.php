		<div class="well" style="margin-bottom: 0;">
        	<div class="container">
            	<div class="row">
                	<div class="col-md-8">
                        <address>
                            <strong>PT.Amartha Mikro Fintek</strong><br />
                            Bukit Indraprasta D3/1, Telaga Kahuripan, Parung, Bogor West Java Indonesia<br />
                            info@amartha.co.id | +62 251 8618 111
                        </address>
                    </div>
                	<div class="col-md-4">
                        <p class="text-right">Copyright &copy;<?php echo date("Y");?></p>
                        <p class="text-right">Alpha Testing Mode</p>
                    </div>
                </div>
            </div>
        </div>
	</body>
</html>