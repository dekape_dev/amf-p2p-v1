<?PHP
	$this->load->view("header_v");
?>

<div class="well well-sm">
    <div class="container">
        <div class="row">
        	<div class="col-md-6">
                <ol class="breadcrumb" style="margin-bottom: 0;">
                    <li>
                        <a href="<?PHP echo site_url(); ?>beranda">
                        	<span class="glyphicon glyphicon-home"></span> Dashboard
                        </a>
                    </li>
                    <li class="active">
                        Issue Report
                    </li>
                </ol>
        	</div>
            <div class="col-md-6 text-right">
            	
                <?PHP
					if($this->session->userdata("user_priv") == "user")
					{
				?>
                
                <a href="#modal_issue" class="btn btn-default add" data-toggle="modal">
                	<span class="glyphicon glyphicon-plus"></span> Add Report
                </a>
                <!--<a href="#modal_confirm" class="btn btn-default delete_all" data-toggle="modal">
                	<span class="glyphicon glyphicon-trash"></span> Delete All
                </a>-->
                
                <?PHP
					}
					else
					{
				?>
                <!--<a href="#modal_confirm" class="btn btn-default delete_all" data-toggle="modal">
                	<span class="glyphicon glyphicon-trash"></span> Delete All
                </a>-->
                
                <?PHP
					}
				?>
                
            </div>
        </div>
    </div>
</div>
<div class="container">
	<div class="panel panel-default">
    	<div class="panel-body">
            
            <?PHP
                if(!empty($status))
                {
					if($status == "error")
					{
            ?>
            
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                ID telah ada
            </div>
            
            <?PHP
					}
					else if($status == "save_success")
					{
			?>
            
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Simpan Berhasil
            </div>
            
            <?PHP
					}
					else
					{
			?>
            
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Perbaharuan Berhasil
            </div>
            
            <?PHP
					}
				}
			?>
            
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                           
                            <th>Category</th>
                            <th>Issue</th>
                            <th>Description</th>
                            <th>Level</th>
                            <th>User</th>
                            <th>Image</th>
                            <th>Responds</th>
                            <th>PIC</th>
                            <th>Issue Date</th>
                            <th>Clear Date</th>
                            <th>Status</th>
                            
                            <?PHP
                                if($this->session->userdata("user_name") != "")
                                {
                            ?>
                            
                            <th>Modify</th>
                            
                            <?PHP
                                }
                            ?>
                            
                        </tr>
                    </thead>
                    <tbody>
                        
                        <?PHP
                            $query = $this->issue_m->view();
                            
                            if($query->num_rows())
							{
								foreach($query->result() as $row)
								{
                        ?>
                        
                        <tr>
                            <!--<td><?PHP echo $row->issue_id; ?></td>-->
                            <td>
                                
                                <?PHP echo $row->cat_name; ?>
                                
                                <input type="hidden" value="<?PHP echo $row->issue_cat; ?>" id="issue_cat_<?PHP echo $row->issue_id; ?>" />
                            </td>
                            <td>
                                
                                <?PHP echo $row->issue_title; ?>
                                
                                <input type="hidden" value="<?PHP echo $row->issue_title; ?>" id="issue_title_<?PHP echo $row->issue_id; ?>" />
                            </td>
                            <td>
                                
                                <?PHP echo $row->issue_desc; ?>
                                
                                <input type="hidden" value="<?PHP echo $row->issue_desc; ?>" id="issue_desc_<?PHP echo $row->issue_id; ?>" />
                            </td>
                            <td>
                                
                                <?PHP echo $row->level_name; ?>
                                
                                <input type="hidden" value="<?PHP echo $row->issue_level; ?>" id="issue_level_<?PHP echo $row->issue_id; ?>" />
                            </td>
                            <td>
                                
                                <?PHP echo $row->issue_user; ?>
                                
                                <input type="hidden" value="<?PHP echo $row->issue_user; ?>" id="issue_user_<?PHP echo $row->issue_id; ?>" />
                            </td>
                            
                            <td>
                                
                               <a href="<?PHP echo base_url(); ?>assets/img/<?PHP echo $row->issue_image; ?>" class="perbesar"><?PHP echo $row->issue_image; ?></a>
                                
                                <input type="hidden" value="<?PHP echo $row->issue_image; ?>" "<?php echo $row->issue_image;?>" id="issue_image_<?PHP echo $row->issue_id; ?>" />
                            </td>
                            
                             <td>
                                
                                <?PHP echo $row->issue_responds; ?>
                                
                                <input type="hidden" value="<?PHP echo $row->issue_responds; ?>" id="issue_responds_<?PHP echo $row->issue_id; ?>" />
                            </td>
                            <td>
                                
                                <?PHP echo $row->issue_dev_clear; ?>
                                
                                <input type="hidden" value="<?PHP echo $row->issue_dev_clear; ?>" id="issue_dev_clear_<?PHP echo $row->issue_id; ?>" />
                            </td>                            
                            <td>
                                
                                <?PHP echo $row->issue_date_new; ?>
                                
                                <input type="hidden" value="<?PHP echo $row->issue_date_new; ?>" id="issue_date_new_<?PHP echo $row->issue_id; ?>" />
                            </td>                              
                            <td>
                                
                                <?PHP echo $row->issue_date_clear; ?>
                                
                                <input type="hidden" value="<?PHP echo $row->issue_date_clear; ?>" id="issue_date_clear_<?PHP echo $row->issue_id; ?>" />
                            </td>
                            <td>
                                    
                                <?PHP 
                                if($row->issue_status == "1")
                                {
                                echo "<span style='background-color:red;'>Open</span>";
                                }
                                else
                                {
                                echo "<span style='background-color:#0F3;'>Closed</span>";   
                                }
                                ?>
                                
                                <input type="hidden" value="<?PHP echo $row->issue_status; ?>" id="issue_status_<?PHP echo $row->issue_id; ?>" />
                            </td>
                           

                           
                            
                            <?PHP
                                if($this->session->userdata("user_priv") == "user")
                                {
							?>
                            
                            <td>
                                <a href="#modal_issue" class="edit" data-toggle="modal" id="edit_<?PHP echo $row->issue_id; ?>">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a href="#modal_confirm" class="delete" data-toggle="modal" id="delete_<?PHP echo $row->issue_id; ?>">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                            
                            <?PHP
								}
							 else if($this->session->userdata("user_priv") == "admin")
							{
							?>
                            
                            <td>
                                <a href="#modal_issue" class="edit" data-toggle="modal" id="edit_<?PHP echo $row->issue_id; ?>">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                            </td>
                            
                            <?PHP
								}
							 else if($this->session->userdata("user_priv") == "dev")
							{
							?>
                            <td>
                            <a href="#modal_issue" class="edit" data-toggle="modal" id="edit_<?PHP echo $row->issue_id; ?>">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a href="#modal_confirm" class="delete" data-toggle="modal" id="delete_<?PHP echo $row->issue_id; ?>">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </a>
                            </td>
                            <?PHP
                                }
                            ?>
                            
                        </tr>
                        
                        <?PHP
                                }
                            }
							
                        ?>
                        
                    </tbody>
                    <tfoot>
                        <tr>
                           
                            <th>Category</th>
                            <th>Issue</th>
                            <th>Description</th>
                            <th>Level</th>
                            <th>User</th>
                            <th>Image</th>
                            <th>Responds</th>
                            <th>PIC</th>
                            <th>Issue Date</th>
                            <th>Clear Date</th>
                            <th>Status</th>
                           
                            <?PHP
                                if($this->session->userdata("user_name") != "")
                                {
                            ?>
                            
                            <th>Modify</th>
                            
                            <?PHP
                                }
                            ?>
                            
                        </tr>
                    </tfoot>
                </table>
            </div>
    	</div>
    </div>
</div>
<div class="modal fade" id="modal_issue" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-left">Form Issue</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" method="post" id="form_issue" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="issue_id" class="col-md-4">Issue ID</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="issue_id" id="issue_id" required />
                            <input type="hidden" name="issue_id_id" id="issue_id_id" />
                        </div>
                    </div>
                    <?PHP
					if($this->session->userdata("user_priv") == ("user"))
							{
					?>
                    <div class="form-group">
                        <label for="cat_name" class="col-md-4">Category</label>
                        <div class="col-md-8">
                            <select class="form-control" name="issue_cat" id="issue_cat">
                            	
                                <?PHP
									$query = $this->issue_m->view_jenis();
									
									if($query->num_rows())
									{
										foreach($query->result() as $row) :
								?>
                                
                                <option value="<?PHP echo $row->cat_id; ?>"><?PHP echo $row->cat_name; ?></option>
                                
                                <?PHP
										endforeach;
									}
								?>
                                
                            </select>
                        </div>
                    </div>
                    <?PHP
							}
							else
							{
					?>
                    <div class="form-group">
                        <label for="cat_name" class="col-md-4">Category</label>
                        <div class="col-md-8">
                            <select class="form-control" name="issue_cat" id="issue_cat" readonly="readonly">
                            	
                                <?PHP
									$query = $this->issue_m->view_jenis();
									
									if($query->num_rows())
									{
										foreach($query->result() as $row) :
								?>
                                
                                <option value="<?PHP echo $row->cat_id; ?>"><?PHP echo $row->cat_name; ?></option>
                                
                                <?PHP
										endforeach;
									}
								?>
                                
                            </select>
                        </div>
                    </div>
                    <?PHP
							}
					?>
                    <?PHP
                                if($this->session->userdata("user_priv") == "user")
                                {
					?>
                    <div class="form-group">
                        <label for="issue_title" class="col-md-4">Issue</label>
                        <div class="col-md-8">
                        	<input type="text" class="form-control" name="issue_title" id="issue_title"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="issue_desc" class="col-md-4">Description</label>
                        <div class="col-md-8">
                            <textarea type="text" class="form-control" name="issue_desc" id="issue_desc" required /></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="issue_level" class="col-md-4">Level</label>
                        <div class="col-md-8">
                            <select class="form-control" name="issue_level" id="issue_level">
                            	
                                <?PHP
									$query = $this->issue_m->view_level();
									
									if($query->num_rows())
									{
										foreach($query->result() as $row) :
								?>
                                
                                <option value="<?PHP echo $row->level_id; ?>"><?PHP echo $row->level_name; ?></option>
                                
                                <?PHP
										endforeach;
									}
								?>
                                
                            </select>
                        </div>
                    </div>
                    <?PHP
								}
								else
								{
					?>
                    <div class="form-group">
                        <label for="issue_title" class="col-md-4">Issue</label>
                        <div class="col-md-8">
                        	<input type="text" class="form-control" name="issue_title" id="issue_title" readonly="readonly"/>    
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="issue_desc" class="col-md-4">Description</label>
                        <div class="col-md-8">
                            <textarea type="hidden" class="form-control" name="issue_desc" id="issue_desc" readonly="readonly"/></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="issue_level" class="col-md-4">Level</label>
                        <div class="col-md-8">
                            <select type="hidden"class="form-control" name="issue_level" id="issue_level" readonly="readonly">
                            	
                                <?PHP
									$query = $this->issue_m->view_level();
									
									if($query->num_rows())
									{
										foreach($query->result() as $row) :
								?>
                                
                                <option value="<?PHP echo $row->level_id; ?>"><?PHP echo $row->level_name; ?></option>
                                
                                <?PHP
										endforeach;
									}
								?>
                                
                            </select>
                        </div>
                    </div>
                    <?PHP
								}
					?>
                    <?PHP
                                if($this->session->userdata("user_priv") == "user")
                                {
					?>
                    <div class="form-group">
                    	<label for="stat_name" class="col-md-4">Status</label>
                         <div class="col-md-8">
                          <?PHP
									$query = $this->issue_m->view_statuss();
									
									if($query->num_rows())
									{
										foreach($query->result() as $row)
									{
							?>
                           <input type="text" class="form-control" value="<?PHP echo $row->stat_name; ?>" name="issue_status_id" disabled="disabled"/>
                           <input type="hidden" class="form-control" value="<?PHP echo $row->stat_id; ?>" name="issue_status"/>
                           
                                <?PHP
									}
									}
								?>

                        </div>
                     </div> 
                    <?PHP
					}
					else	
					{
					?>
                     <div class="form-group">
                        <label for="stat_name" class="col-md-4">Status</label>
                        <div class="col-md-8">
                            <select class="form-control" name="issue_status" id="issue_status">
                            	
                                <?PHP
									$query = $this->issue_m->view_status();
									
									if($query->num_rows())
									{
										foreach($query->result() as $row) :
								?>
                                
                                <option value="<?PHP echo $row->stat_id; ?>"><?PHP echo $row->stat_name; ?></option>
                                
                                <?PHP
										endforeach;
									}
								?>
                                
                            </select>
                        </div>
                    </div>
                    <?PHP
					}
					?>
                    <?PHP
					if($this->session->userdata("user_priv")=="user")
						{
					?>
                     <div class="form-group">
                        <label for="issue_user" class="col-md-4">User</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="issue_user" id="issue_user" required />
                        </div>
                    </div>
                    <?PHP
						}
						else
						{
					?>
                     <div class="form-group">
                        <label for="issue_user" class="col-md-4">User</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="issue_user" id="issue_user" readonly="readonly" required />
                        </div>
                    </div>
                    <?PHP
						}
					?>
                     <div class="form-group">
                        <label for="iisue_image" class="col-md-4">Image</label>
                        <div class="col-md-8">
                        <input type="file"  id="userfile"  name="userfile" requaired />
  						  <p class="help-block">Choose Image </p>                  
                        </div>
                    </div>
                    <?PHP
                                if($this->session->userdata("user_priv") == "user")
                                {
					?>
                     <div class="form-group">
                        <!--<label for="issue_responds" class="col-md-4">Responds</label>
                        <div class="col-md-8">
                            <textarea type="text" class="form-control" name="issue_responds" id="issue_responds" required /></textarea>
                        </div>-->
                    </div>
                    <?PHP
								}
								else
								{
					?>
                     <div class="form-group">
                        <label for="issue_responds" class="col-md-4">Responds</label>
                        <div class="col-md-8">
                            <textarea type="text" class="form-control" name="issue_responds" id="issue_responds" required /></textarea>
                        </div>
                    </div>
                    <?PHP
								}
					?>
                    
                     <?PHP
                                if($this->session->userdata("user_priv") == "user")
                                {
					?>
                    <div class="form-group">
                        <label for="issue_date_new" class="col-md-4">Date</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="issue_date_new" id="issue_date_new" required />
                            <input type="hidden" name="issue_date_new_id" id="issue_date_new_id" />
                        </div>
                    </div>
                    <div class="form-group">
                        <!--<label for="issue_dev_clear" class="col-md-4">Developmet</label>-->
                        <div class="col-md-8">
                            <input type="hidden" class="form-control" name="issue_dev_clear" id="issue_dev_clear" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <!--<label for="issue_date_clear" class="col-md-4">Date Clear</label>-->
                        <div class="col-md-8">
                            <input type="hidden" class="form-control" name="issue_date_clear" id="issue_date_clear" required />
                        </div>
                    </div>
                    <?PHP
								}
								else
								{
					?>
                    <div class="form-group">
                        <label for="issue_date_new" class="col-md-4">Date</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="issue_date_new" id="issue_date_new" readonly="readonly" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="issue_dev_clear" class="col-md-4">PIC</label>
                        <div class="col-md-8">
                             <input type="text" class="form-control" name="issue_dev_clear" id="issue_dev_clear" required />
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="issue_date_clear" class="col-md-4">Clear Date</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" name="issue_date_clear" id="issue_date_clear" required />
                        </div>
                    </div>
                    <?PHP
								}
					?>
                  
						         
                
                
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" id="save" form="form_issue">
                	<span class="glyphicon glyphicon-floppy-disk"></span> Simpan
                </button>
                <button type="submit" class="btn btn-primary" id="update" form="form_issue">
                	<span class="glyphicon glyphicon-floppy-disk"></span> Perbaharui
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_confirm" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-left">Konfirmasi</h4>
            </div>
            <div class="modal-body">
                <p class="text-left" id="confirm_str">
                    Apakah Anda yakin akan menghapus semua data ?
                </p>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(null);" class="btn btn-primary" id="delete_all">Ok</a>
                <a href="javascript:void(null);" class="btn btn-primary" id="delete">Ok</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </div>
</div>

<?PHP
	$this->load->view("footer_v");
?>

<script type="text/javascript">
$(document).ready(function(e) {
		$("#issue_date_new").datepicker({
			format: "yyyy-mm-dd",
            startDate: 'date'
		});
        $("#issue_date_clear").datepicker({
            format: "yyyy-mm-dd",
            startDate: 'date'
        });
	
		$(document).ready(function(e) {
		$(".add").click(function() {
			$("#save").show();
			$("#update").hide();
			
			$("#issue_id").attr("disabled", true);
		
			
			$("#form_issue").attr("action", "<?PHP echo site_url(); ?>issue/save");
			
			$("#issue_id").val("");
			$("#issue_cat").val("");
			$("#issue_title").val("");
			$("#issue_desc").val("");
			$("#issue_level").val("");
			$("#issue_status").val("");
			$("#issue_user").val("");
			$("#issue_image").val("");
			$("#issue_responds").val("");
			$("#issue_date_new").val("");
			$("#issue_dev_clear").val("");
			$("#issue_date_clear").val("0000-00-00 00:00:00");
			
		});
		});
		  $(".edit").click(function() {
			$("#save").hide();
			$("#update").show();
			
			$("#issue_id").attr("disabled", true);
		
			
			
			$("#form_issue").attr("action", "<?PHP echo site_url(); ?>issue/update");
			
			var id = this.id.substr(5);
			
			$("#issue_id").val(id);
			$("#issue_id_id").val(id);
			$("#issue_cat").val($("#issue_cat_" + id).val())
			$("#issue_title").val($("#issue_title_" + id).val())
			$("#issue_desc").val($("#issue_desc_" + id).val())
			$("#issue_level").val($("#issue_level_" + id).val())
			$("#issue_status").val($("#issue_status_" + id).val())
			$("#issue_user").val($("#issue_user_" + id).val())
			$("#issue_image").val($("#issue_image_" + id).val())
			$("#issue_responds").val($("#issue_responds_" + id).val())
			$("#issue_date_new").val($("#issue_date_new_" + id).val())
			$("#issue_dev_clear").val($("#issue_dev_clear_" + id).val())
			$("#issue_date_clear").val($("#issue_date_clear_" + id).val());
				
		});
		
		$(".delete").click(function() {
			$("#delete").show();
			$("#delete_all").hide();
			
			$("#confirm_str").html("Apakah Anda yakin akan menghapus data ?");
			
			var id = this.id.substr(7);
			
			$("#issue_id").val(id);
		});
		
		$(".delete_all").click(function() {
			$("#delete").hide();
			$("#delete_all").show();
			
			$("#confirm_str").html("Apakah Anda yakin akan menghapus semua data ?");
		});
		

		$("#delete").click(function() {
			$.post("<?PHP echo site_url(); ?>issue/delete", { 
				issue_id: $("#issue_id").val() }, 
				function() {
					window.location = "<?PHP echo site_url(); ?>issue";
				}
			);
		});
		
		$("#delete_all").click(function() {
			$.post("<?PHP echo site_url(); ?>issue/delete_all", {}, 
				function() {
					window.location = "<?PHP echo site_url(); ?>issue";
				}
			);
		});
		
		$(".table").dataTable();
    });
</script>