<?php
$this->session->unset_userdata("user_name");
	
?>

<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        
    	<title>Login</title>
        
        <link href="<?PHP echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet" media="screen">
        <link href="<?PHP echo base_url(); ?>assets/css/datepicker.css" rel="stylesheet">
        <link href="<?PHP echo base_url(); ?>assets/css/jquery.dataTables.css" rel="stylesheet">
                
        <script src="<?PHP echo base_url(); ?>assets/js/jquery.js"></script>
        <script src="<?PHP echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?PHP echo base_url(); ?>assets/js/bootstrap-datepicker.js"></script>
        <script src="<?PHP echo base_url(); ?>assets/js/jquery.dataTables.js"></script>
        <script src="<?PHP echo base_url(); ?>assets/js/countdown.js"></script> 
        <script src="<?PHP echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
      
       
	
       
    </head>
    <body> 
    <div class="col-md-4 col-md-offset-4 form-login" style="margin-top: 100px;">
    	
    	<div class="outter-form-login">
  			<header class="panel-heading text-center" style="color:#909; font-size:15px;"><img src="<?PHP echo base_url(); ?>assets/amartha/logo_amartha.png" alt="amartha" style="width:200px">	</header>
           	<form class="inner-login" role="form" method="post" action="<?PHP echo site_url(); ?>masuk/login">
                 <div class="form-group" style="margin-top:20px;">
           				  <h3 align="center">
                          
                       	  </h3>
     	 		
                    	
                        <?PHP
							if(!empty($status))
							{
								if($status == "error")
								{
						?>
              
                        <div class="row">
                            <div class="col-md-3"></div>
								<div class="col-md-6">
								
									<div class="alert alert-danger alert-dismissable">
										<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
											Masuk Gagal
										
									</div>
								</div>
                            <div class="col-md-3"></div>
                        </div>
                        
                        <?PHP
								}
								else
								{
						?>
                        
                        <div class="row">
                            <div class="col-md-3"></div>
                            <div class="col-md-6">
                                <div class="alert alert-success alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    Masuk Berhasil
                                </div>
								
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                        
                        <?PHP
								}
							}
						?>
                    
               
                        <div class="form-group">
                    <input type="text" class="form-control" name="user_name" placeholder="Username">
                </div>
 
                <div class="form-group">
                    <input type="password" class="form-control" name="user_pass" placeholder="Password">
                </div>
                
                <input type="submit" class="btn btn-block btn-custom-green" value="LOGIN" />
                 <div class="text-center forget">
                    <p> <small>&copy; <?php echo date("Y");?>. Amartha Mikro Fintek. All Rights Reserved</small></p>
                </div>
            </form>
        </div>
    </div>
 
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

<script type="text/javascript">
	$(document).ready(function(e) {
		
    });
</script>           
               
               
                
