<?PHP
	if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Issue extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			
			//Model
			
			$this->load->model("issue_m");
			$this->load->helper(array('form', 'url'));
		
		}
		
		public function index()
		{
			$this->load->view("issue_v");
		}
		
		public function upload()
		{
			$config['upload_path'] = './assets/img';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '100';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
			
			$this->load->library("upload", $config);
			
			if($this->upload->do_upload("issue_image"))
			{
				$data = $this->upload->data();
				
				$this->issue_m->set_issue_image($data["file_name"]);
				
				$this->issue_m->insert();
				
				redirect("issue");
			}
			else
			{
				echo $this->upload->display_errors();
			}
		}
		
		/*
		public function save()
		{
			$this->issue_m->set_issue_id($this->input->post("issue_id"));
			
			$query = $this->issue_m->view_by_issue_id();
			
			if($query->num_rows())
				$data["status"] = "error";
			else
			{
				$this->issue_m->set_issue_cat($this->input->post("issue_cat"));
				$this->issue_m->set_issue_title($this->input->post("issue_title"));
				$this->issue_m->set_issue_desc($this->input->post("issue_desc"));
				$this->issue_m->set_issue_level($this->input->post("issue_level"));
				$this->issue_m->set_issue_status($this->input->post("issue_status"));
				$this->issue_m->set_issue_user($this->input->post("issue_user"));
				$this->issue_m->set_issue_image($this->input->post("issue_image"));
				$this->issue_m->set_issue_date_new($this->input->post("issue_date_new"));
				$this->issue_m->set_issue_dev_clear($this->input->post("issue_dev_clear"));
				$this->issue_m->set_issue_date_clear($this->input->post("issue_date_clear"));

				$this->issue_m->insert();
				
				$data["status"] = "save_success";
				
			}
			
			$this->load->view("issue_v", $data);
			//redirect('issue');
		}*/
		
		public function save()
		{
			
			$config['upload_path'] = './assets/img';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '10000';
			
			
			
			
				//$data = $this->upload->data();
				$this->issue_m->set_issue_cat($this->input->post("issue_cat"));
				$this->issue_m->set_issue_title($this->input->post("issue_title"));
				$this->issue_m->set_issue_desc($this->input->post("issue_desc"));
				$this->issue_m->set_issue_level($this->input->post("issue_level"));
				$this->issue_m->set_issue_status($this->input->post("issue_status"));
				$this->issue_m->set_issue_user($this->input->post("issue_user"));
				//$this->issue_m->set_issue_image($data["file_name"]);
				$this->issue_m->set_issue_responds($this->input->post("issue_responds"));
				$this->issue_m->set_issue_date_new($this->input->post("issue_date_new"));
				$this->issue_m->set_issue_dev_clear($this->input->post("issue_dev_clear"));
				$this->issue_m->set_issue_date_clear($this->input->post("issue_date_clear"));
				$this->load->library("upload", $config);	
					
							
				$query = $this->issue_m->view_by_issue_id();
				
				if($this->upload->do_upload("userfile"))
			{
				$data = $this->upload->data();
				if($data["file_name"]!= "")
				{
					
					
					$this->issue_m->set_issue_image($data["file_name"]);
					
				}
			}
			else
			{
					$this->issue_m->set_issue_image($this->input->post("issue_image"));
					 
			}
				
				$this->issue_m->insert();
					
					$data["status"] = "save_success";
					
				
				$data["query"]=$this->issue_m->view();
				$this->load->view("issue_v", $data);
			}
			
			
			
		
		
		public function update()
		{
			$config['upload_path'] = './assets/img';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '10000';
		
			
				$this->issue_m->set_issue_id($this->input->post("issue_id_id"));
				$this->issue_m->set_issue_cat($this->input->post("issue_cat"));
				$this->issue_m->set_issue_title($this->input->post("issue_title"));
				$this->issue_m->set_issue_desc($this->input->post("issue_desc"));
				$this->issue_m->set_issue_level($this->input->post("issue_level"));
				$this->issue_m->set_issue_status($this->input->post("issue_status"));
				$this->issue_m->set_issue_user($this->input->post("issue_user"));
				$this->issue_m->set_issue_responds($this->input->post("issue_responds"));
				$this->issue_m->set_issue_date_new($this->input->post("issue_date_new"));
				$this->issue_m->set_issue_dev_clear($this->input->post("issue_dev_clear"));
				$this->issue_m->set_issue_date_clear($this->input->post("issue_date_clear"));
				$this->load->library("upload", $config);
				
				if($this->upload->do_upload("userfile"))
			{
				$data = $this->upload->data();
				if($data["file_name"]!= "")
				{
					
					
					$this->issue_m->set_issue_image($data["file_name"]);
					
				}
			}
			else
			{
					$this->issue_m->set_issue_image($this->input->post("issue_image"));
					 
			}
			$this->issue_m->update();
					
			$data["status"] = "update_success";
			$data["query"]=$this->issue_m->view();
			$this->load->view("issue_v", $data);
			
		}
			
		public function delete()
		{
			$this->issue_m->set_issue_id($this->input->post("issue_id"));
			
			$this->issue_m->delete();
		}
		
		public function delete_all()
		{
			$this->issue_m->delete_all();
		}
	}
?>