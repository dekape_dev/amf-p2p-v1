<?PHP
	if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Masuk extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			
			//Model
			
			$this->load->model("pengguna_m");
		}
		
		public function index()
		{
			$this->load->view("masuk_v");
		}
		
		public function login()
		{
			$this->pengguna_m->set_user_name($this->input->post("user_name"));
			$this->pengguna_m->set_user_pass($this->input->post("user_pass"));
			
			$query = $this->pengguna_m->view_by_userpass();
			
			if($query->num_rows())
			{
				$this->session->set_userdata("user_name", $this->input->post("user_name"));
				$this->session->set_userdata("user_priv", $this->cek_hak_akses());

				
				$data["status"] = "success";
				$this->load->view("beranda_v", $data);
				
			}

			
			else
			{
				$data["status"] = "error";
				$this->load->view("masuk_v", $data);
			}
		}
		
		public function cek_hak_akses(){
			$query=$this->pengguna_m->view_cek_id();
			
			if($query->num_rows()){
				foreach($query->result() as $row){
					if($row->user_priv=="user")
						return "user";
					else if($row->user_priv=="admin")
							return "admin";
					else if($row->user_priv=="dev")
							return "dev";
				}
			}
		}
		
		
		public function logout()
		{
			$this->session->unset_userdata("user_name");
			
			$this->session->sess_destroy();
			
			redirect(site_url());
		}
	}
?>