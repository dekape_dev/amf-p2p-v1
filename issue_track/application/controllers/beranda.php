<?PHP
	if(!defined('BASEPATH')) exit('No direct script access allowed');
	
	class Beranda extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();
			
			//Model
		}
		
		public function index()
		{
			$this->load->view('beranda_v');
		}
	}
?>