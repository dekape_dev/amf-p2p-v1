-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 04, 2016 at 08:15 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_issue_track`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cat`
--

CREATE TABLE IF NOT EXISTS `tbl_cat` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(50) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_cat`
--

INSERT INTO `tbl_cat` (`cat_id`, `cat_name`) VALUES
(1, 'Peer-to-Peer Platform'),
(2, 'Management Information System (MIS)'),
(3, 'Android Application'),
(4, 'IT Hardware/Infrastruktur'),
(5, 'Others');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_issue`
--

CREATE TABLE IF NOT EXISTS `tbl_issue` (
  `issue_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id Issue',
  `issue_cat` varchar(255) DEFAULT NULL COMMENT 'Jenis Kategori',
  `issue_title` varchar(255) DEFAULT NULL COMMENT 'Judul Issue',
  `issue_desc` text COMMENT 'Deskripsi Issue',
  `issue_level` varchar(10) DEFAULT NULL COMMENT 'Level Issue (H/M/L)',
  `issue_status` varchar(10) DEFAULT NULL COMMENT 'Status (open/closed)',
  `issue_user` varchar(100) DEFAULT NULL COMMENT 'Siapa yang lapor',
  `issue_image` varchar(100) DEFAULT NULL COMMENT 'upload photo',
  `issue_responds` text,
  `issue_date_new` datetime DEFAULT NULL COMMENT 'Kapan dilaporkan',
  `issue_dev_clear` varchar(100) DEFAULT NULL COMMENT 'Siapan yang perbaiki',
  `issue_date_clear` datetime DEFAULT NULL COMMENT 'Kapan perbaiki (saat ubah status jadi closed)',
  PRIMARY KEY (`issue_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_issue`
--

INSERT INTO `tbl_issue` (`issue_id`, `issue_cat`, `issue_title`, `issue_desc`, `issue_level`, `issue_status`, `issue_user`, `issue_image`, `issue_responds`, `issue_date_new`, `issue_dev_clear`, `issue_date_clear`) VALUES
(1, '1', 'My wallet', 'When clicking top up my wallet - i got the validation error\r\n', '1', '1', 'user', 'no_image.jpg', '', '2016-04-04 00:00:00', '', '0000-00-00 00:00:00'),
(2, '1', 'My wallet ', 'History of e-wallet transaction : ini transaction siapa? \r\n', '1', '1', 'user', 'no_image1.jpg', '', '2016-04-04 00:00:00', '', '0000-00-00 00:00:00'),
(3, '1', 'Akad AHA', 'Contoh Ibu Mariah - bagian akad poin 6.2 dan 6.3 belum lengkap : 6.2 Ujrah = bunga 30% ditampilkan dengan angka langsung sudah terhitung, 6.3 total pokok + ujrah, ditampilkan dengan angka. \r\n', '1', '1', 'user', 'no_image2.jpg', '', '2016-04-04 00:00:00', '', '0000-00-00 00:00:00'),
(4, '1', 'Akad MBA', '(sama dengan point 3)Contoh Ibu Een - bagian akad poin 6.2 dan 6.3 belum lengkap : 6.2 Ujrah = bunga 30% ditampilkan dengan angka langsung sudah terhitung, 6.3 total pokok + ujrah, ditampilkan dengan angka. \r\n', '1', '1', 'user', 'no_image3.jpg', '', '2016-04-04 00:00:00', '', '0000-00-00 00:00:00'),
(5, '1', 'SSUP', 'Format penulisan memang beda ya? Di poin 8.7 - 8.9, 9.2, 10.1, 10.2, 10.7 \r\n', '1', '1', 'user', 'no_image4.jpg', '', '2016-04-04 00:00:00', '', '0000-00-00 00:00:00'),
(6, '1', 'Payment - veritrans', 'Waktu klik back ketendang keluar, ke page login. Apa prosesnya memang begitu.\r\n', '1', '1', 'user', 'no_image7.jpg', '', '2016-04-04 00:00:00', '', '0000-00-00 00:00:00'),
(7, '1', 'Payment - veritrans', 'Kalau di instruksi harus lewat ATM atau online banking. Apakah memungkinkan transfer langsung/ kliring? Konsider koperasi pasti akan transfer langsung dengan amount yang besar. \r\n', '1', '1', 'user', 'no_image8.jpg', '', '2016-04-04 00:00:00', '', '0000-00-00 00:00:00'),
(8, '1', 'Follw up after payment', 'setelah bayar apa langsung ter-detect? Atau investor harus laporaon ke sistem? Mungkin harus ada semacam note - dana otomatis masuk bila sudah melakukan pembayaran.. \r\n', '1', '1', 'user', 'no_image10.jpg', '', '2016-04-04 00:00:00', '', '0000-00-00 00:00:00'),
(9, '1', 'Top up my wallet', 'Klik top up my wallet - masukin angka 1jt. Bila dana 0, lalu mau top up hanya 1 juta bisa ya? Atau harus ada minimum 3 juta? \r\n', '1', '1', 'user', 'no_image11.jpg', '', '2016-04-04 00:00:00', '', '0000-00-00 00:00:00'),
(10, '1', 'Input Tambal Sulam Error', 'Input anggota baru tambal sulam terkadang error dan tidak bisa di save. Sistem membaca bahwa terjadi duplicate primary key pada no_rekening client.', '1', '1', 'user', 'error_tambal_sulam.png', '', '2016-04-04 00:00:00', '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_level`
--

CREATE TABLE IF NOT EXISTS `tbl_level` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(10) NOT NULL,
  PRIMARY KEY (`level_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_level`
--

INSERT INTO `tbl_level` (`level_id`, `level_name`) VALUES
(1, 'High'),
(2, 'Medium'),
(3, 'Low');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stat`
--

CREATE TABLE IF NOT EXISTS `tbl_stat` (
  `stat_id` int(11) NOT NULL AUTO_INCREMENT,
  `stat_name` varchar(10) NOT NULL,
  PRIMARY KEY (`stat_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_stat`
--

INSERT INTO `tbl_stat` (`stat_id`, `stat_name`) VALUES
(1, 'Open'),
(2, 'Closed');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_priv` varchar(100) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `user_pass`, `user_priv`) VALUES
(4, 'user', '6ad14ba9986e3615423dfca256d04e3f', 'user'),
(5, 'admin', '0192023a7bbd73250516f069df18b500', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
