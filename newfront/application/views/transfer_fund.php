	<!-- *****************************************************************************************************************
	 CONTENTWRAP
	 ********************************************************************************************************************* -->
	<div id="content-wrap">
	    <div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="text-center header-title title">
						<span>TRANSFER<strong> FUND</strong></span>
					</div> 			
				</div>
			</div><!-- /row -->
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="box-white">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="box-title text-center color-title">
								<span><b>MY WALLET</b></span>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 height-space-paragraph">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-left">MY WALLET DISBURSEMENT</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-right">Rp 0</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-gray-small height-space-paragraph">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-left">MY WALLET AVAILABLE FUND</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-right">Rp 4,000,000</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 height-space-paragraph">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-left"><b>MY WALLET ACCUMULATION</b></div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-right">Rp 0</div>
							</div>
						</div>
					</div> 			
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="box-white">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="box-title text-center color-title">
								<span><b>MY INVESTMENT</b></span>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 height-space-paragraph">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-left">MY PROSFECTIVE BORROWERS</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-right with-padding">Rp 0</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-gray-small height-space-paragraph">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-left">MY INVESTMENT FUND</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-right">Rp 0</div>
							</div>
						</div>
					</div>		
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 height-space-paragraph">
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 height-space-paragraph">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-buttom">
						<div class="btn btn-block btn-purple text-right">
		 					<a>FUND TRANSFER</a>
		 				</div>
				 	</div>
				</div>
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /contentwrap -->
	<style type="text/css">
	.with-padding{
		margin-right: 30px;
	}
	.color-title{
		color: #451b55;
		font-size: 20px;
	}
	.title{
		font-size: 28px;
	}
	.height-space-paragraph{
		padding-top: 7px;
		font-size: 14px;
	}
	.padding-buttom{
		padding-top: 50px;
	}
	</style>