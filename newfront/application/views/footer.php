	<!-- *****************************************************************************************************************
	 FOOTER
	 ***************************************************************************************************************** -->
	 <div id="footerwrap">
	 	<div class="container">
		 	<div class="row">
		 		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		 			<img src="<?php echo IMG_URI; ?>footer-logo.png"></img>
		 		</div>
		 		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		 			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 none-padding-right">
		 				<div class="btn btn-block btn-white">
		 					<a>START INVESTING</a>
		 				</div>
		 			</div>
			 		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 none-padding-right">
		 				<div class="btn btn-block btn-purple">
		 					<a>START BORROWING</a>
		 				</div>
		 			</div>
			 	</div>
		 	</div><!--/row -->
		 	<div class="row">
		 		<div class="col-lg-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
		 			<div class="navbar-left">
		 				<div class="nav navbar-icon">
			 				<a href="#"><i class="fa fa-facebook fa-lg"></i></a>
			 				<a href="#"><i class="fa fa-twitter fa-lg"></i></a>
							<a href="#"><i class="fa fa-youtube-play fa-lg"></i></a>
							<a href="#"><i class="fa fa-linkedin fa-lg"></i></a>
						</div>
		 			</div>
		 		</div>
		 		<div class="col-lg-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
	                <div class="navbar-right">
	                    <ul class="nav navbar-footer">
	                        <li><a href="#">Terms</a></li>
	                        <li><a href="#">FAQ</a></li>
	                        <li><a href="#">Privacy Policy</a></li>
	                    </ul>
	                </div>
		 		</div>
		 	</div><!--/row -->
		 	<div class="row">
		 		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		 			<div class="purple-line">
		 			</div>
		 		</div>
		 	</div><!--/row -->
		 	<div class="row">
		 		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		 			<div class="text-left text-copyright">
		 				&copy; 2016. Amartha Microfinance. All Right Reserved. 
		 			</div>
		 		</div>
		 		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			 		<div class="navbar-right">
	                    <ul class="nav navbar-footer-language">
	                        <li class="active"><a href="#">ENG</a></li>
	                        <li><a href="#">IDN</a></li>
	                    </ul>
	                </div>
		 		</div>
		 	</div><!--/row -->
	 	</div><!--/container -->
	 </div><!--/footerwrap -->

	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo JS_LIBRARY_URI; ?>jquery/jquery-1.12.3.min.js"></script>
    <script src="<?php echo JS_LIBRARY_URI; ?>bootstrap/bootstrap.min.js"></script>
	<script src="<?php echo JS_PLUGINS_URI; ?>retina-1.1.0.js"></script>
	<script src="<?php echo JS_PLUGINS_URI; ?>jquery.hoverdir.js"></script>
	<script src="<?php echo JS_PLUGINS_URI; ?>jquery.hoverex.min.js"></script>
	<script src="<?php echo JS_PLUGINS_URI; ?>jquery.prettyPhoto.js"></script>
  	<script src="<?php echo JS_PLUGINS_URI; ?>jquery.isotope.min.js"></script>
  	<script src="<?php echo JS_APP; ?>custom.js"></script>


    <script>
// Portfolio
(function($) {
	"use strict";
	var $container = $('.portfolio'),
		$items = $container.find('.portfolio-item'),
		portfolioLayout = 'fitRows';
		
		if( $container.hasClass('portfolio-centered') ) {
			portfolioLayout = 'masonry';
		}
				
		$container.isotope({
			filter: '*',
			animationEngine: 'best-available',
			layoutMode: portfolioLayout,
			animationOptions: {
			duration: 750,
			easing: 'linear',
			queue: false
		},
		masonry: {
		}
		}, refreshWaypoints());
		
		function refreshWaypoints() {
			setTimeout(function() {
			}, 1000);   
		}
				
		$('nav.portfolio-filter ul a').on('click', function() {
				var selector = $(this).attr('data-filter');
				$container.isotope({ filter: selector }, refreshWaypoints());
				$('nav.portfolio-filter ul a').removeClass('active');
				$(this).addClass('active');
				return false;
		});
		
		function getColumnNumber() { 
			var winWidth = $(window).width(), 
			columnNumber = 1;
		
			if (winWidth > 1200) {
				columnNumber = 5;
			} else if (winWidth > 950) {
				columnNumber = 4;
			} else if (winWidth > 600) {
				columnNumber = 3;
			} else if (winWidth > 400) {
				columnNumber = 2;
			} else if (winWidth > 250) {
				columnNumber = 1;
			}
				return columnNumber;
			}       
			
			function setColumns() {
				var winWidth = $(window).width(), 
				columnNumber = getColumnNumber(), 
				itemWidth = Math.floor(winWidth / columnNumber);
				
				$container.find('.portfolio-item').each(function() { 
					$(this).css( { 
					width : itemWidth + 'px' 
				});
			});
		}
		
		function setPortfolio() { 
			setColumns();
			$container.isotope('reLayout');
		}
			
		$container.imagesLoaded(function () { 
			setPortfolio();
		});
		
		$(window).on('resize', function () { 
		setPortfolio();          
	});
})(jQuery);
</script>
  </body>
</html>