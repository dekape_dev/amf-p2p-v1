<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>AMARTHA</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo CSS_URI; ?>bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo CSS_URI; ?>style.css" rel="stylesheet">
    <link href="<?php echo CSS_URI; ?>font-awesome.min.css" rel="stylesheet">


    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>

    <!-- Fixed navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand"><img src="<?php echo IMG_URI; ?>header-logo.png"></img></a>
            </div>
            <div class="navbar-collapse collapse navbar-right">
                <ul class="nav navbar-nav">
                    <li><a>ABOUT</a></li>
                    <li><a>IMPACTS</a></li>
                    <li><a>BORROW</a></li>
                    <li><a>INVEST</a></li>
                    <li><a><span><i class="fa fa-sign-out"></i></span> LOG OUT</a></li>
                
                
                    <!-- <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">PAGES <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="blog.html">BLOG</a></li>
                        <li><a href="single-post.html">SINGLE POST</a></li>
                        <li><a href="portfolio.html">PORTFOLIO</a></li>
                        <li><a href="single-project.html">SINGLE PROJECT</a></li>
                      </ul>
                    </li> -->
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
    <div class="clearfix"></div>
    <!-- Second navber -->
    <div id="second-navbar-wrapper">
        <div class="navbar navbar-blue">
            <div class="container">
                <div class="navbar-left">
                    <ul class="nav navbar-nav">
                        <li><a>My Wallet</a></li>
                        <li><a>Portofolio</a></li>
                        <li><a>History</a></li>
                        <li><a>Market Performance</a></li>
                        <!-- <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">PAGES <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="blog.html">BLOG</a></li>
                            <li><a href="single-post.html">SINGLE POST</a></li>
                            <li><a href="portfolio.html">PORTFOLIO</a></li>
                            <li><a href="single-project.html">SINGLE PROJECT</a></li>
                          </ul>
                        </li> -->
                    </ul>
                </div>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a class="active">Hi, Andi Taufan Rp (Rp 4.200.000)</a></li>
                        <li><a><i class="fa fa-search"></i></a></li>
                        <li><a><i class="fa fa-cog"></i></a></li>
                    
                    
                        <!-- <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">PAGES <b class="caret"></b></a>
                          <ul class="dropdown-menu">
                            <li><a href="blog.html">BLOG</a></li>
                            <li><a href="single-post.html">SINGLE POST</a></li>
                            <li><a href="portfolio.html">PORTFOLIO</a></li>
                            <li><a href="single-project.html">SINGLE PROJECT</a></li>
                          </ul>
                        </li> -->
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </div>