	<!-- *****************************************************************************************************************
	 CONTENTWRAP
	 ***************************************************************************************************************** -->
	<div id="content-wrap">
	    <div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="text-center header-title">
						<span>MY<strong> WALLET</strong></span>
					</div> 			
				</div>
			</div><!-- /row -->
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="box-white">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="box-title text-left">
								<span>MY ACCOUNT <strong>#15000012</strong></span>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-left">Saldo My Wallet</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-right">Rp 4.200.000</div>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 box-gray-small">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-left"><strong>Total My Wallet</strong></div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="text-right"><strong>Rp 4.200.000</strong></div>
							</div>
						</div>
					</div> 			
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="box-gray">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="box-title text-left">
								<span>TOP UP<strong> E-WALLET</strong></span>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="text-left small-margin-bottom">
								<span>Top Up Amount (IDR)</span>
							</div>
						</div>
						<form id="top-up-form">
							<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
								<input type="number" id="top_up_amount" name="top_up_amount" class="form-control" placeholder="Min. Amount Rp 50.000">
							</div>
							<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
								<div class="btn btn-block btn-purple">
				 					<a>SUBMIT</a>
				 				</div>
				 			</div>
		 				</form>
					</div> 			
				</div>
			</div><!-- /row -->
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="table-responsive">
					 	<table class="table table-my-wallet">
					 		<thead>
						 		<tr>
						 			<td>No</td>
						 			<td>Debet</td>
						 			<td>Credit</td>
						 			<td>Balance</td>
						 			<td>Remark</td>
						 			<td>Time</td>
						 			<td>Status</td>
						 			<td>Channel</td>
						 		</tr>
					 		</thead>
					 		<tbody>
					 			<tr>
					 				<td>1.</td>
						 			<td>Rp 1.000.000</td>
						 			<td>Rp. 0</td>
						 			<td><strong>Rp 1.000.000 </strong></td>
						 			<td>Top up Andi Taufan 1.000.000</td>
						 			<td>22 Mar 2016 17:22:14</td>
						 			<td>Success</td>
						 			<td>E-Wallet</td>
					 			</tr>
					 			<tr>
					 				<td>2.</td>
						 			<td>Rp 1.000.000</td>
						 			<td>Rp. 0</td>
						 			<td><strong>Rp 1.000.000 </strong></td>
						 			<td>Top up Andi Taufan 1.000.000</td>
						 			<td>22 Mar 2016 17:22:14</td>
						 			<td>Success</td>
						 			<td>E-Wallet</td>
					 			</tr>
					 			<tr>
					 				<td>3.</td>
						 			<td>Rp 1.000.000</td>
						 			<td>Rp. 0</td>
						 			<td><strong>Rp 1.000.000 </strong></td>
						 			<td>Top up Andi Taufan 1.000.000</td>
						 			<td>22 Mar 2016 17:22:14</td>
						 			<td>Success</td>
						 			<td>E-Wallet</td>
					 			</tr>
					 			<tr>
					 				<td>4.</td>
						 			<td>Rp 1.000.000</td>
						 			<td>Rp. 0</td>
						 			<td><strong>Rp 1.000.000 </strong></td>
						 			<td>Top up Andi Taufan 1.000.000</td>
						 			<td>22 Mar 2016 17:22:14</td>
						 			<td>Success</td>
						 			<td>E-Wallet</td>
					 			</tr>
					 			<tr>
					 				<td>5.</td>
						 			<td>Rp 1.000.000</td>
						 			<td>Rp. 0</td>
						 			<td><strong>Rp 1.000.000 </strong></td>
						 			<td>Top up Andi Taufan 1.000.000</td>
						 			<td>22 Mar 2016 17:22:14</td>
						 			<td>Success</td>
						 			<td>E-Wallet</td>
					 			</tr>
					 			<tr>
					 				<td>6.</td>
						 			<td>Rp 1.000.000</td>
						 			<td>Rp. 0</td>
						 			<td><strong>Rp 1.000.000 </strong></td>
						 			<td>Top up Andi Taufan 1.000.000</td>
						 			<td>22 Mar 2016 17:22:14</td>
						 			<td>Success</td>
						 			<td>E-Wallet</td>
					 			</tr>
					 			<tr>
					 				<td>7.</td>
						 			<td>Rp 1.000.000</td>
						 			<td>Rp. 0</td>
						 			<td><strong>Rp 1.000.000 </strong></td>
						 			<td>Top up Andi Taufan 1.000.000</td>
						 			<td>22 Mar 2016 17:22:14</td>
						 			<td>Success</td>
						 			<td>E-Wallet</td>
					 			</tr>
					 			<tr>
					 				<td>8.</td>
						 			<td>Rp 1.000.000</td>
						 			<td>Rp. 0</td>
						 			<td><strong>Rp 1.000.000 </strong></td>
						 			<td>Top up Andi Taufan 1.000.000</td>
						 			<td>22 Mar 2016 17:22:14</td>
						 			<td>Success</td>
						 			<td>E-Wallet</td>
					 			</tr>
					 			<tr>
					 				<td>9.</td>
						 			<td>Rp 1.000.000</td>
						 			<td>Rp. 0</td>
						 			<td><strong>Rp 1.000.000 </strong></td>
						 			<td>Top up Andi Taufan 1.000.000</td>
						 			<td>22 Mar 2016 17:22:14</td>
						 			<td>Success</td>
						 			<td>E-Wallet</td>
					 			</tr>
					 			<tr>
					 				<td>10.</td>
						 			<td>Rp 1.000.000</td>
						 			<td>Rp. 0</td>
						 			<td><strong>Rp 1.000.000 </strong></td>
						 			<td>Top up Andi Taufan 1.000.000</td>
						 			<td>22 Mar 2016 17:22:14</td>
						 			<td>Success</td>
						 			<td>E-Wallet</td>
					 			</tr>
					 		</tbody>
					  	</table>
					  	<nav class="nav navbar-right">
					    	<ul class="pagination">
					    		<li>
					      			<a href="#" aria-label="Previous">
					        			<span aria-hidden="true">&laquo;</span>
					      			</a>
					    		</li>
							    <li class="active"><a href="#">1</a></li>
							    <li><a href="#">2</a></li>
							    <li><a href="#">3</a></li>
							    <li><a href="#">4</a></li>
							    <li><a href="#">5</a></li>
							    <li>
					      			<a href="#" aria-label="Next">
					        			<span aria-hidden="true">&raquo;</span>
					      			</a>
					    		</li>
					  		</ul>
						</nav>
					</div>			
				</div>
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /contentwrap -->