<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
| http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
| $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
| $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller']   = "home";
$route['language/(:any)']      = "home/switchLang/$1";
/*$route['404_override']       = 'home';*/

// Static Articles
$route['about']                       = 'home/about';
$route['impact']                      = 'home/impact';
$route['payment']                     = 'home/payment';
$route['borrowing']                   = 'home/borrow';
$route['borrow/letmein']              = 'borrower/login';
$route['borrow/letmein_to_borrow']    = 'borrower/VerifyLogin';
$route['borrow/([0-9a-zA-Z_-]+)']     = 'home/borrow/$1';
$route['formborrow/([0-9a-zA-Z_-]+)'] = 'home/formborrow/$1';

// $route['story']                    = 'home/story';
$route['story/([0-9a-zA-Z_-]+)']      = 'home/story/$1';
$route['contact']                     = 'home/contact';
$route['career']                      = 'home/career';
$route['faq']                         = 'home/faq';
$route['partners']                    = 'home/partners';

// Borrower
$route['register_to_borrow']             = 'borrower/register';
$route['login_to_borrow']                = 'borrower/login';
$route['logout_to_borrow']               = 'borrower/logout';
$route['letmein_to_borrow']              = 'borrower/VerifyLogin';
$route['borrower/loan/([0-9a-zA-Z_-]+)'] = 'home/borrower/loan/$1';

// Admin
$route['admin']                              = 'borrower/admin';
$route['admin/([a-zA-Z_-]+)/(:any)']         = '$1/admin/$2';
$route['admin/([a-zA-Z_-]+)/(:any)/(:any)']  = '$1/admin/$2/$3';
$route['admin/([a-zA-Z_-]+)']                = '$1/admin/index';

//Alias
$route['dashboard']                   = 'investor/dashboard';
$route['register']                    = 'investor/register';
$route['profile']                     = 'investor/profile';
$route['login']                       = 'investor/login';
$route['letmein']                     = 'investor/VerifyLogin';
$route['logout']                      = 'investor/logout';
$route['forget']                      = 'investor/forget';
$route['resetpasswd/(:any)']          = 'investor/resetpasswd/$1';
$route['resendactivation/(:any)']     = 'investor/resend_activation/$1';
$route['activation/(:any)']     	  = 'investor/activate/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
