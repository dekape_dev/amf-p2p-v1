<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 /*
 * @author 		fikriwirawan
 * @since       Nov 08, 2015
 * @package		Arkana
 * @version		1.0
 */


class marketperformance_model extends MY_Model {

    protected $table        = 'tbl_lender';
    protected $key          = 'lender_id';
    protected $soft_deletes =  true;
    protected $date_format  = 'datetime';

    public function __construct(){
        parent::__construct();
    }

	public function get_all($limit)	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deleted', '0');
		$this->db->order_by('lender_id', 'desc');
		return $this->db->get();

	}

	public function get_lender($param){
        $this->db->where('lender_id', $param);
		$this->db->where('deleted', '0');
        return $this->db->get($this->table);
    }
}
