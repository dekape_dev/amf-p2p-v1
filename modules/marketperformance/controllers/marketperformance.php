<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Marketperformance extends Front_Controller{

	public function __construct(){
		parent::__construct();
		$this->template->set_layout('dashboard');
	}

	public function index(){

		$this->template	->set('sitetitle', 'Market Performance')
						->set('menu_marketperformance', 'active')
						->build('marketperformance');

	}

	public function borrower(){

		$this->template	->set('sitetitle', 'Market Performance')
						->set('menu_marketperformance', 'active')
						->build('borrower');

	}

	public function investor(){

		$this->template	->set('sitetitle', 'Market Performance')
						->set('menu_marketperformance', 'active')
						->build('investor');

	}

	public function statistics(){

		$this->template	->set('sitetitle', 'Market Performance')
						->set('menu_marketperformance', 'active')
						->build('statistics');

	}



}
