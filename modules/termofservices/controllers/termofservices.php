<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Termofservices extends Front_Controller{

	public function __construct(){
		parent::__construct();
	}

	public function index(){

		$this->template	->set('sitetitle', 'Term of services')
						->set('menu_termofservices', 'active')
						->build('termofservices');

	}

}