<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invesment extends Front_Controller{

	public function __construct(){
		parent::__construct();
		/*$this->template->set_layout('dashboard');*/
		$this->load->model('loan_model');
		$this->load->model('clients_pembiayaan_model');
		$this->load->model('tabsukarela_model');
		$this->load->model('tabwajib_model');
		$this->load->model('sector_model');
	}

	public function index(){
		$this->template ->set_layout('dashboard'); 
		$this->template	->set('sitetitle', 'Invesment')
						->set('menu_invesment', 'active')
						->build('invesment_timeline');
	}

	public function invesment1(){
		$this->template ->set_layout('dashboard'); 
		$this->template	->set('sitetitle', 'Invesment')
						->set('menu_invesment', 'active')
						->build('invesment1');
	}

	public function invesment2(){
		$this->template ->set_layout('dashboard'); 
		$this->template	->set('sitetitle', 'Invesment')
						->set('menu_invesment', 'active')
						->build('invesment2');
	}

	public function invesment3(){
		$this->template ->set_layout('dashboard'); 
		$this->template	->set('sitetitle', 'Invesment')
						->set('menu_invesment', 'active')
						->build('invesment3');
	}

	public function invesment4(){
		$this->template	->set('sitetitle', 'Invesment')
						->set('menu_invesment', 'active')
						->build('invesment4');
	}
/*
	public function invesment2(){
		$this->template -> set('sitetitle','Invesment')
						-> set('menu_invesment2','active')
						-> bulid('invesment2');

	public function invesment3(){
		$this->template -> set('sitetitle','Invesment')
						-> set('menu_invesment3','active')
						-> bulid('invesment3');

	public function invesment4(){
		$this->template -> set('sitetitle','Invesment')
						-> set('menu_invesment4','active')
						-> bulid('invesment4');
	}
*/

}
