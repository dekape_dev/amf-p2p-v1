<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Investor extends Front_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('investor_model');
		$this->load->model('transaction/transaction_model');
		$this->load->model('location/location_model');
		//GET WALLET BALANCE
		$userdata            = $this->session->userdata('logged_in_user');
		$this->user_fullname = $userdata['ufullname'];
		$this->user_id       = $userdata['uid'];

		$wallet = $this->transaction_model->get_saldo_by_lender($userdata['uid']);
		$this->session->set_userdata('wallet', $wallet[0]->wallet_saldo);
		$this->session->set_userdata('wallet_id', $wallet[0]->wallet_id);
	}

	public function index(){

		$this->template	->set('sitetitle', 'Investor')
						->set('menu_invest', 'active')
						->build('investor');

	}

	public function dashboard(){
		if($this->session->userdata('logged_in_user')) {
			$userdata = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid'] = $userdata['uid'];

			//INVESTOR DETAIL
			$lender = $this->investor_model->get_lender($profile['uid'])->result();
			$lender = $lender[0];
			
			if(	$lender->lender_first_name AND $lender->lender_last_name AND $lender->lender_idcard AND $lender->lender_taxcard AND $lender->lender_email AND $lender->lender_phone AND $lender->lender_sex AND
				$lender->lender_birth_date AND $lender->lender_birth_place AND $lender->lender_martial AND 
				$lender->lender_address AND $lender->lender_province AND $lender->lender_city AND $lender->lender_kecamatan AND $lender->lender_kelurahan AND $lender->lender_zipcode AND
				$lender->lender_job_name AND $lender->lender_job_company AND $lender->lender_income AND $lender->lender_income_source_fund AND $lender->lender_income_source_country AND
				$lender->lender_bank_account AND $lender->lender_bank_holder AND $lender->lender_bank_name AND $lender->lender_bank_branch)
			{

				$lender_notes['finished'] = $this->investor_model->count_lender_history($profile['uid'],3);
				$lender_notes['current'] = $this->investor_model->count_lender_history($profile['uid'],1);
				$lender_notes['all'] = $lender_notes['current'] + $lender_notes['finished'];
				$lender_par['1'] = $this->investor_model->count_lender_history_par_1($profile['uid']);
				$lender_par['2'] = $this->investor_model->count_lender_history_par_2($profile['uid']);
				$lender_par['3'] = $this->investor_model->count_lender_history_par_3($profile['uid']);

				//GET DEPOSIT
				$deposit['total'] = $this->investor_model->get_deposit_total_by_lender($profile['uid']);
				$deposit['outstanding'] = $this->investor_model->get_deposit_outstanding_by_lender($profile['uid']);
				$deposit['available'] = abs($deposit['total'] - $deposit['outstanding']);
				$deposit['percentage'] = $deposit['outstanding']/$deposit['total']*100;

				$this->template->set_layout('dashboard');
				$this->template	->set('sitetitle', 'Investor')
								->set('menu_overview', 'active')
								->set('menu_account', 'active')
								->set('profile', $profile)
								->set('lender', $lender)
								->set('lender_notes', $lender_notes)
								->set('lender_par', $lender_par)
								->set('deposit', $deposit)
								->build('dashboard');
			}else{
				$this->session->set_flashdata('message', 'success|Please complete your profile to continue investing.');
				redirect('profile');
			}
		}else{
			redirect('login');
		}


	}

	public function wallet($id='0'){
		if($this->session->userdata('logged_in_user')) {
			$userdata = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid'] = $userdata['uid'];

			//GET LENDER
			$lender = $this->investor_model->get_lender($profile['uid'])->result();
			$lender = $lender[0];
			
			if(	$lender->lender_first_name AND $lender->lender_last_name AND $lender->lender_idcard AND $lender->lender_taxcard AND $lender->lender_email AND $lender->lender_phone AND $lender->lender_sex AND
				$lender->lender_birth_date AND $lender->lender_birth_place AND $lender->lender_martial AND 
				$lender->lender_address AND $lender->lender_province AND $lender->lender_city AND $lender->lender_kecamatan AND $lender->lender_kelurahan AND $lender->lender_zipcode AND
				$lender->lender_job_name AND $lender->lender_job_company AND $lender->lender_income AND $lender->lender_income_source_fund AND $lender->lender_income_source_country AND
				$lender->lender_bank_account AND $lender->lender_bank_holder AND $lender->lender_bank_name AND $lender->lender_bank_branch)
			{
				
				//GET DEPOSIT
				$deposit['total'] = $this->investor_model->get_deposit_total_by_lender($profile['uid']);
				$deposit['outstanding'] = $this->investor_model->get_deposit_outstanding_by_lender($profile['uid']);
				$deposit['available'] = $deposit['total'] - $deposit['outstanding'];
				$deposit['percentage'] = $deposit['outstanding']/$deposit['total']*100;

				//GET TRANSACTION HISTORY
				$trx_history = $this->transaction_model->get_all_transaction_by_lender($profile['uid']);
				//GET WALLET BALANCE
				$wallet 	 = $this->transaction_model->get_saldo_by_lender($profile['uid']);
				//var_dump($profile['uid']); var_dump($trx_history);
				$this->template->set_layout('dashboard');
				$this->template	->set('sitetitle', 'Investor')
								->set('menu_wallet', 'active')
								->set('profile', $profile)
								->set('lender', $lender)
								->set('deposit', $deposit)
								->set('history', $trx_history)
								->build('wallet');
			}else{
				$this->session->set_flashdata('message', 'success|Please complete your profile to continue investing.');
				redirect('profile');
			}
		}else{
			$this->session->set_flashdata('message', 'success|Register Success! Please check your email to activate your account.');
			redirect('login');
		}


	}

	public function history(){
		if($this->session->userdata('logged_in_user')) {
			$userdata = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid'] = $userdata['uid'];

			//GET LENDER
			$lender = $this->investor_model->get_lender($profile['uid'])->result();
			$lender = $lender[0];
			
			if(	$lender->lender_first_name AND $lender->lender_last_name AND $lender->lender_idcard AND $lender->lender_taxcard AND $lender->lender_email AND $lender->lender_phone AND $lender->lender_sex AND
				$lender->lender_birth_date AND $lender->lender_birth_place AND $lender->lender_martial AND 
				$lender->lender_address AND $lender->lender_province AND $lender->lender_city AND $lender->lender_kecamatan AND $lender->lender_kelurahan AND $lender->lender_zipcode AND
				$lender->lender_job_name AND $lender->lender_job_company AND $lender->lender_income AND $lender->lender_income_source_fund AND $lender->lender_income_source_country AND
				$lender->lender_bank_account AND $lender->lender_bank_holder AND $lender->lender_bank_name AND $lender->lender_bank_branch)
			{

				$this->template->set_layout('dashboard');

				$borrower = $this->investor_model->get_history_loan($this->user_id)->result();

				$this->template	->set('sitetitle', 'Investor')
								->set('borrower', $borrower)
								->set('menu_history', 'active')
								->build('history');
			}else{
				$this->session->set_flashdata('message', 'success|Please complete your profile to continue investing.');
				redirect('profile');
			}
		}else{
			redirect('login');
		}
	}

	public function portfolio(){
		if($this->session->userdata('logged_in_user')) {
			$userdata = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid'] = $userdata['uid'];

			//GET LENDER
			$lender = $this->investor_model->get_lender($profile['uid'])->result();
			$lender = $lender[0];
			
			if(	$lender->lender_first_name AND $lender->lender_last_name AND $lender->lender_idcard AND $lender->lender_taxcard AND $lender->lender_email AND $lender->lender_phone AND $lender->lender_sex AND
				$lender->lender_birth_date AND $lender->lender_birth_place AND $lender->lender_martial AND 
				$lender->lender_address AND $lender->lender_province AND $lender->lender_city AND $lender->lender_kecamatan AND $lender->lender_kelurahan AND $lender->lender_zipcode AND
				$lender->lender_job_name AND $lender->lender_job_company AND $lender->lender_income AND $lender->lender_income_source_fund AND $lender->lender_income_source_country AND
				$lender->lender_bank_account AND $lender->lender_bank_holder AND $lender->lender_bank_name AND $lender->lender_bank_branch)
			{

				$borrower = $this->investor_model->get_active_loan($this->user_id)->result();
				$this->template->set_layout('dashboard');

				$this->template	->set('sitetitle', 'Investor')
								->set('borrower', $borrower)
								->set('menu_portfolio', 'active')
								->build('portfolio');
			}else{
				$this->session->set_flashdata('message', 'success|Please complete your profile to continue investing.');
				redirect('profile');
			}
		}else{
			redirect('login');
		}
	}

	public function register(){
		if($this->save_register()){
			$this->session->set_flashdata('message', 'success|Register Success! Please check your email to activate your account.');
			redirect('login');
		}

		$this->template->set_layout('login');
		$this->template	->set('sitetitle', 'Investor')
						->build('register');

	}
	

	public function profile(){
		if($this->session->userdata('logged_in_user')) {
			$userdata            = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid']      = $userdata['uid'];

			if($this->input->post()){				
                $province 	= ($this->input->post('lender_province') != '' ? $this->input->post('lender_province') : '');
                $city 		= ($this->input->post('lender_city') != '' ? $this->input->post('lender_city') : '');
                $kecamatan 	= ($this->input->post('lender_kecamatan') != '' ? $this->input->post('lender_kecamatan') : '');
                $kelurahan 	= ($this->input->post('lender_kelurahan') != '' ? $this->input->post('lender_kelurahan') : '');
				
                $province_domisili 	= ($this->input->post('lender_province_domisili') != '' ? $this->input->post('lender_province_domisili') : '');
                $city_domisili 		= ($this->input->post('lender_city_domisili') != '' ? $this->input->post('lender_city_domisili') : '');
                $kecamatan_domisili = ($this->input->post('lender_kecamatan_domisili') != '' ? $this->input->post('lender_kecamatan_domisili') : '');
                $kelurahan_domisili = ($this->input->post('lender_kelurahan_domisili') != '' ? $this->input->post('lender_kelurahan_domisili') : '');
				
				
				$data = array(
						'lender_first_name'		     	=> $this->input->post('lender_first_name'),
						'lender_last_name'         	 	=> $this->input->post('lender_last_name'),
						'lender_phone'             		=> $this->input->post('lender_phone'),
						'lender_birth_date'        		=> $this->input->post('lender_birth_date'),
						'lender_birth_place'        	=> $this->input->post('lender_birth_place'),
						'lender_sex'        			=> $this->input->post('lender_sex'),
						'lender_nationality'        	=> $this->input->post('lender_nationality'),
						'lender_idcard'        			=> $this->input->post('lender_idcard'),
						'lender_taxcard'        		=> $this->input->post('lender_taxcard'),
						'lender_martial'        		=> $this->input->post('lender_martial'),
						
						'lender_address'		       	=> $this->input->post('lender_address'),
						'lender_province'		       	=> $province,
						'lender_city'              		=> $city,
						'lender_kecamatan'              => $kecamatan,
						'lender_kelurahan'              => $kelurahan,
						'lender_zipcode'           		=> $this->input->post('lender_zipcode'),
						'lender_address_domisili'		=> $this->input->post('lender_address_domisili'),
						'lender_province_domisili'		=> $province_domisili,
						'lender_city_domisili'          => $city_domisili,
						'lender_kecamatan_domisili'     => $kecamatan_domisili,
						'lender_kelurahan_domisili'     => $kelurahan_domisili,
						'lender_zipcode_domisili'       => $this->input->post('lender_zipcode_domisili'),					
						
						'lender_job_name'       		=> $this->input->post('lender_job_name'),
						'lender_job_company'       		=> $this->input->post('lender_job_company'),
						'lender_job_company_address'    => $this->input->post('lender_job_company_address'),
						
						'lender_income'    				=> $this->input->post('lender_income'),
						'lender_income_source_fund'    	=> $this->input->post('lender_income_source_fund'),
						'lender_income_source_country'  => $this->input->post('lender_income_source_country'),
						
						'lender_bank_account'    		=> $this->input->post('lender_bank_account'),
						'lender_bank_holder'    		=> $this->input->post('lender_bank_holder'),
						'lender_bank_name'    			=> $this->input->post('lender_bank_name'),
						'lender_bank_branch'    		=> $this->input->post('lender_bank_branch'),						
						
						//'lender_email'      	   		=> $this->input->post('email'),
						//'lender_password'  		   	=> $this->input->post('lastname'),
						//'lender_register_date'   		=> date('Y-m-d H:i:s'),
						//'lender_activation_code' 		=> $activation_code,
				);
				$this->investor_model->update_lender($profile['uid'], $data);
				$this->session->set_flashdata('message', 'success|Account update was successful.');
				redirect('profile');
			}

			//INVESTOR DETAIL
			$lender = $this->investor_model->get_lender($profile['uid'])->result();
			$lender = $lender[0];
						
			$province = $this->location_model->get_province();
			
			$get_province = $this->location_model->get_location_by_code($lender->lender_province.".00.00.0000");
			$location['province'] = $get_province[0]->lokasi_nama;
			$get_city = $this->location_model->get_location_by_code($lender->lender_city.".00.0000");
			$location['city'] = $get_city[0]->lokasi_nama;
			$get_kec = $this->location_model->get_location_by_code($lender->lender_kecamatan.".0000");
			$location['kec'] = $get_kec[0]->lokasi_nama;
			$get_kel = $this->location_model->get_location_by_code($lender->lender_kelurahan);
			$location['kel'] = $get_kel[0]->lokasi_nama;
			
			$get_province2 = $this->location_model->get_location_by_code($lender->lender_province_domisili.".00.00.0000");
			$location['province_2'] = $get_province2[0]->lokasi_nama;
			$get_city2 = $this->location_model->get_location_by_code($lender->lender_city_domisili.".00.0000");
			$location['city_2'] = $get_city2[0]->lokasi_nama;
			$get_kec2 = $this->location_model->get_location_by_code($lender->lender_kecamatan_domisili.".0000");
			$location['kec_2'] = $get_kec2[0]->lokasi_nama;
			$get_kel2 = $this->location_model->get_location_by_code($lender->lender_kelurahan_domisili);
			$location['kel_2'] = $get_kel2[0]->lokasi_nama;
			
			$this->template->set_layout('dashboard');
			$this->template	->set('sitetitle', 'Investor')
							->set('data', $lender)
							->set('province', $province)
							->set('location', $location)
							->build('personalinfo');
		}else{
			redirect('login');
		}

	}


	private function save_register(){
		//set form validation
		$this->form_validation->set_rules('register_first_name', 'First Name', 'required');
		$this->form_validation->set_rules('register_email', 'Email', 'required');
		$this->form_validation->set_rules('register_passwd', 'Password', 'required');
		//$this->form_validation->set_rules('register_address', 'Address', 'required');
		$this->form_validation->set_rules('register_phone', 'Phone', 'required');

		if($this->form_validation->run() === TRUE){

			//Check Email
			$email = $this->input->post('register_email');
			$cek_email = $this->investor_model->check_email($email)->result();
			$cek_email = $cek_email[0];

			if($cek_email->lender_email){
				$this->session->set_flashdata('message', 'success|Email already registered. Please login using your email.');
				redirect('login');
			}elseif($this->input->post('register_passwd') != $this->input->post('register_passwd_confirm')){
				$this->session->set_flashdata('message', 'success|Password not match');
				redirect('register');
			}else{
				//process the form
				$this->db->trans_start();

					$activation_code =  sha1($this->input->post('register_email').date('Ymdhis'));
					$data = array(
						'lender_first_name'		=> $this->input->post('register_first_name'),
						'lender_last_name'      => $this->input->post('register_last_name'),
						'lender_email'      	=> $this->input->post('register_email'),
						'lender_password'  		=> md5($this->input->post('register_passwd')),
						'lender_phone'      	=> $this->input->post('register_phone'),
						//'lender_address'      	=> $this->input->post('register_address'),
						//'lender_city'      		=> $this->input->post('register_city'),
						//'lender_province'      	=> $this->input->post('register_province'),
						//'lender_zipcode'      	=> $this->input->post('register_zipcode'),
						'lender_register_date'  => date('Y-m-d H:i:s'),
						'lender_activation_code' => $activation_code,
					);


					$this->investor_model->insert($data);
					$insert_id     = $this->db->insert_id();
					$lender_number = (date('y')*1000000) + $insert_id;
					$data2         = array(
						'lender_number' => $lender_number,
					);
					$this->investor_model->update($insert_id, $data2);

					//Creating Wallet upon Registration
					$data_wallet = array(
						'wallet_lender' => $insert_id,
						'wallet_debet'  => 0,
						'wallet_credit' => 0,
						'wallet_saldo'  => 0,
						'wallet_remark' => 'Created at Registration'
					);
					$this->db->insert('p2p_wallet', $data_wallet);
					$wallet_id = $this->db->insert_id();
					$this->session->set_userdata('wallet_id', $wallet_id);

				$this->db->trans_complete();

				//UPDATE EMAIL
				$this->load->library('email');
				$config = Array(
					'protocol'  => 'smtp',
					'smtp_host' => 'ssl://smtp.mailgun.org',
					'smtp_port' => '465',
					'smtp_user' => 'postmaster@mg.arkana.co.id', // change it to yours
					'smtp_pass' => '2d81625a14771268a92d806fe856bb55', // change it to yours
					'mailtype'  => 'html',
					'charset'   => 'utf-8',
					'wordwrap'  => FALSE,
					'newline'   => "\r\n"
				);

				/*
				//SMTP Arkana
				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'mail.arkana.co.id',
					'smtp_port' => '25',
					'smtp_user' => 'info@arkana.co.id', // change it to yours
					'smtp_pass' => 'arkana12345', // change it to yours
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'wordwrap' => FALSE,
					'newline' => "\r\n"
				);
				*/
				$this->email->initialize($config);

				$this->email->from('info@amartha.com','Amartha'); // change it to yours
				$this->email->to($this->input->post('register_email')); // change it to yours
				$this->email->bcc('info@amartha.com');
				$this->email->subject('Activate Your Amartha Account');

				$messagebody = '';
				$messagebody ='<html lang="en">
								<head>
								  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
								  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
								  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
								  <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
								  <title>Amartha</title>

								  <style type="text/css">
									body {
									  margin: 0;
									  padding: 0;
									  -ms-text-size-adjust: 100%;
									  -webkit-text-size-adjust: 100%;
									}

									table {
									  border-spacing: 0;
									}

									table td {
									  border-collapse: collapse;
									}

									.ExternalClass {
									  width: 100%;
									}

									.ExternalClass,
									.ExternalClass p,
									.ExternalClass span,
									.ExternalClass font,
									.ExternalClass td,
									.ExternalClass div {
									  line-height: 100%;
									}

									.ReadMsgBody {
									  width: 100%;
									  background-color: #ebebeb;
									}

									table {
									  mso-table-lspace: 0pt;
									  mso-table-rspace: 0pt;
									}

									img {
									  -ms-interpolation-mode: bicubic;
									}

									.yshortcuts a {
									  border-bottom: none !important;
									}

									@media screen and (max-width: 599px) {
									  table[class="force-row"],
									  table[class="container"] {
										width: 100% !important;
										max-width: 100% !important;
									  }
									}
									@media screen and (max-width: 400px) {
									  td[class*="container-padding"] {
										padding-left: 12px !important;
										padding-right: 12px !important;
									  }
									}
									.ios-footer a {
									  color: #aaaaaa !important;
									  text-decoration: underline;
									}
									</style>

								</head>
								<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

								<!-- 100% background wrapper (grey background) -->
								<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
								  <tr>
									<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

									  <br>

									  <!-- 600px container (white background) -->
									  <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
										<tr>
										  <td class="container-padding header" align="left" style="padding-bottom:12px;color:#99cc00;padding-left:24px;padding-right:24px">
										   <img src="'.$this->template->get_theme_path().'img/logo_amartha_small_color.png" alt="Amartha"/>
										  </td>
										</tr>
										<tr>
										  <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">
											<br>
											<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">Account Activation</div>
											<br>

											<div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
											  Hi <b>'.$this->input->post('register_first_name').' '.$this->input->post('register_last_name').'</b>, thank you for registering at Amartha. Please activate your account by clicking the button below.<br/>
											  <a href="'.site_url().'activation/'.$activation_code.'" title=""><b><h3>Activate Account</h3></b></a>

											 Thanks
											</div>

										  </td>
										</tr>
										<tr>
										  <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
											<br><br>


											<strong>Amartha</strong><br>
											<span class="ios-footer">
											 <a href="http://amartha.com" style="color:#aaaaaa;text-decoration: none;">info@amartha.com</a><br/>
											</span>
											<a href="http://amartha.com" style="color:#aaaaaa;text-decoration: none;">www.amartha.com</a><br>

											<br><br>

										  </td>
										</tr>
									  </table>


									</td>
								  </tr>
								</table>

								</body>
								</html>
								';

				$this->email->message($messagebody);
				$this->email->send();


				return true;
			}
		}
	}
	
	public function resend_activation($code){
		if($code){
			$check = $this->investor_model->check_activation($code)->result();
			$user = $check[0];
			
			//UPDATE EMAIL
				$this->load->library('email');
				$config = Array(
					'protocol'  => 'smtp',
					'smtp_host' => 'ssl://smtp.mailgun.org',
					'smtp_port' => '465',
					'smtp_user' => 'postmaster@mg.arkana.co.id', // change it to yours
					'smtp_pass' => '2d81625a14771268a92d806fe856bb55', // change it to yours
					'mailtype'  => 'html',
					'charset'   => 'utf-8',
					'wordwrap'  => FALSE,
					'newline'   => "\r\n"
				);

				$this->email->initialize($config);

				$this->email->from('info@amartha.com','Amartha'); // change it to yours
				$this->email->to($user->lender_email); // change it to yours
				$this->email->bcc('info@amartha.com');
				$this->email->subject('Activate Your Amartha Account');

				$messagebody = '';
				$messagebody ='<html lang="en">
								<head>
								  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
								  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
								  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
								  <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
								  <title>Amartha</title>

								  <style type="text/css">
									body {
									  margin: 0;
									  padding: 0;
									  -ms-text-size-adjust: 100%;
									  -webkit-text-size-adjust: 100%;
									}

									table {
									  border-spacing: 0;
									}

									table td {
									  border-collapse: collapse;
									}

									.ExternalClass {
									  width: 100%;
									}

									.ExternalClass,
									.ExternalClass p,
									.ExternalClass span,
									.ExternalClass font,
									.ExternalClass td,
									.ExternalClass div {
									  line-height: 100%;
									}

									.ReadMsgBody {
									  width: 100%;
									  background-color: #ebebeb;
									}

									table {
									  mso-table-lspace: 0pt;
									  mso-table-rspace: 0pt;
									}

									img {
									  -ms-interpolation-mode: bicubic;
									}

									.yshortcuts a {
									  border-bottom: none !important;
									}

									@media screen and (max-width: 599px) {
									  table[class="force-row"],
									  table[class="container"] {
										width: 100% !important;
										max-width: 100% !important;
									  }
									}
									@media screen and (max-width: 400px) {
									  td[class*="container-padding"] {
										padding-left: 12px !important;
										padding-right: 12px !important;
									  }
									}
									.ios-footer a {
									  color: #aaaaaa !important;
									  text-decoration: underline;
									}
									</style>

								</head>
								<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

								<!-- 100% background wrapper (grey background) -->
								<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
								  <tr>
									<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

									  <br>

									  <!-- 600px container (white background) -->
									  <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
										<tr>
										  <td class="container-padding header" align="left" style="padding-bottom:12px;color:#99cc00;padding-left:24px;padding-right:24px">
										   <img src="'.$this->template->get_theme_path().'img/logo_amartha_small_color.png" alt="Amartha"/>
										  </td>
										</tr>
										<tr>
										  <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">
											<br>
											<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">Account Activation</div>
											<br>

											<div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
											  Hi <b>'.$user->lender_first_name.' '.$user->lender_last_name.'</b>, thank you for registering at Amartha. Please activate your account by clicking the button below.<br/>
											  <a href="'.site_url().'activation/'.$code.'" title=""><b><h3>Activate Account</h3></b></a>

											 Thanks
											</div>

										  </td>
										</tr>
										<tr>
										  <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
											<br><br>


											<strong>Amartha</strong><br>
											<span class="ios-footer">
											 <a href="http://amartha.com" style="color:#aaaaaa;text-decoration: none;">info@amartha.com</a><br/>
											</span>
											<a href="http://amartha.com" style="color:#aaaaaa;text-decoration: none;">www.amartha.com</a><br>

											<br><br>

										  </td>
										</tr>
									  </table>


									</td>
								  </tr>
								</table>

								</body>
								</html>
								';

				$this->email->message($messagebody);
				$this->email->send();

			$this->session->set_flashdata('message', "success|Email activation sent. Please check your email to activate account.");
			redirect('login', 'refresh');
		}else{
			redirect('login', 'refresh');
		}
	}

	public function activate($code){

		$check = $this->investor_model->check_activation($code)->result();
		$check = $check[0]->lender_id;

		if($check){
			$this->investor_model->activate($code);
			$msg='Your account has been active. Please <a href="'.site_url().'">login</a> using your email and password.';
			$msg_style="success";
		}else{
			$msg='Failed in activation';
			$msg_style="warning";
		}

		$this->template->set_layout('login');
		$this->template	->set('sitetitle', 'Activation')
						->set('menu_home', 'active')
						->set('msg', $msg)
						->set('msg_style', $msg_style)
						->build('activation');
	}

	public function login(){
		$this->template->set_layout('login');
		$this->template	->set('sitetitle', 'Investor Login')
						->set('menu_login', 'active')
						->build('login');
	}

	public function VerifyLogin()
	{
		//Validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('login_email', 'Email', 'trim|required|xss_clean');
		$this->form_validation->set_rules('login_passwd', 'Password', 'trim|required|xss_clean|callback_check_database');

		if($this->form_validation->run() == FALSE)
		{
			//Field validation failed.  User redirected to login page
			//Go to Login Page
			$this->session->set_flashdata('message', 'Field validation failed');
			redirect('signin/login', 'refresh');
		}
		else
		{
			$email = $this->input->post('login_email');
			$password = $this->input->post('login_passwd');
			
			//check login
			$result = $this->investor_model->login($email, $password);

			if($result)
			{
				$sess_array = array();
				foreach($result as $row)
				{
					$sess_array = array(
						'uid' => $row->lender_id,
						'uemail' => $row->lender_email,
						'ufirstname' => $row->lender_first_name,
						'ulastname' => $row->lender_last_name,
						'ufullname' => $row->lender_first_name." ".$row->lender_last_name,
						'ustatus' => $row->lender_status
					);
					$this->session->set_userdata('logged_in_user', $sess_array);
					$this->session->set_userdata('ufirstname', $row->lender_first_name);
					$this->session->set_userdata('ulastname',  $row->lender_last_name);
					$this->session->set_userdata('uemail',  $row->lender_email);
					$this->session->set_userdata('ufullname',  $row->lender_first_name." ".$row->lender_last_name);
					$this->session->set_userdata('wallet', $row->wallet_saldo);
					$this->session->set_userdata('wallet_id', $row->wallet_id);
					//echo 'sess_array: ';
					//var_dump($this->session->userdata('logged_in_user')); die();
				}
				//Go to Success Page

				if($row->lender_status == "Enable"){
					redirect('dashboard', 'refresh');
				}else{
					$this->session->set_flashdata('message', "error|Your account is not active. Please activate your account first. <u><a href='".site_url('resendactivation/'.$row->lender_activation_code)."'>Resend Activation Email</a></u>");
					redirect('login', 'refresh');
				}
			}
			else
			{
				//Go to Login Page
				$this->session->set_flashdata('message', 'error|Invalid email or password');
				redirect('login', 'refresh');
			}

		}

	}

	
	public function forget(){
		if($this->forget_password()){
			$this->session->set_flashdata('message', 'success|Please check your email to continue reset your password.');
			redirect('login');
		}

		$this->template->set_layout('login');
		$this->template	->set('sitetitle', 'Forget Password')
						->build('forget');

	}
	
	
	private function forget_password(){
		$this->form_validation->set_rules('email', 'Email', 'required');
		
		if($this->form_validation->run() === TRUE){
			$email = $this->input->post('email');
			$check = $this->investor_model->check_email($email)->result();
			$lender_id = $check[0]->lender_id;
			
			if($lender_id){
				$lender_forget_code = sha1(date("Ymdhis"));
				$data = array(
						'lender_forget_password' => 1,
						'lender_forget_code'      => $lender_forget_code
					);


				$this->investor_model->update($lender_id,$data);
				
				//UPDATE EMAIL
				$this->load->library('email');
				$config = Array(
					'protocol'  => 'smtp',
					'smtp_host' => 'ssl://smtp.mailgun.org',
					'smtp_port' => '465',
					'smtp_user' => 'postmaster@mg.arkana.co.id', // change it to yours
					'smtp_pass' => '2d81625a14771268a92d806fe856bb55', // change it to yours
					'mailtype'  => 'html',
					'charset'   => 'utf-8',
					'wordwrap'  => FALSE,
					'newline'   => "\r\n"
				);

				/*
				//SMTP Arkana
				$config = Array(
					'protocol' => 'smtp',
					'smtp_host' => 'mail.arkana.co.id',
					'smtp_port' => '25',
					'smtp_user' => 'info@arkana.co.id', // change it to yours
					'smtp_pass' => 'arkana12345', // change it to yours
					'mailtype' => 'html',
					'charset' => 'utf-8',
					'wordwrap' => FALSE,
					'newline' => "\r\n"
				);
				*/
				$this->email->initialize($config);

				$this->email->from('info@amartha.com','Amartha'); // change it to yours
				$this->email->to($check[0]->lender_email); // change it to yours
				$this->email->bcc('info@amartha.com');
				$this->email->subject('Reset Password Amartha');

				$messagebody = '';
				$messagebody ='<html lang="en">
								<head>
								  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
								  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
								  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
								  <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
								  <title>Amartha</title>

								  <style type="text/css">
									body {
									  margin: 0;
									  padding: 0;
									  -ms-text-size-adjust: 100%;
									  -webkit-text-size-adjust: 100%;
									}

									table {
									  border-spacing: 0;
									}

									table td {
									  border-collapse: collapse;
									}

									.ExternalClass {
									  width: 100%;
									}

									.ExternalClass,
									.ExternalClass p,
									.ExternalClass span,
									.ExternalClass font,
									.ExternalClass td,
									.ExternalClass div {
									  line-height: 100%;
									}

									.ReadMsgBody {
									  width: 100%;
									  background-color: #ebebeb;
									}

									table {
									  mso-table-lspace: 0pt;
									  mso-table-rspace: 0pt;
									}

									img {
									  -ms-interpolation-mode: bicubic;
									}

									.yshortcuts a {
									  border-bottom: none !important;
									}

									@media screen and (max-width: 599px) {
									  table[class="force-row"],
									  table[class="container"] {
										width: 100% !important;
										max-width: 100% !important;
									  }
									}
									@media screen and (max-width: 400px) {
									  td[class*="container-padding"] {
										padding-left: 12px !important;
										padding-right: 12px !important;
									  }
									}
									.ios-footer a {
									  color: #aaaaaa !important;
									  text-decoration: underline;
									}
									</style>

								</head>
								<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

								<!-- 100% background wrapper (grey background) -->
								<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
								  <tr>
									<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

									  <br>

									  <!-- 600px container (white background) -->
									  <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
										<tr>
										  <td class="container-padding header" align="left" style="padding-bottom:12px;color:#99cc00;padding-left:24px;padding-right:24px">
										   <img src="'.$this->template->get_theme_path().'img/logo_amartha_small_color.png" alt="Amartha"/>
										  </td>
										</tr>
										<tr>
										  <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">
											<br>
											<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">Reset Password</div>
											<br>

											<div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
											  Hi <b>'.$check[0]->lender_first_name.' '.$check[0]->lender_last_name.'</b>, we have receive reset password request to your account.<br/>
											  Please click <a href="'.site_url().'resetpasswd/'.$lender_forget_code.'" title=""><b>here</b></a> to continue reset your password. 
											<br/>
											  Thank You
											</div>

										  </td>
										</tr>
										<tr>
										  <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
											<br><br>


											<strong>Amartha</strong><br>
											<span class="ios-footer">
											 <a href="http://amartha.com" style="color:#aaaaaa;text-decoration: none;">info@amartha.com</a><br/>
											</span>
											<a href="http://amartha.com" style="color:#aaaaaa;text-decoration: none;">www.amartha.com</a><br>

											<br><br>

										  </td>
										</tr>
									  </table>


									</td>
								  </tr>
								</table>

								</body>
								</html>
								';

				$this->email->message($messagebody);
				$this->email->send();


				return true;
				
			}else{
				$this->session->set_flashdata('message', 'success|Email not registered.');
				redirect('forget');
			}
		}
		
	}
	
	
	public function resetpasswd($code){
		if($this->reset_password()){
			$this->session->set_flashdata('message', 'success|Success reset password. Please login.');
			redirect('login');
		}

		$this->template->set_layout('login');
		$this->template	->set('sitetitle', 'Reset Password')
						->build('reset');

	}
	
	
	private function reset_password(){
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('confirmpassword', 'Confirm Password', 'required');
		$this->form_validation->set_rules('resetcode', 'Resetcode', 'required');
		
		if($this->form_validation->run() === TRUE){
			$password = $this->input->post('password');
			$confirmpassword = $this->input->post('confirmpassword');
			$resetcode = $this->input->post('resetcode');
			$check = $this->investor_model->check_resetcode($resetcode)->result();
			$lender_id = $check[0]->lender_id;
			
			if($lender_id){
				if($password == $confirmpassword){
					$data = array(
							'lender_forget_password' => 0,
							'lender_password'      => md5($password)
						);


					$this->investor_model->update($lender_id,$data);
					
					//UPDATE EMAIL
					$this->load->library('email');
					$config = Array(
						'protocol'  => 'smtp',
						'smtp_host' => 'ssl://smtp.mailgun.org',
						'smtp_port' => '465',
						'smtp_user' => 'postmaster@mg.arkana.co.id', // change it to yours
						'smtp_pass' => '2d81625a14771268a92d806fe856bb55', // change it to yours
						'mailtype'  => 'html',
						'charset'   => 'utf-8',
						'wordwrap'  => FALSE,
						'newline'   => "\r\n"
					);

					/*
					//SMTP Arkana
					$config = Array(
						'protocol' => 'smtp',
						'smtp_host' => 'mail.arkana.co.id',
						'smtp_port' => '25',
						'smtp_user' => 'info@arkana.co.id', // change it to yours
						'smtp_pass' => 'arkana12345', // change it to yours
						'mailtype' => 'html',
						'charset' => 'utf-8',
						'wordwrap' => FALSE,
						'newline' => "\r\n"
					);
					*/
					$this->email->initialize($config);

					$this->email->from('info@amartha.com','Amartha'); // change it to yours
					$this->email->to($check[0]->lender_email); // change it to yours
					$this->email->bcc('info@amartha.com');
					$this->email->subject('Success Reset Password Amartha');

					$messagebody = '';
					$messagebody ='<html lang="en">
									<head>
									  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
									  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
									  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
									  <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
									  <title>Amartha</title>

									  <style type="text/css">
										body {
										  margin: 0;
										  padding: 0;
										  -ms-text-size-adjust: 100%;
										  -webkit-text-size-adjust: 100%;
										}

										table {
										  border-spacing: 0;
										}

										table td {
										  border-collapse: collapse;
										}

										.ExternalClass {
										  width: 100%;
										}

										.ExternalClass,
										.ExternalClass p,
										.ExternalClass span,
										.ExternalClass font,
										.ExternalClass td,
										.ExternalClass div {
										  line-height: 100%;
										}

										.ReadMsgBody {
										  width: 100%;
										  background-color: #ebebeb;
										}

										table {
										  mso-table-lspace: 0pt;
										  mso-table-rspace: 0pt;
										}

										img {
										  -ms-interpolation-mode: bicubic;
										}

										.yshortcuts a {
										  border-bottom: none !important;
										}

										@media screen and (max-width: 599px) {
										  table[class="force-row"],
										  table[class="container"] {
											width: 100% !important;
											max-width: 100% !important;
										  }
										}
										@media screen and (max-width: 400px) {
										  td[class*="container-padding"] {
											padding-left: 12px !important;
											padding-right: 12px !important;
										  }
										}
										.ios-footer a {
										  color: #aaaaaa !important;
										  text-decoration: underline;
										}
										</style>

									</head>
									<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

									<!-- 100% background wrapper (grey background) -->
									<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
									  <tr>
										<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

										  <br>

										  <!-- 600px container (white background) -->
										  <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
											<tr>
											  <td class="container-padding header" align="left" style="padding-bottom:12px;color:#99cc00;padding-left:24px;padding-right:24px">
											   <img src="'.$this->template->get_theme_path().'img/logo_amartha_small_color.png" alt="Amartha"/>
											  </td>
											</tr>
											<tr>
											  <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">
												<br>
												<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">Success Reset Password</div>
												<br>

												<div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
												  Hi <b>'.$check[0]->lender_first_name.' '.$check[0]->lender_last_name.'</b>, your password has been reset.<br/>
												  Please click <a href="'.site_url().'login" title=""><b>here</b></a> to login to your account. 
												  <br/><br/>
												  Thank You
												</div>

											  </td>
											</tr>
											<tr>
											  <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
												<br><br>


												<strong>Amartha</strong><br>
												<span class="ios-footer">
												 <a href="http://amartha.com" style="color:#aaaaaa;text-decoration: none;">info@amartha.com</a><br/>
												</span>
												<a href="http://amartha.com" style="color:#aaaaaa;text-decoration: none;">www.amartha.com</a><br>

												<br><br>

											  </td>
											</tr>
										  </table>


										</td>
									  </tr>
									</table>

									</body>
									</html>
									';

					$this->email->message($messagebody);
					$this->email->send();


					return true;
				}else{
					$this->session->set_flashdata('message', 'success|Password not match');
					redirect('forget/'.$resetcode);
					
				}
			}else{
				$this->session->set_flashdata('message', 'success|Failed reset password');
				redirect('forget');
			}
		}
		
	}
	
	
	function logout()
	{
		$this->session->unset_userdata('logged_in_user');
		session_destroy();
		redirect(site_url(), 'refresh');
	}

	public function testemail(){
		$param = $this->input->post("PARAM");

		if($param){
			//UPDATE EMAIL
			$this->load->library('email');
			$config = Array(
				'protocol'  => 'smtp',
				'smtp_host' => 'ssl://smtp.mailgun.org',
				'smtp_port' => '465',
					'smtp_user' => 'postmaster@mg.arkana.co.id', // change it to yours
					'smtp_pass' => '2d81625a14771268a92d806fe856bb55', // change it to yours
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				'wordwrap'  => FALSE,
				'newline'   => "\r\n"
			);

			$this->email->initialize($config);

			$this->email->from('info@amartha.com','Amartha'); // change it to yours
			$this->email->to('fikriwirawan@gmail.com'); // change it to yours
			$this->email->subject('Mailgun Test Email');

			$messagebody ='Hello';

			$this->email->message($messagebody);
			if($this->email->send()){
				echo "success";
			}else{
				echo "failed<br/>";
				echo $this->email->print_debugger();
			}

		}else{
			echo "no parameter";
		}
	}
}
