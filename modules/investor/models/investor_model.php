<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 /*
 * @author 		fikriwirawan
 * @since       Nov 08, 2015
 * @package		Arkana
 * @version		1.0
 */


class investor_model extends MY_Model {

    protected $table        = 'p2p_lender';
    protected $key          = 'lender_id';
    protected $soft_deletes =  true;
    protected $date_format  = 'datetime';

    public function __construct(){
        parent::__construct();
    }

	public function get_all($limit)	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deleted', '0');
		$this->db->order_by('lender_id', 'desc');
		return $this->db->get();

	}

	public function get_lender($param){
        $this->db->where('lender_id', $param);
		$this->db->where('deleted', '0');
        return $this->db->get($this->table);
    }

    public function update_lender($id, $data){
      $this->db->where($this->key, $id);
      $this->db->where('deleted', '0');
      return $this->db->update($this->table, $data);
  }

	public function check_activation($param){
        $this->db->where('lender_activation_code', $param);
		    $this->db->where('deleted', '0');
        return $this->db->get($this->table);
    }
	
	public function check_email($param){
        $this->db->where('lender_email', $param);
		    $this->db->where('deleted', '0');
        return $this->db->get($this->table);
    }
	public function check_resetcode($param){
        $this->db->where('lender_forget_code', $param);
        $this->db->where('lender_forget_password', '1');
		$this->db->where('deleted', '0');
        return $this->db->get($this->table);
    }

	public function activate($param){
		$data = array(
               'lender_status' => 'Enable'
            );
        $this->db->where('lender_activation_code', $param)
				 ->where('deleted', '0');
        return $this->db->update($this->table, $data);
    }

	function login($username, $password)
    {
        $this->db->select('*');
        $this->db->join('p2p_wallet', 'p2p_wallet.wallet_lender = p2p_lender.lender_id', 'left');
        $this->db->from($this->table);
        $this->db->where('lender_email = ' . "'" . $username . "'");
        $this->db->where('lender_password = ' . "'" . MD5($password) . "'");
        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }




	public function get_deposit_total_by_lender($lender_id){
		return $this->db->select("SUM(deposit_amount) AS ammount")
						->from('p2p_deposit')
						->where('deposit_lender', $lender_id)
						->where('deleted', '0')
						->get()
						->row()
						->ammount;
    }
	public function get_deposit_outstanding_by_lender($lender_id){
		return $this->db->select("SUM(trx_amount) AS ammount")
						->from('p2p_trx')
						->where('trx_lender', $lender_id)
						->where('trx_status', 'Success')
						->where('trx_borrower !=', '0')
						->where('deleted', '0')
						->get()
						->row()
						->ammount;
    }

	public function count_lender_history($lender_id,$status_pembiayaan){
		return $this->db->select("COUNT(data_id) AS ammount")
						->from('tbl_pembiayaan')
						->where('data_p2p_lender', $lender_id)
						->where('data_status', $status_pembiayaan)
						->where('deleted', '0')
						->get()
						->row()
						->ammount;
    }

	public function count_lender_history_par_1($lender_id){
		return $this->db->select("COUNT(data_id) AS ammount")
						->from('tbl_pembiayaan')
						->where('data_p2p_lender', $lender_id)
						->where('data_status', 3)
						->where("data_par <= 2")
						->where('deleted', '0')
						->get()
						->row()
						->ammount;
    }

	public function count_lender_history_par_2($lender_id){
		return $this->db->select("COUNT(data_id) AS ammount")
						->from('tbl_pembiayaan')
						->where('data_p2p_lender', $lender_id)
						->where('data_status', 3)
						->where("data_par > 2")
						->where("data_par <= 4")
						->where('deleted', '0')
						->get()
						->row()
						->ammount;
    }

	public function count_lender_history_par_3($lender_id){
		return $this->db->select("COUNT(data_id) AS ammount")
						->from('tbl_pembiayaan')
						->where('data_p2p_lender', $lender_id)
						->where('data_status', 3)
						->where("data_par > 4")
						->where('deleted', '0')
						->get()
						->row()
						->ammount;
    }

    public function refill_wallet($amount, $lender_id){

    }

    public function reset_wallet($amount, $lender_id){

    }
	
	
	public function get_active_loan($lender_id){
		return $this->db->select("*")
						->from('tbl_pembiayaan')
						->join('tbl_clients', 'tbl_clients.client_id = tbl_pembiayaan.data_client', 'left')
						->join('tbl_sector','tbl_sector.sector_id = tbl_pembiayaan.data_sector')
						->where('data_p2p_lender', $lender_id)
						->where('data_status !=', 3)
						->where('data_p2p', 1)
						->where('tbl_pembiayaan.deleted', '0')
						->get();
						
    }
	
	public function get_history_loan($lender_id){
		return $this->db->select("*")
						->from('tbl_pembiayaan')
						->join('tbl_clients', 'tbl_clients.client_id = tbl_pembiayaan.data_client', 'left')
						->join('tbl_sector','tbl_sector.sector_id = tbl_pembiayaan.data_sector')
						->where('data_p2p_lender', $lender_id)
						->where('data_status', 3)
						->where('data_p2p', 1)
						->where('tbl_pembiayaan.deleted', '0')
						->get();
						
    }
	
	
}
