<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * 
 * @author 	fikriwirawan
 * @since	Oct 14, 2013
 */
class news_model extends MY_Model {

    protected $table        = 'p2p_news';
    protected $key          = 'news_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    protected $set_created  = true;
    protected $set_modified = false;

    public function __construct(){
        parent::__construct();
    }
    
	public function get_all($limit)	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deleted', '0');
		$this->db->limit('3', '0');
		$this->db->order_by('news_date', 'desc');
		return $this->db->get();
		
	} 
	
	public function get_latest()	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deleted', '0');
		$this->db->limit('1', '0');
		$this->db->order_by('news_date', 'desc');
		return $this->db->get();
		
	} 
  
	public function get_news($param){
        $this->db->where('news_id', $param);
        return $this->db->get($this->table);    
    }
	function getArchiveArticle()
	{	
		$sql="SELECT count('news_title') as jumlah , date_format(news_date, '%M %Y') as date,
		date_format(news_date, '%Y-%m') as date1 	FROM ".$this->table." GROUP BY date1 order by date1  DESC";
		return $this->db->query($sql); 
	}  
   public function get_archieve($date1)	{ 
		$sql="SELECT news_id, news_title, news_date,news_content, date_format(news_date, '%Y-%m') as date1 FROM "
		.$this->table." WHERE date_format(news_date, '%Y-%m')='$date1' order by date1  DESC";
		return $this->db->query($sql);     
	}
}