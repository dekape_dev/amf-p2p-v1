<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * 
 * @author 	fikriwirawan
 * @since	Oct 14, 2013
 */
class blog_model extends MY_Model {

    protected $table        = 'p2p_blog';
    protected $key          = 'blog_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    protected $set_created  = true;
    protected $set_modified = false;

    public function __construct(){
        parent::__construct();
    }
    
	public function get_all($limit)	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deleted', '0');
		$this->db->limit('3', '0');
		$this->db->order_by('blog_date', 'desc');
		return $this->db->get();
		
	} 
	
	public function get_latest($limit)	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deleted', '0');
		$this->db->limit('1', '0');
		$this->db->order_by('blog_date', 'desc');
		return $this->db->get();
		
	} 
	
	
	public function get_blog($param){
        $this->db->where('blog_id', $param);
        return $this->db->get($this->table);    
    }
	function getArchiveArticle()
	{	
		$sql="SELECT count('blog_title') as jumlah , date_format(blog_date, '%M %Y') as date,
		date_format(blog_date, '%Y-%m') as date1 	FROM ".$this->table." GROUP BY date1 order by date1  DESC";
		return $this->db->query($sql); 
	}  
   public function get_archieve($date1)	{ 
		$sql="SELECT blog_id, blog_title, blog_date,blog_content, date_format(blog_date, '%Y-%m') as date1 FROM "
		.$this->table." where  date_format(blog_date, '%Y-%m')='$date1' order by date1  DESC";
		return $this->db->query($sql);     
	}
}