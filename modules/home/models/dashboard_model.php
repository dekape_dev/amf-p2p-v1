<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 /*
 * @author 		fikriwirawan
 * @since       Jan 02, 2016
 * @package		Amartha
 */
 

class dashboard_model extends MY_Model {

    protected $table        = 'p2p_borrower';
    protected $key          = 'borrower_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';

    public function __construct(){
        parent::__construct();
    }
    
	//BORROWER COUNT
	public function count_borrower_p2p()	{
		return $this->db->select("COUNT(data_id) AS ammount")
					->from('tbl_pembiayaan')
					->where('data_p2p != 0')
					->where('tbl_pembiayaan.deleted', '0')						
					->get()
					->row()
					->ammount;
	} 
	
	public function count_borrower_funding()	{
		return $this->db->select("COUNT(data_id) AS ammount")
					->from('tbl_pembiayaan')
					->where('data_status', '2')
					->where('data_p2p_lender = 0')
					->where('data_p2p = 1')
					->where('tbl_pembiayaan.deleted', '0')						
					->get()
					->row()
					->ammount;
	} 

	public function count_borrower_funded()	{
		return $this->db->select("COUNT(data_id) AS ammount")
					->from('tbl_pembiayaan')
					->where('data_p2p_lender != 0')
					->where('data_p2p = 1')
					->where('tbl_pembiayaan.deleted', '0')						
					->get()
					->row()
					->ammount;
	} 

	public function count_borrower_notfunded()	{
		return $this->db->select("COUNT(data_id) AS ammount")
					->from('tbl_pembiayaan')
					->where('data_p2p_lender = 0')
					->where('data_p2p = 2')
					->where('tbl_pembiayaan.deleted', '0')						
					->get()
					->row()
					->ammount;
	} 
	
	//BORROWER SUM
	public function sum_borrower_funding()	{
		return $this->db->select("SUM(data_plafond) AS ammount")
					->from('tbl_pembiayaan')
					->where('data_status', '2')
					->where('data_p2p_lender = 0')
					->where('data_p2p = 1')
					->where('tbl_pembiayaan.deleted', '0')						
					->get()
					->row()
					->ammount;
	} 

	public function sum_borrower_funded()	{
		return $this->db->select("SUM(data_plafond) AS ammount")
					->from('tbl_pembiayaan')
					->where('data_p2p_lender != 0')
					->where('data_p2p = 1')
					->where('tbl_pembiayaan.deleted', '0')						
					->get()
					->row()
					->ammount;
	} 

	public function sum_borrower_notfunded()	{
		return $this->db->select("SUM(data_plafond) AS ammount")
					->from('tbl_pembiayaan')
					->where('data_p2p_lender = 0')
					->where('data_p2p = 2')
					->where('tbl_pembiayaan.deleted', '0')						
					->get()
					->row()
					->ammount;
	} 

	//INVESTOR COUNT
	public function count_lender_register()	{
		return $this->db->select("COUNT(lender_id) AS ammount")
					->from('p2p_lender')
					->where('deleted', '0')		
					->get()
					->row()
					->ammount;
	} 
	public function count_lender_enable()	{
		return $this->db->select("COUNT(lender_id) AS ammount")
					->from('p2p_lender')
					->where('lender_status', 'Enable')
					->where('deleted', '0')		
					->get()
					->row()
					->ammount;
	} 
	public function count_lender_disable()	{
		return $this->db->select("COUNT(lender_id) AS ammount")
					->from('p2p_lender')
					->where('lender_status', 'Disable')
					->where('deleted', '0')		
					->get()
					->row()
					->ammount;
	} 
	public function count_lender_invest()	{
		return $this->db->select("COUNT(DISTINCT (deposit_lender)) AS ammount")
					->from('p2p_deposit')
					->where('deposit_status', 'Success')
					->where('deleted', '0')		
					->get()
					->row()
					->ammount;
	} 
	
	//COUNT DEPOSIT
	public function count_deposit_success()	{
		return $this->db->select("COUNT(deposit_id) AS ammount")
					->from('p2p_deposit')
					->where('deposit_status', 'Success')
					->where('deleted', '0')		
					->get()
					->row()
					->ammount;
	} 
	public function count_deposit_verification()	{
		return $this->db->select("COUNT(deposit_id) AS ammount")
					->from('p2p_deposit')
					->where('deposit_status', 'Verification')
					->where('deleted', '0')		
					->get()
					->row()
					->ammount;
	} 
	
}