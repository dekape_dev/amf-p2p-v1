<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Front_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('blog_model');
		$this->load->model('news_model');

		$siteLanguage = $this->session->userdata('site_lang');
		if ($siteLanguage) {
        $this->lang->load('aboutus',$siteLanguage);
    } else {
        $this->lang->load('aboutus','english');
    }
	}

	public function index(){
		$this->template	->set('sitetitle', 'Home')
						->set('menu_homes', 'active')
						->build('home');
	}

	public function about(){
		$this->template	->set('sitetitle', 'About Us')
						->set('menu_about', 'active')
						->build('about');
	}

	public function impact(){
		$this->template	->set('sitetitle', 'Impact')
						->set('menu_impact', 'active')
						->build('impact');
	}

	public function payment(){
		$this->template	->set('sitetitle', 'Payment')
						->set('menu_payment', 'active')
						->build('payment');
	}

	public function borrow($type='under20mil'){
		if($type == 'under20mil')
		{
			$this->template	->set('sitetitle', 'Borrowing')
						->set('menu_borrow', 'active')
						->build('borrowless20mil');
		}
		else if($type == 'above20mil'){
			$this->template	->set('sitetitle', 'Borrowing')
						->set('menu_borrow', 'active')
						->build('borrowmore20mil');
		}

	}

	// public function story(){
	// 	$this->template	->set('sitetitle', 'Registration')
	// 					->set('menu_impact', 'active')
	// 					->build('story');
	// }

	public function story($type){
		if($type == 'story')
		{
			$this->template	->set('sitetitle', 'Enah: Micro-entrepreneur with a Big Vision')
						->set('menu_story', 'active')
						->build('story');
		}

		else if($type == 'story2')
		{
			$this->template->set('sitetitle', 'Yanih: Samak Maker, Pejuang Keluarga')
						->set('menu_story', 'active')
						->build('story_2');
		}

		else if($type == 'story3')
		{
			$this->template->set('sitetitle', 'Onih Triana: Striving for a Dream')
						->set('menu_story', 'active')
						->build('story_3');
		}

		else if($type == 'story4')
		{
			$this->template->set('sitetitle', 'Onih Triana: Striving for a Dream')
						->set('menu_story', 'active')
						->build('story_4');
		}

		else if($type == 'story5')
		{
			$this->template->set('sitetitle', 'Onih Triana: Striving for a Dream')
						->set('menu_story', 'active')
						->build('story_5');
		}

		else if($type == 'story6')
		{
			$this->template->set('sitetitle', 'Onih Triana: Striving for a Dream')
						->set('menu_story', 'active')
						->build('story_6');
		}

		else if($type == 'healthy')
		{
			$this->template	->set('sitetitle', 'Healthy Children, Happy Families')
						->set('menu_story', 'active')
						->build('story_healthy');
		}
		else if($type == 'glasses'){
			$this->template	->set('sitetitle', 'Free Glasses to Increase Women Productivity')
						->set('menu_story', 'active')
						->build('story_glasses');
		}

		else if($type == 'duck'){
			$this->template	->set('sitetitle', 'Duck Breeding: Local Entrepreneurship Training')
						->set('menu_story', 'active')
						->build('story_duck');
		}

		else if($type == 'vote'){
			$this->template	->set('sitetitle', 'Let’s Vote Smart!')
						->set('menu_story', 'active')
						->build('story_vote');
		}

		else if($type == 'tooth'){
			$this->template	->set('sitetitle', 'Tooth Fairies Paint a Million Smiles on World Oral Health Day')
						->set('menu_story', 'active')
						->build('story_tooth');
		}

		else if($type == 'groceries'){
			$this->template	->set('sitetitle', 'Ramadhan Cheap Groceries')
						->set('menu_story', 'active')
						->build('story_groceries');
		}

		else if($type == 'financial'){
			$this->template	->set('sitetitle', 'Financial Literacy in Rural Areas')
						->set('menu_story', 'active')
						->build('story_financial');
		}

		else if($type == 'talkshow'){
			$this->template	->set('sitetitle', 'Inspirational Talk Show')
						->set('menu_story', 'active')
						->build('story_talkshow');
		}

		elseif($type == 'eid'){
			$this->template->set('sitetitle', 'Eid Al-Adha')
						->set('menu_story','active')
						->build('story_eid');

		}

	}



	public function formborrow($type){
		if($type == 'under20mil')
		{
			$this->template	->set('sitetitle', 'Form Registration')
						->set('menu_form', 'active')
						->build('formless20mil');
		}
		else if($type == 'above20mil'){
			$this->template	->set('sitetitle', 'Form Registration')
						->set('menu_form', 'active')
						->build('formmore20mil');
		}
	}

	public function contact(){
		if($this->send_contact()){
			$this->session->set_flashdata('message', 'success|Message sent. We will reply your message soon.');
			redirect('contact');
		}

		$this->template	->set('sitetitle', 'Contact')
						->set('menu_contact', 'active')
						->build('contact');
	}
	public function career(){

		$this->template	->set('sitetitle', 'Career')
						->set('menu_career', 'active')
						->build('career');
	}
	public function faq(){
		$this->template	->set('sitetitle', 'FAQ')
						->set('menu_faq', 'active')
						->build('faq');
	}
	public function partners(){
		$this->template	->set('sitetitle', 'Partners')
						->set('menu_partners', 'active')
						->build('partners');
	}

	public function switchLang($language='') {
        $language = ($language != "") ? $language : "indonesia"; //english
        $this->session->set_userdata('site_lang', $language);
        redirect($_SERVER['HTTP_REFERER']);
  	}

	private function send_contact(){
		$name = $this->input->post("c_name");
		$email = $this->input->post("c_email");
		$phone = $this->input->post("c_phone");
		$title = $this->input->post("c_subject");
		$msg = nl2br($this->input->post("c_msg"));
		$spam = $this->input->post("c_phone");

		if($name AND $email AND $msg AND !$spam){
			//UPDATE EMAIL
			$this->load->library('email');
			$config = Array(
				'protocol'  => 'smtp',
				'smtp_host' => 'ssl://smtp.mailgun.org',
				'smtp_port' => '465',
				'smtp_user' => 'postmaster@mg.amartha.com', // change it to yours
				'smtp_pass' => '91aa7113ba6e002f94883dd4421a14c1', // change it to yours
				'mailtype'  => 'html',
				'charset'   => 'utf-8',
				'wordwrap'  => FALSE,
				'newline'   => "\r\n"
			);

			$this->email->initialize($config);

			$this->email->from('noreply@amartha.com','Amartha Web Message'); // change it to yours
			$this->email->to('info@amartha.com'); // change it to yours
			$this->email->subject("Web Message - $name");

			$messagebody = 'Hello, you got a message<br/><br/>';
			$messagebody .= "<b>NAME</b> : $name <br/>";
			$messagebody .= "<b>EMAIL</b> : $email <br/>";
			$messagebody .= "<b>PHONE</b> : $phone <br/>";
			$messagebody .= "<b>SUBJECT</b> : $title <br/>";
			$messagebody .= "<b>MESSAGE</b> : $msg <br/><br/>";
			$messagebody .= "Thanks";

			$this->email->message($messagebody);
			$this->email->send();
			return true;

			// RECAPTCHA
			// Load the library
			$this->load->library('recaptcha');
			// Catch the user's answer
			$captcha_answer = $this->input->post('g-recaptcha-response');
			// Verify user's answer
			$response = $this->recaptcha->verifyResponse($captcha_answer);
			// Processing ...
			if ($response['success']) {
			    // Your success code here
			} else {
			    // Something goes wrong
			    var_dump($response);
			}

		}else{
			return false;
		}
	}
}
