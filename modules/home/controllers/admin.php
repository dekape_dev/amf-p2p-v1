<?php

 /*
 * @author 		fikriwirawan
 * @since       Nov 08, 2015
 * @package		Arkana
 * @version		1.0
 */

class Admin extends Admin_Controller{
	
	private $per_page 	= '10';
	private $title 		= 'Dashboard';
	private $module 	= 'home';
	
	public function __construct(){
		parent::__construct();		
		$this->load->model('dashboard_model');
	}
	
	public function index(){
		if($this->session->userdata('logged_in'))
		{
			//COUNT BORROWER
			$borrower['funding'] = $this->dashboard_model->count_borrower_funding();
			$borrower['funded'] = $this->dashboard_model->count_borrower_funded();
			$borrower['notfunded'] = $this->dashboard_model->count_borrower_notfunded();
			$borrower['total'] = $this->dashboard_model->count_borrower_p2p();			
			
			//SUM PLAFOND BORROWER
			$borrower['sum_funding'] = $this->dashboard_model->sum_borrower_funding();
			$borrower['sum_funded'] = $this->dashboard_model->sum_borrower_funded();
			$borrower['sum_notfunded'] = $this->dashboard_model->sum_borrower_notfunded();
			
			
			//COUNT INVESTOR
			$lender['register'] = $this->dashboard_model->count_lender_register();
			$lender['register_enable'] = $this->dashboard_model->count_lender_enable();
			$lender['register_disable'] = $this->dashboard_model->count_lender_disable();
			$lender['invest'] = $this->dashboard_model->count_lender_invest();
			
			//COUNT DEPOSIT
			$deposit['success'] = $this->dashboard_model->count_deposit_success();
			$deposit['verification'] = $this->dashboard_model->count_deposit_verification();
			$deposit['total'] = $deposit['success'] + $deposit['verification'];
			
			$this->template	->set('menu_title', 'Dashboard')
							->set('borrower', $borrower )
							->set('lender', $lender )
							->set('deposit', $deposit )
							->build('dashboard');	
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('admin/login', 'refresh');
		}
	}
	
}