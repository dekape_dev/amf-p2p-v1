<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location extends Front_Controller{

	public function __construct(){
		parent::__construct();		
		$this->load->model('location/location_model');
	}

	public function index(){

	}

	public function kota(){
		$q = $this->input->get('q');
		$city = $this->location_model->get_city($q);
		echo "<option selected value=''>Pilih Kota/Kab</option>";
		foreach($city as $row){
			echo "<option value='".$row->lokasi_propinsi.".".$row->lokasi_kabupatenkota."'>".$row->lokasi_nama."</option>";
		}
	}
	
	public function kecamatan(){
		$q = $this->input->get('q');
		$kec = $this->location_model->get_kecamatan($q);
		echo "<option selected value=''>Pilih Kecamatan</option>";
		foreach($kec as $row){
			echo "<option value='".$row->lokasi_propinsi.".".$row->lokasi_kabupatenkota.".".$row->lokasi_kecamatan."'>".$row->lokasi_nama."</option>";
		}
	}
	
	public function kelurahan(){
		$q = $this->input->get('q');
		$kel = $this->location_model->get_kelurahan($q);
		echo "<option selected value=''>Pilih Kelurahan</option>";
		foreach($kel as $row){
			echo "<option value='".$row->lokasi_propinsi.".".$row->lokasi_kabupatenkota.".".$row->lokasi_kecamatan.".".$row->lokasi_kelurahan."'>".$row->lokasi_nama."</option>";
		}
	}

	

}