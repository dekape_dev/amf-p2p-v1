<?php (defined('BASEPATH')) OR exit('No direct script access allowed');


class location_model extends MY_Model {

    protected $table        = 'inf_lokasi';
    protected $key          = 'lender_id';
    protected $soft_deletes =  true;
    protected $date_format  = 'datetime';

    public function __construct(){
        parent::__construct();
    }
	
	public function get_province()	{
		return $this->db->select("*")
						->from('inf_lokasi')
						->where('lokasi_kabupatenkota', '0')
						->where('lokasi_kecamatan', '0')
						->where('lokasi_kelurahan', '0')
						->order_by('lokasi_nama', 'asc')
						->get()
						->result();

	}
	
	
	public function get_city($province)	{
		return $this->db->select("*")
						->from('inf_lokasi')
						->where('lokasi_propinsi', $province)
						->where('lokasi_kabupatenkota != 0')
						->where('lokasi_kecamatan', '0')
						->where('lokasi_kelurahan', '0')
						->order_by('lokasi_nama', 'asc')
						->get()
						->result();

	}
	
	public function get_kecamatan($code)	{
		return $this->db->select("*")
						->from('inf_lokasi')
						->where("lokasi_kode LIKE '$code%'")
						->where('lokasi_kecamatan != 0')
						->where('lokasi_kelurahan', '0')
						->order_by('lokasi_nama', 'asc')
						->get()
						->result();

	}
	
	public function get_kelurahan($code)	{
		return $this->db->select("*")
						->from('inf_lokasi')
						->where("lokasi_kode LIKE '$code%'")
						->where('lokasi_kelurahan != 0')
						->order_by('lokasi_nama', 'asc')
						->get()
						->result();

	}
	
	public function get_location_by_code($code)	{
		return $this->db->select("*")
						->from('inf_lokasi')
						->where("lokasi_kode", $code)
						->get()
						->result();

	}
}