<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Incomplete extends Front_Controller{

  public function __construct(){
    parent::__construct();
    $this->load->library('cart');
  }

  public function index(){
    echo 'Incomplete!<br/>';
    $vt_code = $this->input->get();
    var_dump($vt_code);
  }

}
