<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Checkout extends Front_Controller{

  public function __construct(){
    parent::__construct();
    $this->load->library('cart');
	  $this->load->model('wallet_model');
	  $this->load->model('wallet_tr_model');
    $this->load->model('deposit_model');
    $this->load->model('transaction_model');
    $this->load->model('investor/investor_model');
    $this->load->model('location/location_model');
  }

  public function pay_with_wallet(){
    $date = date("Y-m-d");
    $time = date("H:i:s");

    $userdata  = $this->session->userdata('logged_in_user');
    $lender_id = $userdata['uid'];

    //Check ewallet account
    $ewallet          = $this->wallet_model->get_wallet($lender_id);
    $ewallet          = $ewallet[0];
    $ewallet_id       = $ewallet->wallet_id;
    $ewallet_credit   = $ewallet->wallet_credit;
    $ewallet_debet    = $ewallet->wallet_debet;
    $ewallet_balance  = $ewallet->wallet_saldo;

    //Transaction amount
    $order_no     = 'WL'.date("ymdHis");
    $gross_amount = $this->cart->total();
    $netto_amount = $ewallet_balance - $gross_amount; //Alwats +ve

    //Record the order
    foreach ($this->cart->contents() as $items):
      $data_client = $this->transaction_model->get_client_of_loan($items['id']);
      $data_client = $data_client[0]->data_client;
      $data_order  = array(
              'order_no'            => $order_no,
              'order_loan_id'       => $items['id'],
              'order_lender_id'     => $lender_id,
              'order_wallet_id'     => $ewallet_id,
              'order_borrower_id'   => $data_client,
              'order_borrower_loan' => $items['price'],
              'order_wallet'        => $ewallet_balance,
              'order_gross_amount'  => $gross_amount,
              'order_nett_amount'   => $netto_amount,
              'order_status'        => 'SUCCESS'
      );
      $this->db->insert('p2p_order', $data_order);
    endforeach;

    //Update e-wallet account SUBTRACT funding from saldo.
    $data_wallet = array(
              'wallet_debet'    => $ewallet_debet,
              'wallet_credit'   => $ewallet_credit + $gross_amount,
              'wallet_saldo'    => $ewallet_balance - $gross_amount,
              'wallet_remark'   => "FUNDING ORDER #".$order_no
    );
    $this->db->update('p2p_wallet', $data_wallet, "wallet_lender = ".$lender_id);

    //Record the wallet transaction per wallet account.
    $data_wallet_tr = array(
            'wallet_id'           => $ewallet_id,
            'wallet_date'         => $date,
            'wallet_lender'       => $lender_id,
            'wallet_debet'        => 0,
            'wallet_credit'       => $gross_amount,
            'wallet_saldo'        => $gross_amount * -1,
            'wallet_remark'       => "PAYMENT LOAN FUNDING ORDER #".$order_no,
            'wallet_status'       => "SUCCESS",
            'wallet_payment_type' => "e-wallet",
            'created_on'          => $date.' '.$time
    );
    $this->db->insert('p2p_tr_wallet', $data_wallet_tr);

    //Record the transaction details
    //& Update Financing Data on 'tbl_pembiayaan'
    $n = 0;
    foreach ($this->cart->contents() as $items):
      $data_client      = $this->transaction_model->get_client_of_loan($items['id']);
      $data_client      = $data_client[0]->data_client;
      $data_transaction = array(
              'trx_order_no' => $order_no,
              'trx_loan'     => $items['id'],
              'trx_borrower' => $data_client,
              'trx_lender'   => $lender_id,
              'trx_date'     => $date,
              'trx_amount'   => $items['price'],
              'trx_type'     => "e-wallet",
              'trx_status'   => 'SUCCESS',
              'created_on'   => $date.' '.$time
      );
      $this->db->insert('p2p_trx', $data_transaction);

      $data_pembiayaan = array(
              'data_p2p'        => 1,
              'data_p2p_lender' => $lender_id
      );
      $this->db->where('data_id', $items['id']);
      $this->db->update('tbl_pembiayaan', $data_pembiayaan);

      $n++;
    endforeach;

    $this->session->set_flashdata('message', 'success|Transaction #'. $order_no .' is successful.');
    redirect('dashboard');

  }

  public function veritrans(){

    $this->load->library('Veritrans');
    // Set Your Veritrans server key
    Veritrans_Config::$serverKey    = "VT-server-QOvgH4UkXItRTuQbBRjiwuiH";
    Veritrans_Config::$isProduction = true;

	
    //Veritrans_Config::$serverKey    = "VT-server-vhLjDuD3Tj55D-TOzB6NKIeZ";
    //Veritrans_Config::$isProduction = false;
	
    // Uncomment for production environment
    // Veritrans_Config::$isProduction = true;

    // Uncomment to enable sanitization
    // Veritrans_Config::$isSanitized = true;

    // Uncomment to enable 3D-Secure
    // Veritrans_Config::$is3ds = true;

    // Required
    $date      = date("Y-m-d");
    $time      = date("H:i:s");

    $wallet    = $this->session->userdata('wallet');
    $wallet_id = $this->session->userdata('wallet_id');
    $userdata  = $this->session->userdata('logged_in_user');
    $lender_id = $userdata['uid'];
	$lender_detail = $this->investor_model->get_lender($lender_id)->result();
	$lender_detail = $lender_detail[0];
	
    $order_id     = 'TL'.date("ymdHis");
    $gross_amount = $this->cart->total();
    //As GROSS > WALLET, TOP UP = |WALLET - GROSS|
    $netto_amount = $gross_amount - $wallet;

    /************* Amartha Transactional Records **************/
    //Record the order so that we can track the variables
    //in order to record to other transactional records' tables
    foreach ($this->cart->contents() as $items):
      $data_client = $this->transaction_model->get_client_of_loan($items['id']);
      $data_client = $data_client[0]->data_client;
      $data_order  = array(
              'order_no'            => $order_id,
              'order_loan_id'       => $items['id'],
              'order_lender_id'     => $lender_id,
              'order_wallet_id'     => $wallet_id,
              'order_borrower_id'   => $data_client,
              'order_borrower_loan' => $items['price'],
              'order_wallet'        => $wallet,
              'order_gross_amount'  => $gross_amount,
              'order_nett_amount'   => $netto_amount,
              'order_status'        => 'WAITING PAYMENT'
      );
      $this->db->insert('p2p_order', $data_order);
    endforeach;

    /************* VT Details **********/
    $transaction_details = array(
      'order_id'     => $order_id,
      'gross_amount' => $netto_amount
      //No decimal allowed for creditcard
      );
    
    // Optional
    $n = 0;
    $item_details = array ();
    foreach ($this->cart->contents() as $items):
      $borrower = array(
          'quantity' => 1,
          'id'       => ($n+1).'p2p',
          'name'     => $items['name'],
          'price'    => $items['price']
      );
      //var_dump($borrower);
      $item_details[$n] = $borrower;
      $n++;
    endforeach;

    $discount = array(
          'quantity' => 1,
          'id'       => ($n+1).'p2p-wallet',
          'name'     => 'Available P2P Wallet',
          'price'    => $wallet * -1
      );

    $item_details[$n] = $discount;
    //var_dump($item_details);
    //die();

    // Optional
    $billing_address = array(
        'first_name'    => $userdata['ufirstname'],
        'last_name'     => $userdata['ulastname'],
        'address'       => "PT Amartha Mikro Fintek",
        'city'          => "Bogor",
        'postal_code'   => "12150",
        'phone'         => "+6289603088338",
        'email'         => $userdata['uemail'],
        'country_code'  => 'IDN'
        );

    // Optional
	$get_province = $this->location_model->get_location_by_code($lender_detail->lender_province.".00.00.0000");
	$location_province = $get_province[0]->lokasi_nama;
	$get_city = $this->location_model->get_location_by_code($lender_detail->lender_city.".00.0000");
	$location_city = $get_city[0]->lokasi_nama;
			
    $shipping_address = array(
        'first_name'    => $userdata['ufirstname'],
        'last_name'     => $userdata['ulastname'],
        'address'       => "$lender_detail->lender_address, ",
        'city'          => "$location_city",
        'postal_code'   => "$lender_detail->lender_zipcode",
        'phone'         => $lender_detail->lender_phone,
        'email'         => $userdata['uemail'],
        'country_code'  => 'IDN'
        );

    // Optional
    $customer_details = array(
        'first_name'       => $userdata['ufirstname'],
        'last_name'        => $userdata['ulastname'],
        'email'            => $userdata['uemail'],
        'phone'            => $lender_detail->lender_phone,
        'billing_address'  => $billing_address,
        'shipping_address' => $shipping_address
        );

    // Fill transaction details
    /*
        'enabled_payments'      => array("credit_card"),
        'credit_card_3d_secure' => true,
        'finish_redirect_url'   => "http://URL/transaction/success",
        'unfinish_redirect_url' => "http://URL/transaction/incomplete",
        'error_redirect_url'    => "http://URL/transaction/error",
    */

    $transaction = array(
        'transaction_details' => $transaction_details,
        'customer_details'    => $customer_details,
        'item_details'        => $item_details,
        );
    //var_dump($transaction); die();

    try {
      // Redirect to Veritrans VTWeb page
      header('Location: ' . Veritrans_VtWeb::getRedirectionUrl($transaction));
    }
    catch (Exception $e) {
      echo $e->getMessage();
      if(strpos ($e->getMessage(), "Access denied due to unauthorized")){
          echo "<code>";
          echo "<h4>Please set real server key from sandbox</h4>";
          echo "In file: " . __FILE__;
          echo "<br>";
          echo "<br>";
          echo htmlspecialchars('Veritrans_Config::$serverKey = \'VT-server-QOvgH4UkXItRTuQbBRjiwuiH\';');
          die();
      }
    }

  }

  public function e_wallet(){
    //echo 'Trx e-Wallet';

	$this->load->library('Veritrans');
	// Set Your Veritrans server key
	Veritrans_Config::$serverKey    = "VT-server-QOvgH4UkXItRTuQbBRjiwuiH";
	Veritrans_Config::$isProduction = true;

  
  //  Veritrans_Config::$serverKey    = "VT-server-vhLjDuD3Tj55D-TOzB6NKIeZ";
  //  Veritrans_Config::$isProduction = false;
	
  // Uncomment for production environment
  // Veritrans_Config::$isProduction = true;

  // Uncomment to enable sanitization
  // Veritrans_Config::$isSanitized = true;

  // Uncomment to enable 3D-Secure
  // Veritrans_Config::$is3ds = true;

  // Required
	$this->db->trans_start();

  //Transaction Details
	$order_id  = 'TW'.date("ymdHis");
	$date      = date("Y-m-d");
	$time      = date("H:i:s");

  $wallet    = $this->session->userdata('wallet');
  $userdata  = $this->session->userdata('logged_in_user');
	$lender_id = $userdata['uid'];
	$lender_detail = $this->investor_model->get_lender($lender_id)->result();
	$lender_detail = $lender_detail[0];

  //Check eWallet Account
  $ewallet          = $this->wallet_model->get_wallet($lender_id);
  $ewallet          = $ewallet[0];

  $wallet_id       = 0;
  $wallet_credit   = 0;
  $wallet_debet    = 0;
  $wallet_balance  = 0;

	if($ewallet->wallet_id){
		  $wallet_id       = $ewallet->wallet_id;
      $wallet_credit   = $ewallet->wallet_credit;
      $wallet_debet    = $ewallet->wallet_debet;
      $wallet_balance  = $ewallet->wallet_saldo;
	}else
  {
		$data_wallet = array(
		  'wallet_lender' 	=> $lender_id,
		  'wallet_debet' 	  => $wallet_debet,
		  'wallet_credit' 	=> $wallet_credit,
		  'wallet_saldo' 	  => $wallet_balance,
		  'wallet_remark' 	=> "Open e-Wallet Account at $date",
		);
		$this->wallet_model->insert($data_wallet);
		$wallet_id = $this->db->insert_id();
	}

  //Record Order of Wallet's Top Up
  $topup_amount = str_replace(",", "", $this->input->post("topup_ammount"));
  $data_order  = array(
      'order_no'            => $order_id,
      'order_loan_id'       => '0',
      'order_lender_id'     => $lender_id,
      'order_wallet_id'     => $wallet_id,
      'order_borrower_id'   => '0',
      'order_borrower_loan' => '0',
      'order_wallet'        => $wallet_balance,
      'order_gross_amount'  => $topup_amount,
      'order_nett_amount'   => $topup_amount,
      'order_status'        => 'WAITING PAYMENT'
  );
  $this->db->insert('p2p_order', $data_order);

	//Insert Wallet Transaction
	$data_wallet_tr = array(
      'wallet_id'   	 => $wallet_id,
      'wallet_date'	 	 => $date,
      'wallet_lender'  => $lender_id,
      'wallet_debet' 	 => $topup_amount,
      'wallet_credit'  => 0,
      'wallet_saldo' 	 => $topup_amount,
      'wallet_status'  => "WAITING PAYMENT",
      'wallet_remark'  => "TOPUP ".$userdata['ufullname']." ".$topup_amount,
      );
	$this->wallet_tr_model->insert($data_wallet_tr);
	$wallet_tr_id = $this->db->insert_id();


    $transaction_details = array(
      'order_id'     => $order_id.'-'.$wallet_tr_id,
      'gross_amount' => $topup_amount // no decimal allowed for creditcard
    );

    // Optional
    $item_details = array(
          'quantity' => 1,
          'id'       => $wallet_tr_id,
          'name'     => 'Top Up Amartha eWallet',
          'price'    => $topup_amount
    );


    //var_dump($item_details);
    //die();

    // Optional
    $billing_address = array(
        'first_name'    =>  $userdata['ufirstname'],
        'last_name'     =>  $userdata['ulastname'],
        'address'       => "PT Amartha Mikro Fintek",
        'city'          => "Bogor",
        'postal_code'   => "12150",
        'phone'         => "+6289603088338",
        'email'         => $userdata['uemail'],
        'country_code'  => 'IDN'
        );

    // Optional
	
	$get_province = $this->location_model->get_location_by_code($lender_detail->lender_province.".00.00.0000");
	$location_province = $get_province[0]->lokasi_nama;
	$get_city = $this->location_model->get_location_by_code($lender_detail->lender_city.".00.0000");
	$location_city = $get_city[0]->lokasi_nama;
    $shipping_address = array(
        'first_name'    =>  $userdata['ufirstname'],
        'last_name'     =>  $userdata['ulastname'],
        'address'       =>  "$lender_detail->lender_address",
        'city'          =>  "$location_city",
        'postal_code'   =>  "$lender_detail->lender_zipcode",
        'phone'         =>  $lender_detail->lender_phone,
        'email'         =>  $userdata['uemail'],
        'country_code'  =>  'IDN'
        );

    // Optional
    $customer_details = array(
        'first_name'       => $userdata['ufirstname'],
        'last_name'        => $userdata['ulastname'],
        'email'            => $userdata['uemail'],
        'phone'            => $lender_detail->lender_phone,
        'billing_address'  => $billing_address,
        'shipping_address' => $shipping_address
        );

    // Fill transaction details
    /*
        'enabled_payments'      => array("credit_card"),
        'credit_card_3d_secure' => true,
        'finish_redirect_url'   => "http://URL/transaction/success",
        'unfinish_redirect_url' => "http://URL/transaction/incomplete",
        'error_redirect_url'    => "http://URL/transaction/error",
    */

    $transaction = array(
        'transaction_details' => $transaction_details,
        'customer_details'    => $customer_details,
        'item_details'        => $item_details,
        );
    //var_dump($transaction); die();
	$this->db->trans_complete();



    try {
      // Redirect to Veritrans VTWeb page
      header('Location: ' . Veritrans_VtWeb::getRedirectionUrl($transaction));
    }
    catch (Exception $e) {
      echo $e->getMessage();
      if(strpos ($e->getMessage(), "Access denied due to unauthorized")){
          echo "<code>";
          echo "<h4>Please set real server key from sandbox</h4>";
          echo "In file: " . __FILE__;
          echo "<br>";
          echo "<br>";
          echo htmlspecialchars('Veritrans_Config::$serverKey = \'VT-server-QOvgH4UkXItRTuQbBRjiwuiH\';');
          die();
      }
    }

  }


  public function e_wallet2(){
    //echo 'Trx e-Wallet';

	$this->load->library('Veritrans');
    // Set Your Veritrans server key
    Veritrans_Config::$serverKey    = "VT-server-QOvgH4UkXItRTuQbBRjiwuiH";
    Veritrans_Config::$isProduction = true;

    // Uncomment for production environment
    // Veritrans_Config::$isProduction = true;

    // Uncomment to enable sanitization
    // Veritrans_Config::$isSanitized = true;

    // Uncomment to enable 3D-Secure
    // Veritrans_Config::$is3ds = true;

    // Required

	$transaction = array(
    'transaction_details' => array(
        'order_id' => rand(),
        'gross_amount' => 10000, // no decimal allowed for creditcard
        )
    );


    try {
      // Redirect to Veritrans VTWeb page
      header('Location: ' . Veritrans_VtWeb::getRedirectionUrl($transaction));
    }
    catch (Exception $e) {
      echo $e->getMessage();
      if(strpos ($e->getMessage(), "Access denied due to unauthorized")){
          echo "<code>";
          echo "<h4>Please set real server key from sandbox</h4>";
          echo "In file: " . __FILE__;
          echo "<br>";
          echo "<br>";
          echo htmlspecialchars('Veritrans_Config::$serverKey = \'VT-server-QOvgH4UkXItRTuQbBRjiwuiH\';');
          die();
      }
    }

  }

  
   public function cashout(){
		$ammount =  $this->input->post("ammount");
		$lender_id =  $this->input->post("lender");
		$date = date('Y-m-d');
		if($ammount AND $lender_id){
			$ammount = str_replace(',','',$ammount);
			$lender = $this->investor_model->get_lender($lender_id)->result();
			$lender = $lender[0];			
			
			$ewallet          = $this->wallet_model->get_wallet($lender_id);
			$ewallet          = $ewallet[0];
			$ewallet_id       = $ewallet->wallet_id;
			$ewallet_credit   = $ewallet->wallet_credit;
			$ewallet_debet    = $ewallet->wallet_debet;
			$ewallet_balance  = $ewallet->wallet_saldo;
			
			if($ewallet_balance > $ammount){
				$cashout_id = date("Ymdhis").$lender->lender_id;
				
				 
				//udpate TR WALLET
				 $data_wallet_tr = array(
							      'wallet_id'      			=> $ewallet_id,
							      'wallet_date'    			=> $date,
							      'wallet_lender'  			=> $lender_id,
							      'wallet_debet'   			=> 0,
							      'wallet_credit'  			=> $ammount,
							      'wallet_saldo'   			=> $ammount,
							      'wallet_remark'  			=> "CASHOUT #$cashout_id $lender->lender_first_name $lender->lender_last_name $ammount",
							      'wallet_status'  			=> "PENDING",
							      'wallet_payment_type' 	=> 'CASHOUT'
								);

				$this->db->insert('p2p_tr_wallet', $data_wallet_tr);
					  
					  
				//UPDATE EMAIL
				$this->load->library('email');
				$config = Array(
						'protocol'  => 'smtp',
						'smtp_host' => 'ssl://smtp.mailgun.org',
						'smtp_port' => '465',
						'smtp_user' => 'postmaster@mg.arkana.co.id', // change it to yours
						'smtp_pass' => '2d81625a14771268a92d806fe856bb55', // change it to yours
						'mailtype'  => 'html',
						'charset'   => 'utf-8',
						'wordwrap'  => FALSE,
						'newline'   => "\r\n"
					);

				$this->email->initialize($config);

				$this->email->from('noreply@amartha.com','Amartha'); // change it to yours
				$this->email->to('info@amartha.com'); // change it to yours
				$this->email->subject("Cashout Request $cashout_id");

				$messagebody = 'Hello, you got a cashout request<br/><br/>';
				$messagebody .= "<b>DATE</b> : ".date('d-m-Y')."<br/>";
				$messagebody .= "<b>CASHOUT ID</b> : $cashout_id <br/>";
				$messagebody .= "<b>NAME</b> : $lender->lender_first_name $lender->lender_last_name <br/>";
				$messagebody .= "<b>EMAIL</b> : $lender->lender_email <br/>";
				$messagebody .= "<b>AMMOUNT</b> : $ammount <br/><br/>";
				$messagebody .= "Thanks";

				$this->email->message($messagebody);
				$this->email->send();
				
				$this->session->set_flashdata('message', 'success|Cashout request successfully submitted.');
				redirect('investor/wallet');
			}else{
				
				$this->session->set_flashdata('message', 'success|Your wallet is not enough.');
				redirect('investor/wallet');
			}

		}else{
			return false;
		}
   }
}
