<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends Front_Controller{

  public function __construct(){
    	parent::__construct();
    	$this->load->library('cart');
    	$this->load->model('deposit_model');
		$this->load->model('order_model');
		$this->load->model('transaction_model');
		$this->load->model('wallet_model');
		$this->load->model('wallet_tr_model');
  }

  public function index()
  {
  		$this->cart->destroy();
		redirect('investor/wallet', 'refresh');
  }


  public function check_tr($order_id)
  {
		//Get Wallet Trasaction Detail
		$tr_wallet = $this->wallet_tr_model->get_transaction($order_id);
		$tr_wallet = $tr_wallet[0];

		$tr_wallet_debet = $tr_wallet->wallet_debet;
		$tr_wallet_credit = $tr_wallet->wallet_credit;
		$tr_wallet_saldo = $tr_wallet_debet - $tr_wallet_credit;

		$ewallet_id = $tr_wallet->wallet_id;
		echo "TR DEBET: ".$tr_wallet_debet."<br/>";
		echo "TR CREDIT: ".$tr_wallet_credit."<br/>";
		echo "TR SALDO: ".$tr_wallet_saldo."<br/><br/>";
		echo "wallet_id: ".$ewallet_id."<br/>";

		//Get Wallet Detail
		$ewallet = $this->wallet_model->get_transaction_by_wallet_id($ewallet_id);
		$ewallet = $ewallet[0];
		$ewallet_saldo = $ewallet->wallet_saldo;
		$ewallet_debet = $ewallet->wallet_debet;
		$ewallet_credit = $ewallet->wallet_credit;

		echo "WALLET DEBET: ".$ewallet_debet."<br/>";
		echo "WALLET CREDIT: ".$ewallet_credit."<br/>";
		echo "WALLET SALDO: ".$ewallet_saldo."<br/><br/>";

		//Store Wallet New Value
		$ewallet_saldo_new = $ewallet_saldo + $tr_wallet_saldo;
		$ewallet_debet_new = $ewallet_debet + $tr_wallet_debet;
		$ewallet_credit_new = $ewallet_credit + $tr_wallet_credit;

		echo "WALLET NEW DEBET: ".$ewallet_debet_new."<br/>";
		echo "WALLET NEW CREDIT: ".$ewallet_credit_new."<br/>";
		echo "WALLET NEW SALDO: ".$ewallet_saldo_new."<br/><br/>";

	}
}
