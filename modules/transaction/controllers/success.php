<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Success extends Front_Controller{
  protected $date = date("Y-m-d");
  protected $time = date("H:i:s");

  protected $wallet    = $this->session->userdata('wallet');
  protected $wallet_id = null;
  protected $userdata  = $this->session->userdata('logged_in_user');
  protected $lender_id = $userdata['uid'];

  public function __construct(){
    parent::__construct();
    $this->load->library('cart');
  }

  public function index(){
    echo 'Success!<br/>';
  }

}
