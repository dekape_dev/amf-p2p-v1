<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{

  public function __construct(){
    parent::__construct();
    $this->load->library('cart');
	$this->load->model('wallet_model');	
	$this->load->model('wallet_tr_model');	
  }

  public function wallet(){
	  
		//GET DATA
		$data = $this->wallet_tr_model->get_all_transaction()->result();
		$this->template	->set('menu_title', 'Wallet Transaction')
						->set('data', $data )
						->build('wallet_trx');	
  }

}
