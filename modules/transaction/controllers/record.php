<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Record extends Front_Controller{

  public function __construct()
  {
	  parent::__construct();
	  $this->load->library('cart');
	  $this->load->model('deposit_model');
		$this->load->model('order_model');
		$this->load->model('transaction_model');
		$this->load->model('wallet_model');
		$this->load->model('wallet_tr_model');
  }

  public function index()
  {
		
    $this->load->library('Veritrans');
    // Set Your Veritrans server key
    Veritrans_Config::$serverKey    = "VT-server-QOvgH4UkXItRTuQbBRjiwuiH";
    Veritrans_Config::$isProduction = true;
	
	
    //Veritrans_Config::$serverKey    = "VT-server-vhLjDuD3Tj55D-TOzB6NKIeZ";
    //Veritrans_Config::$isProduction = false;
	
	

	$date = date("Y-m-d");
	$time = date("H:i:s");

	$notif 		    = new Veritrans_Notification();

	$transaction    = $notif->transaction_status;
	$type 		    = $notif->payment_type;
	$order_id 	    = $notif->order_id;		
	$order_code     = substr($order_id, 0, 2);
	$fraud 		    = $notif->fraud_status;

	$orders    	    = $this->order_model->get_orders($order_id);
	$lender_id      = $orders[0]->order_lender_id;

	$wallet         = $this->wallet_model->get_wallet($lender_id);
	$wallet         = $wallet[0];
	$wallet_id      = $wallet->wallet_id;
	$wallet_credit  = $wallet->wallet_credit;
	$wallet_debet   = $wallet->wallet_debet;
	$wallet_balance = $wallet->wallet_saldo;

	$gross_amount	= $orders[0]->order_gross_amount;
	$netto_amount	= $orders[0]->order_nett_amount;

	//[1] When order is TOP UP for LOANS FUNDINGS (TL)
	if($order_code == 'TL'){ 
				if ($transaction == 'capture') 
				{
						// For credit card transaction,
						// We need to check whether transaction is challenge by FDS or not.
						if ($type == 'credit_card'){
							//CHALLENGED
						    if($fraud == 'challenge')
						    {
						      	  // TODO set payment status in merchant's database to 'Challenge by FDS'
						      	  // TODO merchant should decide whether this transaction is authorized or not in MAP
						      	  echo "Transaction order_id: " . $order_id ." is challenged by FDS";
						      	  //[0] Update Order Status as Challenged by FDS
						      	  $data_order  = array( 'order_status' => 'CHALLENGED' );
						      	  $this->db->where('order_no', $order_id);
						      	  $this->db->update('p2p_order', $data_order);
						    }
						    //NOT CHALLENGED
						    else 
						    {
						     	  // TODO set payment status in merchant's database to 'Settlement'
								  echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
								  //[0] Update Order Status as SUCCESS
								  $data_order  = array( 'order_status' => 'SUCCESS' );
								  $this->db->where('order_no', $order_id);
								  $this->db->update('p2p_order', $data_order);

								  //[1] Update e-wallet account SET saldo to 0.
								  $data_wallet = array(
										  'wallet_debet'    => $wallet_debet  + $netto_amount,
							              'wallet_credit'   => $wallet_credit + $gross_amount,
										  'wallet_saldo'    => 0,
										  'wallet_remark'   => "FUNDING FOR ORDER #".$order_id
								  );
								  $this->db->where('wallet_lender', $lender_id);
								  $this->db->update('p2p_wallet', $data_wallet);

								  //[2] Record the wallet transaction per wallet account.
								  $data_wallet_tr = array(
										      'wallet_id'      			=> $wallet_id,
										      'wallet_date'    			=> $date,
										      'wallet_lender'  			=> $lender_id,
										      'wallet_debet'   			=> $netto_amount,
										      'wallet_credit'  			=> 0,
										      'wallet_saldo'   			=> $netto_amount,
										      'wallet_remark'  			=> "PLEDGE FOR LOAN FUNDING ORDER #".$order_id,
										      'wallet_status'  			=> "SETTLED",
										      'wallet_payment_type' 	=> $type
								  );

								  $this->db->insert('p2p_tr_wallet', $data_wallet_tr);

								  //[3] Record the deposit transaction
								  $data_deposit = array(
									      'deposit_lender' => $lender_id,
									      'deposit_date'   => $date,
									      'deposit_amount' => $netto_amount,
									      'deposit_type'   => $type,
									      'deposit_status' => 'SUCCESS',
									      'created_by'     => $lender_id,
									      'modified_by'    => $lender_id,
									      'created_on'     => $date.' '.$time
							      );
							      $this->db->insert('p2p_deposit', $data_deposit);

							      //[4] Record the transaction details
								  // & Update Financing Data on 'tbl_pembiayaan'
								  for($n=0; $n<count($orders); $n++)
								  {
									$data_transaction = array(
											'trx_order_no' => $order_id,
											'trx_loan'     => $orders[$n]->order_loan_id,
											'trx_borrower' => $orders[$n]->order_borrower_id,
											'trx_lender'   => $lender_id,
											'trx_date' 	   => $date,
											'trx_amount'   => $orders[$n]->order_borrower_loan,
											'trx_type'	   => $type,
											'trx_status'   => 'SUCCESS',
										    'created_on'   => $date.' '.$time
									);
									$this->db->insert('p2p_trx', $data_transaction);

									$data_pembiayaan = array(
											'data_p2p' 		  => 1,
											'data_p2p_lender' => $lender_id
									);
									$this->db->where('data_id', $orders[$n]->order_loan_id);
									$this->db->update('tbl_pembiayaan', $data_pembiayaan);

								  } 
						    }
						}
						else if($type != 'credit_card'){
								//debit_card, cash card, etc.
						}
				}
				else if ($transaction == 'settlement')
				{
					$this->db->trans_start();
					  // TODO set payment status in merchant's database to 'Settlement'
					  echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
					  //[0] Update Order Status as SUCCESS
					  $data_order  = array( 'order_status' => 'SUCCESS' );
					  $this->db->where('order_no', $order_id);
					  $this->db->update('p2p_order', $data_order);

					  //[1] Update e-wallet account SET saldo to 0.
					  $data_wallet = array(
							  'wallet_debet'    => $wallet_debet  + $netto_amount,
				              'wallet_credit'   => $wallet_credit + $gross_amount,
							  'wallet_saldo'    => 0,
							  'wallet_remark'   => "FUNDING FOR ORDER #".$order_id
					  );
					  $this->db->where('wallet_lender', $lender_id);
					  $this->db->update('p2p_wallet', $data_wallet);

					  //[2] Record the wallet transaction per wallet account.
					  $data_wallet_tr = array(
							      'wallet_id'      			=> $wallet_id,
							      'wallet_date'    			=> $date,
							      'wallet_lender'  			=> $lender_id,
							      'wallet_debet'   			=> $netto_amount,
							      'wallet_credit'  			=> 0,
							      'wallet_saldo'   			=> $netto_amount,
							      'wallet_remark'  			=> "TOPUP FOR LOAN FUNDING ORDER #".$order_id,
							      'wallet_status'  			=> "SETTLED",
							      'wallet_payment_type' 	=> $type
					  );

					  $this->db->insert('p2p_tr_wallet', $data_wallet_tr);

					  //[3] Record the deposit transaction
					  $data_deposit = array(
						      'deposit_lender' => $lender_id,
						      'deposit_date'   => $date,
						      'deposit_amount' => $netto_amount,
						      'deposit_type'   => $type,
						      'deposit_status' => 'SUCCESS',
						      'created_by'     => $lender_id,
						      'modified_by'    => $lender_id,
						      'created_on'     => $date.' '.$time
				      );
				      $this->db->insert('p2p_deposit', $data_deposit);

				      //[4] Record the transaction details
					  // & Update Financing Data on 'tbl_pembiayaan'
					  for($n=0; $n<count($orders); $n++)
					  {
						$data_transaction = array(
								'trx_order_no' => $order_id,
								'trx_loan'     => $orders[$n]->order_loan_id,
								'trx_borrower' => $orders[$n]->order_borrower_id,
								'trx_lender'   => $lender_id,
								'trx_date' 	   => $date,
								'trx_amount'   => $orders[$n]->order_borrower_loan,
								'trx_type'	   => $type,
								'trx_status'   => 'SUCCESS',
							    'created_on'   => $date.' '.$time
						);
						$this->db->insert('p2p_trx', $data_transaction);

						$data_pembiayaan = array(
								'data_p2p' 		  => 1,
								'data_p2p_lender' => $lender_id
						);
						$this->db->where('data_id', $orders[$n]->order_loan_id);
						$this->db->update('tbl_pembiayaan', $data_pembiayaan);

					  }
					$this->db->trans_complete();
				}
				else if($transaction == 'pending'){
					  // TODO set payment status in merchant's database to 'Pending'
					  echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
					  //[0] Update Order Status as PENDING
					  $data_order  = array( 'order_status' => 'PENDING' );
					  $this->db->where('order_no', $order_id);
					  $this->db->update('p2p_order', $data_order);
				}
				else if ($transaction == 'deny') {
					  // TODO set payment status in merchant's database to 'Denied'
					  echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
					  //[0] Update Order Status as DENIED
					  $data_order  = array( 'order_status' => 'DENIED' );
					  $this->db->where('order_no', $order_id);
					  $this->db->update('p2p_order', $data_order);
				}
				else if ($transaction == 'cancel') {
					  // TODO set payment status in merchant's database to 'Denied'
					  echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is canceled.";
					  //[0] Update Order Status as CANCEL
					  $data_order  = array( 'order_status' => 'CANCEL' );
					  $this->db->where('order_no', $order_id);
					  $this->db->update('p2p_order', $data_order);
				}
	}

	//[2] When order is TOP UP for E-WALLET BALANCE
	else if($order_code == 'TW') 
	{
					//Get Wallet Trasaction Detail
					$order_id = explode('-', $order_id);
					//$order_id[0] = 'TMXXXXXXXXX';
					//$order_id[1] = $tr_wallet_id;

					$tr_wallet         = $this->wallet_tr_model->get_transaction($order_id[1]);
					$tr_wallet         = $tr_wallet[0];
					$tr_wallet_debet   = $tr_wallet->wallet_debet;
					$tr_wallet_credit  = $tr_wallet->wallet_credit;
					$tr_wallet_saldo   = $tr_wallet_debet - $tr_wallet_credit;

					$ewallet_id        = $tr_wallet->wallet_id;
					$ewallet_lender_id = $tr_wallet->wallet_lender;

					//Get Wallet Detail
					$ewallet        = $this->wallet_model->get_transaction_by_wallet_id($ewallet_id);
					$ewallet        = $ewallet[0];
					$ewallet_saldo  = $ewallet->wallet_saldo;
					$ewallet_debet  = $ewallet->wallet_debet;
					$ewallet_credit = $ewallet->wallet_credit;

					//Store Wallet New Value
					$ewallet_saldo_new  = $ewallet_saldo  + $tr_wallet_saldo;
					$ewallet_debet_new  = $ewallet_debet  + $tr_wallet_debet;
					$ewallet_credit_new = $ewallet_credit + $tr_wallet_credit;

					if ($transaction == 'capture')
					{
							// For credit card transaction, we need to check whether transaction is challenge by FDS or not
							if ($type == 'credit_card')
							{
									if($fraud == 'challenge')
									{
										// TODO set payment status in merchant's database to 'Challenge by FDS'
										// TODO merchant should decide whether this transaction is authorized or not in MAP
										echo "Transaction order_id: " . $order_id[0] ." is challenged by FDS";
										
										//Update Order Status as CHALLENGED
									    $data_order  = array( 'order_status' => 'CHALLENGED' );
									    $this->db->where('order_no', $order_id[0]);
									    $this->db->update('p2p_order', $data_order);
										
										//Update TR Wallet Record as CHALLENGED
										$data_wallet_tr = array( 'wallet_status' 	=> "CHALLENGED" );
										$this->wallet_tr_model->update($order_id[1], $data_wallet_tr);
									}
									else
									{
										// TODO set payment status in merchant's database to 'Success'
										echo "Transaction order_id: " . $order_id[0] ." successfully captured using " . $type;
										
										//Update Order Status as SUCCESS
									    $data_order  = array( 'order_status' => 'SUCCESS' );
									    $this->db->where('order_no', $order_id[0]);
									    $this->db->update('p2p_order', $data_order);
										
										//Update Wallet Transaction
										$data_wallet_tr = array(
															'wallet_status' 	    => "SUCCESS",
															'wallet_payment_type' 	=> $type,
														);
										$this->wallet_tr_model->update($order_id[1], $data_wallet_tr);

										//Update Wallet Saldo
										$data_wallet = array(
														'wallet_debet' 	=> $ewallet_debet_new,
														'wallet_credit' => $ewallet_credit_new,
														'wallet_saldo' 	=> $ewallet_saldo_new,
														'wallet_remark' => "TOP UP TRX WALLET #"
																		   .$order_id[0]. " (".$tr_wallet_debet.")"
													);
										$this->wallet_model->update($ewallet_id, $data_wallet);

										//Insert Deposit
										$data_deposit = array(
														'deposit_lender' 	=> $ewallet_lender_id,
														'deposit_date' 		=> date('Y-m-d'),
														'deposit_amount' 	=> $tr_wallet_saldo,
														'deposit_type'	    => $type,
														'deposit_status' 	=> "SUCCESS"
													);
										$this->deposit_model->insert($data_deposit);

										$data_transaction = array(
							              'trx_order_no' => $order_id[0],
							              'trx_borrower' => 0,
							              'trx_lender'   => $ewallet_lender_id,
							              'trx_date'     => $date,
							              'trx_amount'   => $tr_wallet_saldo,
							              'trx_type'     => "e-wallet deposit",
							              'trx_status'   => 'SUCCESS',
							              'created_on'   => $date.' '.$time
				            			);
				            			$this->db->insert('p2p_trx', $data_transaction);

									}
							}
					}

					else if ($transaction == 'settlement')
					{
						$this->db->trans_start();
									// TODO set payment status in merchant's database to 'Settlement'
									echo "Transaction order_id: " . $order_id[0] ." successfully transfered using " . $type;									
									
									$cek_order_status = $this->order_model->find_all_by(array(order_no => $order_id[0]));
									if($cek_order_status[0]->order_status != "SUCCESS"){										
										//Update Order Status as SUCCESS
										$data_order  = array( 'order_status' => 'SUCCESS' );
										$this->db->where('order_no', $order_id[0]);
										$this->db->update('p2p_order', $data_order);

										//Update Wallet Transaction
										$data_wallet_tr = array(
														'wallet_status' 	   => "SUCCESS",
														'wallet_payment_type'  => $type,
													);
										$this->wallet_tr_model->update($order_id[1], $data_wallet_tr);

										//Update Wallet Saldo
										$data_wallet = array(
														'wallet_debet' 	=> $ewallet_debet_new,
														'wallet_credit' => $ewallet_credit_new,
														'wallet_saldo' 	=> $ewallet_saldo_new,
													);
										$this->wallet_model->update($ewallet_id, $data_wallet);
									
										//Insert Deposit
										$data_deposit = array(
														'deposit_lender' 	=> $ewallet_lender_id,
														'deposit_date' 		=> date('Y-m-d'),
														'deposit_amount' 	=> $tr_wallet_saldo,
														'deposit_status' 	=> "SUCCESS",
													);
										$this->deposit_model->insert($data_deposit);

										$data_transaction = array(
													  'trx_order_no' => $order_id[0],
													  'trx_borrower' => 0,
													  'trx_loan'     => 0,
													  'trx_lender'   => $ewallet_lender_id,
													  'trx_date'     => $date,
													  'trx_amount'   => $tr_wallet_saldo,
													  'trx_type'     => "e-wallet deposit",
													  'trx_status'   => 'SUCCESS',
													  'created_on'   => $date.' '.$time
													);
										$this->db->insert('p2p_trx', $data_transaction);
									}
						$this->db->trans_complete();
					}

					else if($transaction == 'pending')
					{
							// TODO set payment status in merchant's database to 'Pending'
							echo "Waiting customer to finish transaction order_id: " . $order_id[0] . " using " . $type;
							//Update Order Status as PENDING
							$data_order  = array( 'order_status' => 'PENDING' );
							$this->db->where('order_no', $order_id[0]);
							$this->db->update('p2p_order', $data_order);

							$data_wallet_tr = array(
									'wallet_status' 		=> "PENDING",
							 		'wallet_payment_type' 	=> $type,
													);
							$this->wallet_tr_model->update($order_id[1], $data_wallet_tr);

							$data_transaction = array(
							              'trx_order_no' => $order_id[0],
							              'trx_borrower' => 0,
										  'trx_loan'     => 0,
							              'trx_lender'   => $ewallet_lender_id,
							              'trx_date'     => $date,
							              'trx_amount'   => $tr_wallet_saldo,
							              'trx_type'     => "e-wallet deposit",
							              'trx_status'   => 'PENDING',
							              'created_on'   => $date.' '.$time
				            );
				            $this->db->insert('p2p_trx', $data_transaction);
					}

					else if ($transaction == 'deny')
					{
									// TODO set payment status in merchant's database to 'Denied'
									echo "Payment using " . $type . " for transaction order_id: " . $order_id[0] . " is denied.";
									//Update Order Status as DENY
									$data_order  = array( 'order_status' => 'DENIED' );
									$this->db->where('order_no', $order_id[0]);
									$this->db->update('p2p_order', $data_order);

									//Update terakhir
									$data_wallet_tr = array(
														'wallet_status' 	   => "DENIED",
														'wallet_payment_type'  => $type,
													);
									$this->wallet_tr_model->update($order_id[1], $data_wallet_tr);

									$data_transaction = array(
							              'trx_order_no' => $order_id[0],
							              'trx_borrower' => 0,
										  'trx_loan'     => 0,
							              'trx_lender'   => $ewallet_lender_id,
							              'trx_date'     => $date,
							              'trx_amount'   => $tr_wallet_saldo,
							              'trx_type'     => "e-wallet deposit",
							              'trx_status'   => 'DENIED',
							              'created_on'   => $date.' '.$time
				            );
				            $this->db->insert('p2p_trx', $data_transaction);
					}

					else if ($transaction == 'cancel')
					{
							// TODO set payment status in merchant's database to 'Cancel'
							echo "Payment using " . $type . " for transaction order_id: " . $order_id[0] . " is canceled.";
							$data_wallet_tr = array(
												'wallet_status' 	   => "CANCEL",
												'wallet_payment_type'  => $type
							);
							$this->wallet_tr_model->update($order_id[1], $data_wallet_tr);

							$data_transaction = array(
							              'trx_order_no' => $order_id[0],
							              'trx_borrower' => 0,
										  'trx_loan'     => 0,
							              'trx_lender'   => $ewallet_lender_id,
							              'trx_date'     => $date,
							              'trx_amount'   => $tr_wallet_saldo,
							              'trx_type'     => "e-wallet deposit",
							              'trx_status'   => 'CANCELED',
							              'created_on'   => $date.' '.$time
				            );
				            $this->db->insert('p2p_trx', $data_transaction);
					}
    }// End [2] TOP UP for E-WALLET BALANCE

  }
  
  public function veritrans_basic_posting()
  {
	$this->load->library('Veritrans');
    // Set Your Veritrans server key
    Veritrans_Config::$serverKey    = "VT-server-QOvgH4UkXItRTuQbBRjiwuiH";
    Veritrans_Config::$isProduction = true;
	$notif = new Veritrans_Notification();
	
		$transaction    = $notif->transaction_status;
		$type 		    = $notif->payment_type;
		$order_id 	    = $notif->order_id;
		echo $order_id;
		echo $type;
		echo $transaction;						  
  }
  
  public function veritrans_raw_output_json_post()
  {
	$input_source = "php://input";
	$raw_notification = json_decode(file_get_contents($input_source), true);
	print_r($raw_notification);
	
  }
  
  public function veritrans_notification_posting()
  {
	$this->load->library('Veritrans');
    // Set Your Veritrans server key
    Veritrans_Config::$serverKey    = "VT-server-QOvgH4UkXItRTuQbBRjiwuiH";
    Veritrans_Config::$isProduction = true;
	$notif = new Veritrans_Notification();

	$date = date("Y-m-d");
	$time = date("H:i:s");
		
	$transaction    = $notif->transaction_status;
	$type 		    = $notif->payment_type;
	$order_id 	    = $notif->order_id;
		
			
	$order_code     = substr($order_id, 0, 2);
	$fraud 		    = $notif->fraud_status;

	$orders    	    = $this->order_model->get_orders($order_id);
	$lender_id      = $orders[0]->order_lender_id;
	$wallet         = $this->wallet_model->get_wallet($lender_id);
	$wallet         = $wallet[0];
	$wallet_id      = $wallet->wallet_id;
	$wallet_credit  = $wallet->wallet_credit;
	$wallet_debet   = $wallet->wallet_debet;
	$wallet_balance = $wallet->wallet_saldo;

	$gross_amount	= $orders[0]->order_gross_amount;
	$netto_amount	= $orders[0]->order_nett_amount;
		

	echo $order_id." - ";
	echo $type." - ";
	echo $transaction." - ";
	echo " lenderid : ".$lender_id;
	echo " - wallet_id : ".$wallet_id;
	echo " - wallet_credit : ".$wallet_credit;
	echo " - wallet_debet : ".$wallet_debet;
	echo " - wallet_balance : ".$wallet_balance;
	echo " - gross amount : ".$gross_amount;
	echo " - netto amount : ".$netto_amount;
	echo "<br>";

				if ($transaction == 'settlement'){ 
					  // TODO set payment status in merchant's database to 'Settlement'
					  echo "Transaction order_id: " . $order_id ." successfully transfered using " . $type;
					  //[0] Update Order Status as SUCCESS
					  $data_order  = array( 'order_status' => 'SUCCESS' );
					  $this->db->where('order_no', $order_id);
					  $this->db->update('p2p_order', $data_order);

					  //[1] Update e-wallet account SET saldo to 0.
					  $data_wallet = array(
							  'wallet_debet'    => $wallet_debet  + $netto_amount,
				              'wallet_credit'   => $wallet_credit + $gross_amount,
							  'wallet_saldo'    => 0,
							  'wallet_remark'   => "FUNDING FOR ORDER #".$order_id
					  );
					  $this->db->where('wallet_lender', $lender_id);
					  $this->db->update('p2p_wallet', $data_wallet);

					  //[2] Record the wallet transaction per wallet account.
					  $data_wallet_tr = array(
							      'wallet_id'      			=> $wallet_id,
							      'wallet_date'    			=> $date,
							      'wallet_lender'  			=> $lender_id,
							      'wallet_debet'   			=> $netto_amount,
							      'wallet_credit'  			=> 0,
							      'wallet_saldo'   			=> $netto_amount,
							      'wallet_remark'  			=> "TOPUP FOR LOAN FUNDING ORDER #".$order_id,
							      'wallet_status'  			=> "SETTLED",
							      'wallet_payment_type' 	=> $type
					  );

					  $this->db->insert('p2p_tr_wallet', $data_wallet_tr);

					  //[3] Record the deposit transaction
					  $data_deposit = array(
						      'deposit_lender' => $lender_id,
						      'deposit_date'   => $date,
						      'deposit_amount' => $netto_amount,
						      'deposit_type'   => $type,
						      'deposit_status' => 'SUCCESS',
						      'created_by'     => $lender_id,
						      'modified_by'    => $lender_id,
						      'created_on'     => $date.' '.$time
				      );
				      $this->db->insert('p2p_deposit', $data_deposit);

				      //[4] Record the transaction details
					  // & Update Financing Data on 'tbl_pembiayaan'
					  for($n=0; $n<count($orders); $n++)
					  {
						$data_transaction = array(
								'trx_order_no' => $order_id,
								'trx_loan'     => $orders[$n]->order_loan_id,
								'trx_borrower' => $orders[$n]->order_borrower_id,
								'trx_lender'   => $lender_id,
								'trx_date' 	   => $date,
								'trx_amount'   => $orders[$n]->order_borrower_loan,
								'trx_type'	   => $type,
								'trx_status'   => 'SUCCESS',
							    'created_on'   => $date.' '.$time
						);
						$this->db->insert('p2p_trx', $data_transaction);

						$data_pembiayaan = array(
								'data_p2p' 		  => 1,
								'data_p2p_lender' => $lender_id
						);
						$this->db->where('data_id', $orders[$n]->order_loan_id);
						$this->db->update('tbl_pembiayaan', $data_pembiayaan);

					  }
				}
				else if($transaction == 'pending'){
					  // TODO set payment status in merchant's database to 'Pending'
					  echo "Waiting customer to finish transaction order_id: " . $order_id . " using " . $type;
					  //[0] Update Order Status as PENDING
					  $data_order  = array( 'order_status' => 'PENDING' );
					  $this->db->where('order_no', $order_id);
					  $this->db->update('p2p_order', $data_order);
				}
				else if ($transaction == 'deny') {
					  // TODO set payment status in merchant's database to 'Denied'
					  echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is denied.";
					  //[0] Update Order Status as DENIED
					  $data_order  = array( 'order_status' => 'DENIED' );
					  $this->db->where('order_no', $order_id);
					  $this->db->update('p2p_order', $data_order);
				}
				else if ($transaction == 'cancel') {
					  // TODO set payment status in merchant's database to 'Denied'
					  echo "Payment using " . $type . " for transaction order_id: " . $order_id . " is canceled.";
					  //[0] Update Order Status as CANCEL
					  $data_order  = array( 'order_status' => 'CANCEL' );
					  $this->db->where('order_no', $order_id);
					  $this->db->update('p2p_order', $data_order);
				}	

	
								  
  }
}
