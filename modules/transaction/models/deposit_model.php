<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 /*
 * @author 		fikri
 * @since     Feb 02, 2016
 * @package		Amartha P2P System
 * @version		1.0
 */


class deposit_model extends MY_Model {

    protected $table    = 'p2p_deposit';
    protected $key     	= 'deposit_id';
    protected $soft_deletes =  true;
    protected $date_format  = 'datetime';

    public function __construct(){
        parent::__construct();
    }


}
