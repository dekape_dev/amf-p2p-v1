<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 /*
 * @author 		fikri
 * @since     Feb 02, 2016
 * @package		Amartha P2P System
 * @version		1.0
 */


class wallet_tr_model extends MY_Model {

    protected $table        = 'p2p_tr_wallet';
    protected $key     	    = 'wallet_tr_id';
    protected $trx_table    = 'p2p_tr_wallet';
    protected $bal_table    = 'p2p_wallet';
    protected $trx_key     	= 'wallet_tr_id';
    protected $bal_key     	= 'wallet_id';
    protected $soft_deletes =  true;
    protected $date_format  = 'datetime';

    public function __construct(){
        parent::__construct();
    }

	public function get_all_transaction()	{
		$this->db->select('*');
		$this->db->from($this->trx_table);
		$this->db->join('p2p_lender','p2p_lender.lender_id = p2p_tr_wallet.wallet_lender');
		$this->db->where('p2p_lender.deleted', '0');
		$this->db->order_by('wallet_tr_id', 'desc');
		return $this->db->get();
	}

	public function get_all_transaction_by_lender($lender_id){
		$this->db->select('*');
		$this->db->from($this->trx_table);
		$this->db->where('wallet_lender', $lender_id);
		$this->db->where('deleted', '0');
		$this->db->order_by('wallet_tr_id', 'desc');
		return $this->db->get()->result();
	}

	public function get_saldo_by_lender($lender_id){
		return $this->db->select('wallet_saldo')
						->from($this->bal_table)
						->where('wallet_lender', $lender_id)
						->where('deleted', '0')
						->get()
						->result();
    }


	public function get_transaction($id){
		return $this->db->select('*')
						->from($this->trx_table)
						->where('wallet_tr_id', $id)
						->where('deleted', '0')
						->get()
						->result();
    }

}
