<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 /*
 * @author    Ali Fahmi
 * @since     Feb 28, 2016
 * @package   Amartha P2P System
 * @version   1.0
 */


class order_model extends MY_Model {

    protected $table        = 'p2p_order';
    protected $key          = 'order_no';
    protected $soft_deletes =  true;
    protected $date_format  = 'datetime';

    public function __construct(){
        parent::__construct();
    }

    public function get_orders($order_no){
		return $this->db->select("*")
					->from($this->table)
					->where('order_no', $order_no)
					->get()
					->result();
    }


}
