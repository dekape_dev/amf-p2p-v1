<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 /*
 * @author 		fikri
 * @since       Feb 02, 2016
 * @package		Amartha P2P System
 * @version		1.0
 */


class wallet_model extends MY_Model {

    protected $table    = 'p2p_wallet';
    protected $key     	= 'wallet_id';
    protected $soft_deletes =  true;
    protected $date_format  = 'datetime';

    public function __construct(){
        parent::__construct();
    }

	public function get_all()	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deleted', '0');
		$this->db->order_by('wallet_id', 'desc');
		return $this->db->get();
	}

	public function get_wallet($lender_id){
		return $this->db->select('*')
						->from($this->table)
						->where('wallet_lender', $lender_id)
						->where('deleted', '0')						
						->get()
						->result();
    }
	
	public function get_transaction_by_wallet_id($wallet_id){
		return $this->db->select('*')
						->from($this->table)
						->where('wallet_id', $wallet_id)
						->where('deleted', '0')						
						->get()
						->result();
    }
	
	public function get_saldo_by_lender($lender_id){
		return $this->db->select('wallet_saldo')
						->from($this->table)
						->where('wallet_lender', $lender_id)
						->where('deleted', '0')						
						->get()
						->result();
    }
	

}
