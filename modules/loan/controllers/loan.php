<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loan extends Front_Controller{

	public function __construct(){
		parent::__construct();
		$this->template->set_layout('dashboard');
		$this->load->model('investor/investor_model');
		$this->load->model('transaction/transaction_model');
		$this->load->model('loan_model');
		$this->load->model('clients_pembiayaan_model');
		$this->load->model('tabsukarela_model');
		$this->load->model('tabwajib_model');
		$this->load->model('sector_model');
		//GET WALLET BALANCE
		$userdata            = $this->session->userdata('logged_in_user');
		$this->user_fullname = $userdata['ufullname'];
		$this->user_id       = $userdata['uid'];

		$wallet = $this->transaction_model->get_saldo_by_lender($userdata['uid']);
		$this->session->set_userdata('wallet', $wallet[0]->wallet_saldo);
		$this->session->set_userdata('wallet_id', $wallet[0]->wallet_id);

		$this->load->library('cart');
	}

	public function index(){
		if($this->session->userdata('logged_in_user')) {
			$userdata = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid'] = $userdata['uid'];
			//GET BORROWERS
			$borrower = $this->loan_model->get_borrower()->result();

			$this->template	->set('sitetitle', 'Loan')
							->set('menu_browse_loan', 'active')
							->set('profile', $profile)
							->set('borrower', $borrower)
							->build('loan');
		}else{
			redirect('login');
		}
	}

	public function detail($borrower_id){
		if($this->session->userdata('logged_in_user')) {
			$userdata = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid'] = $userdata['uid'];
			//GET BORROWER
			$borrower = $this->loan_model->get_borrower_detail($borrower_id)->result();
			$borrower = $borrower[0];

			$client_id = $borrower->data_client;
			$history = $this->loan_model->get_client_history($client_id)->result();


			$open_credit_line = $this->loan_model->count_open_credit_line($client_id);
			$total_credit_line = $this->loan_model->count_total_credit_line($client_id);
			$revolving_credit_balance = $this->loan_model->revolving_credit_balance($client_id);
			$count_par = $this->loan_model->count_par($client_id);
			$count_par_amount = $this->loan_model->count_par_amount($client_id);

			$borrower_detail['earliest_credit_line'] = date('M Y', strtotime($history[0]->data_date_accept));
			$borrower_detail['open_credit_line'] = $open_credit_line;
			$borrower_detail['total_credit_line'] = $total_credit_line;
			$borrower_detail['revolving_credit_balance'] = $revolving_credit_balance;
			$borrower_detail['count_par'] = $count_par;
			$borrower_detail['count_par_amount'] = $count_par_amount;


			//GET PEMBIAYAAN
			//$pembiayaan = $this->loan_model->get_pembiayaan_by_client($client_id )->result();

			$client_account = $borrower->data_rekening;
			//TABUNGAN SUKARELA
			$tabsukarela = $this->tabsukarela_model->get_account($client_account);
			$tabsukarela = $tabsukarela[0];

			//TABUNGAN WAJIB
			$tabwajib = $this->tabwajib_model->get_account($client_account);
			$tabwajib = $tabwajib[0];

			$this->template	->set('sitetitle', 'Loan')
							->set('menu_browse_loan', 'active')
							->set('profile', $profile)
							->set('borrower', $borrower)
							->set('borrower_detail', $borrower_detail)
							->set('history', $history)
							->set('pembiayaan', $pembiayaan)
							->set('tabsukarela', $tabsukarela)
							->set('tabwajib', $tabwajib)
							->build('loan_detail');
		}else{
			redirect('login');
		}
	}

	public function view($data_id){
		if($this->session->userdata('logged_in_user')) {
			$userdata = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid'] = $userdata['uid'];
			//GET BORROWER
			$borrower = $this->loan_model->view_borrower_detail($data_id)->result();
			$borrower = $borrower[0];

			$client_id = $borrower->data_client;
			$history = $this->loan_model->get_client_history($client_id)->result();


			$open_credit_line = $this->loan_model->count_open_credit_line($client_id);
			$total_credit_line = $this->loan_model->count_total_credit_line($client_id);
			$revolving_credit_balance = $this->loan_model->revolving_credit_balance($client_id);
			$count_par = $this->loan_model->count_par($client_id);
			$count_par_amount = $this->loan_model->count_par_amount($client_id);

			$borrower_detail['earliest_credit_line'] = date('M Y', strtotime($history[0]->data_date_accept));
			$borrower_detail['open_credit_line'] = $open_credit_line;
			$borrower_detail['total_credit_line'] = $total_credit_line;
			$borrower_detail['revolving_credit_balance'] = $revolving_credit_balance;
			$borrower_detail['count_par'] = $count_par;
			$borrower_detail['count_par_amount'] = $count_par_amount;


			//GET PEMBIAYAAN
			//$pembiayaan = $this->loan_model->get_pembiayaan_by_client($client_id )->result();

			$client_account = $borrower->data_rekening;
			//TABUNGAN SUKARELA
			$tabsukarela = $this->tabsukarela_model->get_account($client_account);
			$tabsukarela = $tabsukarela[0];

			//TABUNGAN WAJIB
			$tabwajib = $this->tabwajib_model->get_account($client_account);
			$tabwajib = $tabwajib[0];

			$this->template	->set('sitetitle', 'Loan')
							->set('menu_browse_loan', 'active')
							->set('profile', $profile)
							->set('borrower', $borrower)
							->set('borrower_detail', $borrower_detail)
							->set('history', $history)
							->set('pembiayaan', $pembiayaan)
							->set('tabsukarela', $tabsukarela)
							->set('tabwajib', $tabwajib)
							->build('loan_detail');
		}else{
			redirect('login');
		}
	}

	/*
	public function borrowerprofile($id='0'){
		if($this->session->userdata('logged_in_user')) {
			$userdata = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid'] = $userdata['uid'];

			//GET LENDER
			$borrower = $this->loan_model->get_borrower_detail($borrower_id)->result();
			$borrower = $borrower[0];

			$client_id = $borrower->data_client;
			$history = $this->loan_model->get_client_history($client_id)->result();

			//GET SINGLE LENDER
			$borrower = $this->loan_model->get_borrower_by_id($id)->result();


			$this->template	->set('sitetitle', 'Loan')
							->set('menu_browse_loan', 'active')
							->set('profile', $profile)
							->set('borrower', $borrower)
							->set('history', $history)
							->build('borrowerprofile');

		}else{
			redirect('login');
		}
	}
	*/

	public function confirm(){
		//var_dump($this->cart->contents()); die();
		if($this->session->userdata('logged_in_user')) {
			$userdata = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid'] = $userdata['uid'];
			if($this->cart->total_items() == 0){
				$this->session->set_flashdata('message', "success|You didn't select a borrower. Please select borrower to continue.");
				redirect('loan');
			}
			$this->template	->set('sitetitle', 'Loan')
							->set('menu_confirm', 'active')
							->set('profile', $profile)
							->build('confirm');
		}else{
			redirect('login');
		}
	}

	public function payment(){
		if($this->session->userdata('logged_in_user')) {
			$userdata = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid'] = $userdata['uid'];

			$this->template	->set('sitetitle', 'Loan')
							->set('menu_transfer', 'active')
							->set('profile', $profile)
							->build('payment');
		}else{
			redirect('login');
		}
	}

	public function addtocart(){

		$no = $this->input->post('no');
		$total_item = 0;
		$j=1;
		for($i=1;$i<=$no;$i++){
			if($this->input->post("check_$i") == 1){
				$total_item++;

				$data[$i] = array(
						   'id'       => $this->input->post("loan_$i"),
						   'qty'      => 1,
						   'price'    => $this->input->post("plafond_$i"),
						   'name'     => $this->input->post("name_$i"),
						   'grade'    => $this->input->post("grade_$i"),
						   'score'    => $this->input->post("score_$i"),
						   'days'     => $this->input->post("days_$i"),
						   'options'  => ''
						);
				//print_r($data[$i]);
				$this->cart->insert($data[$i]);
				$j++;
			}
		}

		redirect('loan/confirm');


	}


	public function delcart(){
		$rowid = $this->input->post('rowid');
		$rowid = $this->uri->segment(3);
		$data = array(
               'rowid' => $rowid,
               'qty'   => 0
            );

		if($this->cart->update($data)) { redirect('loan/confirm'); }
	}

	public function repayment($data_id){
		if($this->session->userdata('logged_in_user')) {
			$userdata = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid'] = $userdata['uid'];
			//GET BORROWER
			$data = $this->loan_model->get_repayment($data_id)->result();


			$this->template	->set('sitetitle', 'Repayment')
							->set('profile', $profile)
							->set('data', $data)
							->build('repayment');
		}else{
			redirect('login');
		}
	}
	
	
	
	public function ssupp(){
		$this->template->set_layout('blank');
			$this->template	->set('sitetitle', 'Akad')
							->set('menu_browse_loan', 'active')
							->set('profile', $profile)
							->set('borrower', $borrower)
							->set('akad', $akad)
							->set('pembiayaanid', $pembiayaanid)
							->build('ssupp');
	}
	public function akad($akad,$pembiayaanid){
			$userdata = $this->session->userdata('logged_in_user');
			$profile['fullname'] = $userdata['ufullname'];
			$profile['uid'] = $userdata['uid'];
			
			//GET BORROWER DETAIL
			$borrower = $this->loan_model->view_borrower_detail($pembiayaanid)->result();
			$borrower = $borrower[0];
			
			if($akad == "MBA"){ $template = "akad_mba"; }
			elseif($akad == "AHA"){ $template = "akad_aha"; }
			elseif($akad == "IJR"){ $template = "akad_ijr"; }
			else{ $template = "akad_mba"; }

			$this->template->set_layout('blank');
			$this->template	->set('sitetitle', 'Akad')
							->set('menu_browse_loan', 'active')
							->set('profile', $profile)
							->set('borrower', $borrower)
							->set('akad', $akad)
							->set('pembiayaanid', $pembiayaanid)
							->build($template);

	}
	
	public function noloan(){
		//var_dump($this->cart->contents()); die();
		if($this->session->userdata('logged_in_user')) {
			$this->template->set_layout('blank');
			$this->template	->set('sitetitle', 'Loan')
							->build('noloan');
		}else{
			redirect('login');
		}
	}
}
