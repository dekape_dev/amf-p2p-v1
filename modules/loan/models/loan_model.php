<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 /*
 * @author 		fikriwirawan
 * @since       Nov 08, 2015
 * @package		Arkana
 * @version		1.0
 */


class loan_model extends MY_Model {

    protected $table        = 'tbl_pembiayaan';
    protected $key          = 'data_id';
    protected $soft_deletes =  true;
    protected $date_format  = 'datetime';

    public function __construct(){
        parent::__construct();
    }

	public function get_all($limit)	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deleted', '0');
		$this->db->order_by('data_id', 'desc');
		return $this->db->get();

	}

	public function get_borrower(){
        $this->db->join('tbl_clients', 'tbl_clients.client_id = tbl_pembiayaan.data_client', 'left');
    		$this->db->join('tbl_group','group_id = client_group');
    		$this->db->join('tbl_branch','branch_id = group_branch');
        $this->db->join('tbl_sector','tbl_sector.sector_id = tbl_pembiayaan.data_sector');
        $this->db->where('data_p2p','1');
        $this->db->where('data_p2p_lender = 0');
        $this->db->where('data_status = 2');
        $this->db->where('data_ke > 1');
    		$this->db->where('tbl_pembiayaan.deleted', '0');
    		$this->db->where('tbl_clients.deleted', '0');
    		$this->db->where('tbl_clients.client_status', '1');
        return $this->db->get($this->table);
    }


    public function get_borrower_by_id($id){
        $this->db->join('tbl_clients', 'tbl_clients.client_id = tbl_pembiayaan.data_client', 'left');
        $this->db->join('tbl_sector','tbl_sector.sector_id = tbl_pembiayaan.data_sector');
        $this->db->where('data_p2p != 0');
        $this->db->where('data_p2p_lender = 0');
        $this->db->where('data_status = 2');
        $this->db->where('data_client = '.$id);
		    $this->db->where('tbl_pembiayaan.deleted', '0');
        return $this->db->get($this->table);
    }


	public function count_borrower()
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('data_p2p != 0')
						->where('data_p2p_lender = 0')
						->where('data_status = 2')
						->where('deleted','0')
						->get()
						->row()
						->numrows;
	}

	public function get_borrower_detail($id){
        $this->db->join('tbl_clients', 'tbl_clients.client_id = tbl_pembiayaan.data_client', 'left');
        $this->db->join('tbl_branch', 'tbl_branch.branch_id = tbl_clients.client_branch', 'left');
        $this->db->join('tbl_sector','tbl_sector.sector_id = tbl_pembiayaan.data_sector');
        $this->db->where('data_id',$id);
		    $this->db->where('data_p2p != 0');
        $this->db->where('data_p2p_lender = 0');
        $this->db->where('data_status = 2');
        $this->db->where('data_ke > 1');
        $this->db->where('data_ke < 5');
		    $this->db->where('tbl_pembiayaan.deleted', '0');
        return $this->db->get($this->table);
    }
	
	public function view_borrower_detail($id){
        $this->db->join('tbl_clients', 'tbl_clients.client_id = tbl_pembiayaan.data_client', 'left');
        $this->db->join('tbl_branch', 'tbl_branch.branch_id = tbl_clients.client_branch', 'left');
        $this->db->join('tbl_sector','tbl_sector.sector_id = tbl_pembiayaan.data_sector');
        $this->db->where('data_id',$id);
        return $this->db->get($this->table);
    }

	public function get_client_history($client_id){
        $this->db->join('tbl_clients', 'tbl_clients.client_id = tbl_pembiayaan.data_client', 'left');
        $this->db->where('data_client',$client_id);
        $this->db->where('data_status = 3');
		    $this->db->where('tbl_pembiayaan.deleted', '0');
		    $this->db->order_by('tbl_pembiayaan.data_ke', 'asc');
        return $this->db->get($this->table);
	}

	public function get_pembiayaan_by_client($client_id)
	  {
		$this->db->where('data_client', $client_id)
				 ->where('deleted', '0');
		return $this->db->get($this->table);
	}

	
	public function count_open_credit_line($client_id)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('data_client', $client_id)
						->where('data_status = 1')
						->where('tbl_pembiayaan.deleted', '0')
						->get()
						->row()
						->numrows;
	}
	
	public function count_total_credit_line($client_id)
	{
		return $this->db->select("count(*) as numrows")
						->from($this->table)
						->where('data_client', $client_id)
						->where('tbl_pembiayaan.deleted', '0')
						->get()
						->row()
						->numrows;
	}
	
	public function revolving_credit_balance($client_id)
	{
		return $this->db->select("sum(data_plafond) as numrows")
						->from($this->table)
						->where('data_client', $client_id)
						->where('tbl_pembiayaan.deleted', '0')
						->get()
						->row()
						->numrows;
	}
	
	
	public function count_par($client_id)
	{
		return $this->db->select("sum(data_par) as numrows")
						->from($this->table)
						->where('data_client', $client_id)
						->where('tbl_pembiayaan.deleted', '0')
						->get()
						->row()
						->numrows;
	}
	
	public function count_par_amount($client_id)
	{
		return $this->db->select("sum(data_par*data_plafond/data_jangkawaktu) as numrows")
						->from($this->table)
						->where('data_client', $client_id)
						->where('tbl_pembiayaan.deleted', '0')
						->get()
						->row()
						->numrows;
	}
	
	public function get_repayment($data_id){
        $this->db->join('tbl_clients', 'tbl_clients.client_id = tbl_transaction.tr_client', 'left');
        $this->db->join('tbl_pembiayaan', 'tbl_pembiayaan.data_id = tbl_transaction.tr_pembiayaan', 'left');
        $this->db->where('tr_pembiayaan',$data_id);
		$this->db->where('tbl_transaction.deleted', '0');
		$this->db->where('tbl_transaction.tr_angsuranke != 0');
		$this->db->order_by('tbl_transaction.tr_angsuranke', 'asc');
        return $this->db->get('tbl_transaction');
	}
}
