<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller{

	function __construct()
	{
		parent::__construct();
		$this->template->set_layout('signin');
		$this->load->model('user');
	}

	function index()
	{
		$this->load->helper('form');
		$this->template	->set('menu_title', 'Login')
						->build('login_view');
	}

	function verifylogin()
	{
		//Validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

		if($this->form_validation->run() == FALSE)
		{
		  //Field validation failed.  User redirected to login page
		  $this->template->set('menu_title', 'Login')
						 ->build('login_view');
    }
    else
    {
		//Field validation succeeded.  Validate against database
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		//query the database
		$result = $this->user->login($username, $password);

		if($result)
		{
			$sess_array = array();
			foreach($result as $row)
			{
				$sess_array = array(
				  'id' => $row->id,
				  'username' => $row->username
				);
				$this->session->set_userdata('logged_in', $sess_array);
			}
			//Go to Success Page
			redirect('admin/home', 'refresh');
		}
		else
		{
			//Go to Login Page
			$this->session->set_flashdata('message', 'Invalid username or password');
			redirect('admin/login', 'refresh');
		}

    }

  }



  function logout()
  {
    $this->session->unset_userdata('logged_in');
    session_destroy();
    redirect('admin/login', 'refresh');
  }
}

?>
