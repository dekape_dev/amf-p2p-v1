<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
/**
 * 
 * @author 	fikriwirawan
 * @since	Nov 28, 2013
 */
class content_model extends MY_Model {

    protected $table        = 'p2p_content';
    protected $key          = 'id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    protected $set_created  = true;
    protected $set_modified = false;

    public function __construct(){
        parent::__construct();
    }
    
	public function get_all()	{
		$this->db->where('deleted', '0');
        return $this->db->get($this->table);  
		
	}
	
	public function get_content($param){
        $this->db->where('title', $param);
		$this->db->where('deleted', '0');
        return $this->db->get($this->table);    
    }
}