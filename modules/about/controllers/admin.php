<?php

/**
 * @since       June 18, 2012
 * @package		project
 * @version		1.0
 */

class Admin extends Admin_Controller{
	
	private $per_page 	= '10';
	private $title 		= 'Amartha';
	private $module 	= 'about';
	
	public function __construct(){
		parent::__construct();		
		$this->load->model('content_model');
	}
	
	public function index(){
		if($this->session->userdata('logged_in'))
		{
			$borrower_id =  1;
			$data = $this->content_model->find($content_id);
			
			if($this->save_project()){
				$this->session->set_flashdata('message', 'success|Content Di Simpan');
				redirect('admin/about');
			}
			
			//GET CONTENT
				$get_content = $this->content_model->get_content('about')->result();
				$content_title=$get_content[0]->title;
				$content_eng=$get_content[0]->content_eng;
				$content_ind=$get_content[0]->content_ind;
				$content_id=$get_content[0]->id;
				$this->template	->set('menu_title', 'Update Content')
								->set('content_title', $content_title )
								->set('content_eng', $content_eng )
								->set('content_ind', $content_ind )
								->set('content_id', $content_id )
								->build('admin/about_form');
			}
			
		else
		{
		  //If no session, redirect to login page
		  redirect('admin/login', 'refresh');
		}
	}
	
	
	public function edit(){
		$content_id =  1;
		$data = $this->content_model->find($content_id);
		
		if($this->save_project()){
			$this->session->set_flashdata('message', 'success|Content telah diedit');
			redirect('admin/about');
		}
		//GET CONTENT
			$get_content = $this->content_model->get_content('about')->result();
			$content=$get_content[0]->content;
			$this->template	->set('menu_title', 'About Amartha Microfinance')
							->set('content', $content )
							->build('admin/about_form');
	}
	
	private function save_project(){
		//set form validation
		$this->form_validation->set_rules('content_eng', 'Content ENG', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('content_id');
	
			//process the form
			$data = array(
					'title'				=> $this->input->post('tittle'),
					'content_eng'       => $this->input->post('content_eng'),
					'content_ind'       => $this->input->post('content_ind')
			);
			
			
			if(!$id)
				return $this->content_model->insert($data);
			else
				return $this->content_model->update($id, $data);
			 
		}
	}
	
	
}