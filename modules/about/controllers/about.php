<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends Front_Controller{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$this->template	->set('sitetitle', 'About')
						->set('menu_about', 'active')
						->build('about');
	}
	
	public function purpose(){
		$this->template	->set('sitetitle', 'Purpose')
						->set('menu_about', 'active')
						->build('purpose');
	}
	public function history(){
		$this->template	->set('sitetitle', 'History')
						->set('menu_about', 'active')
						->build('history');
	}
	
	
	public function leadership(){
		$this->template	->set('sitetitle', 'Leadership')
						->set('menu_about', 'active')
						->build('leadership');
	}
	public function our_approach(){
		$this->template	->set('sitetitle', 'Our Approach')
						->set('menu_about', 'active')
						->build('our_approach');
	}
	public function values_principles(){
		$this->template	->set('sitetitle', 'Values and Principles')
						->set('menu_about', 'active')
						->build('values_principles');
	}
	public function awards(){
		$this->template	->set('sitetitle', 'Awards')
						->set('menu_about', 'active')
						->build('awards');
	}
	public function contact(){
		$this->template	->set('sitetitle', 'Contact')
						->set('menu_about', 'active')
						->build('contact/contact');
	}
}
