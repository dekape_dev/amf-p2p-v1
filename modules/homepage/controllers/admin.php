<?php

/**
 * @since       Dec 05, 2013
 * @package		project
 * @version		1.0
 */

class Admin extends Admin_Controller{
	
	private $per_page 	= '10';
	private $title 		= 'Amartha';
	private $module 	= 'homepage';
	
	public function __construct(){
		parent::__construct();		
		$this->load->model('homepage_model');
	}
	
	public function index(){
		if($this->session->userdata('logged_in'))
		{
			//GET ALL PROJECT
			$project = $this->homepage_model->get_all()->result();
			$this->template	->set('menu_title', 'Homepage Images')
							->set('project', $project )
							->build('admin/homepage_list');	
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('admin/login', 'refresh');
		}
	}

	
	public function delete_photo($id = '0'){
			$this->module = "homepage";
				if($this->homepage_model->delete($id)){
					$this->session->set_flashdata('message', 'success|Photo telah dihapus');
					redirect('admin/homepage/');
					exit;
				}
	}
	
	
	
	public function add_photo(){
		
		if($this->save_photo()){
			$this->session->set_flashdata('message', 'success|Homepage telah diedit');
			redirect('admin/'.$this->module.'/');
		}
		$this->template	->set('data', $data)
						->set('menu_title', 'Add')
						->build('admin/homepage_form');	
	}
	public function edit_photo(){
		
		if($this->save_photo()){
			$this->session->set_flashdata('message', 'success|Homepage telah diedit');
			redirect('admin/'.$this->module.'/');
		}
		$home_id =  $this->uri->segment(4);
		$data = $this->homepage_model->get_photo($home_id)->result();
		$data = $data[0];
		
		$this->template	->set('data', $data)
						->set('menu_title', 'Edit')
						->set('home_id', $home_id)
						->build('admin/homepage_form');	
	}
	private function save_photo(){
		//set form validation
		$this->form_validation->set_rules('flag', 'flag', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('home_id');
			//process the form
				$data = array(
						'home_title'       => $this->input->post('home_title'),
						'home_subtitle'    	=> $this->input->post('home_subtitle')
				);
			if($id){
				//process the form
				$data = array(
						'home_id'       => $this->input->post('home_id'),
						'home_title'       => $this->input->post('home_title'),
						'home_subtitle'       => $this->input->post('home_subtitle')
				);
			}
			//try to upload image first
			try{
				$timestamp = date("Ymdhis");
				$filename = "dgreenvilla_home_".$timestamp;
				$config['upload_path'] 		= 'files/';
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
				$config['max_size']			= '3000';
				$config['encrypt_name']	 	= FALSE;
				$config['file_name']	 	= $filename;
				$this->load->library('upload', $config);
				if($this->upload->do_upload('image')){
					$upload 			= $this->upload->data();
					$data['home_file']  = $filename.$upload['file_ext'];
				}
			}catch(Exception $e){
				echo $e;
				exit;
			}
	
			if(!$id)
				return $this->homepage_model->insert($data);
			else
				return $this->homepage_model->update($id, $data);
			 
		}
	}
	
	
}