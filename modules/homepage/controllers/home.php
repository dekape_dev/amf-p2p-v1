<?php

class Home extends Front_Controller{
	
	public function __construct(){
		parent::__construct();
	}
	
	public function index(){
		$this->template	->set('sitetitle', 'Home')
						->set('menu_home', 'active')
						->build('home');
	}
	
}
?>