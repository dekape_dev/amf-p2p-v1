<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * 
 * @author 	fikriwirawan
 * @since	Oct 14, 2013
 */
class homepage_model extends MY_Model {

    protected $table        = 'p2p_homepage';
    protected $key          = 'home_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';
    protected $set_created  = true;
    protected $set_modified = false;

    public function __construct(){
        parent::__construct();
    }
    
	public function get_all($limit)	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deleted', '0');
		return $this->db->get();
		
	}
	
	public function get_photo($param){
        $this->db->where('home_id', $param);
        return $this->db->get($this->table);    
    }
	
   
}