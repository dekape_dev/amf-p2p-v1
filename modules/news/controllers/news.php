<?php



class News extends Front_Controller{

	

	public function __construct(){

		parent::__construct();

		$this->load->model('news_model');

	

		

	}

	

	public function index(){

		$project = $this->news_model->get_all()->result();

		$this->template	->set('sitetitle', 'News')

						->set('project', $project )

						->set('news', 'active')

						->build('news');

	}
	
	
	public function news_2(){

		$project = $this->news_model->get_all()->result();

		$this->template	->set('sitetitle', 'News')

						->set('project', $project )

						->set('news', 'active')

						->build('news_2');

	}


	

	public function detail($id){

		$project = $this->news_model->get_news($id)->result();

		$this->template	->set('sitetitle', 'News')

						->set('project', $project )

						->set('news', 'active')

						->build('detail');	

	}

	

	public function archieve($date1){

		$project = $this->news_model->get_archieve($date1)->result();

		$this->template	->set('sitetitle', 'News Archieve')

						->set('project', $project )

						->set('news','active')

						->build('archieve');

	}

	

			

}

?>