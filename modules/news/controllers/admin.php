<?php

/**
 * @since       Dec 05, 2013
 * @package		project
 * @version		1.0
 */

class Admin extends Admin_Controller{
	
	private $per_page 	= '10';
	private $title 		= 'Amartha';
	private $module 	= 'news';
	
	public function __construct(){
		parent::__construct();		
		$this->load->model('news_model');
	}
	
	public function index(){
		if($this->session->userdata('logged_in'))
		{
			//GET ALL PROJECT
			$project = $this->news_model->get_all()->result();
			$this->template	->set('menu_title', 'News')
							->set('project', $project )
							->build('admin/news_list');	
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('admin/login', 'refresh');
		}
	}
	public function delete_news($id = '0'){
			$this->module = "news";
				if($this->news_model->delete($id)){
					$this->session->set_flashdata('message', 'success|News telah dihapus');
					redirect('admin/news/');
					exit;
				}
	}
	public function add_news(){
	if($this->save_news()){
			$this->session->set_flashdata('message', 'success|News telah diedit');
			redirect('admin/'.$this->module.'/');
		}
		$this->template	->set('data', $data)
						->set('menu_title', 'Add News')
						->build('admin/news_form');	
	}
	public function edit_news(){
		
		if($this->save_news()){
			$this->session->set_flashdata('message', 'success|News telah diedit');
			redirect('admin/'.$this->module.'/');
		}
		$news_id =  $this->uri->segment(4);
		$data = $this->news_model->get_news($news_id)->result();
		$data = $data[0];	
		$this->template	->set('data', $data)
						->set('menu_title', 'Edit News')
						->set('news_id', $news_id)
						->build('admin/news_form');	
	}
	private function save_news(){
		//set form validation
		$this->form_validation->set_rules('flag', 'flag', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('news_id');
			//process the form
				$data = array(
						'news_title'		=> $this->input->post('news_title'),
						'news_date'    		=> $this->input->post('news_date'),
						'news_content'		=> $this->input->post('news_content')
						
				);
			if($id){
				//process the form
				$data = array(
						'news_id'       	=> $this->input->post('news_id'),
						'news_title'		=> $this->input->post('news_title'),
						'news_date'    		=> $this->input->post('news_date'),
						'news_content'    	=> $this->input->post('news_content')
				);
			}
			//try to upload image first
			try{
				$timestamp = date("Ymdhis");
				$filename = "amartha_microfinance_".$timestamp;
				$config['upload_path'] 		= 'files/';
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
				$config['max_size']			= '3000'; 
				$config['encrypt_name']	 	= FALSE;
				$config['file_name']	 	= $filename;
				$this->load->library('upload', $config);
				if($this->upload->do_upload('image')){
					$upload 			= $this->upload->data(); 
					$data['news_picture']  = $filename.$upload['file_ext'];
				}
			}catch(Exception $e){
				echo $e;
				exit;
			}
			
			if(!$id)
				return $this->news_model->insert($data);
			else
				return $this->news_model->update($id, $data);
			 
		}
	}
	
	
}