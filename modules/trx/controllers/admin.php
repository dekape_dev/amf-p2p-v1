<?php

 /*
 * @author 		fikriwirawan
 * @since       Nov 08, 2015
 * @package		Arkana
 * @version		1.0
 */

class Admin extends Admin_Controller{
	
	private $per_page 	= '10';
	private $title 		= 'trx';
	private $module 	= 'trx';
	
	public function __construct(){
		parent::__construct();		
		$this->load->model('trx_model');
	}
	
	public function index(){
		if($this->session->userdata('logged_in'))
		{
			//GET DATA
			$data = $this->trx_model->get_all()->result();
			$this->template	->set('menu_title', 'Transaction List')
							->set('data', $data )
							->build('trx_list');	
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('admin/login', 'refresh');
		}
	}
	
	
	public function create(){
		if($this->save_data()){
			$this->session->set_flashdata('message', 'success|Success add new transaction');
			redirect('admin/'.$this->module.'/');
		}
		$this->template	->set('data', $data)
						->set('menu_title', 'Add trx')
						->build('trx_form');	
	}
	
	public function edit($trx_id){
		
		if($this->save_data()){
			$this->session->set_flashdata('message', 'success|Success edit transaction');
			redirect('admin/'.$this->module.'/');
		}
		$data = $this->trx_model->get_trx($trx_id)->result();
		$data = $data[0];	
		$this->template	->set('data', $data)
						->set('menu_title', 'Edit trx')
						->build('trx_form');	
	}
	
	public function del($id = '0'){
		if($this->trx_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Success delete trx');
			redirect('admin/'.$this->module.'/');
			exit;
		}
	}
	
	private function save_data(){
		//set form validation
		$this->form_validation->set_rules('flag', 'flag', 'required');
	
	
		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('trx_id');
			//process the form
				$data = array(
						'trx_borrower'		=> $this->input->post('trx_borrower'),
						'trx_lender'		=> $this->input->post('trx_lender'),
						'trx_date'		=> $this->input->post('trx_date'),
						'trx_ammount'	=> $this->input->post('trx_ammount'),						
						'trx_status'		=> $this->input->post('trx_status'),
						
				);
			
			
			if(!$id)
				return $this->trx_model->insert($data);
			else
				return $this->trx_model->update($id, $data);
			 
		}
	}
	
}