<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 /*
 * @author 		fikriwirawan
 * @since       Nov 08, 2015
 * @package		Arkana
 * @version		1.0
 */
 

class trx_model extends MY_Model {

    protected $table        = 'p2p_trx';
    protected $key          = 'trx_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';

    public function __construct(){
        parent::__construct();
    }
    
	public function get_all($limit)	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->join('tbl_pembiayaan', 'tbl_pembiayaan.data_id = p2p_trx.trx_borrower', 'left');
		$this->db->join('tbl_clients', 'tbl_clients.client_id = tbl_pembiayaan.data_client', 'left');
		$this->db->join('p2p_lender', 'p2p_lender.lender_id = p2p_trx.trx_lender', 'left');
		$this->db->where('p2p_trx.deleted', '0');
		$this->db->order_by('trx_id', 'desc');
		return $this->db->get();
	} 
 
	public function get_trx($param){
        $this->db->where('trx_id', $param);
		$this->db->where('deleted', '0');
        return $this->db->get($this->table);    
    }
}