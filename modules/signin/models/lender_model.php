<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 /*
 * @author 		fikriwirawan
 * @since       Nov 08, 2015
 * @package		Arkana
 * @version		1.0
 */
 

class Lender_model extends MY_Model {

    protected $table        = 'tbl_lender';
    protected $key          = 'lender_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';

    public function __construct(){
        parent::__construct();
    }
    
	public function get_all($limit)	{
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deleted', '0');
		$this->db->order_by('lender_id', 'desc');
		return $this->db->get();
		
	} 
 
	public function get_lender($param){
        $this->db->where('lender_id', $param);
		$this->db->where('deleted', '0');
        return $this->db->get($this->table);    
    }
	
	
	public function check_activation($param){
        $this->db->where('lender_activation_code', $param);
		$this->db->where('deleted', '0');
        return $this->db->get($this->table);    
    }
	
	public function activate($param){
		$data = array(
               'lender_status' => 'Enable'
            );
        $this->db->where('lender_activation_code', $param)
				 ->where('deleted', '0');
        return $this->db->update($this->table, $data);    
    }
	
	function login($username, $password)
    {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('lender_email = ' . "'" . $username . "'");
        $this->db->where('lender_password = ' . "'" . MD5($password) . "'");
        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
}