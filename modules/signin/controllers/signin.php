<?php

class Signin extends Front_Controller{
	
	public function __construct(){
		parent::__construct();
		$this->load->model('lender_model');	
	}
	
	public function index(){
		
		if($this->save_register()){
			$this->session->set_flashdata('message', 'success|Register Success!');
			redirect('signin/');
		}
		
		$this->template	->set('sitetitle', 'Sign in')
						->set('menu_home', 'active')
						->build('signin');
	}
	
	private function save_register(){
		//set form validation
		$this->form_validation->set_rules('register_first_name', 'First Name', 'required');
		$this->form_validation->set_rules('register_email', 'Email', 'required');
		$this->form_validation->set_rules('register_passwd', 'Password', 'required');

		if($this->form_validation->run() === TRUE){
			//process the form
			$activation_code =  sha1($this->input->post('register_email').date('Ymdhis'));
			$data = array(
				'lender_first_name'		=> $this->input->post('register_first_name'),
				'lender_last_name'      => $this->input->post('register_last_name'),
				'lender_email'      	=> $this->input->post('register_email'),
				'lender_password'  		=> md5($this->input->post('register_passwd')),
				'lender_terms'      	=> $this->input->post('register_terms'),
				'lender_privacy'      	=> $this->input->post('register_policy'),
				'lender_activation_code' => $activation_code,
				
			);
			
			
			$this->lender_model->insert($data);
			
			
			//UPDATE EMAIL	
			$this->load->library('email');
			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'mail.arkana.co.id',
				'smtp_port' => '25',
				'smtp_user' => 'info@arkana.co.id', // change it to yours
				'smtp_pass' => 'arkana12345', // change it to yours
				'mailtype' => 'html',
				'charset' => 'utf-8',
				'wordwrap' => FALSE,
				'newline' => "\r\n"
			);

			$this->email->initialize($config);

			$this->email->from('info@arkana.co.id','Arkana Micro Lending'); // change it to yours
			$this->email->to($this->input->post('register_email')); // change it to yours
			$this->email->bcc('info@arkana.co.id'); 
			$this->email->subject('Activate your Arkana Account');

			$messagebody = '';
			$messagebody ='<html lang="en">
							<head>
							  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
							  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
							  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
							  <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
							  <title>ARKANA Micro Lending</title>

							  <style type="text/css">
								body {
								  margin: 0;
								  padding: 0;
								  -ms-text-size-adjust: 100%;
								  -webkit-text-size-adjust: 100%;
								}

								table {
								  border-spacing: 0;
								}

								table td {
								  border-collapse: collapse;
								}

								.ExternalClass {
								  width: 100%;
								}

								.ExternalClass,
								.ExternalClass p,
								.ExternalClass span,
								.ExternalClass font,
								.ExternalClass td,
								.ExternalClass div {
								  line-height: 100%;
								}

								.ReadMsgBody {
								  width: 100%;
								  background-color: #ebebeb;
								}

								table {
								  mso-table-lspace: 0pt;
								  mso-table-rspace: 0pt;
								}

								img {
								  -ms-interpolation-mode: bicubic;
								}

								.yshortcuts a {
								  border-bottom: none !important;
								}

								@media screen and (max-width: 599px) {
								  table[class="force-row"],
								  table[class="container"] {
									width: 100% !important;
									max-width: 100% !important;
								  }
								}
								@media screen and (max-width: 400px) {
								  td[class*="container-padding"] {
									padding-left: 12px !important;
									padding-right: 12px !important;
								  }
								}
								.ios-footer a {
								  color: #aaaaaa !important;
								  text-decoration: underline;
								}
								</style>

							</head>
							<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

							<!-- 100% background wrapper (grey background) -->
							<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
							  <tr>
								<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

								  <br>

								  <!-- 600px container (white background) -->
								  <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
									<tr>
									  <td class="container-padding header" align="left" style="padding-bottom:12px;color:#99cc00;padding-left:24px;padding-right:24px">
									   ARKANA Micro Lending
									  </td>
									</tr>
									<tr>
									  <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">
										<br>
										<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">Account Activation</div>
										<br>

										<div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
										  Hi <b>'.$this->input->post('register_first_name').' '.$this->input->post('register_last_name').'</b>, thank you for registering at Arkana. Please activate your account by clicking the button below.<br/>
										  <a href="'.site_url().'/signin/activate/'.$activation_code.'" title=""><b><h3>Activate Account</h3></b></a>
										 
										 Thanks
										</div>

									  </td>
									</tr>
									<tr>
									  <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
										<br><br>
										
										
										<strong>ARKANA Micro Lending</strong><br>
										<span class="ios-footer">
										 <a href="http://arkana.co.id" style="color:#aaaaaa;text-decoration: none;">info@arkana.co.id</a><br/>
										</span>
										<a href="http://arkana.co.id" style="color:#aaaaaa;text-decoration: none;">www.arkana.co.id</a><br>

										<br><br>

									  </td>
									</tr>
								  </table>


								</td>
							  </tr>
							</table>

							</body>
							</html>
							';

			$this->email->message($messagebody);
			$this->email->send();
			
			
			return true;
		}
	}
	
	public function activate($code){ 
		
		$check = $this->lender_model->check_activation($code)->result();
		$check = $check[0]->lender_id;		
		
		if($check){
			$this->lender_model->activate($code);
			$msg='Your account has been active. Please <a href="'.site_url().'/signin/login">login</a> using your email and password.';
			$msg_style="success";
		}else{
			$msg='Failed in activation';
			$msg_style="warning";			
		}
		
		$this->template	->set('sitetitle', 'Activation')
						->set('menu_home', 'active')
						->set('msg', $msg)
						->set('msg_style', $msg_style)
						->build('activation');
	}
	
	
	public function login(){
		redirect('login');
		$this->template	->set('sitetitle', 'Login')
						->set('menu_login', 'active')
						->build('login');
	}

	function VerifyLogin()
	{
		//Validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('login_email', 'Email', 'trim|required|xss_clean');
		$this->form_validation->set_rules('login_passwd', 'Password', 'trim|required|xss_clean|callback_check_database');

		if($this->form_validation->run() == FALSE)
		{
			//Field validation failed.  User redirected to login page
			//Go to Login Page
			$this->session->set_flashdata('message', 'Field validation failed');
			redirect('signin/login', 'refresh');
		}
		else
		{
			//Field validation succeeded.  Validate against database
			$username = $this->input->post('login_email');
			$password = $this->input->post('login_passwd');

			//query the database
			$result = $this->lender_model->login($username, $password);

			if($result)
			{
				$sess_array = array();
				foreach($result as $row)
				{
					$sess_array = array(
						'uid' => $row->lender_id,
						'uemail' => $row->lender_email,
						'ufullname' => $row->lender_first_name." ".$row->lender_last_name,
						'ufirstname' => $row->lender_first_name,
						'ulasttname' => $row->lender_last_name,
						'ustatus' => $row->lender_status
					);
					$this->session->set_userdata('logged_in_user', $sess_array);
				}
				//Go to Success Page
				
				if($row->lender_status == "Enable"){
					redirect('home', 'refresh');
				}else{
					$this->session->set_flashdata('message', 'error|Your account is not active. Please activate your account first.');
					redirect('signin/login', 'refresh');
				}
			}
			else
			{
				//Go to Login Page
				$this->session->set_flashdata('message', 'error|Invalid email or password');
				redirect('signin/login', 'refresh');
			}

		}

	}
	
	function logout()
	{
		$this->session->unset_userdata('logged_in_user');
		session_destroy();
		redirect('home', 'refresh');
	}
	
	public function email(){
		
			//UPDATE EMAIL	
			$this->load->library('email');
			$config = Array(
				'protocol' => 'smtp',
				'smtp_host' => 'ssl://smtp.mailgun.org',
				'smtp_port' => '465',
				'smtp_user' => 'postmaster@mg.arkana.co.id', // change it to yours
				'smtp_pass' => '2d81625a14771268a92d806fe856bb55', // change it to yours
				'mailtype' => 'html',
				'charset' => 'utf-8',
				'wordwrap' => FALSE,
				'newline' => "\r\n"
			);

			$this->email->initialize($config);

			$this->email->from('info@arkana.co.id','Arkana Micro Lending'); // change it to yours
			$this->email->to('fikriwirawan@gmail.com'); // change it to yours
			$this->email->bcc('info@arkana.co.id'); 
			$this->email->subject('Activate your Arkana Account');

			$messagebody = '';
			$messagebody ='<html lang="en">
							<head>
							  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
							  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
							  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
							  <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
							  <title>ARKANA Micro Lending</title>

							  <style type="text/css">
								body {
								  margin: 0;
								  padding: 0;
								  -ms-text-size-adjust: 100%;
								  -webkit-text-size-adjust: 100%;
								}

								table {
								  border-spacing: 0;
								}

								table td {
								  border-collapse: collapse;
								}

								.ExternalClass {
								  width: 100%;
								}

								.ExternalClass,
								.ExternalClass p,
								.ExternalClass span,
								.ExternalClass font,
								.ExternalClass td,
								.ExternalClass div {
								  line-height: 100%;
								}

								.ReadMsgBody {
								  width: 100%;
								  background-color: #ebebeb;
								}

								table {
								  mso-table-lspace: 0pt;
								  mso-table-rspace: 0pt;
								}

								img {
								  -ms-interpolation-mode: bicubic;
								}

								.yshortcuts a {
								  border-bottom: none !important;
								}

								@media screen and (max-width: 599px) {
								  table[class="force-row"],
								  table[class="container"] {
									width: 100% !important;
									max-width: 100% !important;
								  }
								}
								@media screen and (max-width: 400px) {
								  td[class*="container-padding"] {
									padding-left: 12px !important;
									padding-right: 12px !important;
								  }
								}
								.ios-footer a {
								  color: #aaaaaa !important;
								  text-decoration: underline;
								}
								</style>

							</head>
							<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

							<!-- 100% background wrapper (grey background) -->
							<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
							  <tr>
								<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

								  <br>

								  <!-- 600px container (white background) -->
								  <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
									<tr>
									  <td class="container-padding header" align="left" style="padding-bottom:12px;color:#99cc00;padding-left:24px;padding-right:24px">
									   ARKANA Micro Lending
									  </td>
									</tr>
									<tr>
									  <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">
										<br>
										<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">Account Activation</div>
										<br>

										<div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
										  Hi <b>'.$this->input->post('register_first_name').' '.$this->input->post('register_last_name').'</b>, thank you for registering at Arkana. Please activate your account by clicking the button below.<br/>
										  <a href="'.site_url().'/signin/activate/'.$activation_code.'" title=""><b><h3>Activate Account</h3></b></a>
										 
										 Thanks
										</div>

									  </td>
									</tr>
									<tr>
									  <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
										<br><br>
										
										
										<strong>ARKANA Micro Lending</strong><br>
										<span class="ios-footer">
										 <a href="http://arkana.co.id" style="color:#aaaaaa;text-decoration: none;">info@arkana.co.id</a><br/>
										</span>
										<a href="http://arkana.co.id" style="color:#aaaaaa;text-decoration: none;">www.arkana.co.id</a><br>

										<br><br>

									  </td>
									</tr>
								  </table>


								</td>
							  </tr>
							</table>

							</body>
							</html>
							';

			$this->email->message($messagebody);
			if($this->email->send()){
				echo "success";
				
			}else{
				
				echo "failed";
				echo $this->email->print_debugger();
			}
			

	}
}
?>