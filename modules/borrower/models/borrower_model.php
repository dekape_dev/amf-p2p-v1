<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 /*
 * @author 		fikriwirawan & alifahmi
 * @since     Feb 02, 2016
 * @package		Arkana
 * @version		2.0
 */


class borrower_model extends MY_Model {

  protected $table        = 'p2p_borrower';
  protected $key          = 'borrower_id';
  protected $soft_deletes = true;
  protected $date_format  = 'datetime';

  public function __construct(){
        parent::__construct();
  }

	public function get_all_borrowers()	{
		$this->db->select('*');
		$this->db->from($this->table);
    $this->db->where('deleted', '0');
		return $this->db->get();
	}

  public function get_all_groups() {
    $this->db->select('group_id, group_name');
    $this->db->from('tbl_group');
    $this->db->where('deleted', '0');
    return $this->db->get()->result();
  }

  public function get_all_borrowers_in_detail() {
    $this->db->select('borrower_id, borrower_first_name, borrower_last_name, borrower_email, borrower_account, borrower_majelis');
    $this->db->select('client_id, client_account, client_fullname');
    $this->db->select('group_id, group_name');
    $this->db->select('data_id, data_ke, data_plafond, data_tgl, data_date_accept');
    $this->db->from($this->table);
    $this->db->join('tbl_clients','client_account = borrower_account', 'left');
    $this->db->join('tbl_group','group_id = borrower_majelis', 'left');
    $this->db->join('tbl_pembiayaan','data_rekening = client_account', 'left');
    //$this->db->where('data_plafond', 'select max("data_plafond") from tbl_pembiayaan');
    $this->db->where('p2p_borrower.deleted', '0');
    $this->db->order_by('borrower_id', 'ASC');
    $this->db->order_by('data_ke', 'DESC');
    $this->db->order_by('data_plafond', 'DESC');
    //$this->db->group_by('borrower_id');
    return $this->db->get()->result();
  }

	public function get_single_borrower($id){
      $this->db->where($this->key, $id);
		  $this->db->where('deleted', '0');
      return $this->db->get($this->table);
  }

	public function update_borrower($id, $data){
      $this->db->where($this->key, $id);
      $this->db->where('deleted', '0');
      return $this->db->update($this->table, $data);
  }

  public function update_p2p($id, $data){
      $this->db->where('data_id', $id);
      $this->db->where('deleted', '0');
      return $this->db->update('tbl_pembiayaan', $data);
  }

  public function delete_borrower($id){
      $data = array(
            'deleted' => '1'
      );
      $this->db->where($this->key, $id);
      return $this->db->update($this->table, $data);
  }

  public function check_email($param){
        $this->db->where('borrower_email', $param);
        $this->db->where('deleted', '0');
        return $this->db->get($this->table);
    }

  public function check_activation($param){
        $this->db->where('borrower_activation_code', $param);
        $this->db->where('deleted', '0');
        return $this->db->get($this->table);
    }

  public function activate($param){
        $data = array(
               'borrower_status' => 'Enable'
        );
        $this->db->where('borrower_activation_code', $param)
                 ->where('deleted', '0');
        return $this->db->update($this->table, $data);
  }

  public function login($username, $password)
  {
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->where('borrower_email = ' . "'" . $username . "'");
        $this->db->where('borrower_password = ' . "'" . MD5($password) . "'");
        $this->db->limit(1);

        $query = $this->db->get();

        if($query->num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
  }

  public function get_notification($borrower_id)
  {
    $this->db->select('borrower_name, type, title, content, from, p2p_notification.created_on, p2p_notification.created_date');
    $this->db->from('p2p_notification');
    $this->db->join('p2p_borrower','borrower_id = to');
    $this->db->where('to', $borrower_id);
    $this->db->where('role', 'b');
    $this->db->where('p2p_borrower.deleted', '0');
    //$this->db->where('created_date', $on);
    $this->db->order_by('created_date', 'DESC');
    return $this->db->get()->result();
  }

  public function get_date_notification($borrower_id)
  {
    $this->db->distinct();
    $this->db->select('created_date');
    $this->db->from('p2p_notification');
    $this->db->join('p2p_borrower','borrower_id = to');
    $this->db->where('to', $borrower_id);
    $this->db->where('role', 'b');
    $this->db->where('p2p_borrower.deleted', '0');
    $this->db->order_by('created_date', 'DESC');
    return $this->db->get()->result();
  }

  public function get_unread_notification($borrower_id)
  {
    $this->db->select('COUNT(id) as unread');
    $this->db->from('p2p_notification');
    $this->db->where('to', $borrower_id);
    $this->db->where('status', '0');
    $this->db->where('role', 'b');
    $this->db->where('deleted', '0');
    $this->db->order_by('created_date', 'DESC');
    return $this->db->get()->result();
  }

  public function set_read_notification($borrower_id)
  {
    $data = array(
        'status' => '1'
    );
    $this->db->where('to', $borrower_id);
    $this->db->where('role', 'b');
    $this->db->where('deleted', '0');
    return $this->db->update('p2p_notification', $data);
  }

  public function get_all_pengajuan()
  {
    //$limit='0', $offset='0', $search='', $key=''
    /*
    if($search != '')
    {
      return $this->db
            ->select('data_id, data_p2p, data_p2p_lender, data_ke, data_status, data_plafond, data_tgl, data_date_accept, data_popi_kategori, data_rmc_kategori')
            ->select('client_fullname, client_account, branch_name, group_name')
            ->from('tbl_pembiayaan')
            ->join('tbl_clients', 'tbl_clients.client_id = tbl_pembiayaan.data_client', 'left')
            ->join('tbl_group', 'tbl_group.group_id = tbl_clients.client_group', 'left')
            ->join('tbl_branch', 'tbl_branch.branch_id = tbl_group.group_branch', 'left')
            ->join('tbl_sector', 'tbl_sector.sector_id = tbl_pembiayaan.data_sector', 'left')
            ->where('tbl_pembiayaan.data_status','2')
            ->where('tbl_pembiayaan.deleted','0')
            ->where('tbl_pembiayaan.data_p2p','0')
            ->or_where('tbl_pembiayaan.data_p2p_lender','0')
            ->like("client_$key",$search)
            ->limit($limit,$offset)
            ->order_by('data_id','DESC')
            ->get()
            ->result();
    }
    else
    {
    */
      return $this->db
            ->select('data_id, data_p2p, data_p2p_lender, data_ke, data_status, data_plafond, data_tgl, data_date_accept, data_popi_kategori, data_rmc_kategori')
            ->select('client_fullname, client_account, branch_name, group_name')
            ->from('tbl_pembiayaan')
            ->join('tbl_clients', 'tbl_clients.client_id = tbl_pembiayaan.data_client', 'left')
            ->join('tbl_group', 'tbl_group.group_id = tbl_clients.client_group', 'left')
            ->join('tbl_branch', 'tbl_branch.branch_id = tbl_group.group_branch', 'left')
            ->join('tbl_sector', 'tbl_sector.sector_id = tbl_pembiayaan.data_sector', 'left')
            ->where('tbl_pembiayaan.data_status','2')
            ->where('tbl_clients.deleted','0')
            ->where('tbl_pembiayaan.deleted','0')
            ->where('tbl_pembiayaan.data_p2p','0')
            ->where('tbl_pembiayaan.data_p2p_lender','0')
            //->limit($limit,$offset)
            ->order_by('data_id','DESC')
            ->order_by('data_ke','DESC')
            ->get()
            ->result();
    //}
  }

  public function get_detail_pengajuan($id) {
    $this->db->select('*');
    $this->db->from('tbl_pembiayaan');
    $this->db->join('tbl_clients','client_id = data_client');
    $this->db->where('tbl_pembiayaan.deleted', '0');
    //$this->db->where('data_p2p', '0');
    $this->db->where('data_id', $id);
    //$this->db->where('data_status', '2');
    return $this->db->get();
  }




// **********************************  //
  public function generic_model() {
    $this->db->select('*');
    $this->db->from('tbl_pembiayaan');
    $this->db->join('tbl_clients','client_id = data_client');
    $this->db->where('tbl_pembiayaan.deleted', '0');
    $this->db->where('data_p2p', '0');
    $this->db->where('data_status', '2');
    $this->db->order_by('data_tgl', 'asc');
    $this->db->group_by('data_id');
    return $this->db->get();
  }


}
