<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 /*
 * @author 		fikriwirawan
 * @since       Nov 08, 2015
 * @package		Arkana
 * @version		1.0
 */
 

class borrower_below_model extends MY_Model {

    protected $table        = 'p2p_borrower_below';
    protected $key          = 'borrower_below_id';
    protected $soft_deletes = true;
    protected $date_format  = 'datetime';

    public function __construct(){
        parent::__construct();
    }
 
	public function get_all($limit){
    	$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('deleted', '0');
		$this->db->order_by('borrower_below_id', 'desc');
		return $this->db->get();
   
    }
	
	public function get_borrower_below($param){
        $this->db->where('borrower_below_id', $param);
		$this->db->where('deleted', '0');
        return $this->db->get($this->table);
    }



	
}

