<?php

 /*
 * @author 		fikriwirawan
 * @since       Nov 08, 2015
 * @package		Arkana
 * @version		1.0
 */

class Admin extends Admin_Controller{

	private $per_page 	= '10';
	private $title 		  = 'Borrower';
	private $module 	  = 'borrower';

	public function __construct(){
		parent::__construct();
		$this->load->model('borrower_model');
		$this->load->model('borrower_below_model');
	}

	public function index(){
		if($this->session->userdata('logged_in'))
		{
			//GET DATA
			$data = $this->borrower_model->get_all_pengajuan();
			$this->template->set('menu_title', 'Borrowers List')
						  	     ->set('data', $data )
							       ->build('borrower_list');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('admin/login', 'refresh');
		}
	}


	public function create(){
		if($this->save_data()){
			$this->session->set_flashdata('message', 'success|Success add new borrower');
			redirect('admin/'.$this->module.'/');
		}
		$this->template	->set('data', $data)
						->set('menu_title', 'Add Borrower')
						->build('borrower_form');
	}

	public function edit($id){

		if($this->save_data()){
			$this->session->set_flashdata('message', 'success|Success edit borrower');
			redirect('admin/'.$this->module.'/');
		}
		$data = $this->borrower_model->get_single_borrower($id)->result();
		$data = $data[0];
		$this->template->set('data', $data)
						       ->set('menu_title', 'Edit Borrower')
						       ->build('borrower_form');
	}

	public function del($id = '0'){
		if($this->borrower_model->delete_borrower($id)){
			$this->session->set_flashdata('message', 'success|Success delete borrower');
			redirect('admin/'.$this->module.'/');
			exit;
		}
	}

	private function save_data(){
		//set form validation
		$this->form_validation->set_rules('flag', 'flag', 'required');


		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('borrower_id');
			//process the form
				$data = array(
						'borrower_date_timestamp'		=> $this->input->post('borrower_date_timestamp'),
						'borrower_client_account'   => $this->input->post('borrower_client_account'),
						'borrower_client_name'			=> $this->input->post('borrower_client_name'),
						'borrower_branch_name'			=> $this->input->post('borrower_branch_name'),
						'borrower_group_name'				=> $this->input->post('borrower_group_name'),
						'borrower_plafond'					=> $this->input->post('borrower_plafond'),
						'borrower_profit'						=> $this->input->post('borrower_profit'),
						'borrower_tenor'						=> $this->input->post('borrower_tenor'),
						'borrower_date_pengajuan'		=> $this->input->post('borrower_date_pengajuan'),
						'borrower_date_pencairan'		=> $this->input->post('borrower_date_pencairan'),
						'borrower_overview'					=> $this->input->post('borrower_overview'),
						'borrower_sector'						=> $this->input->post('borrower_sector'),
						'borrower_monthly_income'		=> $this->input->post('borrower_monthly_income'),
						'borrower_employment'				=> $this->input->post('borrower_employment'),
						'borrower_location'					=> $this->input->post('borrower_location'),
						'borrower_fund_status'			=> $this->input->post('borrower_fund_status'),
						'borrower_status'						=> $this->input->post('borrower_status'),
						'borrower_disbursh_id'			=> $this->input->post('borrower_disbursh_id')

				);

			//try to upload image first
			try{
				$timestamp 								= date("Ymdhis");
				$filename 								= "arkana_".$this->input->post('borrower_client_account')."_".$timestamp;
				$config['upload_path'] 		= 'files/';
				$config['allowed_types'] 	= 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
				$config['max_size']				= '3000';
				$config['encrypt_name']	 	= FALSE;
				$config['file_name']	 		= $filename;
				$this->load->library('upload', $config);
				if($this->upload->do_upload('image')){
					$upload 								= $this->upload->data();
					$data['borrower_photo'] = $filename.$upload['file_ext'];
				}
			}catch(Exception $e){
				echo $e;
				exit;
			}

			if(!$id)
				return $this->borrower_model->insert($data);
			else
				return $this->borrower_model->update($id, $data);

		}
	}

	public function mis_connection(){
		if($this->session->userdata('logged_in'))
		{
			//GET DATA
			$data   = $this->borrower_model->get_all_borrowers_in_detail();
			//echo $this->db->last_query();

			//GET GROUP
			$groups = $this->borrower_model->get_all_groups();
			$this->template->set('data', $data)
										 ->set('groups', $groups)
						         ->set('menu_title', 'Data Borrower yg ingin dikoneksikan ke MIS')
						         ->build('borrower_mis_connection');
		}
		else
		{
			//If no session, redirect to login page
		  redirect('admin/login', 'refresh');
		}
	}

	public function connect_to_mis($borrower_id){
		if($this->session->userdata('logged_in'))
		{
			if($this->input->post()){
				$data = array(
					'borrower_account'	=> $this->input->post('b_account'),
					'borrower_majelis'	=> $this->input->post('b_group')
				);
			if($this->borrower_model->update_borrower($this->input->post('b_id'), $data)){
				$this->session->set_flashdata('message', 'success|Success connecting borrower to MIS');
				redirect('admin/borrower/mis_connection');
			}else{
				$this->session->set_flashdata('message', 'success|Failed connecting borrower to MIS');
				redirect('admin/borrower/mis_connection');
			}
			}
		}
		else
		{
			//If no session, redirect to login page
		  redirect('admin/login', 'refresh');
		}
	}

	public function disconnect_from_mis($borrower_id){
		if($this->session->userdata('logged_in'))
		{
			$data = array(
				'borrower_account'	=> '0',
				'borrower_majelis'	=> '999999999'
			);

			if($this->borrower_model->update_borrower($borrower_id, $data)){
				$this->session->set_flashdata('message', 'success|Success disconnecting borrower to MIS');
				redirect('admin/borrower/mis_connection');
			}else{
				$this->session->set_flashdata('message', 'success|Failed disconnecting borrower to MIS');
				redirect('admin/borrower/mis_connection');
			}
		}
		else
		{
			//If no session, redirect to login page
		  redirect('admin/login', 'refresh');
		}
	}

	public function candidate(){
		if($this->session->userdata('logged_in'))
		{
			//GET DATA
			$data = $this->borrower_model->get_all_pengajuan();
			$this->template->set('data', $data)
						         ->set('menu_title', 'Data Pengajuan yang Belum di-Flag P2P')
						         ->build('borrower_available');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('admin/login', 'refresh');
		}
	}

	public function detail($data_id){
		if($this->session->userdata('logged_in'))
		{
			//GET DATA
			$data = $this->borrower_model->get_detail_pengajuan($data_id)->result();
			$data = $data[0];

			$this->template->set('data', $data)
										 ->set('menu_title', 'Data Borrower')
										 ->build('borrower_detail');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('admin/login', 'refresh');
		}
	}

	public function add($data_id){

		$data = array(
					'data_p2p'	=> '1'
				);
		if($this->borrower_model->update_p2p($data_id,$data)){
			$this->session->set_flashdata('message', 'success|Success add new borrower');
			redirect('admin/borrower');
		}else{
			$this->session->set_flashdata('message', 'success|Failed add new borrower');
			redirect('admin/borrower');
		}

	}

	public function remove($data_id){

		$data = array(
					'data_p2p'	=> '2'
				);
		if($this->borrower_model->update_p2p($data_id,$data)){
			$this->session->set_flashdata('message', 'success|Success remove borrower');
			redirect('admin/borrower');
		}else{
			$this->session->set_flashdata('message', 'success|Failed remove borrower');
			redirect('admin/borrower');
		}

	}

	public function reset($data_id){

		$data = array(
					'data_p2p'	=> '0'
				);
		if($this->borrower_model->update_p2p($data_id,$data)){
			$this->session->set_flashdata('message', 'success|Success reset borrower P2P status');
			redirect('admin/borrower');
		}else{
			$this->session->set_flashdata('message', 'success|Failed reset borrower P2P status');
			redirect('admin/borrower');
		}

	}


}
