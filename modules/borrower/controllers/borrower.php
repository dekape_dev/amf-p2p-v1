<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Borrower extends Front_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('borrower_model');
		$this->load->model('borrower_below_model');
		//UNREAD NOTIFICATIONS
		$userdata             = $this->session->userdata('logged_in_borrower');
		$profile['bfullname'] = $userdata['bfullname'];
		$profile['bid']       = $userdata['bid'];
		$unread_notifications = $this->borrower_model->get_unread_notification($profile['bid']);
		$unread_notifications = $unread_notifications[0];
		$this->session->set_userdata('unread_notifications', $unread_notifications->unread);
	}

	public function index(){
		if($this->session->userdata('logged_in_borrower')) {
			//BORROWER DETAIL
			$borrower = $this->borrower_model->get_single_borrower($profile['bid'])->result();
			$borrower = $borrower[0];
			/*
			$lender_notes['finished'] = $this->investor_model->count_lender_history($profile['uid'],3);
			$lender_notes['current'] = $this->investor_model->count_lender_history($profile['uid'],1);
			$lender_notes['all'] = $lender_notes['current'] + $lender_notes['finished'];
			$lender_par['1'] = $this->investor_model->count_lender_history_par_1($profile['uid']);
			$lender_par['2'] = $this->investor_model->count_lender_history_par_2($profile['uid']);
			$lender_par['3'] = $this->investor_model->count_lender_history_par_3($profile['uid']);

			//GET DEPOSIT
			$deposit['total'] = $this->investor_model->get_deposit_total_by_lender($profile['uid']);
			$deposit['outstanding'] = $this->investor_model->get_deposit_outstanding_by_lender($profile['uid']);
			$deposit['available'] = $deposit['total'] - $deposit['outstanding'];
			$deposit['percentage'] = $deposit['outstanding']/$deposit['total']*100;
			*/

			$this->template->set('sitetitle', 'Borrower')
						   			 ->build('dashboard');

		}else{
			redirect('login_to_borrow');
		}

	}

	public function profile(){
		if($this->session->userdata('logged_in_borrower')) {
			if($this->input->post()){
				$data = array(
						'borrower_first_name'		     => $this->input->post('firstname'),
						'borrower_last_name'         => $this->input->post('lastname'),
						'borrower_birth_date'        => $this->input->post('birthday'),
						'borrower_address'		       => $this->input->post('address'),
						'borrower_city'              => $this->input->post('city'),
						'borrower_province'		       => $this->input->post('province'),
						'borrower_zipcode'           => $this->input->post('zipcode'),
						//'borrower_email'      	   => $this->input->post('email'),
						//'borrower_password'  		   => $this->input->post('lastname'),
						//'borrower_register_date'   => date('Y-m-d H:i:s'),
						//'borrower_activation_code' => $activation_code,
						'borrower_phone'             => $this->input->post('phone'),
						'borrower_idcard'            => $this->input->post('idcard'),
						'borrower_taxcard'            => $this->input->post('taxcard')
				);
				$this->borrower_model->update($this->session->userdata('bid'), $data);
				$this->session->set_flashdata('message', 'success|Account update was successful.');
			}

			$userdata            = $this->session->userdata('logged_in_borrower');
			$profile['fullname'] = $userdata['bfullname'];
			$profile['bid']      = $userdata['bid'];

			//BORROWER DETAIL
			$borrower = $this->borrower_model->get_single_borrower($profile['bid'])->result();
			$borrower = $borrower[0];
			$this->template->set('sitetitle', 'Borrower')
										 ->set('borrower', $borrower)
									   ->build('personalinfo');
		}else{
			redirect('login_to_borrow');
		}
	}

	public function loanunder20mil(){
		if($this->session->userdata('logged_in_borrower')) {
			if($this->save_data()){
				$this->session->set_flashdata('message', 'success|Success add new borrower');
				redirect('borrower/loanunder20mil');
			}

			$this->template->set('sitetitle', 'Borrower - Loan Request')
									   ->set('menu_loan', 'active')
										 ->build('loanunder20mil_new');
		}else{
			redirect('login_to_borrow');
		}
	}

	public function loanabove20mil(){
		if($this->session->userdata('logged_in_borrower')) {
			if($this->save_data()){
				$this->session->set_flashdata('message', 'success|Success add Loan Request');
				redirect('borrower/loanabove20mil');
			}

			$this->template->set('sitetitle', 'Borrower - Loan Request')
										 ->set('menu_loan', 'active')
										 ->build('loanabove20mil');
		}else{
			redirect('login_to_borrow');
		}
	}

	public function notification(){
		/*
		SQL:
		(1)
		ALTER TABLE `p2p_notification`
		CHANGE `to` `to` int(11) NOT NULL COMMENT 'id of borrower/admin' AFTER `from`,
		ADD `role` varchar(1) NOT NULL COMMENT 'b:borrower, l:lender' AFTER `to`;
		(2)
		ALTER TABLE `p2p_notification` CHANGE `role` `role` VARCHAR(1) CHARACTER
		SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'b' COMMENT 'b:borrower, l:lender';
		(3)
		ALTER TABLE `p2p_notification`
		ADD `type` varchar(3) NOT NULL DEFAULT 'APR' COMMENT 'APR: Approval, FIN: Ended Funding' AFTER `id`;

		NOTES:
		$data = array(
				'title'   => 'title of notifications',
				'content' => $body_of_notification,
				'from'    => $admin_id, //43
				'to'      => $borrower_id/$lender_id,
				'role'		=> 'b/l'
		);
		$this->db->insert('p2p_notification', $data);
		*/
		$userdata             = $this->session->userdata('logged_in_borrower');
		$profile['bfullname'] = $userdata['bfullname'];
		$profile['bid']       = $userdata['bid'];
		//BORROWER DETAIL
		$borrower = $this->borrower_model->get_single_borrower($profile['bid'])->result();
		$borrower = $borrower[0];
		//NOTIFICATIONS
		$date_notifications 	= $this->borrower_model->get_date_notification($profile['bid']);
		$notifications      	= $this->borrower_model->get_notification($profile['bid']);

		$this->borrower_model->set_read_notification($profile['bid']);
		$unread_notifications = $this->borrower_model->get_unread_notification($profile['bid']);
		$unread_notifications = $unread_notifications[0];
		$this->session->set_userdata('unread_notifications', $unread_notifications->unread);
		//var_dump($date_notifications);
		//var_dump($notifications);
		if($this->session->userdata('logged_in_borrower')) {
			$this->template->set('sitetitle', 'Borrower - Notification')
										 ->set('menu_loan', 'active')
										 ->set('notifications', $notifications)
										 ->set('date_notifications', $date_notifications)
										 ->build('notification');
		}else{
			redirect('login_to_borrow');
		}
	}

	public function invoice(){
		if($this->session->userdata('logged_in_borrower')) {
			$this->template->set('sitetitle', 'Borrower - Invoice')
										 ->set('menu_loan', 'active')
										 ->build('invoice');
		}else{
			redirect('login_to_borrow');
		}
	}

	private function save_data(){
		//set form validation
		$this->form_validation->set_rules('borrower_below_firstname', 'First Name', 'required');


		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('borrower_below_id');
			//process the form
				$data = array(
						'borrower_below_firstname'  => $this->input->post('borrower_below_firstname'),
						'borrower_below_lastname'		=> $this->input->post('borrower_below_lastname'),
						'borrower_below_email'			=> $this->input->post('borrower_below_email'),
						'borrower_below_ktp'			  => $this->input->post('borrower_below_ktp'),
						'borrower_below_hp'				  => $this->input->post('borrower_below_hp'),
						'borrower_below_borndate'		=> $this->input->post('borrower_below_borndate'),
						'borrower_below_plafond'		=> $this->input->post('borrower_below_plafond'),
						'borrower_below_sector'			=> $this->input->post('borrower_below_sector'),
						'borrower_below_desa'			  => $this->input->post('borrower_below_desa'),
						'borrower_below_tenor'			=> $this->input->post('borrower_below_tenor'),
						'borrower_below_location'		=> $this->input->post('borrower_below_location')
				);


			if(!$id)
				return $this->borrower_below_model->insert($data);
			else
				return $this->borrower_below_model->update($id, $data);
		}
	}

	public function login(){
		$this->template->set_layout('login');
		$this->template->set('sitetitle', 'Borrower Login')
									 ->set('menu_login', 'active')
									 ->build('login');
	}

	public function logout()
	{
		$this->session->unset_userdata('logged_in_borrower');
		session_destroy();
		redirect(site_url(), 'refresh');
	}

	public function VerifyLogin()
	{
		//Validation
		$this->load->library('form_validation');
		$this->form_validation->set_rules('login_email', 'Email', 'trim|required|xss_clean');
		$this->form_validation->set_rules('login_passwd', 'Password', 'trim|required|xss_clean|callback_check_database');

		if($this->form_validation->run() == FALSE)
		{
			//Field validation failed.  Borrower redirected to login page
			//Go to Login Page
			$this->session->set_flashdata('message', 'Field validation failed');
			redirect(site_url('login_to_borrow'), 'refresh');
		}
		else
		{
			//Field validation succeeded.  Validate against database
			$username = $this->input->post('login_email');
			$password = $this->input->post('login_passwd');

			//Query the database
			$result = $this->borrower_model->login($username, $password);

			if($result)
			{
				$sess_array = array();
				foreach($result as $row)
				{
					$sess_array = array(
						'bid'        => $row->borrower_id,
						'bemail'     => $row->borrower_email,
						'bfirstname' => $row->borrower_first_name,
						'blastname'  => $row->borrower_last_name,
						'bfullname'  => $row->borrower_first_name." ".$row->borrower_last_name,
						'baccount'   => $row->borrower_account,
						'bmajelis'   => $row->borrower_majelis,
						'bstatus'    => $row->borrower_status
					);

					$this->session->set_userdata('logged_in_borrower', $sess_array);
					$this->session->set_userdata('bid', $row->borrower_id);
					$this->session->set_userdata('bfirstname', $row->borrower_first_name);
					$this->session->set_userdata('blastname',  $row->borrower_last_name);
					$this->session->set_userdata('bemail',  $row->borrower_email);
					$this->session->set_userdata('bfullname',  $row->borrower_first_name." ".$row->borrower_last_name);
					$this->session->set_userdata('baccount',  $row->borrower_account);
					$this->session->set_userdata('bmajelis',  $row->borrower_majelis);

					//echo 'sess_array: ';
					//var_dump($this->session->userdata('logged_in_user')); die();
				}

				//Go to Success Page
				if($row->borrower_status == "Enable"){
					redirect(site_url('borrower'), 'refresh');
				}else{
					$this->session->set_flashdata('message', 'error|Your account is not yet activated. Please activate your account first.');
					redirect(site_url('login_to_borrow'), 'refresh');
				}
			}
			else
			{
				//Go to Login Page
				$this->session->set_flashdata('message', 'error|Invalid email or password');
				redirect(site_url('login_to_borrow'), 'refresh');
			}

		}

	}

	public function activate($code){

		$check = $this->borrower_model->check_activation($code)->result();
		$check = $check[0]->borrower_id;

		if($check){
			$this->borrower_model->activate($code);
			$msg='Your account has been activated. Please <a href="'.site_url().'">login</a> using your email and password.';
			$msg_style="success";
		}else{
			$msg='Failed in activation';
			$msg_style="warning";
		}

		$this->template->set_layout('login');
		$this->template	->set('sitetitle', 'Account Activation')
						->set('menu_home', 'active')
						->set('msg', $msg)
						->set('msg_style', $msg_style)
						->build('activation');
	}

	public function register(){
		if($this->save_register()){
			$this->session->set_flashdata('message', 'success|Registration Success! Please check your email to activate your account.');
			redirect('login_to_borrow');
		}

		$this->template->set_layout('login');
		$this->template->set('sitetitle', 'Borrower')
									 ->build('register');
	}

	private function save_register(){
		//set form validation
		$this->form_validation->set_rules('register_first_name', 'First Name', 'required');
		$this->form_validation->set_rules('register_email', 'Email', 'required');
		$this->form_validation->set_rules('register_passwd', 'Password', 'required');

		if($this->form_validation->run() === TRUE){

			//Check Email Already Exists?
			$email = $this->input->post('register_email');
			$cek_email = $this->borrower_model->check_email($email)->result();
			$cek_email = $cek_email[0];

			if($cek_email->borrower_email){
				$this->session->set_flashdata('message', 'success|Email is already registered. Please login using your email.');
				redirect('login');
			}elseif($this->input->post('register_passwd') != $this->input->post('register_passwd_confirm')){
				$this->session->set_flashdata('message', 'success|Password not match');
				redirect('register');
			}else{
				//process the form
				$this->db->trans_start();

					$activation_code =  sha1($this->input->post('register_email').date('Ymdhis'));
					$data = array(
						'borrower_first_name'	   => $this->input->post('register_first_name'),
						'borrower_last_name'       => $this->input->post('register_last_name'),
						'borrower_name'		   	   => $this->input->post('register_first_name').' '.$this->input->post('register_first_name'),
						'borrower_email'      	   => $this->input->post('register_email'),
						'borrower_password'  	   => md5($this->input->post('register_passwd')),
						'borrower_register_date'   => date('Y-m-d H:i:s'),
						'borrower_activation_code' => $activation_code
					);


					$this->borrower_model->insert($data);
					$insert_id = $this->db->insert_id();
					$borrower_number = (date('y')*1000000) + $insert_id;
					$data2 = array(
						'borrower_number' => $borrower_number,
					);
					$this->borrower_model->update($insert_id, $data2);

				$this->db->trans_complete();

				//UPDATE EMAIL
				$this->load->library('email');
				$config = Array(
					'protocol'  => 'smtp',
					'smtp_host' => 'ssl://smtp.mailgun.org',
					'smtp_port' => '465',
					'smtp_user' => 'postmaster@mg.arkana.co.id', // change it to yours
					'smtp_pass' => '2d81625a14771268a92d806fe856bb55', // change it to yours
					'mailtype'  => 'html',
					'charset'   => 'utf-8',
					'wordwrap'  => FALSE,
					'newline'   => "\r\n"
				);

				$this->email->initialize($config);

				$this->email->from('info@amartha.com','Amartha Microfinance'); // change it to yours
				$this->email->to($this->input->post('register_email')); // change it to yours
				$this->email->bcc('info@amartha.com');
				$this->email->subject('Activate Your Amartha Account');

				$messagebody = '';
				$messagebody ='<html lang="en">
								<head>
								  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
								  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
								  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
								  <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
								  <title>Amartha Micro Lending</title>

								  <style type="text/css">
									body {
									  margin: 0;
									  padding: 0;
									  -ms-text-size-adjust: 100%;
									  -webkit-text-size-adjust: 100%;
									}

									table {
									  border-spacing: 0;
									}

									table td {
									  border-collapse: collapse;
									}

									.ExternalClass {
									  width: 100%;
									}

									.ExternalClass,
									.ExternalClass p,
									.ExternalClass span,
									.ExternalClass font,
									.ExternalClass td,
									.ExternalClass div {
									  line-height: 100%;
									}

									.ReadMsgBody {
									  width: 100%;
									  background-color: #ebebeb;
									}

									table {
									  mso-table-lspace: 0pt;
									  mso-table-rspace: 0pt;
									}

									img {
									  -ms-interpolation-mode: bicubic;
									}

									.yshortcuts a {
									  border-bottom: none !important;
									}

									@media screen and (max-width: 599px) {
									  table[class="force-row"],
									  table[class="container"] {
										width: 100% !important;
										max-width: 100% !important;
									  }
									}
									@media screen and (max-width: 400px) {
									  td[class*="container-padding"] {
										padding-left: 12px !important;
										padding-right: 12px !important;
									  }
									}
									.ios-footer a {
									  color: #aaaaaa !important;
									  text-decoration: underline;
									}
									</style>

								</head>
								<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

								<!-- 100% background wrapper (grey background) -->
								<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
								  <tr>
									<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

									  <br>

									  <!-- 600px container (white background) -->
									  <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
										<tr>
										  <td class="container-padding header" align="left" style="padding-bottom:12px;color:#99cc00;padding-left:24px;padding-right:24px">
										   <img src="'.$this->template->get_theme_path().'img/logo_amartha_small_color.png" alt="Amartha"/>
										  </td>
										</tr>
										<tr>
										  <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">
											<br>
											<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">Account Activation</div>
											<br>

											<div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
											  Hi <b>'.$this->input->post('register_first_name').' '.$this->input->post('register_last_name').'</b>, thank you for registering at Amartha. Please activate your account by clicking the button below.<br/>
											  <a href="'.site_url().'borrower/activate/'.$activation_code.'" title=""><b><h3>Activate Account</h3></b></a>

											 Thanks
											</div>

										  </td>
										</tr>
										<tr>
										  <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
											<br><br>


											<strong>Amartha</strong><br>
											<span class="ios-footer">
											 <a href="http://amartha.com" style="color:#aaaaaa;text-decoration: none;">info@amartha.com</a><br/>
											</span>
											<a href="http://amartha.com" style="color:#aaaaaa;text-decoration: none;">www.amartha.com</a><br>

											<br><br>

										  </td>
										</tr>
									  </table>


									</td>
								  </tr>
								</table>

								</body>
								</html>
								';

				$this->email->message($messagebody);
				$this->email->send();


				return true;
			}
		}
	}

	public function testemail(){
		//UPDATE EMAIL
				$this->load->library('email');
				$config = Array(
					'protocol'  => 'smtp',
					'smtp_host' => 'ssl://smtp.mailgun.org',
					'smtp_port' => '465',
					'smtp_user' => 'postmaster@mg.arkana.co.id', // change it to yours
					'smtp_pass' => '2d81625a14771268a92d806fe856bb55', // change it to yours
					'mailtype'  => 'html',
					'charset'   => 'utf-8',
					'wordwrap'  => FALSE,
					'newline'   => "\r\n"
				);

				$this->email->initialize($config);

				$this->email->from('info@amartha.com','Amartha Microfinance'); // change it to yours
				$this->email->to('fikriwirawan@gmail.com'); // change it to yours
				$this->email->bcc('info@amartha.com');
				$this->email->subject('Activate Your Amartha Account');

				$messagebody = '';
				$messagebody ='<html lang="en">
								<head>
								  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
								  <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- So that mobile will display zoomed in -->
								  <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- enable media queries for windows phone 8 -->
								  <meta name="format-detection" content="telephone=no"> <!-- disable auto telephone linking in iOS -->
								  <title>Amartha Micro Lending</title>

								  <style type="text/css">
									body {
									  margin: 0;
									  padding: 0;
									  -ms-text-size-adjust: 100%;
									  -webkit-text-size-adjust: 100%;
									}

									table {
									  border-spacing: 0;
									}

									table td {
									  border-collapse: collapse;
									}

									.ExternalClass {
									  width: 100%;
									}

									.ExternalClass,
									.ExternalClass p,
									.ExternalClass span,
									.ExternalClass font,
									.ExternalClass td,
									.ExternalClass div {
									  line-height: 100%;
									}

									.ReadMsgBody {
									  width: 100%;
									  background-color: #ebebeb;
									}

									table {
									  mso-table-lspace: 0pt;
									  mso-table-rspace: 0pt;
									}

									img {
									  -ms-interpolation-mode: bicubic;
									}

									.yshortcuts a {
									  border-bottom: none !important;
									}

									@media screen and (max-width: 599px) {
									  table[class="force-row"],
									  table[class="container"] {
										width: 100% !important;
										max-width: 100% !important;
									  }
									}
									@media screen and (max-width: 400px) {
									  td[class*="container-padding"] {
										padding-left: 12px !important;
										padding-right: 12px !important;
									  }
									}
									.ios-footer a {
									  color: #aaaaaa !important;
									  text-decoration: underline;
									}
									</style>

								</head>
								<body style="margin:0; padding:0;" bgcolor="#F0F0F0" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

								<!-- 100% background wrapper (grey background) -->
								<table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" bgcolor="#F0F0F0">
								  <tr>
									<td align="center" valign="top" bgcolor="#F0F0F0" style="background-color: #F0F0F0;">

									  <br>

									  <!-- 600px container (white background) -->
									  <table border="0" width="600" cellpadding="0" cellspacing="0" class="container" style="width:600px;max-width:600px">
										<tr>
										  <td class="container-padding header" align="left" style="padding-bottom:12px;color:#99cc00;padding-left:24px;padding-right:24px">
										   <img src="'.$this->template->get_theme_path().'img/logo_amartha_small_color.png" alt="Amartha"/>
										  </td>
										</tr>
										<tr>
										  <td class="container-padding content" align="left" style="padding-left:24px;padding-right:24px;padding-top:12px;padding-bottom:12px;background-color:#ffffff">
											<br>
											<div class="title" style="font-family:Helvetica, Arial, sans-serif;font-size:18px;font-weight:600;color:#374550">Account Activation</div>
											<br>

											<div class="body-text" style="font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333">
											  Hi <b>'.$this->input->post('register_first_name').' '.$this->input->post('register_last_name').'</b>, thank you for registering at Amartha. Please activate your account by clicking the button below.<br/>
											  <a href="'.site_url().'borrower/activate/'.$activation_code.'" title=""><b><h3>Activate Account</h3></b></a>

											 Thanks
											</div>

										  </td>
										</tr>
										<tr>
										  <td class="container-padding footer-text" align="left" style="font-family:Helvetica, Arial, sans-serif;font-size:12px;line-height:16px;color:#aaaaaa;padding-left:24px;padding-right:24px">
											<br><br>


											<strong>Amartha</strong><br>
											<span class="ios-footer">
											 <a href="http://amartha.com" style="color:#aaaaaa;text-decoration: none;">info@amartha.com</a><br/>
											</span>
											<a href="http://amartha.com" style="color:#aaaaaa;text-decoration: none;">www.amartha.com</a><br>

											<br><br>

										  </td>
										</tr>
									  </table>


									</td>
								  </tr>
								</table>

								</body>
								</html>
								';

				$this->email->message($messagebody);
				$this->email->send();
	}
	// public function loan($type){
	// 	if($type == 'under20mil')
	// 	{
	// 		$this->template	->set('sitetitle', 'Borrower - Loan Request')
	// 					->set('menu_laon', 'active')
	// 					->build('loanunder20mil');
	// 	}
	// 	else if($type == 'above20mil'){
	// 		$this->template	->set('sitetitle', 'Borrower - Loan Request')
	// 					->set('menu_loan', 'active')
	// 					->build('loanmore20mil');
	// 	}

	// }

	// public function profile($id='0'){
	// 	if($this->session->userdata('logged_in_user')) {
	// 		$userdata = $this->session->userdata('logged_in_user');
	// 		$profile['fullname'] = $userdata['ufullname'];
	// 		$profile['uid'] = $userdata['uid'];

	// 		//GET LENDER
	// 		$lender = $this->investor_model->get_lender($profile['uid'])->result();
	// 		$lender = $lender[0];
	// 		//GET BORROWER

	// 		//GET DEPOSIT
	// 		$deposit['total'] = $this->investor_model->get_deposit_total_by_lender($profile['uid']);
	// 		$deposit['outstanding'] = $this->investor_model->get_deposit_outstanding_by_lender($profile['uid']);
	// 		$deposit['available'] = $deposit['total'] - $deposit['outstanding'];
	// 		$deposit['percentage'] = $deposit['outstanding']/$deposit['total']*100;

	// 		$this->template->set_layout('dashboard');
	// 		$this->template	->set('sitetitle', 'Investor')
	// 						->set('menu_overview', 'active')
	// 						->set('menu_account', 'active')
	// 						->set('profile', $profile)
	// 						->set('lender', $lender)
	// 						->set('deposit', $deposit)
	// 						->build('profile');

	// 	}else{
	// 		redirect('login');
	// 	}


	// }


}
