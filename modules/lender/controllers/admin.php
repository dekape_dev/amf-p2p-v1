<?php

 /*
 * @author 		fikriwirawan
 * @since       Nov 08, 2015
 * @package		Arkana
 * @version		1.0
 */

class Admin extends Admin_Controller{

	private $per_page 	= '10';
	private $title 		  = 'Lender';
	private $module 	  = 'lender';

	public function __construct(){
		parent::__construct();
		$this->load->model('lender_model');
	}

	public function index(){
		if($this->session->userdata('logged_in'))
		{
			//GET DATA
			$data = $this->lender_model->get_all()->result();
			$this->template	->set('menu_title', 'Lenders List')
							->set('data', $data )
							->build('lender_list');
		}
		else
		{
		  //If no session, redirect to login page
		  redirect('admin/login', 'refresh');
		}
	}


	public function create(){
		if($this->save_data()){
			$this->session->set_flashdata('message', 'success|Success add new lender');
			redirect('admin/'.$this->module.'/');
		}
		$this->template	->set('data', $data)
						->set('menu_title', 'Add Lender')
						->build('lender_form');
	}

	public function edit($lender_id){

		if($this->save_data()){
			$this->session->set_flashdata('message', 'success|Success edit lender');
			redirect('admin/'.$this->module.'/');
		}
		$data = $this->lender_model->get_lender($lender_id)->result();
		$data = $data[0];
		$this->template	->set('data', $data)
						->set('menu_title', 'Edit Lender')
						->build('lender_form');
	}

	public function del($id = '0'){
		if($this->lender_model->delete($id)){
			$this->session->set_flashdata('message', 'success|Success delete lender');
			redirect('admin/'.$this->module.'/');
			exit;
		}
	}

	private function save_data(){
		//set form validation
		$this->form_validation->set_rules('flag', 'flag', 'required');


		if($this->form_validation->run() === TRUE){
			$id = $this->input->post('lender_id');
			//process the form
				$data = array(
						'lender_name'		=> $this->input->post('lender_name'),
						'lender_email'		=> $this->input->post('lender_email'),
						'lender_phone'		=> $this->input->post('lender_phone'),
						'lender_address'	=> $this->input->post('lender_address'),
						'lender_birth_date'	=> $this->input->post('lender_address'),
						'lender_bank_account'	=> $this->input->post('lender_bank_account'),
						'lender_bank_name'	=> $this->input->post('lender_bank_name'),
						'lender_status'		=> $this->input->post('lender_status'),

				);


			if(!$id)
				return $this->lender_model->insert($data);
			else
				return $this->lender_model->update($id, $data);

		}
	}

}
