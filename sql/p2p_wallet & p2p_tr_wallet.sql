

--
-- Table structure for table `p2p_wallet`
--

CREATE TABLE IF NOT EXISTS `p2p_wallet` (
  `wallet_id` int(11) NOT NULL,
  `wallet_lender` int(11) NOT NULL,
  `wallet_debet` int(11) NOT NULL,
  `walet_credit` int(11) NOT NULL,
  `wallet_saldo` int(11) NOT NULL,
  `wallet_remark` varchar(100) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `p2p_tr_wallet`
--
ALTER TABLE `p2p_tr_wallet`
  ADD PRIMARY KEY (`wallet_tr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `p2p_tr_wallet`
--
ALTER TABLE `p2p_tr_wallet`
  MODIFY `wallet_tr_id` int(11) NOT NULL AUTO_INCREMENT;



--
-- Table structure for table `p2p_tr_wallet`
--

CREATE TABLE IF NOT EXISTS `p2p_tr_wallet` (
  `wallet_tr_id` int(11) NOT NULL,
  `wallet_id` int(11) NOT NULL,
  `wallet_lender` int(11) NOT NULL,
  `wallet_debet` int(11) NOT NULL,
  `walet_credit` int(11) NOT NULL,
  `wallet_saldo` int(11) NOT NULL,
  `wallet_remark` varchar(100) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `p2p_tr_wallet`
--
ALTER TABLE `p2p_tr_wallet`
  ADD PRIMARY KEY (`wallet_tr_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `p2p_tr_wallet`
--
ALTER TABLE `p2p_tr_wallet`
  MODIFY `wallet_tr_id` int(11) NOT NULL AUTO_INCREMENT;