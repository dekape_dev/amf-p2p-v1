-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2015 at 07:07 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mis_amartha_20150111`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pembiayaan`
--

CREATE TABLE IF NOT EXISTS `tbl_pembiayaan` (
`data_id` int(11) NOT NULL,
  `data_client` int(11) NOT NULL,
  `data_p2p` int(11) NOT NULL DEFAULT '0',
  `data_p2p_lender` int(11) NOT NULL DEFAULT '0',
  `data_tgl` varchar(20) DEFAULT NULL,
  `data_date_accept` varchar(20) DEFAULT NULL,
  `data_date_first` varchar(20) DEFAULT NULL,
  `data_jatuhtempo` varchar(20) DEFAULT NULL,
  `data_pengajuan` varchar(10) DEFAULT NULL,
  `data_ke` int(11) NOT NULL DEFAULT '0',
  `data_rekening` varchar(30) DEFAULT NULL,
  `data_plafond` int(11) NOT NULL DEFAULT '0',
  `data_jangkawaktu` int(11) NOT NULL DEFAULT '50',
  `data_akad` varchar(20) DEFAULT NULL,
  `data_totalangsuran` varchar(11) DEFAULT '0',
  `data_angsuranpokok` varchar(11) NOT NULL DEFAULT '0',
  `data_tabunganwajib` varchar(11) NOT NULL DEFAULT '0',
  `data_margin` varchar(11) DEFAULT '0',
  `data_angsuranke` int(11) DEFAULT NULL,
  `data_pertemuanke` int(11) NOT NULL DEFAULT '0',
  `data_sisaangsuran` varchar(100) DEFAULT NULL,
  `data_status` int(11) DEFAULT '2' COMMENT '1:berjalan, 2:pengajuan, 3:selesai',
  `data_par` int(11) NOT NULL DEFAULT '0',
  `data_status_pengajuan` varchar(1) DEFAULT 'v',
  `data_sumber_pembiayaan` varchar(5) DEFAULT NULL,
  `data_pembiayaan1_nama` varchar(30) DEFAULT NULL,
  `data_pembiayaan1_lama` varchar(30) DEFAULT NULL,
  `data_pembiayaan1_plafond` varchar(30) DEFAULT NULL,
  `data_pembiayaan1_total` varchar(30) DEFAULT NULL,
  `data_pembiayaan1_status` varchar(30) DEFAULT NULL,
  `data_pembiayaan2_nama` varchar(30) DEFAULT NULL,
  `data_pembiayaan2_lama` varchar(30) DEFAULT NULL,
  `data_pembiayaan2_plafond` varchar(30) DEFAULT NULL,
  `data_pembiayaan2_total` varchar(30) DEFAULT NULL,
  `data_pembiayaan2_status` varchar(30) DEFAULT NULL,
  `data_pembiayaan3_nama` varchar(30) DEFAULT NULL,
  `data_pembiayaan3_lama` varchar(30) DEFAULT NULL,
  `data_pembiayaan3_plafond` varchar(30) DEFAULT NULL,
  `data_pembiayaan3_total` varchar(30) DEFAULT NULL,
  `data_pembiayaan3_status` varchar(30) DEFAULT NULL,
  `data_pembiayaan4_nama` varchar(30) DEFAULT NULL,
  `data_pembiayaan4_lama` varchar(30) DEFAULT NULL,
  `data_pembiayaan4_plafond` varchar(30) DEFAULT NULL,
  `data_pembiayaan4_total` varchar(30) DEFAULT NULL,
  `data_pembiayaan4_status` varchar(30) DEFAULT NULL,
  `data_suami` varchar(30) DEFAULT NULL,
  `data_suami_tgllahir` varchar(30) DEFAULT NULL,
  `data_suami_umur` varchar(10) DEFAULT NULL,
  `data_suami_pekerjaan` varchar(30) DEFAULT NULL,
  `data_suami_komoditas` varchar(30) DEFAULT NULL,
  `data_suami_pendidikan` varchar(30) DEFAULT NULL,
  `data_keluarga_anak` varchar(10) DEFAULT NULL,
  `data_keluarga_belumsekolah` varchar(10) DEFAULT NULL,
  `data_keluarga_tk` varchar(10) DEFAULT NULL,
  `data_keluarga_tidaksekolah` varchar(10) DEFAULT NULL,
  `data_keluarga_tidaktamatsd` varchar(10) DEFAULT NULL,
  `data_keluarga_sd` varchar(10) DEFAULT NULL,
  `data_keluarga_smp` varchar(10) DEFAULT NULL,
  `data_keluarga_sma` varchar(10) DEFAULT NULL,
  `data_keluarga_kuliah` varchar(10) DEFAULT NULL,
  `data_keluarga_tanggungan` varchar(10) DEFAULT NULL,
  `data_popi_anggotart` varchar(10) DEFAULT NULL,
  `data_popi_masihsekolah` varchar(10) DEFAULT NULL,
  `data_popi_pendidikanistri` varchar(30) DEFAULT NULL,
  `data_popi_pekerjaansuami` varchar(30) DEFAULT NULL,
  `data_popi_jenislantai` varchar(50) DEFAULT NULL,
  `data_popi_jeniswc` varchar(50) DEFAULT NULL,
  `data_popi_bahanbakar` varchar(50) DEFAULT NULL,
  `data_popi_gas` varchar(30) DEFAULT NULL,
  `data_popi_kulkas` varchar(30) DEFAULT NULL,
  `data_popi_motor` varchar(30) DEFAULT NULL,
  `data_popi_total` varchar(30) DEFAULT NULL,
  `data_popi_kategori` varchar(30) DEFAULT NULL,
  `data_rmc_ukuranrumah` varchar(30) DEFAULT NULL,
  `data_rmc_kondisirumah` varchar(50) DEFAULT NULL,
  `data_rmc_jenisatap` varchar(30) DEFAULT NULL,
  `data_rmc_jenisdinding` varchar(30) DEFAULT NULL,
  `data_rmc_jenislantai` varchar(30) DEFAULT NULL,
  `data_rmc_listrik` varchar(30) DEFAULT NULL,
  `data_rmc_sumberair` varchar(30) DEFAULT NULL,
  `data_rmc_total` varchar(30) DEFAULT NULL,
  `data_rmc_kategori` varchar(30) DEFAULT NULL,
  `data_rmc_kepemilikan` varchar(30) DEFAULT NULL,
  `data_rmc_hargaperbulan` varchar(30) DEFAULT NULL,
  `data_aset_lahan` varchar(30) DEFAULT NULL,
  `data_aset_jumlahlahan` varchar(10) DEFAULT NULL,
  `data_aset_ternak` varchar(30) DEFAULT NULL,
  `data_aset_jumlahternak` varchar(10) DEFAULT NULL,
  `data_aset_tabungan` varchar(30) DEFAULT NULL,
  `data_aset_deposito` varchar(30) DEFAULT NULL,
  `data_aset_lain` varchar(30) DEFAULT NULL,
  `data_aset_total` varchar(30) DEFAULT NULL,
  `data_pendapatan_suami` varchar(30) DEFAULT NULL,
  `data_pendapatan_suamijenisusaha` varchar(30) DEFAULT NULL,
  `data_pendapatan_suamilama` varchar(30) DEFAULT NULL,
  `data_pendapatan_istri` varchar(30) DEFAULT NULL,
  `data_pendapatan_istrijenisusaha` varchar(30) DEFAULT NULL,
  `data_pendapatan_istrilama` varchar(30) DEFAULT NULL,
  `data_pendapatan_lain` varchar(30) DEFAULT NULL,
  `data_pendapatan_lainjenisusaha` varchar(30) DEFAULT NULL,
  `data_pendapatan_lainlama` varchar(30) DEFAULT NULL,
  `data_pendapatan_total` varchar(30) DEFAULT NULL,
  `data_pengeluaran_beras` varchar(30) DEFAULT NULL,
  `data_pengeluaran_dapur` varchar(30) DEFAULT NULL,
  `data_pengeluaran_rekening` varchar(30) DEFAULT NULL,
  `data_pengeluaran_pulsa` varchar(30) DEFAULT NULL,
  `data_pengeluaran_kreditan` varchar(30) DEFAULT NULL,
  `data_pengeluaran_arisan` varchar(30) DEFAULT NULL,
  `data_pengeluaran_pendidikan` varchar(30) DEFAULT NULL,
  `data_pengeluaran_umum` varchar(30) DEFAULT NULL,
  `data_pengeluaran_angsuranlain` varchar(30) DEFAULT NULL,
  `data_pengeluaran_total` varchar(30) DEFAULT NULL,
  `data_savingpower` varchar(30) DEFAULT NULL,
  `data_tujuan` text CHARACTER SET latin1,
  `data_sector` int(11) NOT NULL DEFAULT '0',
  `data_keterangan` text CHARACTER SET latin1,
  `data_verify` varchar(30) DEFAULT NULL,
  `data_alasan` text,
  `data_tr` int(11) NOT NULL DEFAULT '0',
  `data_monitoring_pembiayaan` int(11) DEFAULT '0',
  `data_monitoring_pembiayaan_date` varchar(20) DEFAULT NULL,
  `data_monitoring_pembiayaan_date_end` varchar(20) DEFAULT NULL,
  `data_monitoring_pembiayaan_result` text,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `title` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17512 DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_pembiayaan`
--
ALTER TABLE `tbl_pembiayaan`
 ADD PRIMARY KEY (`data_id`), ADD KEY `data_id` (`data_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_pembiayaan`
--
ALTER TABLE `tbl_pembiayaan`
MODIFY `data_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17512;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
