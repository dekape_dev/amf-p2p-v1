-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2015 at 02:25 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mis_amartha_20150111`
--

-- --------------------------------------------------------

--
-- Table structure for table `p2p_deposit`
--

CREATE TABLE IF NOT EXISTS `p2p_deposit` (
`deposit_id` int(11) NOT NULL,
  `deposit_lender` int(11) NOT NULL,
  `deposit_date` varchar(100) DEFAULT NULL,
  `deposit_ammount` varchar(50) NOT NULL DEFAULT '0',
  `deposit_status` varchar(20) DEFAULT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p2p_deposit`
--

INSERT INTO `p2p_deposit` (`deposit_id`, `deposit_lender`, `deposit_date`, `deposit_ammount`, `deposit_status`, `deleted`, `created_by`, `modified_by`, `created_on`, `modified_on`) VALUES
(1, 1, '2015-11-20', '1000000', 'Verification', 0, 0, 0, '0000-00-00 00:00:00', '2015-11-24 17:47:47'),
(3, 1, '2015-11-22', '1500000', 'Success', 0, 0, 0, '0000-00-00 00:00:00', '2015-11-24 17:53:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `p2p_deposit`
--
ALTER TABLE `p2p_deposit`
 ADD PRIMARY KEY (`deposit_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `p2p_deposit`
--
ALTER TABLE `p2p_deposit`
MODIFY `deposit_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
