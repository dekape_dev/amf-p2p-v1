-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 03, 2016 at 05:00 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_mis_amartha`
--

-- --------------------------------------------------------

--
-- Table structure for table `p2p_borrower_below`
--

CREATE TABLE IF NOT EXISTS `p2p_borrower_below` (
  `borrower_below_id` int(11) NOT NULL AUTO_INCREMENT,
  `borrower_below_firstname` varchar(50) DEFAULT NULL,
  `borrower_below_lastname` varchar(50) DEFAULT NULL,
  `borrower_below_email` varchar(50) DEFAULT NULL,
  `borrower_below_ktp` varchar(100) DEFAULT NULL,
  `borrower_below_hp` varchar(50) DEFAULT NULL,
  `borrower_below_borndate` varchar(20) DEFAULT NULL,
  `borrower_below_plafond` int(100) DEFAULT NULL,
  `borrower_below_sector` varchar(100) DEFAULT NULL,
  `borrower_below_desa` varchar(50) NOT NULL,
  `borrower_below_tenor` int(100) NOT NULL,
  `borrower_below_location` varchar(100) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL DEFAULT '0',
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`borrower_below_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `p2p_borrower_below`
--

INSERT INTO `p2p_borrower_below` (`borrower_below_id`, `borrower_below_firstname`, `borrower_below_lastname`, `borrower_below_email`, `borrower_below_ktp`, `borrower_below_hp`, `borrower_below_borndate`, `borrower_below_plafond`, `borrower_below_sector`, `borrower_below_desa`, `borrower_below_tenor`, `borrower_below_location`, `deleted`, `created_by`, `modified_by`, `created_on`, `modified_on`) VALUES
(1, 'Enah', 'Sukmiati', 'Sukmara@gmail.com', '797686868686', '087777018176', '12/09/1960', 15000, 'Perdagangan', 'Kampung Ijo', 4, 'Bogor', 0, 13, 13, '2016-02-02 00:00:00', '2016-02-02 10:43:22'),
(2, 'Enah', 'Sukmiati', 'Sukmara@gmail.com', '797686868686', '087777018176', '12/09/1960', 15000, 'Perdagangan', 'Kampung Ijo', 4, 'Bogor', 0, 13, 13, '2016-02-02 00:00:00', '2016-02-02 10:43:41'),
(3, 'Entin', 'Sukritin', 'entin@gmail.com', '1232103812038018', '087777018176', '12/09/1960', 1500000, 'Perdagangan', 'Kampung Ijo', 4, 'Bogor', 1, 13, 13, '2016-02-02 00:00:00', '2016-02-02 12:01:27');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
